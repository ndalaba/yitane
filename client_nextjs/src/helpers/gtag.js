export const GA_TRACKING_ID = 'UA-36357264-10' // This is your GA Tracking ID

// https://developers.google.com/analytics/devguides/collection/gtagjs/pages
export const pageview = (url) => {
  try {
    window.gtag('config', GA_TRACKING_ID, {
      page_path: url,
    });
  } catch (error) {

  }

}

// https://developers.google.com/analytics/devguides/collection/gtagjs/events
export const event = ({ action, category, label, value }) => {
  try {
    window.gtag('event', action, {
      event_category: category,
      event_label: label,
      value: value,
    });
  } catch (error) {

  }
}