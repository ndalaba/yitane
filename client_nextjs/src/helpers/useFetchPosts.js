import PostStore from "../store/PostStore"
import CategoryStore from "../store/CategoryStore"

const useFetchHomePosts = async (token = '') => {
    let posts = PostStore.state.homePosts;
    let videos = PostStore.state.videos;
    let trends = PostStore.state.trends;
    if (!posts.length) {
        posts = await PostStore.getHomePosts(1, token);
    }
    if (!videos.length) {
        videos = await PostStore.getVideos(1, token);
    }
    if (!trends.length) {
        trends = await PostStore.getTrendPosts(token);
    }
    posts = PostStore.state.homePosts;
    videos = PostStore.state.videos;
    trends = PostStore.state.trends;

    const categories = await CategoryStore.getCategories();
    return { initPosts: posts.slice(5), initTop: posts[0], initVideos: videos, initTrends: trends, initTops: posts.slice(1, 4), initCategories: categories }
}

const useFetchHomeVideos = async (token = '') => {
    let videos = PostStore.state.videos;
    if (!videos.length) {
        videos = await PostStore.getVideos(1, token);
    }
    videos = PostStore.state.videos;
    return { initTop: videos[0], initVideos: videos.slice(5), initTops: videos.slice(1, 4) }
}

export { useFetchHomePosts, useFetchHomeVideos }