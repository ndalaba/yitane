import { store } from "react-notifications-component";
const dayjs = require("dayjs");
const relativeTime = require("dayjs/plugin/relativeTime");
dayjs.extend(relativeTime);
import "dayjs/locale/fr";
dayjs.locale("fr");

const LOCAL_STORAGE_PREFIX = "_yi_";

export const redirectIfNotLogged = (user, router) => {
  if (!user.is_authenticated) {
    sessionStorage.setItem("last_url", router.asPath);
    return router.push("/auth/login");
  }
};

export const notification = (title, message, type = "default") => {
  store.addNotification({
    title,
    message,
    type,
    insert: "top",
    container: "top-right",
    animationIn: ["animated", "fadeIn"],
    animationOut: ["animated", "fadeOut"],
    dismiss: {
      duration: 4000,
      onScreen: true,
    },
  });
};

export const relative_time = function (timestamp) {
  return dayjs(timestamp).fromNow();
};

export const wrapText = function (text, innerHTML) {
  const index = innerHTML.indexOf(text);
  if (index >= 0) {
    innerHTML =
      innerHTML.substring(0, index) +
      "<span class='font-semibold'>" +
      innerHTML.substring(index, index + text.length) +
      "</span>" +
      innerHTML.substring(index + text.length);
  }
  return innerHTML;
};

/**
 * Will store in item by applying a prefix on its key.
 *
 * @param {string} key the key of the item to store
 * @param {*} value the value of the item to store
 */
export function storeItem(key, value) {
 if (process.browser)
    localStorage.setItem(LOCAL_STORAGE_PREFIX + key, value);
}

/**
 * Get a stored item by key by applying a prefix on its key.
 *
 * @param {string} the key of the item to retreive
 * @returns {string}
 */
export function getStoredItem(key, _default = "") {
 if (process.browser)
    return localStorage.getItem(LOCAL_STORAGE_PREFIX + key) !== null ? localStorage.getItem(LOCAL_STORAGE_PREFIX + key) : _default
}

/**
 * remove a stored item by applying a prefix on its key.
 *
 * @param {string} key the key of the item to remove
 */
export function removeStoredItem(key) {
  if (localStoreIsDefined)
    localStorage.removeItem(LOCAL_STORAGE_PREFIX + key);
}

export function hasStoredItem(key) {
  if (process.browser)
    return localStorage.getItem(LOCAL_STORAGE_PREFIX + key) !== null;
}

export function doLogout() {
 if (process.browser)
    localStorage.clear();
  window.location.reload();
}


export function readTime(text) {
  const wordsPerMinute = 200; // Average case.
  let result;
  let textLength = text.split(" ").length; // Split by words
  if (textLength > 0) {
    let value = Math.ceil(textLength / wordsPerMinute);
    result = `~${value} min de lecture`;
  }
  return result;
}
