import { Headers } from "node-fetch"
import { getStoredItem } from "./functions"

export const API_ROUTE = "https://api.news.piaafrica.com/"
//export const API_ROUTE = "http://localhost:5000/"
export const NAME = "PIAAFRICA NEWS"
export const TITLE = "RESTEZ INFORMÉS, TROUVEZ DE L'INSPIRATION"
export const DESCRIPTION = "Vous apporte les dernières et les plus pertinentes informations, et vous maintient à jour. Découvrez et suivez vos medias et magazines favoris, et commencer à recevoir leurs informations "
export const FACEBOOK_ID = "604465756787479"


export const FETCH_OPTIONS = function (token = "") {
    if (token == "") {
        try {
            if (process.browser)
                token = getStoredItem('token');
        } catch (e) {

        }
    }
    const headers = new Headers({
        "Content-Type": "text/plain",
        "Content-Length": token.length.toString(),
        "token": token,
    })
    return {
        method: 'GET',
        headers: headers,
        mode: 'cors',
        cache: 'default'
    }
};
