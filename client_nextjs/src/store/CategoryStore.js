import { API_ROUTE, FETCH_OPTIONS } from "../helpers/global";


const CategoryStore = {
  state: {
    categories: [],
  },

  async getCategories(token = "") {
    const response = await fetch(API_ROUTE + "/categories",
      FETCH_OPTIONS(token)
    );
    const data = await response.json();
    this.state.categories = data;
    return data;
  },

  getCategoryPostsFromPosts(category, posts, size = 4) {
    const _posts = posts.filter(post => post.magazine.category.uid == category.uid);
    return _posts.slice(0, size);
  }

};

export default CategoryStore;
