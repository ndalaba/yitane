
import fetch from "node-fetch"
import { API_ROUTE, FETCH_OPTIONS } from "../helpers/global";
import { getStoredItem, hasStoredItem, notification, storeItem } from "../helpers/functions"

let _token = ""
try {
   if (process.browser)
      _token = getStoredItem("token") != null ? getStoredItem("token") : ""
} catch (e) {

}


const UserStore = {
   state: {
      currentUser: hasStoredItem('user') ? JSON.parse(getStoredItem('user')) : { 'email': '', 'password': '', 'remember_me': false, is_authenticated: false, country: {}, image: `/images/account.svg` },
      userSavedVideos: [],
      userLovedVideos: [],
      userSavedArticles: [],
      userLovedArticles: [],
      userCategories: [],
      userChannels: [],
      userMagazines: [],
   },

   setCurrentUser(user) {
      this.state.currentUser = user;
   },

   async getUserMazines(uid) {
      const response = await fetch(API_ROUTE + "get-user/magazines?uid=" + uid);
      const data = await response.json();
      return data;
   },

   async getUserChannels(uid) {
      const response = await fetch(API_ROUTE + "get-user/channels?uid=" + uid);
      const data = await response.json();
      return data;
   },

   async getUserFeed(uid, page = 1) {
      const response = await fetch(API_ROUTE + "get-user?uid=" + uid + "&page=" + page);
      const data = await response.json();
      return data;
   },

   async getUser() {
      const response = await fetch(API_ROUTE + "get_auth_user", FETCH_OPTIONS())
      const data = await response.json();
      return data;
   },

   async updateUser(formData) {
      const response = await fetch(API_ROUTE + "account?token=" + getStoredItem('token'), { method: 'POST', body: formData })
      const data = await response.json();
      return data;
   },

   async updatePassword(formData) {
      const response = await fetch(API_ROUTE + "password?token=" + getStoredItem("token"), { method: 'POST', body: formData })
      const data = await response.json();
      return data;
   },

   async signin(formData) {
      const response = await fetch(API_ROUTE + "auth/login", { method: 'POST', body: formData })
      const data = await response.json()
      if (data.status) {
         UserStore.setCurrentUser(data.member);
         storeItem('token', data.token);
         storeItem('user', JSON.stringify(data.member));
         if (hasStoredItem('last_url'))
            window.location.href = getStoredItem('last_url');
         else
            window.history.back();
      }
      else { notification("Erreur", data.message, 'danger'); setstate({ ...form, ['password']: "" }) }
   },

   async signup(formData) {
      const response = await fetch(API_ROUTE + "auth/registration", { method: 'POST', body: formData })
      const data = await response.json();
      if (data.status) {
         notification("Success", data.message, "info");
         storeItem('token', data.token);
         localStorage.setItem('token', data.token)
         if (hasStoredItem('last_url'))
            window.location.href = getStoredItem('last_url');
         else
            window.location.href = '/';
      }
      else
         notification("Erreur", data.message, "danger");
   },

   async checkToken(token = _token) {
      let user = this.state.currentUser;
      if (token !== null && token !== undefined && token !== "undefined" && token.length) {
         const response = await fetch(API_ROUTE + "auth/token_check?token=" + token)
         const data = await response.json()
         if (data.status) {
            this.state.currentUser = data.user
         }
      }
      return user
   },
   async checkMagazines(token = _token) {
      if (token !== null && token !== undefined && token !== "undefined" && token.length) {
         const reponse = await fetch(API_ROUTE + "user/magazines", FETCH_OPTIONS())
         const data = await reponse.json();
         this.state.userMagazines = data.magazines;
         return data.magazines
      }
      return []
   },

   async checkChannels(token = _token) {
      if (token !== null && token !== undefined && token !== "undefined" && token.length) {
         const reponse = await fetch(API_ROUTE + "user/channels", FETCH_OPTIONS())
         const data = await reponse.json();
         this.state.userChannels = data.channels;
         return data.channels
      }
      return []
   },
   async checkCategories(token = _token) {
      if (token !== null && token !== undefined && token !== "undefined" && token.length) {
         const reponse = await fetch(API_ROUTE + "user/categories", FETCH_OPTIONS())
         const data = await reponse.json();;
         this.state.userCategories = data.categories;
         return data.categories
      }
      return []
   },

   async checkLovedArticles(token = _token) {
      if (token !== null && token !== undefined && token !== "undefined" && token.length) {
         const reponse = await fetch(API_ROUTE + "user/loved_articles", FETCH_OPTIONS())
         const data = await reponse.json();
         this.state.userLovedArticles = data.articles;
         return data.articles
      }
      return []
   },

   async checkSavedArticles(token = _token) {
      if (token !== null && token !== undefined && token !== "undefined" && token.length) {
         const reponse = await fetch(API_ROUTE + "user/saved_articles", FETCH_OPTIONS())
         const data = await reponse.json();
         this.state.userSavedArticles = data.articles;
         return data.articles
      }
      return []
   },


   async checkLovedVideos(token = _token) {
      if (token !== null && token !== undefined && token !== "undefined" && token.length) {
         const reponse = await fetch(API_ROUTE + "user/loved_videos", FETCH_OPTIONS())
         const data = await reponse.json();
         this.state.userLovedVideos = data.videos;
         return data.videos
      }
      return []
   },

   async checkSavedVideos(token = _token) {
      if (token !== null && token !== undefined && token !== "undefined" && token.length) {
         const reponse = await fetch(API_ROUTE + "user/saved_videos", FETCH_OPTIONS())
         const data = await reponse.json();
         this.state.userSavedVideos = data.videos;
         return data.videos
      }
      return []
   },

   setUserSavedArticles(articles) {
      this.state.userSavedArticles = articles;
   },
   setUserSavedVideos(videos) {
      this.state.userSavedVideos = videos;
   }
};

export default UserStore;