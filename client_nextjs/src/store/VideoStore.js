import { API_ROUTE, FETCH_OPTIONS } from "../helpers/global";


const VideoStore = {
  state: {
    videos: [],
  },

  async getVideo(chan_slug, video_slug, uid) {
    const response = await fetch(API_ROUTE + "videos/" + chan_slug + "/" + video_slug + "/" + uid);
    const data = await response.json();
    return data;
  },

  async getVideos(page = 0, token = "") {
    const response = await fetch(
      API_ROUTE + "/videos?page=" + page,
      FETCH_OPTIONS(token)
    );
    const data = await response.json();
    this.state.videos = data.videos;
    return { videos: data.videos, page: data.page };
  },

  async findComments(uid) {
    const response = await fetch(API_ROUTE + "videos/comments/" + uid);
    const data = await response.json();
    return data;
  },

  async findByCategory(slug, page = 1) {
    const response = await fetch(API_ROUTE + "videos/topic/" + slug + "?page=" + page);
    const data = await response.json();
    return data;
  },

  async findByChannel(slug, page = 1) {
    const response = await fetch(API_ROUTE + "channels/" + slug + "?page=" + page);
    const data = await response.json();
    return data;
  },

  async searchVideos(value, page = 1) {
    const response = await fetch(API_ROUTE + "find/videos?q=" + value + "&page=" + page);
    const data = await response.json();
    return data;
  },

  async getchannels(size = 100) {
    const response = await fetch(API_ROUTE + "channels/trends?size=" + size, FETCH_OPTIONS())
    const data = await response.json();
    return data;
  }

};

export default VideoStore;
