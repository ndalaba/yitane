import { API_ROUTE, FETCH_OPTIONS } from "../helpers/global";


const PostStore = {
  state: {
    posts: [],
    homePosts: [],
    videos: [],
    trends: [],
    categories: [],
  },

  setHomePosts(posts) {
    this.state.homePosts = posts;
  },

  setVideos(videos) {
    this.state.videos = videos;
  },
  setTrends(trends) {
    this.state.trends = trends;
  },

  async findByMagazine(slug, page = 1) {
    const response = await fetch(API_ROUTE + slug + "?page=" + page);
    const data = await response.json();
    this.state.posts = data.articles;
    return data;
  },

  async findByCategory(slug, page = 1) {
    const response = await fetch(API_ROUTE + "topic/" + slug + "?page=" + page);
    const data = await response.json();
    this.state.posts = data.articles;
    return data;
  },

  async findPost(mag_slug, post_slug, uid) {
    const response = await fetch(API_ROUTE + mag_slug + "/" + post_slug + "/" + uid);
    const data = await response.json();
    return { init_article: data.article, related_articles: data.related_articles }
  },

  async findNextPosts(page = 1, token = "") {
    const response = await fetch(
      API_ROUTE + "/next?page=" + page,
      FETCH_OPTIONS(token)
    );
    const data = await response.json();
    this.state.posts = [...this.state.posts, ...data.articles];
    return { posts: data.articles, page: data.page };
  },

  async getHomePosts(page = 0, token = "") {
    const response = await fetch(
      API_ROUTE + "/home?page=" + page,
      FETCH_OPTIONS(token)
    );
    const data = await response.json();
    this.state.homePosts = data.articles;
    this.state.posts = data.articles;
    return { posts: data.articles, page: data.page };
  },

  async getTrendPosts(token = "") {
    const response = await fetch(
      API_ROUTE + "trend_articles",
      FETCH_OPTIONS(token)
    );
    const data = await response.json();
    this.state.trends = data.articles;
    return { trends: data.articles };
  },

  async getVideos(page = 0, token = "") {
    const response = await fetch(
      API_ROUTE + "/videos?page=" + page,
      FETCH_OPTIONS(token)
    );
    const data = await response.json();
    this.state.videos = data.videos;
    return { videos: data.videos, page: data.page };
  },

  async findComments(uid) {
    const response = await fetch(API_ROUTE + "articles/comments/" + uid);
    const data = await response.json();
    return data;
  },

  async getMagazines(size = 100) {
    const response = await fetch(API_ROUTE + "magazines/trends?size=" + size, FETCH_OPTIONS());
    const data = await response.json();
    return data;
  },

  async findPosts(q, page = 1) {
    const response = await fetch(API_ROUTE + "find/articles?q=" + q + "&page=" + page);
    const data = await response.json();
    return data;
  },

  getContent(uid) {
    return fetch(API_ROUTE + "/article/get_content/" + uid);
  }

};

export default PostStore;
