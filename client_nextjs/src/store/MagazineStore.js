import fetch from "node-fetch"
import { API_ROUTE } from "../helpers/global"

const MagazineStore = {

   state: {},

   refreshFeed(magazine) {
      return fetch(API_ROUTE + "magazines/check_feed/" + magazine.uid)
   }
}

export default MagazineStore;