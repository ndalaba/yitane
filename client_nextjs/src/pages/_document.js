import Document, { Html, Head, Main, NextScript } from 'next/document'
import { DESCRIPTION } from '../helpers/global'
import { GA_TRACKING_ID } from '../helpers/gtag'

class MyDocument extends Document {
    static async getInitialProps(ctx) {
        const initialProps = await Document.getInitialProps(ctx)

        // Check if in production
        const isProduction = process.env.NODE_ENV === 'production'

        return { ...initialProps, isProduction }
    }

    render() {
        const { isProduction } = this.props
        return (
            <Html lang="fr">
                <Head>
                    <meta name="theme-color" content="#f8f8f8" />
                    <link rel="manifest" href="/manifest.json" />
                    <link rel="shortcut icon" href="/images/logo.png" />
                    <meta name="apple-mobile-web-app-capable" content="yes" />
                    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
                    <meta name="apple-mobile-web-app-title" content={DESCRIPTION} />
                    <link rel="apple-touch-icon" href="/images/icons/icon-192x192.png"></link>

                    <link rel="preconnect" href="https://fonts.gstatic.com" />
                    <link href="https://fonts.googleapis.com/css2?family=Dosis:wght@200;300;400;500;600;700&display=swap" rel="stylesheet" />

                    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.14.0/css/all.css" integrity="sha384-HzLeBuhoNPvSl5KYnjx0BT+WB0QEEqLprO+NBkkk5gbc67FTaL7XIGa2w1L0Xbgc" crossOrigin="anonymous" />

                    {isProduction && (
                        <>
                            <script async src={`https://www.googletagmanager.com/gtag/js?id=${GA_TRACKING_ID}`} />
                            <script
                                dangerouslySetInnerHTML={{
                                    __html: `
                                            window.dataLayer = window.dataLayer || [];
                                            function gtag(){dataLayer.push(arguments);}
                                            gtag('js', new Date());

                                            gtag('config', '${GA_TRACKING_ID}', {
                                            page_path: window.location.pathname,
                                            });
                                        `,
                                }}
                            />
                            <script data-ad-client="ca-pub-3883880951212572" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        </>
                    )}
                </Head>
                <body>
                    <Main />
                    <NextScript />
                </body>
            </Html>
        )
    }
}

export default MyDocument