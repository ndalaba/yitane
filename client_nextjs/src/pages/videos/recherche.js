import Layout from '../../components/Layout'
import Video from '../../components/videos/Video'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import Loading from '../../components/Loading'
import { parseCookies } from 'nookies'
import VideoTrend from '../../components/videos/VideoTrend'
import VideoStore from '../../store/VideoStore'
import AdBanner from '../../components/AdBanner'
import Head from 'next/head'
import UserStore from '../../store/UserStore'


export default function Index({ initVideos }) {

    const router = useRouter()
    const { q } = router.query

    const [page, setPage] = useState(2);
    const [nextVideos, setNextVideos] = useState([]);
    const [videos, setVideos] = useState(initVideos);
    const [loading, setLoading] = useState(false);
    const [notFound, setNotFound] = useState(false);

    useEffect(() => {
        if (videos.length === 0) {
            init();
        }
    }, []);

    const init = async () => {
        const data = await VideoStore.searchVideos(q);
        setVideos(data.videos);
        setPage(data.page);
        if (!data.videos.length)
            setNotFound(true);
    }

    const Body = () => {
        const findNextVideos = async () => {
            setLoading(true);
            const data = await VideoStore.searchVideos(q, page);
            setNextVideos([...nextVideos, ...data.videos]);
            setPage(data.page);
            setLoading(false);
        }

        if (videos == undefined)
            return <Loading />
        return (
            <>
                <Head>
                    <title>{q || ""} - Vidéos</title>
                </Head>

                <div className="text-center border-b pb-2">
                    <h1 className="w-full text-gray-800 text-4xl px-5 font-semibold leading-none text-center">Resultat recherche: {q}</h1>
                </div>

                <div className="flex flex-wrap space-x-0 lg:flex">
                    {(videos.length == 0 && !notFound) && <Loading />}
                    {(videos.length == 0 && notFound) && <h2 className="font-medium text-lg text-center w-full p-3">Aucun resultat correspondant à votre recherche</h2>}
                    {videos.map(video => <Video key={video.id} video={video} />)}
                </div>
                <AdBanner />
                <div className="mt-2 flex flex-wrap">
                    {nextVideos.map(video => <Video video={video} key={video.id} />)}
                    {page > 0 &&
                        <div className="flex flex-col items-center w-full py-2">
                            <button className="btn rounded inline-flex item-center" onClick={() => findNextVideos()}>
                                {loading && <span className="text-white opacity-75 my-0 mx-auto block relative w-0 h-0 -ml-1 mr-7">
                                    <i className="fas fa-circle-notch fa-spin text-2xl fa-1x"></i>
                                </span>
                                }
                                Afficher plus
                                </button>
                        </div>
                    }
                </div>

                {!videos.length &&
                    <>
                        <AdBanner />
                        <div className="p-2">
                            <VideoTrend size={10} />
                        </div>
                        <AdBanner />
                    </>}
            </>
        )
    }
    return (
        <Layout body={<Body />} />
    )
}

Index.getInitialProps = async (ctx) => {
    const cookies = parseCookies(ctx)
    if (ctx.req) {
        const user = await UserStore.checkToken(cookies.token);
        const data = await VideoStore.searchVideos(ctx.query.q);
        return { initVideos: data.videos, init_search: ctx.query.q, current_user: user }
    }
    return { initVideos: [], init_search: "", current_user: UserStore.state.currentUser }
}