import Layout from '../../../components/Layout'
import Video from '../../../components/videos/Video'
import { useRouter } from 'next/router'
import Head from 'next/head'
import { useEffect, useState } from 'react'
import { FollowingCategory } from '../../../components/Following'
import { parseCookies } from 'nookies'
import AdBanner from '../../../components/AdBanner'
import VideoStore from '../../../store/VideoStore'
import UserStore from '../../../store/UserStore'
import Loading from '../../../components/Loading'


export default function Index({ initCategory, initVideos }) {

    const router = useRouter()
    const { cat_slug } = router.query

    const [page, setPage] = useState(2);
    const [nextVideos, setNextVideos] = useState([]);
    const [videos, setVideos] = useState(initVideos);
    const [category, setCategory] = useState(initCategory);

    const [loading, setLoading] = useState(false);

    useEffect(() => {
        if (videos.length === 0) {
            init();
        }
    }, []);

    const init = async () => {
        const data = await VideoStore.findByCategory(cat_slug);
        setVideos(data.videos);
        setCategory(data.category);
    }


    const Body = () => {

        const findNextVideos = async () => {
            setLoading(true);
            const data = await VideoStore.findByCategory(cat_slug, page);
            setNextVideos([...nextVideos, ...data.videos]);
            setPage(data.page);
            setLoading(false);
        }

        if (category == undefined || videos == undefined)
            return "<p>Chargement....</p>"
        return (
            <>
                <Head>
                    <title>{category.name || ""} - Vidéos</title>
                    <meta name="description" content={category.name || ""} />
                </Head>

                <div className="text-center border-b pb-2">
                    <div className="flex flex-col items-center text-center mx-auto mb-4">
                        <div className=" w-48 mt-1 mr-3 overflow-hidden flex flex-col items-center">
                            <img src={category.image} alt={category.title} className="h-14" />
                        </div>
                    </div>

                    <h1 className="w-full text-gray-800 text-4xl px-5 font-bold leading-none text-center">{category.title}</h1>
                    <FollowingCategory category={category} />
                    <div className="text-base text-gray-700 pt-2">
                        {category.description}
                    </div>
                </div>

                <AdBanner />

                <div className="flex flex-wrap space-x-0 lg:flex">
                    {!videos.length && <Loading />}
                    {videos.map(video => <Video key={video.id} video={video} />)}
                </div>
                <AdBanner />
                <div className="mt-2 flex flex-wrap">
                    {nextVideos.map(video => <Video video={video} key={video.id} />)}
                    {page > 0 &&
                        <div className="flex flex-col items-center w-full py-2">
                            <button className="btn rounded inline-flex item-center" onClick={() => findNextVideos()}>
                                {loading && <span className="text-white opacity-75 my-0 mx-auto block relative w-0 h-0 -ml-1 mr-7">
                                    <i className="fas fa-circle-notch fa-spin text-2xl fa-1x"></i>
                                </span>
                                }
                        Afficher plus
                        </button>
                        </div>
                    }
                </div>
            </>
        )
    }
    return (
        <Layout body={<Body />} />
    )
}

Index.getInitialProps = async (ctx) => {
    const cookies = parseCookies(ctx)
    if (ctx.req) {
        const user = await UserStore.checkToken(cookies.token)
        const data = await VideoStore.findByCategory(ctx.query.cat_slug);
        return { initCategory: data.category, initVideos: data.videos, current_user: user }
    }
    return { initCategory: {}, initVideos: [], current_user: UserStore.state.currentUser }
}