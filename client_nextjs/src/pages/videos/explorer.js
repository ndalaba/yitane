import Layout from '../../components/Layout'
import Head from 'next/head'
import { useEffect, useRef, useState } from 'react'
import Loading from '../../components/Loading'
import Channel from '../../components/videos/Channel'
import { useRouter } from 'next/router'
import { parseCookies } from 'nookies'
import VideoStore from '../../store/VideoStore'
import { NAME } from '../../helpers/global'
import AdBanner from "../../components/AdBanner"
import UserStore from '../../store/UserStore'

const SearchForm = ({ search, setSearch }) => {

    const elementRef = useRef()

    const handleChange = (event) => {
        setSearch(event.target.value)
    }
    const reset = () => {
        setSearch("")
    }
    useEffect(() => {
        elementRef.current.focus()
    }, [search])

    return (
        <div className="relative w-full">
            <input className="pl-12 rounded-lg font-medium w-full p-2 bg-gray-200 focus:outline-none focus:bg-gray-100"
                value={search}
                onChange={handleChange}
                ref={elementRef}
            />
            <i className="fas fa-search absolute left-0 top-0 mt-3 ml-3 text-sm text-gray-700" />
            {(search.length > 0) && <i className={`fas fa-times-circle absolute top-0 mt-3 right-0 pr-3 text-sm text-teal-900 cursor-pointer`}
                onClick={reset}
            />}
        </div>
    )

}

export default function Explorer({ initChannels }) {


    const [channels, setChannels] = useState(initChannels)
    const [filtredChannels, setFiltredChannels] = useState(initChannels)
    const [loading, setLoading] = useState(false)

    const [search, setSearch] = useState('')

    useEffect(() => {
        fetch_trend_channel()
    }, [])

    useEffect(() => {
        if (search.trim().length > 0)
            filter()
        else
            resetFilter()
    }, [search])

    const fetch_trend_channel = async () => {
        setLoading(true)
        const data = await VideoStore.getchannels();
        setChannels(data.channels);
        setFiltredChannels(data.channels);
        setLoading(false);
    }
    const filter = (event) => {
        setFiltredChannels(channels.filter(chan => {
            const q = search.toLowerCase()
            return chan.title.toLowerCase().includes(q) || chan.category.name.toLowerCase().includes(q) || chan.country.name.toLowerCase().includes(q)
        }))
    }
    const resetFilter = () => {
        setFiltredChannels(channels)
    }

    const Body = () => {
        return (
            <>
                <Head>
                    <title>Explorer nos chaines / {NAME}</title>
                </Head>

                <SearchForm search={search} setSearch={setSearch} />

                <div className="mt-16 mb-12">
                    <div>
                        {filtredChannels.map((channel, index) => <Channel chan={channel} key={index} show={true} />)}
                        {loading && <Loading />}
                    </div>
                    <AdBanner />
                </div>
            </>
        )
    }
    return (
        <Layout body={<Body />} />
    )
}

Explorer.getInitialProps = async (ctx) => {
    const cookies = parseCookies(ctx)
    if (ctx.req) {
        const user = await UserStore.checkToken(cookies.token)
        const data = await VideoStore.getchannels();
        return { initChannels: data.channels, current_user: user }
    }
    return { initChannels: [], current_user: UserStore.state.currentUser }
}