import Layout from '../../../components/Layout'
import Video from '../../../components/videos/Video'
import { useRouter } from 'next/router'
import Head from 'next/head'
import { useEffect, useState } from 'react'
import { FollowingChannel } from '../../../components/Following'
import { parseCookies } from 'nookies'
import AdBanner from '../../../components/AdBanner'
import VideoStore from '../../../store/VideoStore'
import UserStore from '../../../store/UserStore'
import Loading from '../../../components/Loading'
import Link from 'next/link'


export default function Index({ initChannel, initVideos }) {

    const router = useRouter()
    const { chan_slug } = router.query

    const [page, setPage] = useState(2);
    const [nextVideos, setNextVideos] = useState([]);
    const [videos, setVideos] = useState(initVideos);
    const [channel, setChannel] = useState(initChannel);

    const [loading, setLoading] = useState(false);

    useEffect(() => {
        if (videos.length === 0) {
            init();
        }
    }, []);

    const init = async () => {
        const data = await VideoStore.findByChannel(chan_slug);
        setVideos(data.videos);
        setChannel(data.channel);
    }


    const Body = () => {

        const findNextVideos = async () => {
            setLoading(true);
            const data = await VideoStore.findByChannel(chan_slug, page);
            setNextVideos([...nextVideos, ...data.videos]);
            setPage(data.page);
            setLoading(false);
        }

        if (channel == undefined || videos == undefined)
            return "<p>Chargement....</p>"
        return (
            <>
                <Head>
                    <title>{channel.title || ""} - Vidéos</title>
                    <meta name="description" content={channel.description || ""} />
                </Head>

                <div className="text-center border-b pb-2">
                    <div className="flex flex-col items-center text-center mx-auto mb-4">
                        <div className=" w-48 mt-1 mr-3 overflow-hidden flex flex-col items-center">
                            <img src={channel.image} alt={channel.title} className="h-14" />
                        </div>
                    </div>

                    <h1 className="w-full text-gray-800 text-4xl px-5 font-bold leading-none text-center">
                        <Link href="/videos/[chan_slug]" as={`/videos/${channel.slug}`}><a title={channel.title}>{channel.title}</a></Link>
                    </h1>
                    <FollowingChannel channel={channel} />
                    <div className="text-base text-gray-700 pt-2">
                        {channel.description}
                    </div>
                </div>


                <AdBanner />

                <div className="flex flex-wrap space-x-0 lg:flex">
                    {!videos.length && <Loading />}
                    {videos.map(video => <Video key={video.id} video={video} />)}
                </div>
                <AdBanner />
                <div className="mt-2 flex flex-wrap">
                    {nextVideos.map(video => <Video video={video} key={video.id} />)}
                    {page > 0 &&
                        <div className="flex flex-col items-center w-full py-2">
                            <button className="btn rounded inline-flex item-center" onClick={() => findNextVideos()}>
                                {loading && <span className="text-white opacity-75 my-0 mx-auto block relative w-0 h-0 -ml-1 mr-7">
                                    <i className="fas fa-circle-notch fa-spin text-2xl fa-1x"></i>
                                </span>
                                }
                        Afficher plus
                        </button>
                        </div>
                    }
                </div>

            </>
        )
    }
    return (
        <Layout body={<Body />} />
    )
}

Index.getInitialProps = async (ctx) => {
    const cookies = parseCookies(ctx)
    if (ctx.req) {
        const user = await UserStore.checkToken(cookies.token)
        const data = await VideoStore.findByChannel(ctx.query.chan_slug);
        return { initChannel: data.channel, initVideos: data.videos, current_user: user }
    }
    return { initChannel: {}, initVideos: [], current_user: UserStore.state.currentUser }
}