import Head from "next/head";
import { useEffect, useRef, useState } from "react"
import Layout from "../../../../components/Layout"
import { FACEBOOK_ID, NAME } from "../../../../helpers/global"
import { useRouter } from "next/router";
import Video from "../../../../components/videos/Video"
import VideoBtn from "../../../../components/videos/VideoBtn";
import { parseCookies } from "nookies";
import YouTube from 'react-youtube';
import AdBanner from "../../../../components/AdBanner";
import VideoStore from "../../../../store/VideoStore";
import { relative_time } from "../../../../helpers/functions";
import Link from "next/link";
import UserStore from "../../../../store/UserStore";


export default function Index({ init_video }) {
    const router = useRouter()
    const { chan_slug, video_slug, uid } = router.query
    const [video, setVideo] = useState(init_video)
    const [relatedVideos, setRelatedVideos] = useState([]);

    useEffect(() => {
        setVideo(init_video);
        fetchData();
    }, [uid])

    const fetchData = async () => {
        const data = await VideoStore.getVideo(chan_slug, video_slug, uid);
        setVideo(data.video);
        setRelatedVideos(data.related_videos);
    }

    const Body = () => {
        const iframeRef = useRef()
        const play = useRef(false)
        const opts = {
            playerVars: {
                autoplay: 1,
            },
        };

        useEffect(() => {
            return () => {
                document.removeEventListener('scroll', () => { console.log('remove') })
            }
        })

        const playing = () => {
            play.current = true
            document.addEventListener('scroll', function (event) {
                if (window.scrollY >= 300 && iframeRef.current != null && play.current) {
                    iframeRef.current.classList.add("fixedBottom");
                    iframeRef.current.classList.remove("relative");
                    iframeRef.current.style.paddingTop = 0;
                }
                else if (iframeRef.current != null) {
                    iframeRef.current.classList.add('relative');
                    iframeRef.current.classList.remove('fixedBottom');
                    iframeRef.current.style.paddingTop = '56.25%';
                }

            })
        }
        const stoped = () => {
            play.current = false
            document.removeEventListener('scroll', () => { console.log('remove') })
            if (iframeRef.current != null) {
                iframeRef.current.classList.add('relative');
                iframeRef.current.classList.remove('fixedBottom');
                iframeRef.current.style.paddingTop = '56.25%';
            }
        }
        const time = relative_time(video.date_published)
        return (

            <>
                <Head>
                    <title>{video.title || ""}</title>
                    <meta name="description" content={video.body || ""} />
                    <meta property="og:locale" content="fr" />
                    <meta property="og:site_name" content={NAME} />
                    <meta property="og:title" content={video.title} />
                    <meta property="og:url" content={video.url} />
                    <meta property="og:type" content="video" />
                    <meta property="og:description" content={video.body} />
                    <meta property="og:image" content={video.image} />
                    <meta property="og:image:url" content={video.image} />
                    <meta property="og:image:secure_url" content={video.image} />
                    <meta property="video:published_time" content={video.date_published} />
                    <meta property="video:modified_time" content={video.created_at} />
                    <meta property="og:updated_time" content={video.created_at} />
                    <meta property="video:section" content="WEB" />
                    <meta property="fb:app_id" content={FACEBOOK_ID} />
                    <meta property="fb:pages" content="" />
                    <meta property="video:publisher" content="" />
                    <meta property="video:author" content="" />

                    <meta itemprop="name" content={video.title} />
                    <meta itemprop="headline" content={video.title} />
                    <meta itemprop="description" content={video.body} />
                    <meta itemprop="image" content={video.image} />
                    <meta itemprop="datePublished" content={video.date_published} />
                    <meta itemprop="dateModified" content={video.created_at} />
                    <meta itemprop="author" content="ndalaba" />

                    <meta name="twitter:title" content={video.title} />
                    <meta name="twitter:url" content={video.url} />
                    <meta name="twitter:description" content={video.body} />
                    <meta name="twitter:image" content={video.image} />
                    <meta name="twitter:card" content="summary_large_image" />
                    <script src="http://www.youtube.com/player_api"></script>
                </Head>


                <div className="w-full md:w-5/5 mx-auto">
                    <div className="flex flex-col items-center text-center mx-auto mb-4">
                        <div className=" w-48 mt-1 mr-3 overflow-hidden flex flex-col items-center">
                            <Link href="/videos/[chan_slug]" as={`/videos/${video.channel.slug}`}>
                                <a title={video.channel.title} className="flex flex-col items-center font-medium">
                                    {video.channel.title}
                                    <img debounce={1} placeholder="../../no_image.jpg" src={video.channel.image} alt={video.channel.title} className="h-14" />
                                </a>
                            </Link>
                        </div>
                    </div>
                    <h1 className="w-full text-gray-800 text-medium md:text-4xl px-2 font-semibold leading-none mb-2" dangerouslySetInnerHTML={{ __html: video.title }} />

                    <div className="mx-0 md:mx-5 md:mt-4 h-64 md:h-auto">
                        <div ref={iframeRef} className="relative overflow-hidden h-64 md:h-auto" style={{ paddingTop: '56.25%' }}>
                            <YouTube videoId={video.video_id} onPlay={playing} onEnd={stoped} opts={opts} className="absolute inset-0 w-full h-64 md:h-full" containerClassName="h-64 md:h-full w-full" />
                        </div>
                    </div>
                   
                    <div className="w-5/6 text-gray-600 text-normal mx-auto">
                        <p className="border-b py-3">{video.author}
                            <span>{time}</span> -
                            {video.channel.category &&
                                <Link href="/videos/topic/[chan_slug]" as={`/videos/topic/${video.channel.category.slug}`}>
                                    <a className="text-teal-600 font-smeibold" title={video.channel.category.name}>
                                        #{video.channel.category.name}
                                    </a>
                                </Link>
                            }
                        </p>
                        <div className="w-full border-b mb-5 pb-2 ">
                            <VideoBtn video={video} />
                        </div>
                    </div>

                    <div className="px-5 w-full mx-auto postBody" dangerouslySetInnerHTML={{ __html: video.body }} />

                    <AdBanner />
                    {relatedVideos.length > 0 ? <h3 className="ml-3 text-3xl text-gray-800 font-semibold mt-12">Recommandées pour vous</h3> : null}

                    <div className="mt-2 flex flex-wrap">
                        {relatedVideos.map((video) => <Video video={video} key={video.id} />)}
                    </div>
                    <AdBanner />
                </div>
            </>
        )
    }

    return (
        <Layout body={<Body />} />
    )
}

Index.getInitialProps = async (ctx) => {
    const cookies = parseCookies(ctx)
    if (ctx.req) {
        const user = await UserStore.checkToken(cookies.token);
        const data = await VideoStore.getVideo(ctx.query.chan_slug, ctx.query.video_slug, ctx.query.uid);
        return { init_video: data.video, related_videos: data.related_videos, current_user: user };
    }
    return { init_video: ctx.query, current_user: UserStore.state.currentUser };
}