import Head from "next/head";
import {  useEffect,  useState } from "react"
import { API_ROUTE } from "../../../../helpers/global"
import { useRouter } from "next/router";
import VideoBtn from "../../../../components/videos/VideoBtn";
import VideoTop from "../../../../components/videos/VideoTop";
import { parseCookies } from "nookies";
import Comment from "../../../../components/Comment";
import Link from "next/link";
import Layout from "../../../../components/Layout";
import VideoStore from "../../../../store/VideoStore";
import UserStore from "../../../../store/UserStore";
import AdBanner from "../../../../components/AdBanner";



export default function Index({ init_video, init_comments }) {
    const router = useRouter()
    const { uid } = router.query
    const [comments, setComments] = useState(init_comments)
    const [video, setVideo] = useState(init_video)
    const  user = UserStore.state.currentUser;
    const [form, setstate] = useState({ 'content': '' })


    useEffect(() => {
        fetchData();
    }, [uid])

    const fetchData = async () => {
        const data = await VideoStore.findComments(uid);
        setComments(data.comments);
        setVideo(data.video)
    }
    const updateField = (e) => {
        setstate({ ...form, [e.target.name]: e.target.value })
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const formData = new FormData(event.target)
        const response = await fetch(API_ROUTE + "videos/comments?uid=" + uid + "&token=" + localStorage.getItem("token"), { method: 'POST', body: formData })
        const data = await response.json()
        if (data.status) {
            setComments(prev => [...comments, data.comment])
            setstate({ ...form, ['content']: "" })
        }

    }
    const is_authenticated = () => {
        if (!user.is_authenticated)
            return router.push('/auth/login')
    }

    const Body = () => {
        return (

            <>
                <Head>
                    <title>{video.title || ""}</title>
                    <meta name="description" content={video.body || ""} />
                </Head>


                {video.id &&
                    <div className="mt-10 mb-12">
                        <div className="w-full p-5">
                            <VideoTop video={video} />
                            <h1 className="w-full mb-2 text-lg font-semibold">
                                <Link href={{ pathname: '/videos/[chan_slug]/[video_slug]/[uid]', query: { uid: video.uid, video_slug: video.slug, chan_slug: video.channel.slug, channel: video.channel } }} as={video.url}>
                                    <a title={video.title}>{video.title}</a>
                                </Link>
                            </h1>
                            <p className="p-2 text-gray-800 border-b border-gray-200 pb-3">{video.body}</p>
                            <div className="w-full border-b mb-2 pb-2 ">
                                <VideoBtn video={video} />
                            </div>

                            <div className="flex w-full mb-2">
                                <div className="mr-4 w-10 float-left">
                                    <img src={user.image} alt={user.name} className="h-10 w-10 rounded-full flex-none object-cover object-left" />
                                </div>
                                <div className="flex float-left w-full">
                                    <form onSubmit={handleSubmit} className="w-full" id="form">
                                        <textarea onFocus={is_authenticated} onChange={updateField} value={form.content} className="w-full bg-gray-100 p-3 focus:outline-none text-gray-700" name="content" placeholder="Ecrivez votre commentaire"></textarea>
                                        {form.content.trim().length ? <button className="btn float-right">Envoyer</button> : ""}
                                    </form>
                                </div>
                            </div>

                        </div>
                        <AdBanner/>

                        {comments.map(comment => {
                            return <div className="w-full p-5 border-b border-gray-200 flex  hover:bg-gray-100" key={comment.id}>
                                <Comment comment={comment} />
                            </div>
                        })}

                    </div>
                }
            </>
        );

    }

    return <Layout body={<Body />} />
}

Index.getInitialProps = async (ctx) => {
    const cookies = parseCookies(ctx)
    if (ctx.req) {
        const user = await UserStore.checkToken(cookies.token)
        const data = await VideoStore.findComments(ctx.query.uid);
        return { init_video: data.video, current_user: user, init_comments: data.comments }
    }
    return { init_video: ctx.query, current_user: UserStore.state.current, init_comments: [] };
}