import ReactNotification from 'react-notifications-component'
import 'react-notifications-component/dist/theme.css'
import '../../public/global.css'
import { useEffect } from 'react'
import { Router, useRouter } from 'next/router';

import * as gtag from '../helpers/gtag'
import UserStore from '../store/UserStore'

try {
  Router.events.on('routeChangeComplete', (url) => gtag.pageview(url))

} catch (error) {

}

function MyApp({ Component, pageProps }) {

  const router = useRouter()
  //const current = pageProps.current_user != undefined ? pageProps.current_user : currentUser

  useEffect(() => {
    sessionStorage.clear();
    /*router.events.on('routeChangeStart', () => {
      get_user_loved_videos()
      get_user_saved_videos()
    });*/
    UserStore.checkToken();
    UserStore.checkCategories();
    UserStore.checkMagazines();
    UserStore.checkChannels();
    UserStore.checkLovedArticles();
    UserStore.checkLovedVideos();
    UserStore.checkSavedArticles();
    UserStore.checkSavedVideos();

   /* return () => {
      router.events.of('routeChangeStart', () => false)
    }*/
  }, [])

  return (
    <>
      <ReactNotification />
      <Component {...pageProps} />
    </>
  )
}

export default MyApp;
