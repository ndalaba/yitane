import Layout from '../../components/Layout'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import { FollowingCategory } from '../../components/Following'
import { parseCookies } from 'nookies'
import AdBanner from '../../components/AdBanner'
import PostStore from '../../store/PostStore';
import Trend from '../../components/posts/Trend';
import Head from 'next/head'
import UserStore from '../../store/UserStore'
import Loading from '../../components/Loading'


export default function Index({ initCategory, initArticles }) {

    const router = useRouter()

    const { cat_slug } = router.query
    const [category, setCategory] = useState(initCategory);
    const [articles, setArticles] = useState(initArticles);
    const [nextPosts, setNextPosts] = useState([]);
    const [page, setPage] = useState(2);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        if (!articles.length) {
            init();
        }
    }, []);

    const init = async () => {
        const data = await PostStore.findByCategory(cat_slug);
        setCategory(data.category);
        setArticles(data.articles);
    };

    const Body = () => {

        const findNextPosts = async () => {
            setLoading(true);
            const data = await PostStore.findByCategory(cat_slug, page);
            setNextPosts([...nextPosts, ...data.articles]);
            setPage(data.page);
            setLoading(false);
        }

        if (category == undefined || articles == undefined)
            return <Loading/>
        return (

            <>
                <Head>
                    <title>{category.name || ""}</title>
                    <meta name="description" content={category.description || ""} />
                </Head>

                <div className="text-center border-b pb-2">
                    <div className="flex flex-col items-center text-center mx-auto mb-4">
                        <div className=" w-48 mt-1 mr-3 overflow-hidden flex flex-col items-center">
                            <img src={category.image} alt={category.name} className="h-14" />
                        </div>
                    </div>

                    <h1 className="w-full text-gray-800 text-4xl px-5 font-bold leading-none text-center">{category.name}</h1>
                    <FollowingCategory category={category} />
                </div>
                <AdBanner />
                <div className="mt-2 flex flex-wrap">
                    {!articles.length && <Loading />}
                    {articles.map(post => <Trend post={post} key={post.id} />)}
                </div>
                <AdBanner />
                <div className="mt-2 flex flex-wrap">
                    {nextPosts.map(post => <Trend post={post} key={post.id} />)}
                    {page > 0 &&
                        <div className="flex flex-col items-center w-full py-2">
                            <button className="btn rounded inline-flex item-center" onClick={() => findNextPosts()}>
                                {loading && <span className="text-white opacity-75 my-0 mx-auto block relative w-0 h-0 -ml-1 mr-7">
                                    <i className="fas fa-circle-notch fa-spin text-2xl fa-1x"></i>
                                </span>
                                }
                            Afficher plus
                            </button>
                        </div>
                    }
                </div>
            </>
        )
    }
    return (
        <Layout body={<Body />} />
    )
}

Index.getInitialProps = async (ctx) => {
    const cookies = parseCookies(ctx)
    if (ctx.req) {
        const user = await UserStore.checkToken(cookies.token);
        const data = await PostStore.findByCategory(ctx.query.cat_slug);
        return { initCategory: data.category, initArticles: data.articles, current_user: user }
    }
    return { initCategory: {}, initArticles: [], current_user: UserStore.state.currentUser }
}