import Layout from '../components/Layout'
import { useState, useEffect } from 'react'
import { useFetchHomeVideos } from "../helpers/useFetchPosts";
import { parseCookies } from 'nookies';
import Link from 'next/link';
import Video from '../components/videos/Video';
import PostStore from '../store/PostStore';
import Head from 'next/head';
import { NAME } from '../helpers/global';
import TopsVideos from "../components/videos/TopsVideos";
import { getStoredItem } from '../helpers/functions';
import AdBanner from '../components/AdBanner';
import Loading from '../components/Loading';

export default function Videos({ initTop, initVideos, initTops }) {

    const [top, setTop] = useState(initTop);
    const [videos, setVideos] = useState(initVideos);
    const [tops, setTops] = useState(initTops);
    const [page, setPage] = useState(2);
    const [nextVideos, setNextVideos] = useState([]);

    const [loading, setLoading] = useState(false);
    const token = getStoredItem('token');

    useEffect(() => {
        if (!videos.length) {
            init();
        }
    }, []);

    const init = async () => {
        const data = await useFetchHomeVideos(token);
        setTop(data.initTop);
        setVideos(data.initVideos);
        setTops(data.initTops);
    };

    const Body = () => {

        const findNextVideos = async () => {
            setLoading(true);
            const data = await PostStore.getVideos(page, token);
            setNextVideos([...nextVideos, ...data.videos]);
            setPage(data.page);
            setLoading(false);
        }
        const { uid, slug, title, url, image, channel } = top;
        const chan_slug = top.channel != undefined ? top.channel.slug : "";

        const onImageError=(e)=>{
            e.target.src="/images/no_image.png";
         }

        return (
            <>
                <Head>
                    <title>Explorer nos vidéos / {NAME}</title>
                </Head>
                <div className="flex flex-col md:flex-row space-x-0 md:space-x-6 sm:mb-10 md:mb-16">
                    {top.channel !== undefined ?
                        <div className="mb-4 lg:mb-0 p-4 lg:p-0 w-full md:w-4/7 relative rounded block">
                            <img onError={onImageError} src={top.image} alt={top.title} className="rounded object-cover w-full h-64" />
                            <h1 className="text-gray-800 text-4xl font-bold mt-2 mb-2 leading-tight">
                                <Link href={{ pathname: '/videos/[chan_slug]/[post_slug]/[uid]', query: { uid, slug, chan_slug, channel, title, image } }} as={url}>
                                    <a title={top.title} dangerouslySetInnerHTML={{__html:top.title}} />
                                </Link>
                            </h1>
                            <p className="hidden md:block text-gray-600 mb-4" dangerouslySetInnerHTML={{ __html: top.body }}/>
                        </div>
                        : <Loading />}

                    <div className="w-full md:w-4/7">
                        {!tops.length && <Loading />}
                        {tops.map(video => <TopsVideos video={video} key={video.id} />)}
                    </div>
                </div>

                <div className="flex flex-wrap space-x-0 lg:flex">
                    {videos.map(video => <Video key={video.id} video={video} />)}
                </div>
                <AdBanner />

                <div className="mt-2 flex flex-wrap">
                    {nextVideos.map(video => <Video video={video} key={video.id} />)}
                    {page > 0 &&
                        <div className="flex flex-col items-center w-full py-2">
                            <button className="btn rounded inline-flex item-center" onClick={() => findNextVideos()}>
                                {loading && <span className="text-white opacity-75 my-0 mx-auto block relative w-0 h-0 -ml-1 mr-7">
                                    <i className="fas fa-circle-notch fa-spin text-2xl fa-1x"></i>
                                </span>
                                }
                        Afficher plus
                        </button>
                        </div>
                    }
                </div>
            </>
        )
    }

    return (
        <Layout body={<Body />} />
    )
}


Videos.getInitialProps = async (ctx) => {
    const cookies = parseCookies(ctx)
    if (ctx.req) {
        return await useFetchHomeVideos(cookies.token);
    }
    else return { initTop: {}, initVideos: [], initTops: [] }
}