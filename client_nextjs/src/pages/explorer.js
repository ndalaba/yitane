import Layout from '../components/Layout'
import { NAME } from "../helpers/global"
import Head from 'next/head'
import { useEffect, useRef, useState } from 'react'
import Loading from '../components/Loading'
import Magazine from '../components/posts/Magazine'
import PostStore from '../store/PostStore'
import AdBanner from '../components/AdBanner'

const SearchForm = ({ search, setSearch }) => {

    const elementRef = useRef()

    const handleChange = (event) => {
        setSearch(event.target.value)
    }
    const reset = () => {
        setSearch("")
    }
    useEffect(() => {
        elementRef.current.focus()
    }, [search])

    return (
        <div className="relative w-full">
            <input className="pl-12 rounded-lg font-medium w-full p-2 bg-gray-200 focus:outline-none focus:bg-gray-100"
                value={search}
                onChange={handleChange}
                ref={elementRef}
            />
            <i className="fas fa-search absolute left-0 top-0 mt-3 ml-3 text-sm text-gray-700" />
            {(search.length > 0) && <i className={`fas fa-times-circle absolute top-0 mt-3 pr-5 right-0 text-sm text-teal-900 cursor-pointer`}
                onClick={reset}
            />}
        </div>
    )

}

export default function Index({ initMagazines }) {

    const [magazines, setMagazines] = useState(initMagazines)
    const [filtredMagazines, setFiltredMagazines] = useState(initMagazines)
    const [loading, setLoading] = useState(false)
    const [search, setSearch] = useState('')

    useEffect(() => {
        fetch_trend_magazine();
    }, [])

    useEffect(() => {
        if (search.trim().length > 0)
            filter()
        else
            resetFilter()
    }, [search])

    const fetch_trend_magazine = async () => {
        setLoading(true)
        const data = await PostStore.getMagazines();
        setMagazines(data.magazines)
        setFiltredMagazines(data.magazines)
        setLoading(false)
    }
    const filter = (event) => {
        setFiltredMagazines(magazines.filter(mag => {
            const q = search.toLowerCase()
            return mag.title.toLowerCase().includes(q) || mag.category.name.toLowerCase().includes(q) || mag.country.name.toLowerCase().includes(q)
        }))
    }
    const resetFilter = () => {
        setFiltredMagazines(magazines)
    }

    const Body = () => {
        return (
            <>
                <Head>
                    <title>Explorer nos magazines / {NAME}</title>
                </Head>

                <SearchForm search={search} setSearch={setSearch} />

                <div className="mt-16 mb-12">
                    <div>
                        {filtredMagazines.map((magazine, index) => <Magazine mag={magazine} key={index} show={true} />)}
                        {loading && <Loading />}
                    </div>
                </div>
                <AdBanner/>
            </>
        )
    }
    return (
        <Layout body={<Body />} />
    )
}

Index.getInitialProps = async (ctx) => {
    if (ctx.req) {
        const data = await PostStore.getMagazines();
        return { initMagazines: data.magazines }
    }
    return { initMagazines: [] }
}