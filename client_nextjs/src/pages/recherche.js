import Layout from '../components/Layout'
import { NAME } from "../helpers/global"
import { useRouter } from 'next/router'
import Head from 'next/head'
import { useEffect, useState } from 'react'
import Loading from '../components/Loading'
import AdBanner from '../components/AdBanner'
import PostStore from '../store/PostStore';
import Trend from "../components/posts/Trend";
import PostTrend from '../components/posts/PostTrend'

export default function Index({ initArticles }) {

    const router = useRouter()
    const { q } = router.query

    const [page, setPage] = useState(2);
    const [nextPosts, setNextPosts] = useState([]);
    const [articles, setArticles] = useState(initArticles);

    const [loading, setLoading] = useState(false);
    const [notFound, setNotFound] = useState(false);

    useEffect(() => {
        if (!articles.length) {
            init();
        }
    }, []);

    const init = async () => {
        const data = await PostStore.findPosts(q, page);
        setPage(data.page);
        setArticles(data.articles);
        if (!data.articles.length)
            setNotFound(true);
    }

    const Body = () => {

        const findNextPosts = async () => {
            setLoading(true);
            const data = await PostStore.findPosts(q, page);
            setNextPosts([...nextPosts, ...data.posts]);
            setPage(data.page);
            setLoading(false);
        }

        if (articles == undefined)
            return <Loading />
        return (
            <>
                <Head>
                    <title>{q || ""} / {NAME}</title>
                </Head>

                <div className="text-center border-b pb-2">
                    <h1 className="w-full text-gray-800 text-4xl px-5 font-semibold leading-none text-center">Resultat recherche: {q}</h1>
                </div>

                <AdBanner />

                <div className="mt-2 flex flex-wrap">
                    {(articles.length == 0 && !notFound) && <Loading />}
                    {(articles.length == 0 && notFound) && <h2 className="font-medium text-lg text-center w-full p-3">Aucun resultat correspondant à votre recherche</h2>}
                    {(articles.length == 0 && notFound) && <PostTrend />}
                    {articles.map(post => <Trend post={post} key={post.id} />)}
                </div>

                <AdBanner />
                <div className="mt-2 flex flex-wrap">
                    {nextPosts.map(post => <Trend post={post} key={post.id} />)}
                    {page > 0 &&
                        <div className="flex flex-col items-center w-full py-2">
                            <button className="btn rounded inline-flex item-center" onClick={() => findNextPosts()}>
                                {loading && <span className="text-white opacity-75 my-0 mx-auto block relative w-0 h-0 -ml-1 mr-7">
                                    <i className="fas fa-circle-notch fa-spin text-2xl fa-1x"></i>
                                </span>
                                }
                        Afficher plus
                        </button>
                        </div>
                    }
                </div>
            </>
        )
    }
    return (
        <Layout body={<Body />} />
    )
}

Index.getInitialProps = async (ctx) => {
    if (ctx.req) {
        const data = await PostStore.findPosts(ctx.query.q);
        return { initArticles: data.articles, init_search: ctx.query.q }
    }
    return { initArticles: [], init_search: "" }
}