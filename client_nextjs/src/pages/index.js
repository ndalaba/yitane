import Layout from '../components/Layout'
import { useState, useEffect } from 'react'
import { useFetchHomePosts } from "../helpers/useFetchPosts";
import { parseCookies } from 'nookies';
import Top from '../components/posts/Top';
import Tops from '../components/posts/Tops';
import Trend from '../components/posts/Trend';
import Link from 'next/link';
import Video from '../components/videos/Video';
import CategoryBloc from '../components/posts/CategoryBloc';
import PostStore from '../store/PostStore';
import Loading from "../components/Loading";
import { getStoredItem } from '../helpers/functions';
import AdBanner from '../components/AdBanner';


export default function Index({ initPosts, initTop, initTrends, initVideos, initTops, initCategories }) {

    const [posts, setPosts] = useState(initPosts);
    const [top, setTop] = useState(initTop);
    const [videos, setVideos] = useState(initVideos);
    const [tops, setTops] = useState(initTops);
    const [trends, setTrends] = useState(initTrends);
    const [categories, setCategories] = useState(initCategories);
    const [page, setPage] = useState(2);
    const [nextPosts, setNextPosts] = useState([]);

    const [loading, setLoading] = useState(false);
    const token = getStoredItem('token');
    
    useEffect(() => {
        if (!posts.length) {
            init();
        }
    }, []);

    const init = async () => {
        const data = await useFetchHomePosts(token);
        setPosts(data.initPosts);
        setTop(data.initTop);
        setVideos(data.initVideos);
        setTrends(data.initTrends);
        setTops(data.initTops);
        setCategories(data.initCategories);
    };

    const Body = () => {

        const findNextPosts = async () => {
            setLoading(true);
            const data = await PostStore.findNextPosts(page, token);
            setNextPosts([...nextPosts, ...data.posts]);
            setPage(data.page);
            setLoading(false);
        }

        return (
            <>
                <div className="flex flex-col md:flex-row space-x-0 md:space-x-6 sm:mb-10 md:mb-16">
                    {top.magazine != undefined ? <Top post={top} /> : <Loading />}
                    <div className="w-full md:w-4/7">
                        {!tops.length && <Loading />}
                        {tops.map(post => <Tops post={post} key={post.id} />)}
                    </div>
                </div>

                <div className="flex sm:mt-10 md:mt-16 mb-4 px-4 lg:px-0 items-center justify-between">
                    <h2 className="font-semibold text-3xl">Dernières vidéos</h2>
                    <Link href="/videos">
                        <a className="bg-teal-500 hover:bg-teal-600 rounded text-white px-3 py-1 cursor-pointer">Tout afficher</a>
                    </Link>
                </div>
                <div className="block space-x-0 lg:flex lg:space-x-6">
                    {videos.length == 0 && <Loading />}
                    {videos.slice(0, 3).map(video => <Video key={video.id} video={video} />)}
                </div>
                <div className="block space-x-0 lg:flex lg:space-x-6 mt-2">
                    {videos.slice(3, 6).map(video => <Video key={video.id} video={video} />)}
                </div>

                {trends.length > 0 && <h3 className="ml-4 md:ml-0 text-3xl text-gray-800 font-semibold mt-12">Les plus populaires</h3>}
                <div className="mt-2 flex flex-wrap">
                    {trends.length == 0 && <Loading />}
                    {trends.map(post => <Trend post={post} key={post.id} />)}
                </div>

                {categories.map(category => <CategoryBloc category={category} posts={posts} key={category.uid} />)}

                <h3 className="ml-2 md:ml-0 text-3xl text-gray-800 font-semibold mt-12">A lire également</h3>
                <div className="mt-2 flex flex-wrap">
                    {posts.length == 0 && <Loading />}
                    {posts.map(post => <Trend post={post} key={post.id} />)}
                </div>
                <AdBanner />
                <div className="mt-2 flex flex-wrap">
                    {nextPosts.map(post => <Trend post={post} key={post.id} />)}
                    {page > 0 &&
                        <div className="flex flex-col items-center w-full py-2">
                            <button className="btn rounded inline-flex item-center" onClick={() => findNextPosts()}>
                                {loading && <span className="text-white opacity-75 my-0 mx-auto block relative w-0 h-0 -ml-1 mr-7">
                                    <i className="fas fa-circle-notch fa-spin text-2xl fa-1x"></i>
                                </span>
                                }
                        Afficher plus
                        </button>
                        </div>
                    }
                </div>
            </>
        )
    }

    return (
        <Layout body={<Body />} />
    )
}


Index.getInitialProps = async (ctx) => {
    const cookies = parseCookies(ctx)
    if (ctx.req) {
        return await useFetchHomePosts(cookies.token);
    }
    else return { initPosts: [], initTop: {}, initVideos: [], initTrends: [], initTops: [], initCategories: [] }
}