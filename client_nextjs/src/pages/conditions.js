import Head from 'next/head'
import Layout from '../components/Layout'
import { NAME } from '../helpers/global'

export default function Condition() {

   const Body = () => {
      return (
         <>
            <Head>
               <title>Conditions d’utilisation de / {NAME}</title>
            </Head>

            <div className="w-full md:w-4/5 mx-auto text-xl">
               <h1 className="mx-5 my-3 text-xl">
                  <span className=" text-teal-600 font-bold tracking-widest">CONDITIONS D’UTILISATION DE PIAAFRICA NEWS</span>
               </h1>
               <div className="w-full text-gray-800 text-2xl px-5 font-bold leading-none">
                  RESTEZ INFORMÉS, TROUVEZ DE L'INSPIRATION
			      </div>
               <div className="px-5 w-full mx-auto">
                  <p className="my-5">La consultation ou la réception de documents n'entraîne aucun transfert de droit de propriété intellectuelle en faveur de l'utilisateur.
                  Ce dernier s'engage à ne pas rediffuser ou reproduire les données fournies autrement que pour son usage personnel.
                  Les données transmises sont traitées en conformité avec les usages en vigueur.
                  L'éditeur fournit l'information et les services associés en l'état et ne saurait accorder une garantie quelconque notamment pour la fiabilité, l'actualité ou l'exhaustivité des données.
				L'utilisateur recherche, sélectionne et interprète les données sous sa propre responsabilité.</p>
               </div>
               <div className="px-5 w-full mx-auto">
                  <p className="my-5">Les données contenues sur le site sont protégées par la loi du 1er juillet 1998 sur les bases de données.
                  En accédant ou en utilisant le site, vous reconnaissez vous conformer aux dispositions de la loi, et notamment en vous interdisant l'extraction, le transfert, le stockage, la reproduction de tout ou partie qualitativement ou quantativement substantielle du contenu des bases de données figurant sur le site. La reproduction, la rediffusion ou l'extraction automatique par tout moyen d'informations figurant sur piaafrica.com est interdite.
				L'emploi de robots, programmes permettant l'extraction directe de données est de même rigoureusement interdit.</p>
               </div>
               <div className="px-5 w-full mx-auto">
                  <p className="my-5">PIAAFRICA NEWS se réserve le droit d'entamer toute action visant à faire cesser le préjudice.</p>
               </div>
            </div>

         </>

      )
   }

   return (
      <Layout body={<Body />} />
   )
}