import Head from 'next/head';
import Layout from '../components/Layout';
import { NAME } from '../helpers/global';

export default function Apropos() {
   
   const Body = () => {
      return (
         <>
            <Head>
               <title>A propos / {NAME}</title>
            </Head>

            <div className="w-full md:w-4/5 mx-auto text-xl">
               <h1 className="mx-5 my-3 text-xl">
                  <span className=" text-teal-600 font-bold tracking-widest">A PROPOS</span>
               </h1>
               <div className="w-full text-gray-800 text-2xl px-5 font-bold leading-none">
                  RESTEZ INFORMÉS, TROUVEZ DE L'INSPIRATION
			      </div>
               <div className="px-5 w-full mx-auto">
                  <p className="my-5">PIAAFRICA NEWS vous apporte les dernières et les plus pertinentes informations, et vous maintient à jour.</p>
               </div>
               <div className="px-5 w-full mx-auto">
                  <p className="my-5">Découvrez et suivez vos medias et magazines favoris, et commencer à recevoir leurs informations.</p>
               </div>
               <div className="px-5 w-full mx-auto">
                  <p className="my-5">Restez au courant des sujets et des magasines et chaines que vous suivez.</p>
               </div>
               <div className="px-5 w-full mx-auto">
                  <p className="my-5">Vous cherchez un sujet, une actualité? Utiliser le champ de recherche et trouver ce que vous cherchez.</p>
               </div>
               <div className="px-5 w-full mx-auto">
                  <p className="my-5">Sauvegardez les articles et vidéos pour plus tard dans une liste pratique.</p>
               </div>
               <div className="px-5 w-full mx-auto">
                  <p className="my-5">
                     PIAAFRICA NEWS a été conçu comme un endroit pour trouver les meilleures infos de la journée, réunissant vos
                     sources d’information favorites avec du contenu social afin de vous offrir une vision globale sur des
                     sujets allant de la politique aux tendances technologiques.
				      </p>
               </div>
            </div>
         </>
      )
   }

   return (
      <Layout body={<Body />} />
   )
}