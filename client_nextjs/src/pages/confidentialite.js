import Head from 'next/head'
import Layout from '../components/Layout'
import { NAME } from '../helpers/global'

export default function Index() {
  
   const Body = () => {
      return (
         <>
            <Head>
               <title>Politique de confidentialité / {NAME}</title>
            </Head>
            <div className="w-full md:w-4/5 mx-auto text-xl">
               <h1 className="mx-5 my-3 text-xl">
                  <span className=" text-teal-600 font-bold tracking-widest">POLITIQUE DE CONFIDENTIALITÉ DE PIAAFRICA NEWS</span>
               </h1>
               <div className="w-full text-gray-800 text-2xl px-5 font-bold leading-none">
                  RESTEZ INFORMÉS, TROUVEZ DE L'INSPIRATION
			</div>
               <div className="px-5 w-full mx-auto">
                  <p className="my-5">Le respect de la vie privée et la protection des données sont importants pour la communauté PIAAFRICA NEWS.</p>
               </div>
               <div className="px-5 w-full mx-auto">
                  <p className="my-5">La transparence l’est aussi. Nous avons tout fait pour éviter le jargon juridique en expliquant en termes simples les informations que nous recueillons à votre sujet, la raison pour laquelle nous les utilisons et les rares situations dans lesquelles nous pourrions être amenés à les partager avec des tiers. Nous vous prions de lire attentivement la présente politique.
Elle s’applique à toutes les interactions que vous avez avec les produits et services de PIAAFRICA NEWS.</p>
               </div>
               <div className="px-5 w-full mx-auto">
                  <p className="my-5">PIAAFRICA NEWS n'enregistre pas d'informations personnelles permettant l'identification, à l'exception des formulaires que l'utilisateur est libre de remplir (ouverture de compte, adhésion volontaire à une liste de diffusion, correction d'une fiche...).</p>
               </div>
               <div className="px-5 w-full mx-auto">
                  <p className="my-5">
                     PIAAFRICA NEWS, ou ses mandants, se réservent le droit de contacter toute personne ayant volontairement donné ses coordonnées au sujet d'opérations ou de promotions sur le site.
				</p>
               </div>
               <div className="px-5 w-full mx-auto">
                  <p className="my-5">
                     PIAAFRICA NEWS pourra procéder à des analyses statistiques sans que celles-ci soient nominatives et pourra en informer des tiers (organismes d'évaluation de fréquentation) sous une forme résumée et non nominative.
				</p>
               </div>
               <div className="px-5 w-full mx-auto">
                  <p className="my-5">
                     La gestion des profils nécessite l'utilisation de cookies. Des informations non personnelles sont enregistrées par ce système de cookies (fichiers texte utilisés pour reconnaître un utilisateur et ainsi faciliter son utilisation du site). Ceux-ci n'ont aucune signification en dehors de leur utilisation sur le site PIAAFRICA NEWS
				</p>
               </div>
            </div>
         </>
      )
   }

   return (
      <Layout body={<Body />} />
   )
}