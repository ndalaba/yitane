import Head from "next/head"
import Link from "next/link"
import { useRouter } from "next/router"
import { useEffect, useState } from "react"
import Loading from "../../components/Loading"
import { getStoredItem, notification } from "../../helpers/functions"
import { API_ROUTE, NAME } from "../../helpers/global"
import UserStore from "../../store/UserStore";
import React from "react";
import Header from "../../components/Header"
import Footer from "../../components/Footer"


export default function Setting() {

    const router = useRouter()

    const currentUser = UserStore.state.currentUser;
    const [form, setState] = useState({ 'email': '', 'name': '', 'phone': '', country: 0 })
    const [countries, setCountries] = useState([])

    useEffect(() => {
        const token = getStoredItem('token');
        if (token.length && currentUser.is_authenticated) {
            initField()
            fetchCountries()
        }
        else
            router.push('/auth/login')

    }, [currentUser])


    const fetchCountries = async () => {
        const response = await fetch(API_ROUTE + "countries");
        const countries = await response.json();
        setCountries(countries);
    }

    const initField = async () => {
        const data = await UserStore.getUser();
        setState({ 'email': data.user.email, 'name': data.user.name, 'phone': data.user.phone, 'country': data.user.country.id })
    }

    const updateField = (e) => {
        setState({ ...form, [e.target.name]: e.target.value })
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        const formData = new FormData(e.target)
        const data = await UserStore.updateUser(formData);
        if (data.status) {
            UserStore.setCurrentUser(data.user);
            notification("Succès", data.message, 'success');
        }
        else { notification("Erreur", data.message, 'danger'); }
    }

    if (currentUser == undefined)
        return <div className="mt-12 w-full text-center">Ce compte n'existe pas</div>
    return (
        <>
            <Head>
                <title>{currentUser.name || ""} / {NAME}</title>
            </Head>
            <Header />
            <div className="max-w-screen-lg mx-auto">
                <main className="mt-4 md:mt-12">
                    <div className="w-full md:w-5/5 mx-auto">
                        <div className="flex flex-col items-center text-center mx-auto pb-6 border-b ">
                            <div className="w-48 mt-1 mr-3 overflow-hidden flex flex-col items-center mb-2">
                                <img src={currentUser.image} alt={currentUser.name} className="w-32 rounded" />
                            </div>
                            <h1 className="w-full text-gray-800 text-4xl px-5 font-medium text-center leading-none">{currentUser.name}</h1>
                        </div>

                        <div>
                            {(form.email == null || !form.email.length) && <Loading />}
                            <form className="w-full rounded-lg p-3 py-5 overflow-x-hidden pb-16" onSubmit={handleSubmit}>
                                <h1 className="text-2xl text-gray-800 px-3 mb-6 mt-3 font-medium text-center">Paramètres de compte</h1>
                                <div className="w-full px-3 mb-6">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="name"> Nom et prénom</label>
                                    <input onChange={updateField} value={form.name} className="block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" name="name" id="name" type="text" placeholder="nom et prénom" required />
                                </div>
                                <div className="w-full px-3 mb-6">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="email">Email</label>
                                    <input onChange={updateField} value={form.email} className="block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" name="email" type="email" placeholder="votre email" required />
                                </div>

                                <div className="w-full px-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="phone">Téléphone</label>
                                    <input onChange={updateField} value={form.phone} className="block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-teal-200" name="phone" id="phone" type="text" placeholder="téléphone" />
                                </div>
                                <div className="w-full px-3 mb-6 mt-6">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="country">Pays</label>
                                    <div className="relative">
                                        <select onChange={updateField} value={form.country} className="block w-full bg-gray-200 border border-teal-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-teal-200" id="country" name="country" required>
                                            <option value="0">Selectionner un pays</option>
                                            {countries.map(country => <option key={country.id} value={country.id}>{country.name}</option>)}
                                        </select>
                                    </div>
                                </div>
                                <div className="w-full px-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="image">Avatar</label>
                                    <input className="block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-teal-200" name="image" id="image" type="file" />
                                </div>
                                <div className="w-full px-3 py-8">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="image">Paramètres</label>
                                    <div className="pl-10">
                                        <Link href="/compte/password">
                                            <a className="text-sm text-gray-800">Changer de mot de passe</a>
                                        </Link>
                                    </div>
                                </div>
                                <div className="flex justify-center w-full px-3">
                                    <button title="Enregistrer" type="submit" className="btn w-full">Enregistrer</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </main>
            </div>

            <Footer />
        </>
    )
}
