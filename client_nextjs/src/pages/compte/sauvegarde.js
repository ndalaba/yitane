import Layout from "../../components/Layout"
import { useRouter } from 'next/router'
import Head from 'next/head'
import {  useEffect, useState } from 'react'
import { NAME } from "../../helpers/global"
import Profil from "../../components/Profil"
import Post from "../../components/posts/Post"
import Video from "../../components/videos/Video"
import UserStore from "../../store/UserStore"


export default function Index() {
    const router = useRouter()
    const { type } = router.query
    const user = UserStore.state.currentUser;
    const current_path = router.pathname

    const [userSavedArticles] = useState(UserStore.state.userSavedArticles)
    const [userSavedVideos] = useState(UserStore.state.userSavedVideos)

    useEffect(() => {
    }, [])


    const Header = () => {
        if (user == undefined)
            return <div className="mt-12 w-full text-center">Ce compte n'existe pas</div>
        return (
            <>
                <Head>
                    <title>{user.name || " à consulter plus tard"} / {NAME}</title>
                </Head>
                <button title="Retour page précédente" className="p-1 px-2 rounded-full text-teal-700 focus:outline-none hover:bg-gray-200 hover:text-teal-900" onClick={router.back}><i className="fa fa-arrow-circle-left" /></button>
                <h3 className="text-xl font-semibold text-teal-800">#{user.name}</h3>
                <i className="far fa-sta text-xl text-teal-800 pr-8" />
            </>
        )
    }

    const Body = () => {
        if (user == undefined)
            return <div className="mt-12 w-full text-center">Ce compte n'existe pas</div>
        return (
            <div className="mt-10 mb-12">
                <Profil member={user} current_path={current_path} />
                {type == "post" &&
                    <div>
                        {userSavedArticles.map(article => <div className="w-full p-5 border-b border-gray-200 flex  hover:bg-gray-100" key={article.id}>
                            <Post post={article} />
                        </div>)}
                    </div>}
                {type == "video" &&
                    <div>
                        {userSavedVideos.map(video => <div className="w-full p-5 border-b border-gray-200 flex  hover:bg-gray-100" key={video.id}>
                            <Video video={video} />
                        </div>)}
                    </div>}
            </div>
        )
    }
    return (
        <Layout header={<Header />} body={<Body />} />
    )
}