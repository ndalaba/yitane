import Layout from "../../components/Layout"
import { useRouter } from 'next/router'
import Head from 'next/head'
import { useEffect, useState } from 'react'
import {  NAME } from "../../helpers/global"
import Channel from "../../components/videos/Channel"
import Profil from "../../components/Profil"
import UserStore from "../../store/UserStore"


export default function Index({ initUser, initChannels }) {

    const router = useRouter()
    const { uid } = router.query
    const current_path = router.pathname

    const [user, setUser] = useState(initUser)
    const [channels, setChannels] = useState(initChannels)

    useEffect(() => {
        getData()
    }, [])

    const getData = async () => {
        const data = await UserStore.getUserChannels(uid);
        setUser(data.user)
        setChannels(data.channels)
    }

    const Body = () => {
        if (user == undefined)
            return <div className="mt-12 w-full text-center">Ce compte n'existe pas</div>
        return (
            <>
                <Head>
                    <title>{user.name || " Chaines"} / {NAME}</title>
                </Head>

                <div className="w-full md:w-5/5 mx-auto">
                    <Profil member={user} current_path={current_path} />
                    <div>
                        {channels.map(chan => <Channel chan={chan} show={true} key={chan.id} />)}
                    </div>
                </div>
            </>

        )
    }
    return (
        <Layout body={<Body />} />
    )
}

Index.getInitialProps = async (ctx) => {
    if (ctx.req) {
        const data = await UserStore.getUserChannels(ctx.query.uid);
        return { initUser: data.user, initChannels: data.channels }
    }
    return { initUser: {}, initChannels: [] }
}