import Head from "next/head"
import { useRouter } from "next/router"
import { useEffect, useState } from "react"
import Footer from "../../components/Footer"
import Header from "../../components/Header"
import { getStoredItem, notification } from "../../helpers/functions"
import { NAME } from "../../helpers/global"
import UserStore from "../../store/UserStore"

export default function Password() {

    const router = useRouter()

    const user = UserStore.state.currentUser;
    const [form, setForm] = useState({ confirmation_password: '', password: '' })

    useEffect(() => {
        const token = getStoredItem('token');
        if (!token.length && !user.is_authenticated)
            router.push('/auth/login')
    }, [])

    const updateField = (e) => {
        setForm({ ...form, [e.target.name]: e.target.value })
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        const formData = new FormData(e.target)
        const data = await UserStore.updatePassword(formData);
        if (data.status) notification("Succès", data.message, 'success');
        else { notification("Erreur", data.message, 'danger'); setstate({ 'confirmation_password': '', 'password': '' }) }
    }

    return (
        <>
            <Head>
                <title>{user.name || ""} / {NAME}</title>
            </Head>

            <Header />
            <div className="max-w-screen-lg mx-auto">
                <main className="mt-4 md:mt-12">
                    <div className="w-full md:w-5/5 mx-auto">
                        <div className="flex flex-col items-center text-center mx-auto pb-6 border-b ">
                            <div className="w-48 mt-1 mr-3 overflow-hidden flex flex-col items-center mb-2">
                                <img src={user.image} alt={user.name} className="w-32 rounded" />
                            </div>
                            <h1 className="w-full text-gray-800 text-4xl px-5 font-medium text-center leading-none">{user.name}</h1>
                        </div>

                        <div>
                            <form className="w-full rounded-lg p-3 py-5 mb-12" onSubmit={handleSubmit}>
                                <h1 className="text-2xl text-gray-800 px-3 mb-6 mt-3 font-medium text-center">Modification de mot de passe</h1>
                                <div className="w-full px-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="password">Mot de passe</label>
                                    <input key="password" autoComplete="off" onChange={updateField} value={form.password} className="block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-teal-200" name="password" id="password" type="password" placeholder="" />
                                    <p className="text-gray-600 text-xs italic">Faites-le aussi long et aussi fou que vous le souhaitez</p>
                                </div>
                                <div className="w-full px-3 py-6">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="confirmation_password">Confirmation mot de passe</label>
                                    <input autoComplete="off" onChange={updateField} value={form.confirmation_password} className="block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-teal-200" name="confirmation_password" id="confirmation_password" type="password" placeholder="" />
                                </div>

                                <div className="flex justify-center w-full px-3">
                                    <button title="Enregistrer" type="submit" className="btn w-full">Enregistrer</button>
                                </div>

                            </form>
                        </div>

                    </div>
                </main>
            </div>

            <Footer />
        </>
    )
}