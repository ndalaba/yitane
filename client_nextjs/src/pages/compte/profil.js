import Layout from "../../components/Layout"
import { useRouter } from 'next/router'
import Head from 'next/head'
import { useEffect, useState } from 'react'
import { NAME } from "../../helpers/global"
import Comment from "../../components/Comment"
import Post from "../../components/posts/Post"
import Magazine from "../../components/posts/Magazine";
import Channel from "../../components/videos/Channel";
import Video from "../../components/videos/Video";
import UserStore from "../../store/UserStore";
import Profil from "../../components/Profil"


export default function Index({ initUser, initFeed }) {

    const router = useRouter()
    const { uid } = router.query;
    const current_path = router.pathname

    const [page, setPage] = useState(2);
    const [feeds, setFeeds] = useState(initFeed);
    const [nextFeed, setNextFeed] = useState([]);
    const [user, setUser] = useState(initUser);

    const [loading, setLoading] = useState(false);

    useEffect(() => {
        if (!feeds.length) {
            init();
        }
    }, []);

    const init = async () => {
        const data = await UserStore.getUserFeed(uid);
        setFeeds(data.feeds);
        setUser(data.user);
    };

    const Body = () => {
        const findNextFeed = async () => {
            setLoading(true);
            const data = await UserStore.getUserFeed(uid, page);
            setPage(data.page);
            setNextFeed(data.feeds);
            setLoading(false);
        }

        if (user == undefined)
            return <div className="mt-12 w-full text-center">Ce compte n'existe pas</div>
        return (
            <>
                <Head>
                    <title>{user.name || ""} / {NAME}</title>
                </Head>

                <div className="w-full md:w-5/5 mx-auto">
                    
                    <Profil member={user} current_path={current_path} />

                    {feeds.map(feed =>
                        <div className="w-full p-5 border-b border-gray-200 flex flex-col  hover:bg-gray-100" key={feed.id}>
                            <p className="text-sm font-semibold text-gray-700 w-full p-2 pb-4"><i className="fas fa-stream"></i> {user.name} {feed.text}</p>
                            {feed.action == "post_comment" && (<div className="mb-2"><Comment comment={feed.comment} /></div>)}
                            {(feed.action == "post_comment" || feed.action == "like_post" || feed.action == "unlike_post") && (<Post post={feed.article} />)}
                            {(feed.action == "video_comment" || feed.action == "like_video" || feed.action == "unlike_video") && (<Video video={feed.video} />)}
                            {(feed.action == "follow_mag" || feed.action == "unfollow_mag" || feed.action == "follow_cat" || feed.action == "unfollow_cat") && (<Magazine mag={feed.magazine} user={user} />)}
                            {(feed.action == "follow_channel" || feed.action == "unfollow_channel") && (<Channel chan={feed.channel} user={user} />)}
                        </div>
                    )}

                    <div className="mt-2 flex flex-wrap">
                        {nextFeed.map(feed =>
                            <div className="w-full p-5 border-b border-gray-200 flex flex-col  hover:bg-gray-100" key={feed.id}>
                                <p className="text-sm font-semibold text-gray-700 w-full p-2 pb-4"><i className="fas fa-stream"></i> {user.name} {feed.text}</p>
                                {feed.action == "post_comment" && (<div className="mb-2"><Comment comment={feed.comment} /></div>)}
                                {(feed.action == "post_comment" || feed.action == "like_post" || feed.action == "unlike_post") && (<Post post={feed.article} />)}
                                {(feed.action == "video_comment" || feed.action == "like_video" || feed.action == "unlike_video") && (<Video video={feed.video} />)}
                                {(feed.action == "follow_mag" || feed.action == "unfollow_mag" || feed.action == "follow_cat" || feed.action == "unfollow_cat") && (<Magazine mag={feed.magazine} user={user} />)}
                                {(feed.action == "follow_channel" || feed.action == "unfollow_channel") && (<Channel chan={feed.channel} user={user} />)}
                            </div>
                        )}
                        {page > 0 &&
                            <div className="flex flex-col items-center w-full py-2">
                                <button className="btn rounded inline-flex item-center" onClick={() => findNextFeed()}>
                                    {loading && <span className="text-white opacity-75 my-0 mx-auto block relative w-0 h-0 -ml-1 mr-7">
                                        <i className="fas fa-circle-notch fa-spin text-2xl fa-1x"></i>
                                    </span>
                                    }
                                Afficher plus
                                </button>
                            </div>
                        }
                    </div>
                </div>
            </>
        )
    }
    return (
        <Layout body={<Body />} />
    )
}

Index.getInitialProps = async (ctx) => {
    if (ctx.req) {
        const data = await UserStore.getUserFeed(ctx.query.uid);
        return { initUser: data.user, initFeed: data.feeds }
    }
    return { initUser: {}, initFeed: [] }
}