import Layout from "../../components/Layout"
import { useRouter } from 'next/router'
import Head from 'next/head'
import { useEffect, useState } from 'react'
import { NAME } from "../../helpers/global"
import Magazine from "../../components/posts/Magazine"
import Profil from "../../components/Profil"
import UserStore from "../../store/UserStore"


export default function Index({ initUser, initMagazines }) {

    const router = useRouter()
    const { uid } = router.query
    const current_path = router.pathname

    const [user, setUser] = useState(initUser)
    const [magazines, setMagazines] = useState(initMagazines)

    useEffect(() => {
        getData()
    }, [])

    const getData = async () => {
        const data = await UserStore.getUserMazines(uid);
        setUser(data.user)
        setMagazines(data.magazines)
    }

    const Body = () => {
        if (user == undefined)
            return <div className="mt-12 w-full text-center">Ce compte n'existe pas</div>
        return (
            <>
                <Head>
                    <title>{user.name || " Magasines"} / {NAME}</title>
                </Head>

                <div className="w-full md:w-5/5 mx-auto">
                    <Profil member={user} current_path={current_path} />
                    <div>
                        {magazines.map(mag => <Magazine mag={mag} show={true} key={mag.id} />)}
                    </div>
                </div>
            </>
        )
    }
    return (
        <Layout body={<Body />} />
    )
}

Index.getInitialProps = async (ctx) => {
    if (ctx.req) {
        const data = await UserStore.getUserMazines(ctx.query.uid);
        return { initUser: data.user, initMagazines: data.magazines }
    }
    return { initUser: {}, initMagazines: [] }
}