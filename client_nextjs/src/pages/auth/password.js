import React, { useState } from "react"
import { notification } from "../../helpers/functions";
import fetch from "node-fetch"
import { API_ROUTE } from "../../helpers/global";
import Head from "next/head"
import Link from "next/link"

export default function Password() {
    
    const [email, setEmail] = useState('')

    const handleSubmit = async (e) => {
        e.preventDefault()
        const response = await fetch(API_ROUTE + "auth/password-forgot?email=" + email)
        const data = await response.json()
        if (data.status)
            notification("Email", data.message, 'success');

        else notification("Erreur", data.message, 'danger');
    }

    return (
        <>
            <Head>
                <title>Mot de passe oublié</title>
            </Head>

            <div className="flex items-center justify-center h-screen">
                <form className="sm:w-full md:w-1/3 auto rounded-lg p-3 mb-12" onSubmit={handleSubmit}>
                    <h1 className="text-2xl text-gray-800 px-3 mb-6 mt-3 font-medium text-center">Réinitialiser votre mot de passe</h1>

                    <div className="w-full px-3 mb-6">
                        <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="email">
                            Email
                        </label>
                        <input value={email} onChange={(e) => setEmail(e.target.value)} className="block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" name="email" id="email" type="email" placeholder="votre email" required />
                    </div>

                    <div className="flex justify-center w-full px-3 mt-6">
                        <button title="Envoyer" type="submit" className="btn w-full">Envoyer</button>
                    </div>
                    <div className="mt-3 text-center text-sm">
                        Vous n'avez pas de compte? <Link href="/auth/signup"><a className=" text-teal-600">S'inscrire</a></Link>
                    </div>

                </form>
            </div>
        </>
    );
}
