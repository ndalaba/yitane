import React, { useState, useEffect } from "react"
import Head from "next/head"
import Link from "next/link"
import Facebook from "../../components/Facebook";
import { NAME } from "../../helpers/global";
import UserStore from "../../store/UserStore";

export default function Login() {
    const [form, setstate] = useState({ 'email': '', 'password': '', 'remember_me': false })

    useEffect(() => {
        if (UserStore.state.currentUser.is_authenticated)
            window.location.href = '/'
    }, [])

    const updateField = (e) => {
        setstate({ ...form, [e.target.name]: e.target.value })
    }
    const handleCheck = () => {
        setstate({ ...form, ['remember_me']: !form.remember_me })
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        const formData = new FormData(e.target)
        UserStore.signin(formData);
    }

    return (
        <>
            <Head>
                <title>Se connecter à {NAME} / {NAME}</title>
            </Head>

            <div className="flex items-center justify-center h-screen">
                <form className="sm:w-full md:w-1/3 auto rounded-lg p-3 mb-12" onSubmit={handleSubmit}>
                    <h1 className="text-2xl text-gray-800 px-3 mb-5 mt-3 font-medium text-center">Se connecter à {NAME}</h1>
                    <Link href="/auth/signup"><a className="inline-block w-full text-center mb-8 text-teal-600 mr-10 text-base">Vous n'avez pas de compte?</a></Link>
                    <div className="w-full px-3 mb-6">
                        <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="email">
                            Email
                        </label>
                        <input onChange={updateField} value={form.email} className="block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" name="email" id="email" type="email" placeholder="votre email" required />
                    </div>

                    <div className="w-full px-3">
                        <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="password">
                            Mot de passe
                        </label>
                        <input onChange={updateField} value={form.password} className="block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-teal-200" name="password" id="password" type="password" placeholder="****************" />
                    </div>

                    <div className="w-full px-3 mt-6">
                        <label className="flex justify-start items-start">
                            <div className="bg-white border-2 rounded border-gray-400 w-6 h-6 flex flex-shrink-0 justify-center items-center mr-2 focus-within:border-blue-500">
                                <input name="remember_me" onChange={handleCheck} checked={form.remember_me} id="remember" type="checkbox" className="opacity-0 absolute" />
                                <svg className="fill-current hidden w-4 h-4 text-green-500 pointer-events-none" viewBox="0 0 20 20"><path d="M0 11l2-2 5 5L18 3l2 2L7 18z" /></svg>
                            </div>
                            <div className="select-none">Resté connecté</div>
                        </label>
                    </div>

                    <div className="flex justify-center w-full px-3 mt-6">
                        <button title="Se connecter" type="submit" className="btn w-full">
                            Se connecter
                    </button>
                    </div>
                    <div className="mt-3 text-center text-sm">
                        <Link href="/auth/password"><a className=" text-teal-600">Mot de passe oublié ?</a></Link>
                    </div>
                    <div className="mt-6">
                        <p className="text-xl text-semibold text-center pb-2">Continuer avec</p>
                        <Facebook />
                    </div>
                </form>
            </div>
        </>
    );
}