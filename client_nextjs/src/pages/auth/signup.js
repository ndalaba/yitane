import Head from "next/head"
import fetch from "node-fetch"
import { useState, useEffect } from "react"
import Link from "next/link"

import { API_ROUTE, NAME } from "../../helpers/global"
import { useRouter } from "next/router"
import UserStore from "../../store/UserStore"

export default function Signup() {
    const [form, setForm] = useState({ name: '', email: '', password: '', country: 0 })
    const [countries, setCountries] = useState([])

    useEffect(() => {
        if (user.is_authenticated)
            window.location.href = '/'
        fetchCountries()
    }, [])

    const updateField = (e) => {
        setForm({
            ...form,
            [e.target.name]: e.target.value
        })
    }

    const fetchCountries = async () => {
        const response = await fetch(API_ROUTE + "countries");
        const countries = await response.json();
        setCountries(countries);
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        const formData = new FormData(e.target);
        UserStore.signup(formData);
    }

    return (
        <>
            <Head>
                <title>S'inscrire sur {NAME} / {NAME}</title>
            </Head>
            <div className="flex items-center justify-center h-screen">
                <form className="sm:w-full md:w-1/3 auto rounded-lg p-3 mb-12" onSubmit={handleSubmit}>
                    <h1 className="text-2xl text-gray-800 px-3 mb-6 mt-3 font-medium text-center">Créer votre compte</h1>
                    <div className="w-full px-3 mb-6">
                        <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="name">
                            Nom et prénom
                        </label>
                        <input onChange={updateField} value={form.name} className="block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" name="name" id="name" type="text" placeholder="nom et prénom" required />
                    </div>
                    <div className="w-full px-3 mb-6">
                        <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="email">
                            Email
                        </label>
                        <input onChange={updateField} value={form.email} className="block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" name="email" id="email" type="email" placeholder="votre email" required />
                    </div>

                    <div className="w-full px-3">
                        <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="password">
                            Mot de passe
                        </label>
                        <input onChange={updateField} value={form.password} className="block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-teal-200" name="password" id="password" type="password" placeholder="****************" />
                        <p className="text-gray-600 text-xs italic">Faites-le aussi long et aussi fou que vous le souhaitez</p>
                    </div>
                    <div className="w-full px-3 mb-6 mt-6">
                        <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="country">
                            Pays
                        </label>
                        <div className="relative">
                            <select onChange={updateField} value={form.country} className="block w-full bg-gray-200 border border-teal-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-teal-200" id="country" name="country" required>
                                <option value="0">Selectionner un pays</option>
                                {countries.map(country => <option key={country.id} value={country.id}>{country.name}</option>)}

                            </select>
                        </div>
                    </div>
                    <div className="flex justify-center w-full px-3">
                        <button title="Nous rejoindre" type="submit" className="btn w-full">
                            Nous rejoindre
                    </button>
                    </div>
                    <div className="mt-3 text-center text-sm">
                        Vous avez déjà un compte? <Link href="/auth/login"><a title="Connectez vous" className=" text-teal-600">Connectez vous</a></Link>
                    </div>
                </form>
            </div>
        </>
    );
}