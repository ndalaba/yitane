import Layout from '../../components/Layout'
import { useRouter } from 'next/router'
import Head from 'next/head'
import { useEffect, useState } from 'react'
import { FollowingMagazine } from '../../components/Following'
import { parseCookies } from 'nookies'
import Loading from '../../components/Loading'
import AdBanner from '../../components/AdBanner'
import PostStore from '../../store/PostStore'
import Trend from "../../components/posts/Trend";
import UserStore from '../../store/UserStore'


export default function Index({ initMagazine, initArticles }) {

    const router = useRouter()
    const { mag_slug } = router.query;
    const [page, setPage] = useState(2);
    const [magazine, setMagazine] = useState(initMagazine);
    const [articles, setArticles] = useState(initArticles);
    const [nextPosts, setNextPosts] = useState([]);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        if (!articles.length) {
            init();
        }
    }, []);

    const init = async () => {
        const data = await PostStore.findByMagazine(mag_slug);
        setMagazine(data.magazine);
        setArticles(data.articles);
    };


    const Body = () => {

        const findNextPosts = async () => {
            setLoading(true);
            const data = await PostStore.findByMagazine(mag_slug, page);
            setNextPosts([...nextPosts, ...data.articles]);
            setPage(data.page);
            setLoading(false);
        }

        if (magazine == undefined || articles == undefined)
            return "<p>Chargement....</p>"
        return (

            <>
                <Head>
                    <title>{magazine.title || initMagazine.title}</title>
                    <meta name="description" content={magazine.description || initMagazine.description} />
                </Head>

                <div className="text-center border-b pb-2">
                    <div className="flex flex-col items-center text-center mx-auto mb-4">
                        <div className=" w-48 mt-1 mr-3 overflow-hidden flex flex-col items-center">
                            <img src={magazine.image} alt={magazine.title} className="h-14" />
                        </div>
                    </div>

                    <h1 className="w-full text-gray-800 text-4xl px-5 font-bold leading-none text-center">{magazine.title}</h1>
                    <FollowingMagazine magazine={magazine} />
                    <div className="text-base text-gray-700 pt-2">
                        {magazine.description}
                    </div>
                </div>
                <AdBanner />


                <div className="mt-2 flex flex-wrap">
                    {articles.length && articles.map(post => <Trend post={post} key={post.id} />)}
                    {!articles.length && <Loading />}
                </div>
                <AdBanner />
                <div className="mt-2 flex flex-wrap">
                    {nextPosts.map(post => <Trend post={post} key={post.id} />)}
                    {page > 0 &&
                        <div className="flex flex-col items-center w-full py-2">
                            <button className="btn rounded inline-flex item-center" onClick={() => findNextPosts()}>
                                {loading && <span className="text-white opacity-75 my-0 mx-auto block relative w-0 h-0 -ml-1 mr-7">
                                    <i className="fas fa-circle-notch fa-spin text-2xl fa-1x"></i>
                                </span>
                                }
                        Afficher plus
                        </button>
                        </div>
                    }
                </div>
            </>
        )
    }
    return (
        <Layout body={<Body />} />
    )
}

Index.getInitialProps = async (ctx) => {
    const cookies = parseCookies(ctx)
    if (ctx.req) {
        const user = await UserStore.checkToken(cookies.token);
        const data = await PostStore.findByMagazine(ctx.query.mag_slug);
        return { initMagazine: data.magazine, initArticles: data.articles, current_user: user }
    }
    return { initMagazine: {}, initArticles: [], current_user: UserStore.state.currentUser }
}