import Head from "next/head";
import { useEffect, useState } from "react"
import { API_ROUTE } from "../../../helpers/global"
import { useRouter } from "next/router";
import PostBtn from "../../../components/posts/PostBtn";
import PostTop from "../../../components/posts/PostTop";
import { parseCookies } from "nookies";
import Comment from "../../../components/Comment";
import Link from "next/link";
import { redirectIfNotLogged } from "../../../helpers/functions"
import Layout from "../../../components/Layout";
import UserStore from "../../../store/UserStore";
import PostStore from "../../../store/PostStore";
import AdBanner from "../../../components/AdBanner";


export default function Index({ init_article, init_comments }) {
    const router = useRouter()
    const { uid } = router.query
    const [comments, setComments] = useState(init_comments)
    const [article, setArticle] = useState(init_article)
    const user = UserStore.state.currentUser;
    const [form, setstate] = useState({ 'content': '' })

    useEffect(() => {
        fetchData();
    }, [uid])

    const fetchData = async () => {
        const data = await PostStore.findComments(uid);
        setComments(data.comments);
        setArticle(data.article)
    }
    const updateField = (e) => {
        setstate({ ...form, [e.target.name]: e.target.value })
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const formData = new FormData(event.target)
        const response = await fetch(API_ROUTE + "articles/comments?uid=" + uid + "&token=" + localStorage.getItem("token"), { method: 'POST', body: formData })
        const data = await response.json()
        if (data.status) {
            setComments(prev => [...comments, data.comment])
            setstate({ ...form, ['content']: "" })
        }

    }

    const Body = () => {

        return (
            <>
                <Head>
                    <title>{article.title || ""}</title>
                    <meta name="description" content={article.summary || ""} />
                </Head>

                {article.id &&
                    <div className="mt-10 mb-12">
                        <div className="w-full p-5">
                            <PostTop post={article} />
                            <h1 className="w-full mb-2 text-lg font-semibold">
                                <Link href={{ pathname: '/[mag_slug]/[post_slug]/[uid]', query: { uid: article.uid, post_slug: article.slug, mag_slug: article.magazine.slug, magazine: article.magazine } }} as={article.url}>
                                    <a title={article.title}>{article.title}</a>
                                </Link>
                            </h1>
                            <p className="p-2 text-gray-800 border-b border-gray-200 pb-3">{article.summary}</p>
                            <div className="w-full border-b mb-2 pb-2 ">
                                <PostBtn post={article} />
                            </div>

                            <div className="flex w-full mb-2">
                                <div className="mr-4 w-10 float-left">
                                    <img src={user.image} alt={user.name} className="h-10 w-10 rounded-full flex-none object-cover object-left" />
                                </div>
                                <div className="flex float-left w-full">
                                    <form onSubmit={handleSubmit} className="w-full" id="form">
                                        <textarea onFocus={() => redirectIfNotLogged(user, router)} onChange={updateField} value={form.content} className="w-full bg-gray-100 p-3 focus:outline-none text-gray-700" name="content" placeholder="Ecrivez votre commentaire"></textarea>
                                        {form.content.trim().length ? <button className="btn float-right">Envoyer</button> : ""}
                                    </form>
                                </div>
                            </div>

                        </div>
                        <AdBanner />
                        {comments.map(comment => {
                            return <div className="w-full p-5 border-b border-gray-200 flex  hover:bg-gray-100" key={comment.id}>
                                <Comment comment={comment} />
                            </div>
                        })}
                        <AdBanner />

                    </div>
                }
            </>
        )
    }

    return (
        <Layout body={<Body />} />
    )
}

Index.getInitialProps = async (ctx) => {
    const cookies = parseCookies(ctx)
    if (ctx.req) {
        const user = await UserStore.checkToken(cookies.token)
        const data = await PostStore.findComments(ctx.query.uid);
        return { init_article: data.article, current_user: user, init_comments: data.comments }
    }
    return { init_article: ctx.query, current_user: UserStore.state.currentUser, init_comments: [] };
}