import Head from "next/head";
import { useEffect, useRef, useState } from "react"
import Layout from "../../../components/Layout"
import { FACEBOOK_ID, NAME } from "../../../helpers/global"
import { useRouter } from "next/router";
import Trend from "../../../components/posts/Trend";
import { parseCookies } from "nookies";
import PostTrend from "../../../components/posts/PostTrend";
import AdBanner from "../../../components/AdBanner";
import { relative_time } from "../../../helpers/functions";
import Link from "next/link";
import PostStore from "../../../store/PostStore";
import MagazineStore from "../../../store/MagazineStore";
import PostBtn from "../../../components/posts/PostBtn";
import UserStore from "../../../store/UserStore";


const ExtraLink = ({ post }) => {
    return (
        <p className="font-semibold text-lg text-teal-700 mt-5 pl-4">
            <a rel="tag" href={post.link} target="_blank">Retrouver cet article sur {post.magazine.title}</a>
        </p>
    );
}

const resolveImage = (article) => {
    const images = document.querySelectorAll(".postBody img");
    images.forEach(image => {
        image.classList.add('rounded-lg')
        image.classList.add('my-2')

        let src = image.dataset.src
        let srcset = image.dataset.srcset
        if (src !== "undefined" && src !== undefined) {
            image.setAttribute('src', src)
        } else if (srcset !== "undefined" && srcset !== undefined) {
            image.setAttribute('src', srcset)
        }
        src = image.getAttribute('src')
        if (src == article.image)
            image.remove()
    });
}

const resolveIframe = () => {
    const iframes = document.querySelectorAll(".postBody iframe");
    iframes.forEach(iframe => {
        let src = iframe.dataset.src
        let srcset = iframe.dataset.srcset
        if (src !== "undefined" && src !== undefined) {
            iframe.setAttribute('src', src)
        } else if (srcset !== "undefined" && srcset !== undefined) {
            iframe.setAttribute('src', srcset)
        }
        if(iframe.getAttribute('src').startsWith('https://www.facebook.com/')){
            iframe.remove();
        }
    });
}


export default function Article({ init_article }) {
    const router = useRouter()
    const { mag_slug, post_slug, uid } = router.query
    const [article, setArticle] = useState(init_article)
    const [relatedArticles, setRelatedArticles] = useState([]);

    useEffect(() => {
        setArticle(init_article)
        fetchData();
    }, [uid])

    const fetchData = async () => {
        const { init_article, related_articles } = await PostStore.findPost(mag_slug, post_slug, uid);
        setArticle(init_article);
        setRelatedArticles(related_articles);

        // Refresh magazine feed
        const unused = init_article.magazine != undefined ? MagazineStore.refreshFeed(init_article.magazine) : null;

        //GEt full article content
        if(article.body===article.summary){
            PostStore.getContent(uid).then(response => response.json())
            .then(data => setArticle(data.article));
        }

        const links = document.querySelectorAll(".postBody a");
        links.forEach(link => {
            link.setAttribute('target', "_blank");
            link.classList.add('font-semibold')
        });

        resolveImage(init_article)
        resolveIframe(init_article)
        
        const frames = document.querySelectorAll('.postBody iframe');
        frames.forEach(frame => {
            const wrapper = document.createElement('div')
            wrapper.classList.add('video-container')
            frame.parentNode.insertBefore(wrapper, frame)
            wrapper.appendChild(frame)
        })

        
    }

    const Body = () => {
        const time = relative_time(article.date_published);
        const onImageError=(e)=>{
            e.target.src="/images/no_image.png";
         }

        if (article == undefined || article.magazine == undefined)
            return (<>
                 <div className="w-full md:w-5/5 mx-auto">
                    <AdBanner />
                    <div className="p-2">
                        <PostTrend size={10} />
                    </div>
                </div>

            </>)
        else
            return (

                <>
                    <Head>
                        <title>{article.title || ""}</title>
                        <meta name="description" content={article.summary || ""} />
                        <meta property="og:locale" content="fr" />
                        <meta property="og:site_name" content={NAME} />
                        <meta property="og:title" content={article.title} />
                        <meta property="og:url" content={article.url} />
                        <meta property="og:type" content="article" />
                        <meta property="og:description" content={article.summary} />
                        <meta property="og:image" content={article.image} />
                        <meta property="og:image:url" content={article.image} />
                        <meta property="og:image:secure_url" content={article.image} />
                        <meta property="article:published_time" content={article.date_published} />
                        <meta property="article:modified_time" content={article.created_at} />
                        <meta property="og:updated_time" content={article.created_at} />
                        <meta property="article:section" content="WEB" />
                        <meta property="fb:app_id" content={FACEBOOK_ID} />
                        <meta property="fb:pages" content="" />
                        <meta property="article:publisher" content="" />
                        <meta property="article:author" content="" />
                        <meta itemProp="name" content={article.title} />
                        <meta itemProp="headline" content={article.title} />
                        <meta itemProp="description" content={article.summary} />
                        <meta itemProp="image" content={article.image} />
                        <meta itemProp="datePublished" content={article.date_published} />
                        <meta itemProp="dateModified" content={article.created_at} />
                        <meta itemProp="author" content="ndalaba" />
                        <meta name="twitter:title" content={article.title} />
                        <meta name="twitter:url" content={article.url} />
                        <meta name="twitter:description" content={article.summary} />
                        <meta name="twitter:image" content={article.image} />
                        <meta name="twitter:card" content="summary_large_image" />
                    </Head>

                    <div className="w-full md:w-5/5 mx-auto">
                        <div className="flex flex-col items-center text-center mx-auto mb-4">
                            <div className=" w-48 mt-1 mr-3 overflow-hidden flex flex-col items-center">
                                <img onError={onImageError} src={article.magazine.image} alt={article.magazine.title} className="h-14" />
                            </div>
                        </div>

                        <h1 className="w-full text-gray-800 text-4xl px-5 font-bold leading-none">{article.title}</h1>

                        {(article.summary !== article.body) ? 
                        <h2 className="w-full text-gray-500 px-5 pb-5 pt-2" dangerouslySetInnerHTML={{ __html: article.summary }} />: null}


                        <div className="mx-5">
                            {
                                article.image !== null &&
                                <img onError={onImageError} className="rounded" placeholder="no_image.jpg" src={article.image.replace('http:', 'https:')} alt={article.title} title={article.title} />
                            }
                            {article.image == null && <AdBanner />}

                        </div>

                        <div className="text-gray-600 text-normal mx-5 border-b">
                            <p className="border-b py-3">{article.author}
                                <span> {time}</span> -
                                        {article.magazine.category != undefined &&
                                    <Link href="/topic/[cat_slug]" as={`/topic/${article.magazine.category.slug}`}>
                                        <a className=" text-teal-600 font-semibold" title={article.magazine.category.name}> @{article.magazine.category.name}</a>
                                    </Link>}
                            </p>
                            <PostBtn post={article} />
                        </div>
                        <div dangerouslySetInnerHTML={{ __html: article.body }} id="postBody" className="px-5 py-2 w-full mx-auto postBody overflow-x-hidden" />

                        {(article.summary === article.body) ? <ExtraLink post={article} /> : null}

                        <AdBanner/>

                        {relatedArticles.length > 0 ? <h3 className="ml-3 text-3xl text-gray-800 font-semibold mt-12">Recommandés pour vous</h3> : null}

                        <section className="flex flex-row flex-wrap mx-auto">

                            {relatedArticles.map((article) => <Trend key={article.id} post={article} />)}

                        </section>
                        <AdBanner/>

                    </div>

                </>

            )
    }

    return (
        <Layout body={<Body />} />
    )
}

Article.getInitialProps = async (ctx) => {
    const cookies = parseCookies(ctx)
    if (ctx.req) {
        const user = await UserStore.checkToken(cookies.token);
        const { init_article, related_articles } = await PostStore.findPost(ctx.query.mag_slug, ctx.query.post_slug, ctx.query.uid);
        return { init_article: init_article, related_articles: related_articles, current_user: user }
    }
    return { init_article: ctx.query, current_user: UserStore.state.currentUser };
}