import { useRouter } from "next/router";
import { useState, useEffect } from "react";
import { notification } from "../helpers/functions";
import { API_ROUTE, FETCH_OPTIONS } from "../helpers/global";
import UserStore from "../store/UserStore";

const FollowingMagazine = ({ magazine }) => {
    const router = useRouter()
    const user = UserStore.state.currentUser;
    const { userMagazines, setUserMagazines } = useState(UserStore.state.userMagazines);
    const [followed, setFollowed] = useState(false)
    const [label, setLabel] = useState('Suivi')

    useEffect(() => {
        isFollowed()
    }, [magazine])

    const followMagazine = async (e) => {
        e.preventDefault()
        if (!user.is_authenticated)
            return router.push('/auth/login')
        let follow = 0
        followed ? (follow = 0) : (follow = 1)
        followed ? setUserMagazines(userMagazines.filter(mag => mag.uid != magazine.uid)) : setUserMagazines([...userMagazines, magazine])
        setFollowed(!followed)
        notification('Succès', "Enregistré", 'success')
        const response = await fetch(API_ROUTE + `follow/magazine/${magazine.uid}?follow=` + follow, FETCH_OPTIONS())

    }
    const isFollowed = () => {
        if (user.is_authenticated && userMagazines != undefined && userMagazines.length) {
            const findedMagazine = userMagazines.find(mag => mag.uid == magazine.uid)
            if (findedMagazine != undefined)
                setFollowed(p => p = true)
        }
    }
    if (followed)
        return <a href={`/follow/magazine/${magazine.uid}`} className="inline-block h-8 w-48 text-center text-sm font-semibold mt-3 btn hover:bg-red-600" title="Ne plus suivre" onMouseOut={() => setLabel('Suivi')} onMouseOver={() => setLabel('Ne plus suivre')} onClick={followMagazine}>{label}</a>
    else
        return <a href={`/follow/magazine/${magazine.uid}`} className="inline-block h-10 w-48 mt-3 text-center text-sm font-semibold primary" title="Suivre" onClick={followMagazine}>Suivre</a>

}

const FollowingCategory = ({ category }) => {
    const router = useRouter()
    const user = UserStore.state.currentUser;
    const [userCategories, setUserCategories] = useState(UserStore.state.userCategories);
    const [followed, setFollowed] = useState(false)
    const [label, setLabel] = useState('Suivi')

    useEffect(() => {
        isFollowed()
    }, [category])

    const followCategory = async (e) => {
        e.preventDefault()
        if (!user.is_authenticated)
            return router.push('/auth/login')
        let follow = 0
        followed ? (follow = 0) : (follow = 1)
        followed ? setUserCategories(userCategories.filter(cat => cat.uid != category.uid)) : setUserCategories([...userCategories, category])
        setFollowed(!followed)
        notification('Succès', "Enregistré", 'success')

        const response = await fetch(API_ROUTE + `follow/category/${category.uid}?follow=` + follow, FETCH_OPTIONS())
    }
    const isFollowed = () => {
        if (user.is_authenticated && userCategories != undefined && userCategories.length) {
            const findedCategory = userCategories.find(cat => cat.slug == category.slug)
            if (findedCategory != undefined)
                setFollowed(p => p = true)
        }
    }
    if (followed)
        return <a href={`/follow/category/${category.uid}`} className="inline-block h-8 w-48 text-center text-sm font-semibold mt-3 btn hover:bg-red-600" title="Ne plus suivre" onMouseOut={() => setLabel('Suivi')} onMouseOver={() => setLabel('Ne plus suivre')} onClick={followCategory}>{label}</a>
    else
        return <a href={`/follow/category/${category.uid}`} className="inline-block h-10 w-48 mt-3 text-center text-sm font-semibold primary" title="Suivre" onClick={followCategory}>Suivre</a>

}

const FollowingChannel = ({ channel }) => {
    const router = useRouter()
    const  user  = UserStore.state.currentUser;
    const { userChannels, setUserChannels } = useState(UserStore.state.userChannels);
    const [followed, setFollowed] = useState(false)
    const [label, setLabel] = useState('Suivi')

    useEffect(() => {
        isFollowed()
    }, [channel])

    const followChannel = async (e) => {
        e.preventDefault()
        if (!user.is_authenticated)
            return router.push('/auth/login')
        let follow = 0
        followed ? (follow = 0) : (follow = 1)
        followed ? setUserChannels(userChannels.filter(mag => mag.uid != channel.uid)) : setUserChannels([...userChannels, channel])
        setFollowed(!followed)
        notification('Succès', "Enregistré", 'success')
        const response = await fetch(API_ROUTE + `follow/channel/${channel.uid}?follow=` + follow, FETCH_OPTIONS())

    }
    const isFollowed = () => {
        if (user.is_authenticated && userChannels != undefined && userChannels.length) {
            const findedChannel = userChannels.find(mag => mag.uid == channel.uid)
            if (findedChannel != undefined)
                setFollowed(p => p = true)
        }
    }
    if (followed)
        return <a href={`/follow/channel/${channel.uid}`} className="inline-block h-8 w-48 text-center text-sm font-semibold mt-3 btn hover:bg-red-600" title="Ne plus suivre" onMouseOut={() => setLabel('Suivi')} onMouseOver={() => setLabel('Ne plus suivre')} onClick={followChannel}>{label}</a>
    else
        return <a href={`/follow/channel/${channel.uid}`} className="inline-block h-10 w-48 mt-3 text-center text-sm font-semibold primary" title="Suivre" onClick={followChannel}>Suivre</a>

}

export { FollowingMagazine, FollowingCategory, FollowingChannel };