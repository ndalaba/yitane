import Link from "next/link"
import { relative_time as relativeTime } from "../helpers/functions"


const Comment = ({ comment }) => {
    const relative_time = relativeTime(comment.created_at);
    return (
        <div className="w-full">
            <div className="flex items-center w-full mb-2">
                <div className="md:block flex-none mr-4">
                    <Link href={`/compte/profil?uid=${comment.user.uid}`}>
                        <a title={comment.user.name}>
                            <img src={comment.user.image} alt={comment.user.name} className="h-10 w-10 rounded-full flex-none object-cover object-left" />
                        </a>
                    </Link>

                </div>
                <div className="flex flex-col w-full">
                    <div className="flex content-between">
                        <div>
                            <span className="font-semibold">
                                <Link href={`/compte/profil?uid=${comment.user.uid}`}>
                                    <a title={comment.user.name}>{comment.user.name}</a>
                                </Link>
                            </span>
                            <span className="text-sm text-dark ml-1"><span className="inline-block">·</span> {relative_time} </span>
                        </div>

                    </div>
                </div>
            </div>
            <p className="py-2 font-semibold">{comment.content} </p>
        </div>
    );
}

export default Comment

