import Head from 'next/head'
import { DESCRIPTION, NAME, TITLE } from "../helpers/global"
import Footer from './Footer';
import Header from './Header';


export default function Layout({ body }) {

    return (
        <>
            <Head>
                <title>{TITLE} | {NAME}</title>
                <meta name="description" content={`${DESCRIPTION}`}></meta>
            </Head>
            <Header />
            <div className="max-w-screen-lg mx-auto">
                <main className="mt-4 md:mt-12">
                    {body}
                </main>
            </div>

            <Footer />
        </>
    );
}