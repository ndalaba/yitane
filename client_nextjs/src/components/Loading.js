export default function Loading() {
    return (
        <div className="w-full flex items-center flex-col">
            <div className="flex bg-white shadow-md p-4 rounded-md w-full">
                <div data-placeholder="true" className="mr-2 h-40 w-20 rounded overflow-hidden relative bg-gray-200 w-1/3"></div>
                <div className="flex flex-col justify-between w-2/3">
                    <div data-placeholder="true" className="mb-2 h-16 overflow-hidden relative bg-gray-200 w-full"></div>
                    <div data-placeholder="true" className="h-20 overflow-hidden relative bg-gray-200 w-full"></div>
                </div>
            </div>
        </div>
    )
}
