import { useRouter } from "next/router"
import fetch from "node-fetch"
import { useState,useEffect } from "react"
import { redirectIfNotLogged } from "../helpers/functions"
import { API_ROUTE, FETCH_OPTIONS} from "../helpers/global"
import UserStore from "../store/UserStore"

const SavePost = ({ post }) => {
    const router = useRouter()
    const  user  = UserStore.state.currentUser;
    const [saveCount, setSaveCount] = useState(post.save)
    const [userSavedArticles] = useState(UserStore.state.userSavedArticles)
    const [saved, setSaved] = useState(false)

    useEffect(() => {
        isSaved()
    }, [])

    const save_article = async (e) => {
        e.preventDefault()
        redirectIfNotLogged(user,router)
        let save = 0
        saved ? (save = 0) : (save = 1)
        saved ? setSaveCount(saveCount - 1) : setSaveCount(saveCount + 1)
        saved ? UserStore.setUserSavedArticles(userSavedArticles.filter(art => art.uid != post.uid)) : UserStore.setUserSavedArticles([...userSavedArticles, post])
        setSaved(!saved)
        await fetch(API_ROUTE + `save/article/${post.uid}?save=` + save, FETCH_OPTIONS())
    }

    const isSaved = () => {
        if (user.is_authenticated && userSavedArticles != undefined && userSavedArticles.length) {
            const saved_article = userSavedArticles.find(article => article.uid == post.uid)
            if (saved_article != undefined)
                setSaved(p => p = true)
        }
    }
    if (saved)
        return (
            <button className="post-btn font-bold" onClick={save_article} title="Lire plus tard">
                <i className="fas fa-bookmark p-2 rounded-full hover:bg-teal-200 hover:text-black" />
            </button>
        )
    else
        return (
            <button className="post-btn" onClick={save_article} title="Lire plus tard">
                <i className="far fa-bookmark p-2 rounded-full hover:bg-teal-200 hover:text-black" />
            </button>
        )
}


const SaveVideo = ({ video }) => {
    const router = useRouter()
    const  user  = UserStore.state.currentUser;
    const [saveCount, setSaveCount] = useState(video.save)
    const [userSavedVideos] = useState(UserStore.state.userSavedVideos)
    const [saved, setSaved] = useState(false)

    useEffect(() => {
        isSaved()
    }, [])

    const save_video = async (e) => {
        e.preventDefault()
        redirectIfNotLogged(user,router)
        let save = 0
        saved ? (save = 0) : (save = 1)
        saved ? setSaveCount(saveCount - 1) : setSaveCount(saveCount + 1)
        //saved ? setUserSavedVideos(userSavedVideos.filter(art => art.uid != video.uid)) : setUserSavedVideos([...userSavedVideos, video])
        setSaved(!saved)
        await fetch(API_ROUTE + `save/video/${video.uid}?save=` + save, FETCH_OPTIONS())
    }

    const isSaved = () => {
        if (user.is_authenticated && userSavedVideos != undefined && userSavedVideos.length) {
            const saved_video = userSavedVideos.find(vid => vid.uid == video.uid)
            if (saved_video != undefined)
                setSaved(p => p = true)
        }
    }
    if (saved)
        return (
            <button className="post-btn font-bold" onClick={save_video} title="Regarder plus tard">
                <i className="fas fa-bookmark p-2 rounded-full hover:bg-teal-200 hover:text-black" />
            </button>
        )
    else
        return (
            <button className="post-btn" onClick={save_video} title="Regarder plus tard">
                <i className="far fa-bookmark p-2 rounded-full hover:bg-teal-200 hover:text-black" />
            </button>
        )
}


export { SavePost, SaveVideo }