import Link from "next/link";
import { NAME } from "../helpers/global";
import UserStore from "../store/UserStore";
import SearchPost from "./posts/SearchPost";
import SearchVideo from "./videos/SearchVideo";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import SideNav from "./SideNav";

export default function Header() {
  const currentUser = UserStore.state.currentUser;
  
  const { pathname } = useRouter();
  const [showVideoShearch, setshowVideoShearch] = useState(false);
  const [sideNavOpen, setSideNavOpen] = useState(false);

  useEffect(() => {
    if (pathname.startsWith('/videos')) {
      setshowVideoShearch(true);
    }

  }, []);

  const toggleSideNav = () => {
    setSideNavOpen(!sideNavOpen)
  }


  return (
    <>
      <div className="flex-1 flex flex-col">
        <nav className="px-1 bg-gray-100 w-full flex relative justify-between items-center mx-auto md:px-8 h-12">
          <div className="md:inline-flex">
            <Link href="/">
              <a>
                <div className="block">
                  <img className="w-10 md:w-10 mx-auto" src="/images/logo.png" alt={`Logo de ${NAME}`} />
                </div>
              </a>
            </Link>
            <Link href="/">
              <a className="hidden md:inline-block pl-0 md:pl-8 lg:pl-0 text-2xl md:text-3xl font-semibold text-gray-700 upper pt-1">{NAME}</a>
            </Link>
          </div>

          <div className="w-2/3 md:w-1/2 sm:block flex-shrink flex-grow-0 justify-start px-0 md:px-2">
            {showVideoShearch ? <SearchVideo /> : <SearchPost />}
          </div>

          <div className="flex-initial ml-3">
            <div className="flex justify-end items-center relative">
              <div className="flex mr-1 md:mr-4 items-center">
                <Link href="/videos">
                  <a className="inline-block py-2 px-1 md:px-3 hover:bg-gray-200 rounded" title="Vidéos yitane">
                    <div className="flex items-center relative cursor-pointer whitespace-nowrap">
                      <svg className="w-7 h-7" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M15 10l4.553-2.276A1 1 0 0121 8.618v6.764a1 1 0 01-1.447.894L15 14M5 18h8a2 2 0 002-2V8a2 2 0 00-2-2H5a2 2 0 00-2 2v8a2 2 0 002 2z" />
                      </svg>
                      <span className="hidden md:block">&nbsp;Vidéos</span>
                    </div>
                  </a>
                </Link>
              </div>

              <div className="block">
                <div className="inline relative w-10">
                  <button name="menuBtn" onClick={toggleSideNav} type="button" className="inline-flex items-center relative px-2 py-1 rounded hover:shadow-lg focus:outline-none bg-white">
                    <div className="pl-1">
                      <svg xmlns="http://www.w3.org/2000/svg" className="icon icon-tabler icon-tabler-align-right h-8" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round">
                        <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                        <line x1="4" y1="6" x2="20" y2="6" />
                        <line x1="10" y1="12" x2="20" y2="12" />
                        <line x1="6" y1="18" x2="20" y2="18" />
                      </svg>
                    </div>
                    <div className="block flex-grow-0 flex-shrink-0 h-7 w-7 md:h-9 md:w-9 pl-1">
                      {
                        currentUser.is_authenticated
                          ? <img src={currentUser.image} alt={currentUser.name} className="w-full h-full rounded" />
                          : <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" aria-hidden="true" role="presentation" focusable="false" style={{ display: 'block', height: '100%', width: '100%', fill: 'currentcolor' }}>
                            <path d="m16 .7c-8.437 0-15.3 6.863-15.3 15.3s6.863 15.3 15.3 15.3 15.3-6.863 15.3-15.3-6.863-15.3-15.3-15.3zm0 28c-4.021 0-7.605-1.884-9.933-4.81a12.425 12.425 0 0 1 6.451-4.4 6.507 6.507 0 0 1 -3.018-5.49c0-3.584 2.916-6.5 6.5-6.5s6.5 2.916 6.5 6.5a6.513 6.513 0 0 1 -3.019 5.491 12.42 12.42 0 0 1 6.452 4.4c-2.328 2.925-5.912 4.809-9.933 4.809z">
                            </path>
                          </svg>
                      }
                    </div>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </nav>
      </div>
      {sideNavOpen && <><SideNav /><div className="z-10 fixed inset-0 transition-opacity"><div onClick={toggleSideNav} tabIndex="0" className="absolute inset-0 bg-black opacity-50"></div></div></>}
    </>
  );
}
