import Link from "next/link"
import React from "react"
import { useRouter } from "next/router"
import { relative_time } from "../helpers/functions"
import { NAME } from "../helpers/global"
import UserStore from "../store/UserStore"

const Profil = React.memo(({ member, current_path }) => {
    const  user  = UserStore.state.currentUser;
    const router = useRouter()
    const asPath = router.asPath
    return (
        <>
            <div className="bg-grey-lighter">
                <div className="bg-white overflow-hidden">
                    <div className="bg-cover h-40" style={{ backgroundImage: `url('https://picsum.photos/700/200')` }}></div>
                    <div className=" px-4 pb-1 w-full">
                        <div className="flex w-full sm:text-left sm:flex mb-4">
                            <img className="h-24 w-24 md:h-32 md:w-32 rounded border-2 border-white -mt-16 mr-2 object-cover object-left" src={member.image} alt={member.name} />
                            <div className="mt-3 w-full">
                                <h1 className="font-bold text-xl mb-1">{member.name}</h1>
                                <p className="text-gray-700 pt-2"><i className="fas fa-map-marker-alt"></i> {member.country != undefined && member.country.name}</p>
                                <p className="text-gray-700 pt-2"><i className="far fa-calendar-alt"></i> a réjoint {NAME} {relative_time(member.created_at)}</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <nav className="flex flex-col sm:flex-row border-b place-content-around font-semibold ">
                <Link href={`/compte/profil?uid=${member.uid}`}>
                    <a className={`tab ${current_path == "/compte/profil" ? "tab_selected" : ""} `}>Activités</a>
                </Link>
                <Link href={`/compte/magazines?uid=${member.uid}`}>
                    <a className={`tab ${current_path == "/compte/magazines" ? "tab_selected" : ""} `}>Magasines</a>
                </Link>
                <Link href={`/compte/channels?uid=${member.uid}`}>
                    <a className={`tab ${current_path == "/compte/channels" ? "tab_selected" : ""} `}>Chaines</a>
                </Link>
                {user.uid == member.uid &&
                    <Link href={`/compte/sauvegarde?type=post`}>
                        <a className={`tab ${asPath == "/compte/sauvegarde?type=post" ? "tab_selected" : ""} `}>Lire plus tard</a>
                    </Link>
                }
                {user.uid == member.uid &&
                    <Link href={`/compte/sauvegarde?type=video`}>
                        <a className={`tab ${asPath == "/compte/sauvegarde?type=video" ? "tab_selected" : ""} `}>Regarder plus tard</a>
                    </Link>
                }
            </nav>
        </>
    )
})

export default Profil