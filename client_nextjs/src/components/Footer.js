import Link from "next/link"
import { NAME } from "../helpers/global";

export default function Footer() {
  return (
    <footer className="flex justify-center px-4 text-gray-700 bg-gray-100 mt-3">
      <div className="container pt-10 pb-6">
        <h2 className="text-center text-lg font-bold lg:text-2xl">
          Souscrire à notre newsletter
        <br />
        et soyer les premiers informés.
      </h2>

        <div className="flex justify-center mt-6">
          <div className="bg-white rounded-lg">
            <div className="flex flex-wrap justify-between md:flex-row">
              <input type="email" id="email" className="m-1 p-2 w-72 appearance-none text-gray-700 text-sm focus:outline-none" placeholder="Entrer votre email" />
              <button name="send" className="w-full text-white m-1 p-2 text-sm bg-teal-600 rounded-lg font-semibold uppercase lg:w-auto">
                souscrire
            </button>
            </div>
          </div>
        </div>

        <hr className="h-px mt-6 bg-white border-none" />

        <div className="flex flex-col items-center justify-between mt-6 md:flex-row">
          <div>
            <a href="{{url_for('front.homepage')}}" className="text-2xl font-bold">
              {NAME}
          </a>
          </div>
          <div className="flex mt-4 md:m-0 ">
            <div className="-mx-4 font-semibold flex flex-wrap items-center">
              <Link href="/apropos">
                <a className="px-4">A propos</a>
              </Link>
              <Link href="/confidentialite">
                <a className="px-4">Confidentialité</a>
              </Link>
              <Link href="/conditions">
                <a className="px-4">Conditions</a>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
}