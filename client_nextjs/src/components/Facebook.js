import { useEffect, useState } from "react"
import { notification, storeItem } from "../helpers/functions";
import { API_ROUTE,FACEBOOK_ID } from "../helpers/global"
import UserStore from "../store/UserStore";


const Facebook = () => {
    const { user, setUser } = useState(UserStore.state.currentUser);

    useEffect(() => {
        loadSdkAsynchronously()
        setFbAsyncInit()
    }, [])

    const setFbAsyncInit = () => {
        window.fbAsyncInit = function () {
            window.FB.init({
                appId: FACEBOOK_ID,
                cookie: true,
                //xfbml: true,
                version: 'v5.0'
            });
            window.FB.getLoginStatus(function (response) {
                if (response.status === 'connected') {
                    window.FB.logout(function () { });
                }
            });
        };
    }

    const loadSdkAsynchronously = () => {
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    }

    const statusChangeCallback = (response) => {
        if (response.status === 'connected')
            responseFacebook(response);
        else {
            window.FB.login(function (response) {
                if (response.status === 'connected')
                    responseFacebook(response);
            }, { scope: 'public_profile,email' });
        }
    }

    const componentClicked = (event) => {
        event.preventDefault();
        window.FB.getLoginStatus(response => statusChangeCallback(response))
    }

    const postData = async (fb_data) => {
        const form = new FormData()
        form.append('name', fb_data.name)
        form.append('email', fb_data.email)
        form.append('id', fb_data.id)
        const response = await fetch(API_ROUTE + "auth/login_facebook", { method: 'POST', body: form })
        const data = await response.json()
        if (data.status) {
            setUser(data.user);
            UserStore.setCurrentUser(data.user);
            storeItem('token',data.token);
            window.location.href = "/"
        }
        else { notification("Erreur", "Erreur connexion Facebook", 'danger') }
    }

    const responseFacebook = async (fb_response) => {
        FB.api('/me?fields=email,name,picture,id', function (response) {
            postData(response)
        });
    }
    return (
        <button type="button" className="btn bg-blue-700 text-white w-full hover:bg-blue-800" onClick={componentClicked}>
            <i className="fab fa-facebook-square"/> Facebook
        </button>
    )

}
export default Facebook;