import { useEffect, useState } from "react"
import { FacebookShareButton, TwitterShareButton, TwitterIcon, FacebookIcon, WhatsappShareButton, WhatsappIcon, FacebookMessengerShareButton, FacebookMessengerIcon } from "react-share";

const Share = ({ post }) => {


    const [dropdown, setDropdown] = useState(false)
    const [path, setPath] = useState()

    useEffect(() => {
        const { protocol, host } = window.location
        setPath(protocol + "//" + host + post.url)
    }, [])
    return (
        <>
            <div className="relative">
                <button className="post-btn focus:outline-none" onClick={() => setDropdown(!dropdown)}>
                    <i className="fas fa-share-alt p-2 rounded-full hover:bg-teal-200 hover:text-black" />
                </button>
                {dropdown &&
                    <div className="absolute z-10 top-0 right-0 w-56 rounded-lg shadow-2xl bg-gray-100" style={{top:"-67px"}}>
                        <p className="text-center font-semibold border-b p-2 text-lg uppercase bg-gray-200 rounded-t-lg">Partager</p>
                        <div className="flex p-1 items-center place-content-around">
                            <div className="p-1">
                                <FacebookShareButton url={`${path}`} className="focus:outline-none">
                                    <FacebookIcon size={40} round={true} />
                                </FacebookShareButton>
                            </div>
                            <div className="p-1">
                                <TwitterShareButton url={`${path}`} className="focus:outline-none">
                                    <TwitterIcon size={40} round={true} />
                                </TwitterShareButton>
                            </div>
                            <div className="p-1">
                                <WhatsappShareButton url={`${path}`} className="focus:outline-none">
                                    <WhatsappIcon size={40} round={true} />
                                </WhatsappShareButton>
                            </div>
                            <div className="p-1">
                                <FacebookMessengerShareButton url={`${path}`} className="focus:outline-none">
                                    <FacebookMessengerIcon size={40} round={true} />
                                </FacebookMessengerShareButton>
                            </div>
                        </div>
                    </div>
                }
                {dropdown && <div className="fixed top-0 bottom-0 left-0 right-0 bg-transparent" onClick={() => setDropdown(false)}></div>}
            </div>
        </>
    )
}

export default Share