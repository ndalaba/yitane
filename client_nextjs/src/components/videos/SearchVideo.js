import Link from "next/link";
import { useRouter } from "next/router";
import { useRef, useState } from "react";
import { wrapText } from "../../helpers/functions";
import VideoStore from "../../store/VideoStore";

function SearchVideo() {

    const router = useRouter()

    const [videos, setVideos] = useState([])
    const default_q = router.query.q != undefined ? router.query.q : ""
    const [q, setQ] = useState(default_q)

    const elementRef = useRef()
    const inputRef = useRef()

    const search = async (event) => {
        setQ(event.target.value)
        const data = await VideoStore.searchVideos(event.target.value) ;
        setVideos(data.videos);
    }
    const resetSearch = () => {
        setQ('');
        setVideos([])
    }

    const handleSubmit = (event) => {
        event.preventDefault()
        router.push('/videos/recherche?q=' + q)
    }

    return (
        <>
            <div className="relative w-full rounded-md">
                <form onSubmit={handleSubmit} className="w-full rounded-md">
                    <input className="pl-8 flex-grow flex-shrink overflow-hidden bg-white w-full ml-2 rounded-md font-medium  p-1 focus:outline-none search_input"
                        placeholder={`Recherche vidéos`}
                        ref={inputRef}
                        onFocus={() => elementRef.current.style.display = "block"}
                        onBlur={() => setTimeout(() => elementRef.current != null ? elementRef.current.style.display = "none" : "", 500)}
                        value={q}
                        onChange={search}
                    />
                    <i className="fas fa-search absolute left-0 top-0 mt-2 ml-3 text-sm text-gray-700" />
                    {(q.length > 0) && <i className={`fas fa-times-circle absolute top-0 mt-3 ml-5 text-sm text-teal-900 cursor-pointer right-0`}
                        onClick={resetSearch}
                    />}
                </form>
                <div className="rounded-md shadow-lg py-2 hidden fixed bg-white overflow-y-auto w-2/3 md:w-1/2 z-10" ref={elementRef}>
                    {!videos.length && <span className="inline-block p-2 text-gray-700">Rechercher <span className="font-semibold">{q}</span></span>}
                    {videos.map(a =>
                        <Link key={a.id} href={{ pathname: '/videos/[chan_slug]/[video_slug]/[uid]', query: { uid: a.uid, id: a.id, title: a.title, image: a.image, url: a.url, channel: a.channel.slug, summary: a.summary, body: a.body } }} as={a.url}>
                            <a className="inline-block w-full px-3 py-3 border-b border-gray-200 text-sm hover:bg-gray-100 text-black" dangerouslySetInnerHTML={{ __html: wrapText(q, a.title) }} />
                        </Link>
                    )}
                </div>
            </div>
        </>

    )
}

export default SearchVideo;