import Link from "next/link"

const Video = ({video}) => {
    const {uid, slug, title, url, image, channel} = video
    const chan_slug = video.channel.slug
    const video_slug = slug
    return (

        <div className="rounded w-full md:w-1/3 p-4 lg:p-2">
            <div className="relative table w-full rounded-lg h-40 md:h-64 mb-3 mt-3" style={{
                backgroundPositionY: "50%",
                backgroundImage: `url(${image})`
            }}>
                <Link href={{
                    pathname: '/videos/[chan_slug]/[post_slug]/[uid]',
                    query: {uid, video_slug, chan_slug, channel, title, image}
                }} as={url}>
                    <a title={title} className="table-cell h-full w-full text-center align-middle">
                        <span className="inline-block border-1 border-white h-12 w-14 py-3 pb-2 pl-5 pr-4 bg-white rounded" style={{backgroundColor: "rgb(31 41 55 / 82%)"}}>
                            <span style={{
                                display: "inline-block",
                                width: 0,
                                height: 0,
                                borderTop: "10px solid transparent",
                                borderBottom: "10px solid transparent",
                                borderLeft: "10px solid #fff"
                            }}/>
                        </span>
                    </a>
                </Link>
            </div>
            <div className="p-0 pl-0">
                <h2 className="font-semibold text-xl text-gray-800">
                    <Link href={{
                        pathname: '/videos/[chan_slug]/[video_slug]/[uid]',
                        query: {uid, video_slug, chan_slug, channel, title, image}
                    }} as={url}>
                        <a title={title} className="table-cell h-full w-full align-middle" dangerouslySetInnerHTML={{__html: title}}/>
                    </Link>
                </h2>
            </div>
        </div>

    );
}

export default Video

