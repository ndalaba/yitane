import Link from "next/link"
import { useRef } from "react"
import { FollowingChannel } from "../Following"

const Channel = ({ chan, show }) => {
    const elementRef = useRef()
    return (
        <>
            <div className="hover:bg-gray-100">
                <div className="w-full flex p-3 border-t border-gray-100 focus:outline-none" ref={elementRef}>
                    <Link href="/videos/[chan_slug]" as={`/videos/${chan.slug}`}>
                        <a className="flex w-full" title={chan.title}>
                            <img src={chan.image} alt={chan.title} className="w-12 h-12 rounded-full border border-teal-200 object-cover object-left" />
                            <div className="ml-4">
                                <p className="text-sm font-bold leading-tight mb-2"> {chan.title} </p>
                                <p className="text-sm leading-tight"> @{chan.category.name} </p>
                            </div>
                        </a>
                    </Link>
                    <FollowingChannel channel={chan} />                   
                </div>
                {show && <p className="text-sm px-4 py-3">{chan.description}</p>}
            </div>
        </>
    );
}

export default Channel;