import Link from "next/link"
import { relative_time } from "../../helpers/functions"

export default function VideoTop({ video }) {
    const time = relative_time(video.date_published);
    return (
        <div className="flex items-center w-full mb-2">
            <div className="md:block flex-none mr-4">
                <img cache debounce={1} placeholder="../../no_image.jpg" src={video.channel.image} alt={video.channel.title} className="h-10 w-10 rounded-full flex-none object-cover object-left" />
            </div>
            <div className="flex flex-col w-full">
                <div className="flex content-between">
                    <div>
                        <span className="font-semibold">
                            <Link href="/videos/[chan_slug]" as={`/videos/${video.channel.slug}`}><a title={video.channel.title}>{video.channel.title}</a></Link>
                        </span>
                        <span className="text-sm text-dark ml-1"><span className="inline-block">·</span> {time} </span>
                    </div>
                </div>
                {video.channel.category &&
                    <Link href="/videos/topic/[chan_slug]" as={`/videos/topic/${video.channel.category.slug}`}>
                        <a className="text-sm text-teal-800 font-semibold" title={video.channel.category.name}>
                            @{video.channel.category.name}
                        </a>
                    </Link>
                }
            </div>
        </div>

    )
}