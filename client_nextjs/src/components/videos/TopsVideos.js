import Link from "next/link"

const TopsVideos = ({ video }) => {
    const { uid, slug, title, url, image, body, channel } = video
    const chan_slug = video.channel.slug
    const video_slug = slug
    return (
        <div className="rounded w-full flex flex-col md:flex-row mb-10">
            <img src={video.image} alt={video.title} className="block md:hidden lg:block rounded-md h-64 md:h-32 m-4 md:m-0" />
            <div className="bg-white rounded px-4">
                <span className="text-teal-800 text-md hidden md:block">
                    {video.channel.category &&
                        <Link href="/videos/topic/[chan_slug]" as={`/videos/topic/${video.channel.category.slug}`}>
                            <a title={video.channel.category.name}>
                                @{video.channel.category.name}
                            </a>
                        </Link>
                    }
                </span>
                <h2 className="md:mt-0 text-gray-800 font-semibold text-xl mb-2">
                    <Link href={{ pathname: '/videos/[chan_slug]/[post_slug]/[uid]', query: { uid, video_slug, chan_slug, channel, title, image } }} as={url}>
                        <a dangerouslySetInnerHTML={{ __html: title }} title={title} />
                    </Link>
                </h2>
                <span className="text-teal-900 text-md hidden md:block">
                    <Link href="/videos/[chan_slug]" as={`/videos/${video.channel.slug}`}><a title={video.channel.title}>@{video.channel.title}</a></Link>
                </span>
            </div>
        </div>
    );
}

export default TopsVideos;

