import Link from "next/link"
import Loading from "../Loading"
import Video from "./Video"

const { useState, useEffect } = require("react")
const { API_ROUTE } = require("../../helpers/global")

const VideoTrend = ({ size }) => {

    const [videos, setVideos] = useState([])

    useEffect(() => {
        fetch_trend_video()
    }, [])

    const fetch_trend_video = async () => {
        const response = await fetch(API_ROUTE + "trend_videos?size=" + size)
        const data = await response.json()
        setVideos(data.videos)
    }

    return (
        <>
            <div className="w-full rounded-lg bg-gray-100" id="trend_video_div">
                <div className="flex items-center justify-between p-3">
                    <p className="text-2xl text-center text-teal-800 font-bold w-full">Tendances</p>
                </div>
                {!videos.length && <Loading />}
                <div className="flex flex-col w-full items-center">
                    {videos.map((video, index) =>
                        <Video video={video} key={video.uid} />
                    )}
                </div>
            </div>

        </>
    );
}
export default VideoTrend