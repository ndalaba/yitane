import Link from "next/link";
import {VideoLove} from "../Love"
import {SaveVideo} from "../Save"
import Share from "../Share"

export default function VideoBtn({ video }) {
    return (
        <div className="flex items-center text-gray-800 justify-around w-full mt-3">
            <Link href={{ pathname: '/videos/[chan_slug]/[video_slug]/comment', query: { uid: video.uid } }} as={`/videos/${video.channel.slug}/${video.slug}/comment?uid=${video.uid}`}>
                <a className="post-btn flex">
                    <i className="far fa-comment p-2 rounded-full hover:bg-teal-200 hover:text-black" />
                    <p> {video.comments_count} </p>
                </a>
            </Link>
            <VideoLove video={video} />
            <SaveVideo video={video} />
            <Share post={video} />
        </div>
    )
}