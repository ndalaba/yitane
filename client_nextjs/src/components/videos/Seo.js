import {FACEBOOK_ID, NAME} from "../../helpers/global"

export default function Seo({video}) {
    return (
        <>
            <meta property="og:locale" content="fr"/>
            <meta property="og:site_name" content={NAME}/>
            <meta property="og:title" content={video.title}/>
            <meta property="og:url" content={video.url}/>
            <meta property="og:type" content="video"/>
            <meta property="og:description" content={video.body}/>
            <meta property="og:image" content={video.image}/>
            <meta property="og:image:url" content={video.image}/>
            <meta property="og:image:secure_url" content={video.image}/>
            <meta property="video:published_time" content={video.date_published}/>
            <meta property="video:modified_time" content={video.created_at}/>
            <meta property="og:updated_time" content={video.created_at}/>
            <meta property="video:section" content="WEB"/>
            <meta property="fb:app_id" content={FACEBOOK_ID}/>
            <meta property="fb:pages" content=""/>
            <meta property="video:publisher" content=""/>
            <meta property="video:author" content=""/>

            <meta itemprop="name" content={video.title}/>
            <meta itemprop="headline" content={video.title}/>
            <meta itemprop="description" content={video.body}/>
            <meta itemprop="image" content={video.image}/>
            <meta itemprop="datePublished" content={video.date_published}/>
            <meta itemprop="dateModified" content={video.created_at}/>
            <meta itemprop="author" content="ndalaba"/>

            <meta name="twitter:title" content={video.title}/>
            <meta name="twitter:url" content={video.url}/>
            <meta name="twitter:description" content={video.body}/>
            <meta name="twitter:image" content={video.image}/>
            <meta name="twitter:card" content="summary_large_image"/>

        </>
    )
}