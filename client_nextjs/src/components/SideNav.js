import UserStore from "../store/UserStore";
import CategoryStore from "../store/CategoryStore";
import { useState, useEffect } from "react";
import Link from "next/link";
import { doLogout } from "../helpers/functions";

export default function SideNav() {

  const user = UserStore.state.currentUser;
  const [categories, setCategories] = useState(CategoryStore.state.categories);

  useEffect(() => {
    if (!categories.length) {
      getCategories();
    }

  }, []);

  const getCategories = async () => {
    const data = await CategoryStore.getCategories();
    setCategories(data);
  }

  return (
    <div className="flex flex-col flex-auto flex-shrink-0 antialiased bg-gray-50 text-teal-800">
      <div className="fixed flex flex-col top-0 left-0 w-80 bg-white h-full border-r z-20">
        <div className="flex items-center justify-center w-full">
          {user.is_authenticated &&
            <div className="flex items-center justify-center border-b w-full">
              <div className="flex flex-col items-center bg-gray-100  w-full py-6 px-4 ">
                <div className="h-20 w-20 rounded-full border overflow-hidden">
                  <img src={user.image} alt={user.name} className="h-full w-full" />
                </div>
                <div className="text-sm font-semibold mt-2">{user.name}</div>
                <div className="text-xs text-teal-500">{user.email}</div>
                <div className="flex flex-row items-center mt-3">
                  <div className="flex flex-col justify-center h-4 w-8 bg-teal-500 rounded-full">
                    <div className="h-3 w-3 bg-white rounded-full self-end mr-1"></div>
                  </div>
                  <div className="leading-none ml-1 text-xs">Actif</div>
                </div>
              </div>
            </div>
          }
        </div>

        <div className="overflow-y-auto overflow-x-hidden flex-grow">
          <ul className="flex flex-col py-4 space-y-1">
            <li className="px-5">
              <div className="flex flex-row items-center h-8">
                <div className="text-lg font-semibold tracking-wide text-teal-900">
                  Menu
              </div>
              </div>
            </li>
            <li>
              <Link href="/explorer">
                <a className="relative flex flex-row items-center h-11 focus:outline-none hover:bg-gray-50 text-teal-800 hover:text-teal-900 border-l-4 border-transparent hover:border-teal-500 pr-6">
                  <span className="inline-flex justify-center items-center ml-4">
                    <svg xmlns="http://www.w3.org/2000/svg" className="icon icon-tabler icon-tabler-news h-5 w-5" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round">
                      <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                      <path d="M16 6h3a1 1 0 0 1 1 1v11a2 2 0 0 1 -4 0v-13a1 1 0 0 0 -1 -1h-10a1 1 0 0 0 -1 1v12a3 3 0 0 0 3 3h11" />
                      <line x1="8" y1="8" x2="12" y2="8" />
                      <line x1="8" y1="12" x2="12" y2="12" />
                      <line x1="8" y1="16" x2="12" y2="16" />
                    </svg>
                  </span>
                  <span className="ml-2 text-lg tracking-wide truncate">Magasines</span>
                </a>
              </Link>
            </li>
            <li>
              <Link href="/videos/explorer">
                <a className="relative flex flex-row items-center h-11 focus:outline-none hover:bg-gray-50 text-teal-800 hover:text-teal-900 border-l-4 border-transparent hover:border-teal-500 pr-6">
                  <span className="inline-flex justify-center items-center ml-4">
                    <svg xmlns="http://www.w3.org/2000/svg" className="icon icon-tabler icon-tabler-device-tv h-5 w-5" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round">
                      <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                      <rect x="3" y="7" width="18" height="13" rx="2" />
                      <polyline points="16 3 12 7 8 3" />
                    </svg>
                  </span>
                  <span className="ml-2 text-lg tracking-wide truncate">Chaines</span>
                </a>
              </Link>
            </li>

          </ul>
          <ul className="flex flex-col py-4 space-y-1 overflow-y-scroll h-60">
            <li className="px-5">
              <div className="flex flex-row items-center h-8">
                <div className="text-lg font-bold tracking-wide text-teal-900">
                  Thèmes
              </div>
              </div>
            </li>

            {categories.map(category =>
              <li key={category.slug}>
                <Link href="/topic/[cat_slug]" as={`/topic/${category.slug}`}>
                  <a title={category.name} className="relative flex flex-row items-center h-11 focus:outline-none hover:bg-gray-50 text-teal-800 hover:text-teal-900 border-l-4 border-transparent hover:border-teal-500 pr-6">
                    <span className="inline-flex justify-center items-center ml-4">
                      <svg className="w-5 h-5" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M9 5H7a2 2 0 00-2 2v12a2 2 0 002 2h10a2 2 0 002-2V7a2 2 0 00-2-2h-2M9 5a2 2 0 002 2h2a2 2 0 002-2M9 5a2 2 0 012-2h2a2 2 0 012 2m-3 7h3m-3 4h3m-6-4h.01M9 16h.01"></path>
                      </svg>
                    </span>
                    <span className="ml-2 text-lg tracking-wide truncate">
                      {category.name}
                    </span>
                  </a>
                </Link>

              </li>)}
          </ul>

          <ul className="flex flex-col py-4 space-y-1">
            <li className="px-5">
              <div className="flex flex-row items-center h-8">
                <div className="text-lg font-bold tracking-wide text-teal-900">
                  Settings
              </div>
              </div>
            </li>

            {user.is_authenticated &&
              <>
                <li>
                  <Link href={`/compte/profil?uid=${user.uid}`}>
                    <a className="relative flex flex-row items-center h-11 focus:outline-none hover:bg-gray-50 text-teal-800 hover:text-teal-900 border-l-4 border-transparent hover:border-teal-500 pr-6">
                      <span className="inline-flex justify-center items-center ml-4">
                        <svg className="w-5 h-5" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                          <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z"></path>
                        </svg>
                      </span>
                      <span className="ml-2 text-lg tracking-wide truncate">Profil</span>
                    </a>
                  </Link>
                </li>
                <li>
                  <Link href="/compte/setting">
                    <a className="relative flex flex-row items-center h-11 focus:outline-none hover:bg-gray-50 text-teal-800 hover:text-teal-900 border-l-4 border-transparent hover:border-teal-500 pr-6">
                      <span className="inline-flex justify-center items-center ml-4">
                        <svg className="w-5 h-5" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                          <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 002.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 001.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 00-1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 00-2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 00-2.573-1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 00-1.065-2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 001.066-2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z"></path>
                          <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"></path>
                        </svg>
                      </span>
                      <span className="ml-2 text-lg tracking-wide truncate">Compte</span>
                    </a>
                  </Link>
                </li>
                <li>
                  <a href="#" onClick={doLogout} className="relative flex flex-row items-center h-11 focus:outline-none hover:bg-gray-50 text-teal-800 hover:text-teal-900 border-l-4 border-transparent hover:border-teal-500 pr-6">
                    <span className="inline-flex justify-center items-center ml-4">
                      <svg className="w-5 h-5" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M17 16l4-4m0 0l-4-4m4 4H7m6 4v1a3 3 0 01-3 3H6a3 3 0 01-3-3V7a3 3 0 013-3h4a3 3 0 013 3v1"></path>
                      </svg>
                    </span>
                    <span className="ml-2 text-lg tracking-wide truncate">Se deconnecter</span>
                  </a>
                </li>
              </>
            }
            {!user.is_authenticated &&
              <li>
                <Link href="/auth/login">
                  <a className="relative flex flex-row items-center h-11 focus:outline-none hover:bg-gray-50 text-teal-800 hover:text-teal-900 border-l-4 border-transparent hover:border-teal-500 pr-6">
                    <span className="inline-flex justify-center items-center ml-4">
                      <svg xmlns="http://www.w3.org/2000/svg" className="icon icon-tabler icon-tabler-login w-5 h-5" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round">
                        <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                        <path d="M14 8v-2a2 2 0 0 0 -2 -2h-7a2 2 0 0 0 -2 2v12a2 2 0 0 0 2 2h7a2 2 0 0 0 2 -2v-2" />
                        <path d="M20 12h-13l3 -3m0 6l-3 -3" />
                      </svg>
                    </span>
                    <span className="ml-2 text-lg tracking-wide truncate">Se connecter</span>
                  </a>
                </Link>
              </li>
            }

          </ul>
        </div>
      </div>
    </div>
  );
}