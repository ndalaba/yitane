import Link from "next/link";
import {PostLove} from "../Love";
import {SavePost} from "../Save";
import Share from "../Share";

export default function PostBtn({ post }) {
    return (
        <div className="flex items-center text-gray-800 justify-around w-full my-1">
            <Link href={{ pathname: '/[mag_slug]/[post_slug]/comment', query: { uid: post.uid } }} as={`/${post.magazine.slug}/${post.slug}/comment?uid=${post.uid}`}>
                <a className="flex">
                    <i className="far fa-comment p-2 rounded-full hover:bg-teal-200 hover:text-black" />
                    <p> {post.comments_count} </p>
                </a>
            </Link>
            <PostLove post={post} />
            <SavePost post={post} />
            <Share post={post} />
        </div>
    )
}