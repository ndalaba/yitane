import Link from "next/link"
import { useEffect, useRef } from "react";
import AdBanner from "../AdBanner";

export default function Top({post}){
   const { uid, slug, title, url, image, summary, magazine, body } = post
   const mag_slug = post.magazine.slug
   //const post_slug = slug
   const imgRef = useRef();
  
   useEffect(() => {
       if (image !== null)
           imgRef.current.setAttribute('src', image.replace('http:','https:'))
   }, []);

   const onImageError=(e)=>{
      e.target.src="/images/no_image.png";
   }

   return(
         <div className="mb-4 lg:mb-0 p-4 lg:p-0 w-full md:w-4/7 relative rounded block">
            {
               image !== null &&
               <Link href={{ pathname: '/[mag_slug]/[post_slug]/[uid]', query: { uid, slug, mag_slug, magazine, title, image, body,summary } }} as={url}>
                  <a title={title}>
                     <img onError={onImageError} ref={imgRef} debounce={1} className="rounded object-cover w-full h-64" placeholder="no_image.jpg" src={`no_image.jpg`} alt={title} title={title} />
                  </a>
               </Link>
            }
         {/*{image == null && <AdBanner />}*/}
            
            <h1 className="text-gray-800 text-4xl font-bold mt-2 mb-2 leading-tight">
               <Link href={{ pathname: '/[mag_slug]/[post_slug]/[uid]', query: { uid, slug, mag_slug, magazine, title, image, body ,summary} }} as={url}>
                  <a title={title}>{title}</a>
               </Link>
            </h1>
            <p className="hidden md:block text-gray-600 mb-4" dangerouslySetInnerHTML={{ __html: summary }}/>
         </div>
   );
}