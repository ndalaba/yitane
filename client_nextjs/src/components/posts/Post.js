import Link from "next/link"
import { useEffect, useRef } from "react";
import AdBanner from "../AdBanner";
import Loading from "../Loading";

const Post = ({ post }) => {
    if (post != undefined) {
        const { uid, slug, title, url, image, magazine, body, summary } = post
        const mag_slug = post.magazine.slug
        const post_slug = slug
        const imgRef = useRef();
        useEffect(() => {
            if (image !== null)
                imgRef.current.setAttribute('src', image.replace('http:', 'https:'))
        }, [])

        const onImageError=(e)=>{
            e.target.src="/images/no_image.png";
         }

        return (
            <div className="rounded w-full md:w-1/2 lg:w-1/4 p-4 lg:p-0">
                {
                    image !== null && <div className="h-32 overflow-hidden rounded"><img onError={onImageError} ref={imgRef} debounce={1} className="w-full rounded" placeholder="no_image.jpg" src={`no_image.jpg`} alt={title} title={title} /></div>
                }
                {image == null && <AdBanner />}
                <div className="p-4 pl-0 pr-0">
                    <h2 className="font-semibold text-gray-800">
                        <Link href={{ pathname: '/[mag_slug]/[post_slug]/[uid]', query: { uid, post_slug, mag_slug, magazine, title, image, body,summary } }} as={url}>
                            <a title={title}>{title}</a>
                        </Link>
                    </h2>
                </div>
            </div>
        );
    }
    else return <Loading />
}

export default Post

