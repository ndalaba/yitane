import Link from "next/link";
import { useEffect, useState } from "react";
import CategoryStore from "../../store/CategoryStore";
import Post from "./Post";

export default function CategoryBloc({ category, posts }) {
   const [_posts,setPosts]= useState([]);
   
   useEffect(() => {
      const $posts = CategoryStore.getCategoryPostsFromPosts(category, posts);
      setPosts($posts);
   }, []);
   
   if(_posts.length){
      return (
         <>
            <div className="flex mt-16 mb-4 px-4 lg:px-0 items-center justify-between">
               <h2 className="font-semibold text-3xl">{category.name}</h2>
               <Link href="/topic/[cat_slug]" as={`/topic/${category.slug}`}>
                  <a className="bg-teal-500 hover:bg-teal-600 rounded text-white px-3 py-1 cursor-pointer">Tout afficher</a>
               </Link>
            </div>
            <div className="block space-x-0 lg:flex lg:space-x-2">
               {_posts.map(post => <Post post={post} key={post.id} />)}
            </div>
         </>
      );
   }
   else{
      return "";
   }

   
}