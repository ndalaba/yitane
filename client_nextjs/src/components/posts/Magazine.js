import Link from "next/link"
import { useRef } from "react"
import { FollowingMagazine } from "../Following"

const Magazine = ({ mag, show }) => {
    const elementRef = useRef()
    return (
        <>
            <div className="hover:bg-gray-100">
                <div className="w-full flex p-3 border-t border-gray-100 focus:outline-none" ref={elementRef}>
                    <Link href="/[mag_slug]" as={`/${mag.slug}`}>
                        <a className="flex w-full" title={mag.title}>
                            <img src={mag.image} alt={mag.title} className="w-12 h-12 rounded-full border border-teal-200 object-cover object-left" />
                            <div className="ml-4">
                                <p className="text-sm font-bold leading-tight mb-2"> {mag.title} </p>
                                <p className="text-sm leading-tight"> @{mag.category.name} </p>
                            </div>
                        </a>
                    </Link>
                    <FollowingMagazine magazine={mag} />                   
                </div>
                {show && <p className="text-sm px-4 py-3">{mag.description}</p>}
            </div>
        </>
    );
}

export default Magazine;