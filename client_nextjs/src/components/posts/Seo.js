import {FACEBOOK_ID, NAME} from "../../helpers/global"

export default function Seo({article}) {
    return (
        <>
            <meta property="og:locale" content="fr"/>
            <meta property="og:site_name" content={NAME}/>
            <meta property="og:title" content={article.title}/>
            <meta property="og:url" content={article.url}/>
            <meta property="og:type" content="article"/>
            <meta property="og:description" content={article.summary}/>
            <meta property="og:image" content={article.image}/>
            <meta property="og:image:url" content={article.image}/>
            <meta property="og:image:secure_url" content={article.image}/>
            <meta property="article:published_time" content={article.date_published}/>
            <meta property="article:modified_time" content={article.created_at}/>
            <meta property="og:updated_time" content={article.created_at}/>
            <meta property="article:section" content="WEB"/>
            <meta property="fb:app_id" content={FACEBOOK_ID}/>
            <meta property="fb:pages" content=""/>
            <meta property="article:publisher" content=""/>
            <meta property="article:author" content=""/>

            <meta itemProp="name" content={article.title}/>
            <meta itemProp="headline" content={article.title}/>
            <meta itemProp="description" content={article.summary}/>
            <meta itemProp="image" content={article.image}/>
            <meta itemProp="datePublished" content={article.date_published}/>
            <meta itemProp="dateModified" content={article.created_at}/>
            <meta itemProp="author" content="ndalaba"/>

            <meta name="twitter:title" content={article.title}/>
            <meta name="twitter:url" content={article.url}/>
            <meta name="twitter:description" content={article.summary}/>
            <meta name="twitter:image" content={article.image}/>
            <meta name="twitter:card" content="summary_large_image"/>

        </>
    )
}