import Loading from "../Loading"
import Post from "./Trend"

const { useState, useEffect } = require("react")
const { API_ROUTE } = require("../../helpers/global")

const PostTrend = ({ size }) => {

    const [articles, setArticles] = useState([])

    useEffect(() => {
        fetch_trend_article()
    }, [])

    const fetch_trend_article = async () => {
        const response = await fetch(API_ROUTE + "trend_articles?size=" + size)
        const data = await response.json()
        setArticles(data.articles)
    }

    return (
        <>
            <div className="w-full rounded-lg bg-gray-100" id="trend_article_div">
                <div className="flex items-center justify-between p-3">
                    <p className="text-2xl text-center text-teal-800 font-bold w-full">Tendances</p>
                </div>
                {!articles.length && <Loading />}
                <div className="flex flex-col w-full items-center">
                    {articles.map(article =>
                        <Post post={article} key={article.uid} />
                    )}
                </div>

            </div>

        </>
    );
}
export default PostTrend