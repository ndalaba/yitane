import Link from "next/link"
import { relative_time } from "../../helpers/functions"

export default function PostTop({ post }) {
    const time = relative_time(post.date_published);
    const onImageError=(e)=>{
        e.target.src="/images/no_image.png";
     }
    return (
        <div className="flex items-center w-full mb-2">
            <div className="md:block flex-none mr-4">
                <img onError={onImageError} placeholder="../../no_image.jpg" src={post.magazine.image} alt={post.magazine.title} className="h-10 w-10 rounded-full flex-none object-cover object-left" />
            </div>
            <div className="flex flex-col w-full">
                <div className="flex content-between">
                    <div>
                        <span className="font-semibold">
                            <Link href="/[mag_slug]" as={`/${post.magazine.slug}`}><a title={post.magazine.title}>{post.magazine.title}</a></Link>
                        </span>
                        <span className="text-sm text-dark ml-1"><span className="inline-block">·</span> {time} </span>
                    </div>
                </div>
                {post.magazine.category &&
                    <Link href="/topic/[cat_slug]" as={`/topic/${post.magazine.category.slug}`}>
                        <a className="text-sm text-teal-800 font-semibold" title={post.magazine.category.name}>
                            @{post.magazine.category.name}
                        </a>
                    </Link>
                }
            </div>
        </div>

    )
}