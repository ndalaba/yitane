import Link from "next/link"
import { useEffect, useRef } from "react";
import AdBanner from "../AdBanner";

export default function Tops({ post }) {
   const { uid, slug, title, url, image, summary, magazine, body } = post
   const mag_slug = post.magazine.slug
   const post_slug = slug
   const imgRef = useRef();

   useEffect(() => {
      if (image !== null)
         imgRef.current.setAttribute('src', image.replace('http:', 'https:'))
   }, []);

   const onImageError=(e)=>{
      e.target.src="/images/no_image.png";
   }

   return (

      <div className="rounded w-full flex flex-col md:flex-row mb-10">
         {
            image !== null &&
            <Link href={{ pathname: '/[mag_slug]/[post_slug]/[uid]', query: { uid, post_slug, mag_slug, magazine, title, image, body, summary } }} as={url}>
               <a title={title}>
                  <img onError={onImageError} ref={imgRef} debounce={1} className="block md:hidden lg:block rounded-md w-11/12 md:w-64 m-4 md:m-0" placeholder="no_image.jpg" src={`no_image.jpg`} alt={title} title={title} />
               </a>
            </Link>
         }
         {image == null && <AdBanner />}
         <div className="bg-white rounded px-4">
            <span className="text-teal-700 text-md hidden md:block">
               <Link href="/topic/[cat_slug]" as={`/topic/${post.magazine.category.slug}`}>
                  <a title={post.magazine.category.name}>{post.magazine.category.name}</a>
               </Link>
            </span>
            <h2 className="md:mt-0 text-gray-800 font-semibold text-lg mb-2">
               <Link href={{ pathname: '/[mag_slug]/[post_slug]/[uid]', query: { uid, post_slug, mag_slug, magazine, title, image, body, summary } }} as={url}>
                  <a title={title}>{title}</a>
               </Link>
            </h2>
            <span className="text-teal-900 text-md hidden md:block">
               <Link href="/[mag_slug]" as={`/${post.magazine.slug}`}><a title={post.magazine.title}>@{post.magazine.title}</a></Link>
            </span>
         </div>
      </div>
   );
}