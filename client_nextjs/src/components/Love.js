import { useRouter } from "next/router"
import fetch from "node-fetch"
import { useState, useEffect } from "react"
import { redirectIfNotLogged } from "../helpers/functions"
import { API_ROUTE, FETCH_OPTIONS } from "../helpers/global"
import UserStore from "../store/UserStore"

const PostLove = ({ post }) => {
    const router = useRouter()
    const user = UserStore.state.currentUser;
    let loveCount = 0
    const [count, setCount] = useState(loveCount)
    const [userLovedArticles, setUserLovedArticles] = useState(UserStore.state.userLovedArticles)
    const [loved, setLoved] = useState(false)

    useEffect(() => {
        loveCount = (sessionStorage != undefined && sessionStorage.getItem(post.uid) != null) ? sessionStorage.getItem(post.uid) : parseInt(post.love)
        setCount(loveCount)
        isLoved()
    }, [])

    const love_article = async (e) => {
        e.preventDefault()
        redirectIfNotLogged(user, router)
        loved ? setCount(count > 0 ? parseInt(count) - 1 : 0) : setCount(parseInt(count) + 1)
        loved ? sessionStorage.setItem(post.uid, parseInt(count) - 1) : sessionStorage.setItem(post.uid, parseInt(count) + 1)

        setTimeout(() => {
            loved ? setUserLovedArticles(userLovedArticles.filter(art => art.uid != post.uid)) : setUserLovedArticles([...userLovedArticles, post])
        }, 1000)
        setLoved(!loved)
        await fetch(API_ROUTE + `love/article/${post.uid}?love=${+(!loved)}`, FETCH_OPTIONS())
    }

    const isLoved = () => {
        if (user.is_authenticated && userLovedArticles != undefined && userLovedArticles.length) {
            const loved_article = userLovedArticles.find(article => article.uid == post.uid)
            if (loved_article != undefined)
                setLoved(p => p = true)
        }
    }
    if (loved)
        return (
            <button className="post-btn flex font-bold" onClick={love_article} title="Nombre j'aime">
                <i className="fas fa-heart p-2 rounded-full hover:bg-teal-200 hover:text-black" />
                <p> {isNaN(count) ? 0 : count} </p>
            </button>
        )
    else
        return (
            <button className="post-btn flex" onClick={love_article} title="Nombre j'aime">
                <i className="far fa-heart p-2 rounded-full hover:bg-teal-200 hover:text-black" />
                <p> {isNaN(count) ? 0 : count} </p>
            </button>
        )
}

const VideoLove = ({ video }) => {
    const router = useRouter()
    const user = UserStore.state.currentUser;
    let loveCount = 0
    const [count, setCount] = useState(loveCount)
    const [userLovedVideos, setUserLovedVideos] = useState(UserStore.state.userLovedVideos)
    const [loved, setLoved] = useState(false)

    useEffect(() => {
        loveCount = (sessionStorage != undefined && sessionStorage.getItem(video.uid) != null) ? sessionStorage.getItem(video.uid) : parseInt(video.love)
        setCount(loveCount)
        isLoved()
    }, [])

    const love_video = async (e) => {
        e.preventDefault()
        redirectIfNotLogged(user, router)
        loved ? setCount(count > 0 ? parseInt(count) - 1 : 0) : setCount(parseInt(count) + 1)
        loved ? sessionStorage.setItem(post.uid, parseInt(count) - 1) : sessionStorage.setItem(post.uid, parseInt(count) + 1)

        /* setTimeout(() => {
             loved ? setUserLovedVideos(userLovedVideos.filter(art => art.uid != post.uid)) : setUserLovedVideos([...userLovedVideos, video])
         }, 1000)*/
        setLoved(!loved)
        await fetch(API_ROUTE + `love/video/${video.uid}?love=${+(!loved)}`, FETCH_OPTIONS())
    }

    const isLoved = () => {
        if (user.is_authenticated && userLovedVideos != undefined && userLovedVideos.length) {
            const loved_video = userLovedVideos.find(vid => vid.uid == video.uid)
            if (loved_video != undefined)
                setLoved(p => p = true)
        }
    }
    if (loved)
        return (
            <button className="post-btn flex font-bold" onClick={love_video} title="Nombre j'aime">
                <i className="fas fa-heart p-2 rounded-full hover:bg-teal-200 hover:text-black" />
                <p> {isNaN(count) ? 0 : count} </p>
            </button>
        )
    else
        return (
            <button className="post-btn flex" onClick={love_video} title="Nombre j'aime">
                <i className="far fa-heart p-2 rounded-full hover:bg-teal-200 hover:text-black" />
                <p> {isNaN(count) ? 0 : count} </p>
            </button>
        )
}

export { PostLove, VideoLove }