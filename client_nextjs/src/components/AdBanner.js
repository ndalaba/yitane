import React, { useEffect } from "react";

const AdBanner = (props) => {
  const { currentPath } = props
  useEffect(() => {
    try {
      (window.adsbygoogle = window.adsbygoogle || []).push({});
    } catch (err) {
      console.log(err);
    }
  }, [currentPath]);

  return (
    <div key={currentPath} style={{overflow:'hidden',maxHeight:'150px!important',height:'150px!important'}} className="rounded">
      <ins className="adsbygoogle" style={{display: "block"}} data-ad-client="ca-pub-3883880951212572" data-ad-slot="7628698108" data-ad-format="auto" data-full-width-responsive="true"/>                  
    </div>
  );
};

export default AdBanner;