import axios from "axios";
import {getStoredItem} from './functions';

export const ENV = 'dev';
//export const API_ROUTE = 'https://api.news.piaafrica.com/';
export const API_ROUTE = "http://localhost:5000"
export const NAME = 'PIAAFRICA NEWS';
export const TITLE = 'RESTEZ INFORMÉS, TROUVEZ DE L\'INSPIRATION';
export const DESCRIPTION = 'Vous apporte les dernières et les plus pertinentes informations, et vous maintient à jour. Découvrez et suivez vos medias et magazines favoris, et commencer à recevoir leurs informations ';
export const FACEBOOK_ID = '604465756787479';


let HttpClient;
const token = getStoredItem('token');

if (token !== null && token !== "" && token !== undefined) {
    HttpClient = axios.create({
        baseURL: API_ROUTE,
        headers: {
            "Content-Type": "application/json",
            'Accept': 'application/json',
            /*"Authorization": `Bearer ${token}`,*/
            'token': token
        }
    });
} else {
    HttpClient = axios.create({
        baseURL: API_ROUTE,
        headers: {
            "Content-Type": "application/json",
            'Accept': 'application/json',
        }
    });
}

export default HttpClient;