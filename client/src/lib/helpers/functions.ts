import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import 'dayjs/locale/fr';
import {goto} from '$app/navigation';
import {toast} from '@zerodevx/svelte-toast';

dayjs.extend(relativeTime);
dayjs.locale('fr');

const LOCAL_STORAGE_PREFIX = '_yi_';

export const redirectIfNotLogged = (user, currentPath) => {
    if (!user.is_authenticated && typeof window !== 'undefined') {
        sessionStorage.setItem('last_url', currentPath);
        goto('/auth/login');
    }
};

export const notification = (title = '', message = 'Notification', type = 'success') => {
    if (type === 'success') {
        toast.push(message, {
            theme: {
                '--toastBackground': '#48BB78',
                '--toastBarBackground': '#2F855A'
            }
        });
    }
    if (type === 'error' || type ==="danger") {
        toast.push(message, {
            theme: {
                '--toastBackground': '#F56565',
                '--toastBarBackground': '#C53030'
            }
        });
    }
    if (type === 'info') {
        toast.push(message, {
            theme: {
                '--toastBackground': '#0f8ace',
                '--toastBarBackground': '#0f54a4'
            }
        });
    }

};

export const relative_time = function (timestamp) {
    return dayjs(timestamp).fromNow();
};

export const wrapText = function (text, innerHTML) {
    const index = innerHTML.indexOf(text);
    if (index >= 0) {
        innerHTML =
            innerHTML.substring(0, index) +
            '<span class=\'font-semibold\'>' +
            innerHTML.substring(index, index + text.length) +
            '</span>' +
            innerHTML.substring(index + text.length);
    }
    return innerHTML;
};


export function storeItem(key, value) {
    if (typeof window !== 'undefined')
        localStorage.setItem(LOCAL_STORAGE_PREFIX + key, value);
}


export function getStoredItem(key, _default = '') {
    if (typeof window !== 'undefined') {
        return localStorage.getItem(LOCAL_STORAGE_PREFIX + key) !== null ? localStorage.getItem(LOCAL_STORAGE_PREFIX + key) : _default;
    } else return _default;

}

export function removeStoredItem(key) {
    if (typeof window !== 'undefined')
        localStorage.removeItem(LOCAL_STORAGE_PREFIX + key);
}

export function hasStoredItem(key) {
    if (typeof window !== 'undefined')
        return localStorage.getItem(LOCAL_STORAGE_PREFIX + key) !== null;
    return false;
}

export function doLogout() {
    if (typeof window !== 'undefined') {
        localStorage.clear();
        window.location.reload();
    }
}

export function readTime(text) {
    const wordsPerMinute = 200; // Average case.
    let result;
    let textLength = text.split(' ').length; // Split by words
    if (textLength > 0) {
        let value = Math.ceil(textLength / wordsPerMinute);
        result = `~${value} min de lecture`;
    }
    return result;
}

export function documentHeight() {
    const body = document.body, html = document.documentElement;
    return Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight);
}