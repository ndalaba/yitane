import HttpClient from '../helpers/global';
import {writable} from "svelte/store";
import type {Magazine} from "$lib/store/MagazineStore";

export const posts = writable([]);
export const homePosts = writable([]);
export const videos = writable([]);
export const trends = writable([]);

export const findByMagazine = async (slug: string, page: number = 1) => {
    const response = await HttpClient.get(slug + '?page=' + page);
    return await response.data;
}

export const findByCategory = async (slug: string, page: number = 1) => {
    const response = await HttpClient.get('topic/' + slug + '?page=' + page);
    return await response.data;
}

export const findPost = async (mag_slug: string, post_slug: string, uid: string) => {
    const response = await HttpClient.get(mag_slug + '/' + post_slug + '/' + uid);
    const data = await response.data;
    return {init_article: data.article, related_articles: data.related_articles};
}

export const findNextPosts = async (page = 1) => {
    const response = await HttpClient.get('/next?page=' + page);
    return await response.data;
}

export const getHomePosts = async (page = 1) => {
    const response = await HttpClient.get('/home?page=' + page);
    const data = await response.data;
    homePosts.update((curr) => [...curr, ...data]);
    return data;
}

export const getTrendPosts = async () => {
    const response = await HttpClient.get('trend_articles');
    const data = await response.data;
    trends.update((curr) => [...curr, ...data]);
    return data;
}

export const findComments = async (uid) => {
    const response = await HttpClient.get('articles/comments/' + uid);
    return await response.data;
}

export const search = async (q, page = 1) => {
    const response = await HttpClient.get('find/articles?q=' + q + '&page=' + page);
    return await response.data;
}

export const getContent = (uid) => {
    return HttpClient.get('article/get_content/' + uid);
}

export interface Post {
    id: number;
    uid: string;
    title: string;
    url: string;
    date_published: Date;
    summary: string;
    body: string;
    tag: string;
    youtube: string;
    tag_slug: string;
    link: string;
    guid: string;
    author: string;
    love: number;
    comments_count: number;
    vue: number;
    image: string;
    magazine_id: number;
    magazine: Magazine;
    updated_at:Date;
    created_at:Date;
}