import {writable} from "svelte/store";
import HttpClient from "$lib/helpers/global";


export const categories = writable([])

export const getCategories = async () => {
    const response = await HttpClient.get("/categories");
    const data = await response.data;
    categories.set(data);
}

export const getCategoryPostsFromPosts = (category: Category, posts, size = 4) => {
    const _posts = posts.filter(post => post.magazine.category.uid == category.uid);
    return _posts.slice(0, size);
}

export interface Category {
    uid: string;
    id: number;
    name: string;
    slug: string;
}