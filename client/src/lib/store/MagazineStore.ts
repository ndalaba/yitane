import {writable} from "svelte/store";
import HttpClient from "../helpers/global"
import type {Category} from "$lib/store/CategoryStore";

export const magazines = writable([]);
export const trendMagazines = writable([]);

export const getMagazines = async () => {
    const response = await HttpClient.get("/magazines");
    const data = await response.data;
    magazines.set(data);
}

export const refreshFeed = (magazine: Magazine) => {
    return HttpClient.get("magazines/check_feed/" + magazine.uid);
}

export const getTrendsMagazines = async (size = 100) => {
    const response = await HttpClient.get('magazines/trends?size=' + size);
    const data = await response.data;
    trendMagazines.set(data);
}

export interface Magazine {
    id: number;
    uid: string;
    name: string;
    title: string;
    category: Category
}