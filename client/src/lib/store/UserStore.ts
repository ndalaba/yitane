import HttpClient from '../helpers/global';
import {getStoredItem, hasStoredItem, notification, storeItem} from '../helpers/functions';
import {writable} from "svelte/store";

const token = getStoredItem('token');

export const currentUser = writable({
    'email': '',
    'password': '',
    'remember_me': false,
    is_authenticated: false,
    country: {},
    image: `/images/account.svg`
});

export const userSavedVideos = writable([]);
export const userLovedVideos = writable([]);
export const userSavedArticles = writable([]);
export const userLovedArticles = writable([]);
export const userCategories = writable([]);
export const userMagazines = writable([]);
export const userChannels = writable([]);

export const getUserMazines = async (uid) => {
    const response = await HttpClient.get(`users/${uid}/magazines`);
    return await response.data;
}

export const getUserChannels = async (uid) => {
    const response = await HttpClient.get(`users/${uid}/channels`);
    return await response.data;
}

export const getUserFeed = async (uid, page = 1) => {
    const response = await HttpClient.get(`users/${uid}/feeds?page=` + page);
    return await response.data;
}

export const getUser = async () => {
    const response = await HttpClient.get('get_auth_user');
    return await response.data;
}

export const updateUser = async (formData) => {
    const response = await HttpClient.post('account?token=' + getStoredItem('token'), formData);
    return await response.data;
}

export const updatePassword = async (formData) => {
    const response = await HttpClient.post('password?token=' + getStoredItem('token'), formData);
    return await response.data;
}

export const signin = async (formData) => {
    const response = await HttpClient.post('auth/login', formData);
    const data = await response.data;
    if (data.token) {
        currentUser.set(data.user);
        storeItem('token', data.token);
        storeItem('user', JSON.stringify(data.user));
        if (hasStoredItem('last_url'))
            window.location.href = getStoredItem('last_url');
        else
            window.history.back();
    } else {
        notification('Erreur', data, 'danger');
    }
}

export const signup = async (formData) => {
    const response = await HttpClient.post('auth/registration', formData);
    const data = await response.data;
    if (data.status) {
        notification('Success', data.message, 'info');
        storeItem('token', data.token);
        localStorage.setItem('token', data.token);
        if (hasStoredItem('last_url'))
            window.location.href = getStoredItem('last_url');
        else
            window.location.href = '/';
    } else
        notification('Erreur', data.message, 'danger');
}

export const getUserInfo = async () => {
    if (token !== null && token !== undefined && token !== 'undefined' && token.length) {
        const response = await HttpClient.get('auth/token_check?token=' + token);
        const data = await response.data;
        if (data.status) {
            currentUser.set(data.user);
            HttpClient.get(`users/${data.user.uid}/magazines`).then(response => {
                userMagazines.set(response.data)
            });
            HttpClient.get(`users/${data.user.uid}/channels`).then(response => {
                userChannels.set(response.data)
            });
            HttpClient.get(`users/${data.user.uid}/categories`).then(response => {
                userCategories.set(response.data)
            });
        }
    }
}

export const checkToken = async () => {
    if (token !== null && token !== undefined && token !== 'undefined' && token.length) {
        const response = await HttpClient.get('auth/token_check?token=' + token);
        const data = await response.data;
        if (data.status) {
            currentUser.set(data.user);
        }
    }
}

export const checkLovedArticles = async () => {
    if (token !== null && token !== undefined && token !== 'undefined' && token.length) {
        const reponse = await HttpClient.get('user/loved_articles');
        const data = await reponse.data;
        userLovedArticles.set(data);
    }
}

export const checkSavedArticles = async () => {
    if (token !== null && token !== undefined && token !== 'undefined' && token.length) {
        const reponse = await HttpClient.get('user/saved_articles');
        const data = await reponse.data;
        userSavedArticles.set(data);
    }
}

export const checkLovedVideos = async () => {
    if (token !== null && token !== undefined && token !== 'undefined' && token.length) {
        const reponse = await HttpClient.get('user/loved_videos');
        const data = await reponse.data;
        userLovedVideos.set(data);
    }
}

export const checkSavedVideos = async () => {
    if (token !== null && token !== undefined && token !== 'undefined' && token.length) {
        const reponse = await HttpClient.get('user/saved_videos');
        const data = await reponse.data;
        userSavedVideos.set(data);
    }
}

export interface User {
    id: number;
    name: string;
    uid: string;
    email: string;
    remember_me: boolean;
    is_authenticated: boolean;
    country;
    image: string;
}
