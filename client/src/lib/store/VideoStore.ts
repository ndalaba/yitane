import HttpClient, {API_ROUTE} from '../helpers/global';
import {writable} from "svelte/store";

export const trendVideos = writable([]);
export const homeVideos = writable([]);


export const getVideos = async (page: number = 1) => {
    const response = await HttpClient.get('/videos?page=' + page);
    const data = await response.data;
    homeVideos.update((current) => [...current, ...data]);
    return data;
}
export const getVideo = async (chan_slug: string, video_slug: string, uid: string) => {
    const response = await HttpClient.get('videos/' + chan_slug + '/' + video_slug + '/' + uid);
    return await response.data;
}

export const findComments = async (uid: string) => {
    const response = await HttpClient.get('videos/comments/' + uid);
    return await response.data;
}

export const findByCategory = async (slug: string, page: number = 1) => {
    const response = await HttpClient.get('videos/topic/' + slug + '?page=' + page);
    return await response.data;
}

export const findByChannel = async (slug, page = 1) => {
    const response = await HttpClient.get('channels/' + slug + '?page=' + page);
    return await response.data;
}

export const searchVideos = async (value, page = 1) => {
    const response = await HttpClient.get('find/videos?q=' + value + '&page=' + page);
    return await response.data;
}

export const getTrendVideos = async (size: number = 1) => {
    const response = await HttpClient(API_ROUTE + 'trend_videos?size=' + size);
    const data = await response.data;
    trendVideos.set(data);
}

export interface Video {
    id: number;
    uid: string;
    title: string;
    video_id: string;
    body: string;
    love: number;
    comments_count: number;
    vue: number;
    image: string;
    date_published: Date;
    channel_id: number;
}
