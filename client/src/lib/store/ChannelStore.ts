import {writable} from "svelte/store";
import HttpClient from "../helpers/global"

export const channels = writable([]);
export const trendChannels = writable([]);

export const getChannels = async () => {
    const response = await HttpClient.get("/channels");
    const data = await response.data;
    channels.set(data);
}

export const getTrendChannels = async (size = 100) => {
    const response = await HttpClient.get('channels/trends?size=' + size);
    const data = await response.data;
    trendChannels.set(data);
}

export const refreshFeed = (channel: Channel) => {
    return HttpClient.get("magazines/check_feed/" + channel.uid);
}

export interface Channel {
    id: number;
    uid: string;
    name: string;
}