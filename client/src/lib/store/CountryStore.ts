import {writable} from "svelte/store";
import HttpClient from "$lib/helpers/global";

export const countries = writable([]);

export const getCountries = async () => {
    const response = await HttpClient.get("/countries");
    const data = await response.data;
    countries.update(() => data);
}

export interface Country {
    uid: string;
    id: number;
    name: string;
}