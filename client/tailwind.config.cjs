module.exports = {
  content: ['./src/**/*.{html,js,svelte,ts}'],
  purge: ['./src/**/*.{html,js,svelte,ts}'],
  darkMode: 'class',
  theme: {
    container:{ center:true},
    extend: {
      fontFamily: {
        sans: ['Barlow Condensed', 'sans-serif'],
      },
    },
  },
  plugins: [],
}