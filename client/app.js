 
var http = require('http');

const express = require('express')
const next = require('next')
    
const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()
    
app.prepare()
.then(() => {
  const application = express();
  
    application.get('*', (req, res) => {
        return handle(req, res)
      });
    var server= http.createServer(application);
   
    server.listen();
})
.catch((ex) => {
  console.error(ex.stack)
  process.exit(1)
})