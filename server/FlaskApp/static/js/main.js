(function ($) {
    "use strict"
    // Mobile dropdown
    $('.has-dropdown>a').on('click', function () {
        $(this).parent().toggleClass('active');
    });
    // Aside Nav
    $(document).click(function (event) {
        if (!$(event.target).closest($('#nav-aside')).length) {
            if ($('#nav-aside').hasClass('active')) {
                $('#nav-aside').removeClass('active');
                $('#nav').removeClass('shadow-active');
            } else {
                if ($(event.target).closest('.aside-btn').length) {
                    $('#nav-aside').addClass('active');
                    $('#nav').addClass('shadow-active');
                }
            }
        }
    });
    $('.nav-aside-close').on('click', function () {
        $('#nav-aside').removeClass('active');
        $('#nav').removeClass('shadow-active');
    });
    $('.search-btn').on('click', function () {
        $('#nav-search').toggleClass('active');
        $('#search_input').focus();
    });

    $('.search-close').on('click', function () {
        $('#nav-search').removeClass('active');
    });

    const popupCenter = function (url, title, width, height) {
        const popupWidth = width || 640;
        const popupHeight = height || 320;
        const windowLeft = window.screenLeft || window.screenX;
        const windowTop = window.screenTop || window.screenY;
        const windowWidth = window.innerWidth || document.documentElement.clientWidth;
        const windowHeight = window.innerHeight || document.documentElement.clientHeight;
        const popupLeft = windowLeft + windowWidth / 2 - popupWidth / 2;
        const popupTop = windowTop + windowHeight / 2 - popupHeight / 2;
        const popup = window.open(url, title, 'scrollbars=yes, width=' + popupWidth + ', height=' + popupHeight + ', top=' + popupTop + ', left=' + popupLeft);
        popup.focus();
        return true;
    };
    if (document.querySelector('.fb-share') != null) {
        $('body').on('click', '.tw-share', function (e) {
            e.preventDefault();
            const url = $(this).data('url');
            const shareUrl = "https://twitter.com/intent/tweet?text=" + encodeURIComponent($(this).attr('title')) +
                "&via=Paper" +
                "&url=" + encodeURIComponent(url);
            popupCenter(shareUrl, "Partager sur Twitter");
        });
        $('body').on('click', '.fb-share', function (e) {
            e.preventDefault();
            const url = $(this).data('url');
            const shareUrl = "https://www.facebook.com/sharer/sharer.php?u=" + encodeURIComponent(url);
            popupCenter(shareUrl, "Partager sur facebook");
        });
        $('body').on('click', '.mail-share', function (e) {
            e.preventDefault();
            const url = $(this).data('url');
            const subject = ($(this).attr('title'));
            console.log(subject);
            const link = "mailto:"
                + "?&subject=" + subject
                + "&body=" + escape(url)
            ;
            window.location.href = link;
        });
    }

    $('a.search').click(function (e) {
        e.preventDefault();
        $('body').children().addClass("hide-me");
        $('#search-overlay').addClass('g-search');
    });
    $('#hide-search').click(function (e) {
        e.preventDefault();
        $('body').children().removeClass("hide-me");
        $('#search-overlay').removeClass('g-search');
    });

    $('#search_input').on('keyup', function (e) {
        const value = $(this).val();
        const url = $("#search_form").attr('action') + "?q=" + value;
        $.get(url, function (response) {
            $("#search_result").html(response);
        });
    });

    let prevScrollpos = window.pageYOffset;
    window.onscroll = function () {
        $("#footer").fadeIn();
        const search_form = document.querySelector("#nav-search");
        if (!search_form.classList.contains("active")) {
            let currentScrollPos = window.pageYOffset;
            if (prevScrollpos > currentScrollPos) {
                $('#nav').slideDown();
                document.getElementById("nav").style.top = "0";
            } else {
                if (currentScrollPos > 142) {
                    $('#nav').slideUp();
                    document.getElementById("nav").style.top = "-142px";
                }
            }
            prevScrollpos = currentScrollPos;
        }
    };

    const load_more_article = $("#load_more_article");
    const url = load_more_article.data("url");
    const parent = "#" + load_more_article.data("parent");
    const size = 24;
    const observe = function (observer) {
        let posts = document.querySelectorAll(".post");
        posts.forEach(function (post) {
            //post.classList.add("not-visible");
            observer.observe(post);
        });
    };
    const unobserve = function (observer) {
        let posts = document.querySelectorAll(".post");
        posts.forEach(function (post) {           
            observer.unobserve(post);
        });
    };

    const loadArticles = function (url, uid, parent, observer) {
        $.get(url + "?uid=" + uid + "&size=" + size, function (response) {
            $(parent).append(response);
            if (observer !== undefined) {
                unobserve(observer);
                observe(observer);
            } else loadImages();
        });
    };

    const load_more_articles = function (e) {
        $('#load_more_article').on('click', function (e) {
            e.preventDefault();
            const uid = $('.article_scroll:last').data("uid");
            loadArticles(url, uid, parent);
        });
    };

    const loadImages = function () {
        const images = document.querySelectorAll("img");
        images.forEach(function (image) {
            image.src = image.dataset.isrc;
        });
    };

    load_more_articles();

    if (IntersectionObserver !== undefined) {        
        let iteration = size;
        let observer = new IntersectionObserver(function (entries) {
            entries.forEach(function (entry) {
                if (entry.intersectionRatio > 0.3) {
                    iteration -= 1;
                    //entry.target.classList.remove('not-visible');
                    observer.unobserve(entry.target);
                    const image = entry.target.querySelector(".post-img img");
                    if (image !== null)
                        image.src = image.dataset.isrc;
                    if (iteration <= 0) {
                        iteration = size;
                        let uid = $('.article_scroll:last').data("uid");
                        loadArticles(url, uid, parent, observer);
                    }
                }
            });

        }, {
            threshold: [0.3]
        });
        observe(observer);
    } else {
        loadImages();
    }

    const get_magazines_list_hmlt = function (magazines) {
        let magazines_list = "";
        magazines.forEach(function (magazine) {
            magazines_list += `
                      <div class="col-md-2 col-sm-6">
                            <div class="post post-thumb" style="background-image:url('${magazine.image}')">
                                <a class="post-img" href="${magazine.url}" title="${magazine.title}"></a>
                                <div class="post-body">
                                    <h3 class="post-title title-lg">
                                        <a href="${magazine.url}" title="${magazine.title}">${magazine.title}</a>
                                    </h3>
                                </div>
                            </div>
                      </div>
                `;
        });
        return magazines_list;
    };

    $('#select_category').change(function (e) {
        const category_id = $(this).val();
        const country_id = $("#select_country").val();
        const url = $(this).data('url') + "?country_id=" + country_id + "&category_id=" + category_id;
        $.get(url, function (magazines) {
            const magazines_list = get_magazines_list_hmlt(magazines);
            $("#magazines_list").html(magazines_list);
        });
    });
    $('#select_country').change(function (e) {
        const country_id = $(this).val();
        const category_id = $("#select_category").val();
        const url = $(this).data('url') + "?country_id=" + country_id + "&category_id=" + category_id;
        $.get(url, function (magazines) {
            const magazines_list = get_magazines_list_hmlt(magazines);
            $("#magazines_list").html(magazines_list);
        });
    });

    /*
        const relative_time = function (timestamp) {
            const date = new Date(parseInt(timestamp) * 1000);
            const delta = Math.round((+new Date - date) / 1000);

            var minute = 60,
                hour = minute * 60,
                day = hour * 24,
                week = day * 7;

            let fuzzy;

            if (delta < 30) {
                fuzzy = 'Maintenant';
            } else if (delta < minute) {
                fuzzy = "il y'a " + delta + ' secondes';
            } else if (delta < 2 * minute) {
                fuzzy = "il y'a une minute"
            } else if (delta < hour) {
                fuzzy = "il y'a " + Math.floor(delta / minute) + ' minutes';
            } else if (Math.floor(delta / hour) == 1) {
                fuzzy = "il y'a 1 heure"
            } else if (delta < day) {
                fuzzy = "il y'a " + Math.floor(delta / hour) + ' heures';
            } else if (delta < day * 2) {
                fuzzy = 'hier';
            }

            return fuzzy;
        };

        const date_times_elements = document.querySelectorAll(".date_time");
        date_times_elements.forEach(function (element) {
            const innerHtml = relative_time(element.dataset.time);
            if (innerHtml != null)
                element.innerHTML = innerHtml;
        });*/

})(jQuery);