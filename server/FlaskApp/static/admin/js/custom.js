$(function () {
        /** START ICHECK SELECT ALL */
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' /* optional */
        });
        $('#select_all').on('ifChecked', function (event) {
            $('.uid').iCheck('check');
            triggeredByChild = false;
        });

        $('#select_all').on('ifUnchecked', function (event) {
            if (!triggeredByChild) {
                $('.uid').iCheck('uncheck');
            }
            triggeredByChild = false;
        });
        // Removed the checked state from "All" if any checkbox is unchecked
        $('.uid').on('ifUnchecked', function (event) {
            triggeredByChild = true;
            $('#select_all').iCheck('uncheck');
        });

        $('.uid').on('ifChecked', function (event) {
            if ($('.uid').filter(':checked').length == $('.check').length) {
                $('#select_all').iCheck('check');
            }
        });

        $('#select_all .iCheck-helper').click(function () {
            // var checkboxes = $(this).closest('form').find('.uid');
            $('td .iCheck-helper').click();
        });

        //Enable check and uncheck all functionality
        $(".checkbox-toggle").click(function () {
            var clicks = $(this).data('clicks');
            if (clicks) {
                //Uncheck all checkboxes
                $(".mailbox-messages input[type='checkbox']").iCheck("uncheck");
                $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
            } else {
                //Check all checkboxes
                $(".mailbox-messages input[type='checkbox']").iCheck("check");
                $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
            }
            $(this).data("clicks", !clicks);
        });


        window.alert = function (message, type = 'red') {
            $.alert({title: 'Alert!', content: message, type: type});
        };
        window.confirmation = function (content = "", callback, title = "Confirmation", dialogType = "dark", btnClass = "btn-primary") {
            $.confirm({
                title: title,
                content: content,
                columnClass: 'col-md-5 col-md-offset-4',
                icon: 'fa fa-question-circle',
                type: dialogType,
                buttons: {
                    ok: {
                        text: "OK",
                        btnClass: btnClass,
                        keys: ['enter'],
                        action: callback
                    },
                    cancel: {
                        text: 'Annuler'
                    }
                }
            });
        };

        $('.delete').click(function (event) {
            event.preventDefault();
            const message = $(this).attr('title');
            const goto = $(this).attr('href');
            confirmation(message, function () {
                window.location.href = goto;
            })
        });

        if (document.querySelector(".select2"))
            $('.select2').select2();

        // SIDE BAR COLLAPSE
        if (sessionStorage.getItem("sidebar-collapse") === "sidebar-collapse") {
            $('body').addClass('sidebar-collapse')
        } else {
            $('body').removeClass('sidebar-collapse');
        }
        $('a.sidebar-toggle').click(function () {
            setTimeout(function () {
                if ($('body').hasClass('sidebar-collapse')) {
                    sessionStorage.setItem("sidebar-collapse", "sidebar-collapse");
                } else {
                    sessionStorage.setItem("sidebar-collapse", "");
                }

            }, 100);
        });



        
    }
);