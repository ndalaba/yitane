from http import HTTPStatus

from flask import request, jsonify
from flask_login import current_user

from App.Domain.Article.UseCase.Article.GetUserLovedArticles import GetUserLovedArticles
from App.Domain.Article.UseCase.Article.GetUserSavedArticles import GetUserSavedArticles
from App.Domain.User.UseCase.LovingArticle import LovingArticle
from App.Domain.User.UseCase.SavingArticle import SavingArticle
from FlaskApp.src.helpers.auth import token_required
from . import api
from ..repository.ArticleRepository import ArticleRepository
from ..repository.FeedRepository import FeedRepository
from ..repository.UserRepository import UserRepository

article_repository = ArticleRepository()


@api.route("/love/article/<uid>", methods = ['GET'])
@token_required
def user_love_article(uid):
    love = int(request.args.get("love", 0))
    response = (LovingArticle(UserRepository(), article_repository, FeedRepository())).execute(uid, bool(love), current_user.uid)
    if response.has_error():
        return jsonify({'error': response.errors}), HTTPStatus.BAD_REQUEST
    return jsonify(), HTTPStatus.OK


@api.route("/user/loved_articles", methods = ['GET'])
@token_required
def user_loved_articles():
    response = (GetUserLovedArticles(article_repository)).execute(current_user.uid, int(request.args.get('page', 1)))
    return jsonify(response.get_data('articles'))


@api.route("/save/article/<uid>", methods = ['GET'])
@token_required
def user_save_article(uid):
    save = int(request.args.get("save", 0))
    response = (SavingArticle(UserRepository(), article_repository, FeedRepository())).execute(uid, bool(save), current_user.uid)
    if response.has_error():
        return jsonify({'error': response.errors}), HTTPStatus.BAD_REQUEST
    return jsonify(), HTTPStatus.OK


@api.route("/user/saved_articles", methods = ['GET'])
@token_required
def user_saved_articles():
    response = (GetUserSavedArticles(article_repository)).execute(current_user.uid, int(request.args.get('page', 1)))
    return jsonify(response.get_data('articles'))
