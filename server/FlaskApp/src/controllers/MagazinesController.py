import logging
from http import HTTPStatus

from flask import request, jsonify

from App.Domain.Article.Request.MagazineFilterRequest import MagazineFilterRequest
from App.Domain.Article.UseCase.Magazine.GetFiltredMagazines import GetFiltredMagazines
from App.Domain.Article.UseCase.Magazine.GetMagazine import GetMagazine
from App.Domain.Article.UseCase.Magazine.RefreshMagazinesEntries import RefreshMagazinesEntries
from . import api
from ..repository.ArticleRepository import ArticleRepository
from ..repository.ErrorRepository import ErrorRepository
from ..repository.MagazineRepository import MagazineRepository

magazine_repository = MagazineRepository()


@api.route('/<slug>', methods = ['GET'])
def magazine(slug):
    page = request.args.get('page', 1, type = int)
    logging.debug(f"Request to get magazine and articles page: {page}")
    response = (GetMagazine(magazine_repository, ArticleRepository())).execute(slug, page)
    return jsonify(response.datas)


@api.route("/magazines/filter", methods = ['GET'])
def filter_magazine():
    logging.debug(f"Request to filter magazine")
    country_id = int(request.args.get('country_id', 0))
    category_id = int(request.args.get('category_id', 0))
    filter_request = MagazineFilterRequest(country_id = country_id, category_id = category_id)
    response = (GetFiltredMagazines(magazine_repository)).execute(filter_request)
    return jsonify(response.get_data('magazines')), HTTPStatus.OK


@api.route("/magazines/check_feed/<uid>")
def magazine_check_feed(uid):
    logging.debug(f"Request to refresh magazine articles: {uid}")
    response = (RefreshMagazinesEntries(magazine_repository, ArticleRepository(), ErrorRepository())).execute([uid], False)
    if response.has_error():
        return jsonify({"error": str(response.errors)}), HTTPStatus.BAD_REQUEST
    return jsonify(response.success_message), HTTPStatus.OK
