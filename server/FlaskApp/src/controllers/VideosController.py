import logging
from http import HTTPStatus

from flask import jsonify, request

from App.Domain.Video.Request.VideoFilterRequest import VideoFilterRequest
from App.Domain.Video.UseCase.Video.GetFiltredVideos import GetFiltredVideos
from App.Domain.Video.UseCase.Video.GetVideo import GetVideo
from . import api
from ..repository.CountryRepository import CountryRepository
from ..repository.VideoRepository import VideoRepository

video_repository = VideoRepository()


@api.route("/videos/<channel>/<slug>/<uid>", methods = ['GET'])
def video(channel, slug, uid):
    response = (GetVideo(video_repository)).execute(uid)
    return jsonify({'video': response.get_data('video'), 'related_videos': response.get_data('related_videos')})


@api.route("/find/videos", methods = ['GET'])
def videos_search():
    logging.debug(f"Request to filter videos title: {request.args.get('q')}")
    filter_request = VideoFilterRequest(title = request.args.get('q'), page = request.args.get('page', 1, type = int))
    response = (GetFiltredVideos(video_repository, CountryRepository())).execute(filter_request)
    return jsonify(response.get_data('videos')), HTTPStatus.OK


@api.route("/videos/topic/<tag>", methods = ['GET'])
def videos(tag):
    page = request.args.get('page', 1, type = int)
    logging.debug(f"Request to get videos by category: {tag}")
    country_id = request.args.get('country_id', None, type = int)
    category_id = request.args.get('category_id', None, type = int)
    video_filter_request = VideoFilterRequest(page = page, country_id = country_id, category_id = category_id)
    response = (GetFiltredVideos(video_repository, CountryRepository())).execute(video_filter_request)
    return jsonify({'videos': response.get_data('videos')}),HTTPStatus.OK
