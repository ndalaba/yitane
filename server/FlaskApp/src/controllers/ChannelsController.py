import logging

from flask import request, jsonify

from App.Domain.Video.Request.ChannelFilterRequest import ChannelFilterRequest
from App.Domain.Video.UseCase.Channel.GetChannel import GetChannel
from App.Domain.Video.UseCase.Channel.GetFiltredChannels import GetFiltredChannels
from . import api
from ..repository.ChannelRepository import ChannelRepository
from ..repository.VideoRepository import VideoRepository

channel_repository = ChannelRepository()


@api.route("/channels/filter", methods = ['GET'])
def filter_channel():
    logging.debug(f"Request to filter channels")
    filter_request = ChannelFilterRequest(country_id = request.args.get('country_id', None), category_id = request.args.get('category_id', None))
    response = (GetFiltredChannels(channel_repository)).execute(filter_request)
    return jsonify(response.get_data('channels'))


@api.route('/channels/<slug>', methods = ['GET'])
def channel(slug):
    page = request.args.get('page', 1, type = int)
    logging.debug(f"Request to get channel and articles page: {page}")
    response = (GetChannel(channel_repository, VideoRepository())).execute(slug, page)
    return jsonify(response.datas)
