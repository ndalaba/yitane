from http import HTTPStatus

from flask import request, jsonify
from flask_login import current_user

from App.Domain.Video.UseCase.Video.GetUserLovedVideos import GetUserLovedVideos
from App.Domain.Video.UseCase.Video.GetUserSavedVideos import GetUserSavedVideos
from App.Domain.User.UseCase.LovingVideo import LovingVideo
from App.Domain.User.UseCase.SavingVideo import SavingVideo
from FlaskApp.src.helpers.auth import token_required
from . import api
from ..repository.VideoRepository import VideoRepository
from ..repository.FeedRepository import FeedRepository
from ..repository.UserRepository import UserRepository

video_repository = VideoRepository()


@api.route("/love/video/<uid>", methods = ['GET'])
@token_required
def user_love_video(uid):
    love = int(request.args.get("love", 0))
    response = (LovingVideo(UserRepository(), video_repository, FeedRepository())).execute(uid, bool(love), current_user.uid)
    if response.has_error():
        return jsonify({'error': response.errors}), HTTPStatus.BAD_REQUEST
    return jsonify(), HTTPStatus.OK


@api.route("/user/loved_videos", methods = ['GET'])
@token_required
def user_loved_videos():
    response = (GetUserLovedVideos(video_repository)).execute(current_user.uid, int(request.args.get('page', 1)))
    return jsonify(response.get_data('videos'))


@api.route("/save/video/<uid>", methods = ['GET'])
@token_required
def user_save_video(uid):
    save = int(request.args.get("save", 0))
    response = (SavingVideo(UserRepository(), video_repository, FeedRepository())).execute(uid, bool(save), current_user.uid)
    if response.has_error():
        return jsonify({'error': response.errors}), HTTPStatus.BAD_REQUEST
    return jsonify(), HTTPStatus.OK


@api.route("/user/saved_videos", methods = ['GET'])
@token_required
def user_saved_videos():
    response = (GetUserSavedVideos(video_repository)).execute(current_user.uid, int(request.args.get('page', 1)))
    return jsonify(response.get_data('videos'))
