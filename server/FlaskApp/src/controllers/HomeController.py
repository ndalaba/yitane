import logging
from http import HTTPStatus

from flask import render_template, request, jsonify, current_app
from flask_login import current_user

from App.Domain.User.Entity.User import User
from FlaskApp.src.helpers.auth import authenticated
from . import api
from ..helpers.Email import Email
from ..repository.ArticleRepository import ArticleRepository
from ..repository.UserRepository import UserRepository
from ..repository.VideoRepository import VideoRepository


@api.route("/home", methods = ['GET'])
@api.route("/", methods = ['GET'])
@authenticated
def homepage():
    page = request.args.get('page', 1, type = int)
    logging.debug(f"Request to load home articles page: {page}")
    if current_user.is_authenticated:
        articles = (ArticleRepository()).find_member_prefered_article(UserRepository.model_to_entity(current_user), page)
    else:
        articles = (ArticleRepository()).filter(page = page)

    articles = [article.serialize() for article in articles]

    return jsonify(articles), HTTPStatus.OK


@api.route("/videos", methods = ['GET'])
@authenticated
def homevideos():
    page = request.args.get('page', 1, type = int)
    logging.debug(f"Request to load home videos page: {page}")
    if current_user.is_authenticated:
        videos = (VideoRepository()).member_prefered_videos(UserRepository.model_to_entity(current_user), page)
    else:
        videos = (VideoRepository()).filter(page = page)

    videos = [video.serialize() for video in videos]
    return jsonify(videos)


@api.route('/contact', methods = ['POST'])
def contact():
    logging.debug("Request to send contact message")
    message = render_template("email/contact.html", message = request.form.get('message'))
    status = Email("Message de contact", (request.form.get('name'), request.form.get('email')), [current_app.config['APP_EMAIL']], message).send()
    if status:
        return jsonify({'status': 1, 'message': 'Votre message a été envoyé'})
    else:
        return jsonify({'status': 0, 'message': 'Erreur envoie message'})


@api.route("/next", methods = ['GET'])
@authenticated
def next_articles():
    page = request.args.get('page', 1, type = int)
    logging.debug(f"Request to get next articles page: {page}")
    if current_user.is_authenticated:
        articles = (ArticleRepository()).find_member_prefered_article(UserRepository.model_to_entity(current_user), page)
    else:
        articles = (ArticleRepository()).filter(page = page)

    articles = [article.serialize() for article in articles]

    return jsonify(articles)
