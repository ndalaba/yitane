import logging
from http import HTTPStatus

from flask import request, jsonify
from flask_login import current_user

from App.Domain.Feed.Entity.Feed import FeedAction
from App.Domain.Feed.Entity.Feed import FeedType
from App.Domain.Feed.Request.FeedRequest import FeedRequest
from App.Domain.Feed.UseCase.CreateFeed import CreateFeed
from App.Domain.Feed.UseCase.GetFeeds import GetFeeds
from App.Domain.ImageRequest import ImageRequest
from App.Domain.User.Entity.User import User
from App.Domain.User.Request.UserPasswordRequest import UserPasswordRequest
from App.Domain.User.Request.UserRequest import UserRequest
from App.Domain.User.UseCase.GetUser import GetUser
from App.Domain.User.UseCase.GetUserCategories import GetUserCategories
from App.Domain.User.UseCase.GetUserChannels import GetUserChannels
from App.Domain.User.UseCase.GetUserMagazines import GetUserMagazines
from App.Domain.User.UseCase.UpdateUser import UpdateUser
from App.Domain.User.UseCase.UpdateUserPassword import UpdateUserPassword
from FlaskApp.src.helpers.auth import token_required
from instance.config import UPLOAD_PATH
from . import api
from ..repository.FeedRepository import FeedRepository
from ..repository.UserRepository import UserRepository

user_repository = UserRepository()
feed_repository = FeedRepository()


@api.get("/users/<uid>/feeds")
def get_user_feeds(uid):
    logging.debug(f"Request to get user: {uid}")
    page = request.args.get('page', 1, type = int)
    response = (GetUser(user_repository)).execute(uid, request.url_root + 'static/upload/users/')
    if response.has_error():
        return jsonify({'error': str(response.errors)}), HTTPStatus.BAD_REQUEST
    rep = (GetFeeds(feed_repository)).execute(response.get_data('user')['id'], page)
    if rep.has_error():
        return jsonify({'error': str(response.errors)}), HTTPStatus.BAD_REQUEST
    return jsonify({'user': response.get_data('user'), 'feeds': rep.get_data('feeds')})


@api.get("/users/<uid>/magazines")
def get_magazines(uid):
    logging.debug(f"Request to get user magazines: {uid}")
    response = (GetUserMagazines(user_repository)).execute(uid)
    if response.has_error():
        return jsonify({'error': str(response.errors)}), HTTPStatus.BAD_REQUEST
    return jsonify(response.get_data('magazines')), HTTPStatus.OK


@api.get("/users/<uid>/categories")
def get_categories(uid):
    logging.debug(f"Request to get user categories: {uid}")
    response = (GetUserCategories(user_repository)).execute(uid)
    if response.has_error():
        return jsonify({'error': str(response.errors)}), HTTPStatus.BAD_REQUEST
    return jsonify(response.get_data('categories')), HTTPStatus.OK


@api.get("/users/<uid>/channels")
def get_channels(uid):
    logging.debug(f"Request to get user channels: {uid}")
    response = (GetUserChannels(user_repository)).execute(uid)
    if response.has_error():
        return jsonify({'error': str(response.errors)}), HTTPStatus.BAD_REQUEST
    return jsonify(response.get_data('channels')), HTTPStatus.OK


@api.get("/get_auth_user")
@token_required
def get_auth_user():
    logging.debug(f"Request to get user")
    if current_user.is_authenticated:
        response = (GetUser(user_repository)).execute(current_user.uid)
        return jsonify({'user': response.get_data('user')})
    else:
        return jsonify({'error': "Erreur authentication."})


@api.post("/account")
@token_required
def account():
    logging.debug(f"Request to update user account")
    user_request = UserRequest(id = current_user.id, uid = current_user.uid, name = request.form['name'], email = request.form['email'], phone = request.form.get('phone'),
                               country_id = request.form.get('country'), role = User.ROLE_USER)
    image_request = ImageRequest(image = request.form.get('image'), path = UPLOAD_PATH + 'users/')
    response = (UpdateUser(user_repository)).execute(user_request, image_request)
    if response.has_error():
        return jsonify({'error': str(response.errors)}), HTTPStatus.BAD_REQUEST
    response = (CreateFeed(feed_repository)).execute(FeedRequest(feedable_id = current_user.id, feedable_type = FeedType.USER, action = FeedAction.UPDATE_PROFIL, user_id = current_user.id))
    if response.has_error():
        return jsonify({'error': str(response.errors), 'status': 0}), HTTPStatus.BAD_REQUEST
    return jsonify({'status': 1, 'message': "Profil modifié."}), HTTPStatus.OK


@api.post("/password")
@token_required
def password():
    logging.debug(f"Request to update user password")
    user_request = UserPasswordRequest(uid = current_user.uid, password = request.form.get('password'), confirmation_password = request.form.get('confirmation_password'))
    response = (UpdateUserPassword(user_repository)).execute(user_request)
    if response.has_error():
        return jsonify({'error': str(response.errors)}), HTTPStatus.BAD_REQUEST
    response = (CreateFeed(feed_repository)).execute(FeedRequest(feedable_id = current_user.id, feedable_type = FeedType.USER, action = FeedAction.UPDATE_PASSWORD, user_id = current_user.id))
    if response.has_error():
        return jsonify({'error': str(response.errors), 'status': 0}), HTTPStatus.BAD_REQUEST
    return jsonify({'status': 1, 'message': "Profil modifié."}), HTTPStatus.OK
