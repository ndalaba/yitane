import logging
from http import HTTPStatus

from flask import jsonify, request
from flask_login import current_user

from App.Domain.Comment.Entity.Comment import CommentableType
from App.Domain.Comment.Request.CommentRequest import CommentRequest
from App.Domain.Comment.UseCase.CreateComment import CreateComment
from App.Domain.Comment.UseCase.GetComments import GetComments
from FlaskApp.src.helpers.auth import token_required
from . import api
from ..repository.ArticleRepository import ArticleRepository
from ..repository.CommentRepository import CommentRepository
from ..repository.FeedRepository import FeedRepository
from ..repository.VideoRepository import VideoRepository

commment_repository = CommentRepository()


@api.route("/articles/comments", methods = ['POST'])
@token_required
def articles_add_comment():
    logging.debug(f"Request to add article comment : {request.args.get('id')}")
    comment_request = CommentRequest(commentable_id = request.args.get('id', type = int), commentable_type = CommentableType.ARTICLE, content = request.form.get('content'), user_id = current_user.id)
    response = (CreateComment(commment_repository, ArticleRepository(), VideoRepository(), FeedRepository())).execute(comment_request)
    if response.has_error():
        return jsonify({'error': str(response.errors)}), HTTPStatus.BAD_REQUEST
    return jsonify(response.get_data('comment')), HTTPStatus.CREATED


@api.route("/articles/comments/<id>", methods = ['GET'])
def articles_comments(id):
    logging.debug(f"Request to get article comments: {id}")
    response = (GetComments(commment_repository)).execute(CommentableType.ARTICLE, id)
    return jsonify(response.get_data('comments')), HTTPStatus.OK


@api.route("/videos/comments", methods = ['POST'])
@token_required
def videos_add_comment():
    logging.debug(f"Request to add video comment : {request.args.get('id')}")
    comment_request = CommentRequest(commentable_id = request.args.get('id', type = int), commentable_type = CommentableType.VIDEO, content = request.form.get('content'), user_id = current_user.id)
    response = (CreateComment(commment_repository, ArticleRepository(), VideoRepository(), FeedRepository())).execute(comment_request)
    if response.has_error():
        return jsonify({'error': str(response.errors)}), HTTPStatus.BAD_REQUEST
    return jsonify(response.get_data('comment')), HTTPStatus.CREATED


@api.route("/videos/comments/<id>", methods = ['GET'])
def videos_comments(id):
    logging.debug(f"Request to get video comments: {id}")
    response = (GetComments(commment_repository)).execute(CommentableType.VIDEO, id)
    return jsonify(response.get_data('comments')), HTTPStatus.OK
