from http import HTTPStatus

from flask import jsonify, request, current_app, render_template, flash, redirect, abort
from flask_login import login_user

import App.Helper.jwt_token as jwt
from App.Domain.User.Entity.User import User
from App.Domain.User.Request.UserRequest import UserRequest
from App.Domain.User.UseCase.CreateUser import CreateUser
from App.Domain.User.UseCase.GetUser import GetUser
from App.Domain.User.UseCase.RequestPasswordReset import RequestPasswordReset
from App.Domain.User.UseCase.ValidateUserEmail import ValidateUserEmail
from App.Helper.Response import Response
from FlaskApp.src.entities import User as UserModel
from . import auth
from ...helpers.Email import Email
from ...repository.UserRepository import UserRepository

user_repository = UserRepository()


@auth.route('/password-forgot', methods = ['GET'])
def password_forgot():
    user = user_repository.find_by('email', request.args['email'])
    if user is not None:
        message = render_template("email/password.html", token = user.confirmation_token, uid = user.uid)
        mail = Email("Mot de passe oublié", (current_app.config['APP_NAME'], current_app.config['APP_NOREPLY']), request.args['email'], message)
        response = (RequestPasswordReset(user_repository, mail)).execute(request.args['email'], user)
        if response.has_error():
            return jsonify({'error': str(response.errors)}), HTTPStatus.BAD_REQUEST
        return jsonify({'message': response.success_message})
    return jsonify({'error': "Utilisateur non trouvé."}), HTTPStatus.BAD_REQUEST


@auth.route("/registration", methods = ["POST"])
def registration():
    if request.method == "POST":
        user_request = UserRequest(name = request.form.name, email = request.form.email)
        response: Response = (CreateUser(user_repository)).execute(user_request)
        if response.has_error():
            return jsonify({'status': 0, 'message': response.errors})
        user: User = response.get_data('user')
        message = render_template("email/registration.html", member = response.get_data('user'))
        email = Email("Confirmation de votre adresse courriel", (current_app.config['APP_NAME'], current_app.config['APP_NOREPLY']), [user.email], message)
        email.send()
        (UserModel(*vars(user)))
        jwt_token = jwt.encode(user.uid)
        return jsonify({'member': user.serialize(), 'token': jwt_token}), HTTPStatus.CREATED


@auth.route('/reset-password/<uid>/<token>', methods = ['GET', 'POST'])
def reset_password(uid, token):
    response: Response = (GetUser(user_repository)).execute(uid)
    if response.has_error():
        abort(404)
    user: User = response.get_data('user')
    if user.confirmation_token == token:
        member = UserModel(*vars(user))
        login_user(member)
        return redirect(current_app.config['HOME_PAGE'] + "/compte/password")

    flash("Ce compte n'existe pas. Veillez vous inscrire", "error")
    return redirect(current_app.config['HOME_PAGE'] + "/auth/signup")


@auth.route("/email-validation/<uid>/<token>", methods = ['GET'])
def email_validation(uid, token):
    response: Response = (ValidateUserEmail(user_repository)).execute(uid, token)
    if response.has_error():
        flash(str(response.errors), "error")
    flash(response.success_message, "success")
    return redirect(current_app.config['HOME_PAGE'])
