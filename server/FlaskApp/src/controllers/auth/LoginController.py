from datetime import datetime
from http import HTTPStatus

from flask import request, jsonify
from flask_login import login_user, logout_user
from sqlalchemy.orm.exc import NoResultFound

import App.Helper.jwt_token as jwt
from App.Domain.User.Entity.User import User
from App.Domain.User.Request.UserRequest import UserRequest
from App.Domain.User.UseCase.CreateUser import CreateUser
from App.Helper.upload import upload_image_from_url
from FlaskApp import db
from FlaskApp.src.entities.User import OAuth, User as UserModel
from instance.config import UPLOAD_PATH
from . import auth
from ...repository.UserRepository import UserRepository

user_repository = UserRepository()


@auth.route('/login', methods = ['POST'])
def login():
    user: User = user_repository.find_by_email(request.form['email'])
    if user is not None and user.verify_password(request.form['password']):
        auth_user: UserModel = UserRepository.entity_to_model(user)
        remember_me = bool(request.form['remember_me']) if 'remember_me' in request.form else False
        login_user(auth_user, remember_me)
        jwt_token = jwt.encode(user.uid)
        user.last_login = datetime.now()
        user_repository.update(user)
        user.is_authenticated = True
        user.set_image_path(request.url_root + 'static/upload/users/')
        return jsonify({'user': user.serialize_all(), 'token': jwt_token})
    else:
        return jsonify("Email ou mot de passe incorrect."), HTTPStatus.OK


@auth.route('/token_check', methods = ['GET'])
def token_check():
    token = request.args.get("token")
    if token:
        decoded = jwt.decode(token)
        try:
            user: User = user_repository.get(decoded['uid'])
            if user is not None:
                auth_user: UserModel = UserRepository.entity_to_model(user)
                login_user(auth_user)
                user.is_authenticated = True
                user.set_image_path(request.url_root + 'static/upload/users/')
                return jsonify({'status': 1, 'user': user.serialize_all()}), HTTPStatus.OK
            else:
                return jsonify({'status': 0}), HTTPStatus.NOT_ACCEPTABLE
        except ValueError:
            pass
    else:
        return jsonify({'status': 0}), HTTPStatus.NOT_ACCEPTABLE


@auth.route('/logout')
def logout():
    logout_user()
    return jsonify({'status': 1, 'message': "Aurevoir."})


@auth.route("/login_facebook", methods = ['POST'])
def login_facebook():
    query = OAuth.query.filter_by(provider = "facebook", provider_user_id = request.form.get("id"))
    try:
        oauth = query.one()
    except NoResultFound:
        image_url = "https://graph.facebook.com/" + request.form.get("id") + "/picture?type=large"
        image = upload_image_from_url(image_url, UPLOAD_PATH + "users")
        oauth = OAuth(provider = "facebook", provider_user_id = request.form.get("id"), image = image)
    if oauth.user:
        login_user(oauth.user)
        user = UserRepository.model_to_entity(oauth.user)
        token = jwt.encode(user.uid)
        return jsonify({'user': user.serialize(), 'token': token})
    else:
        user: User = user_repository.find_by_email(request.form.get('email'))
        if user is None:
            user_request = UserRequest(name = request.form.get("name"), email = request.form.get("email"), role = User.ROLE_USER)
            response = (CreateUser(user_repository)).execute(user_request)
            if response.has_error():
                return jsonify({'error': str(response.errors)}), HTTPStatus.BAD_REQUEST
            user: User = response.get_data('user')
            user_model = UserModel(*vars(user))
            oauth.user = user_model
            db.session.add_all([oauth])
            db.session.commit()
            login_user(user_model)
            token = jwt.encode(user.uid)
            return jsonify({'user': user.serialize(), 'token': token}), HTTPStatus.OK
