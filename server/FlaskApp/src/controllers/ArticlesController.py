import logging

from flask import jsonify, request

from App.Domain.Article.Request.ArticleFilterRequest import ArticleFilterRequest
from App.Domain.Article.UseCase.Article.GetArticle import GetArticle
from App.Domain.Article.UseCase.Article.GetArticleContent import GetArticleContent
from App.Domain.Article.UseCase.Article.GetFiltredArticles import GetFiltredArticles
from App.Domain.Article.UseCase.Article.GetTrendArticles import GetTrendArticles
from . import api
from ..repository.ArticleRepository import ArticleRepository
from ..repository.CountryRepository import CountryRepository

article_repository = ArticleRepository()


@api.route('/<mag>/<slug>/<uid>', methods = ['GET'])
def article(mag, slug, uid):
    logging.debug(f"Request to get article: {uid}")
    response = (GetArticle(article_repository)).execute(uid)
    return jsonify({'article': response.get_data('article'), 'related_articles': response.get_data('related_articles')})


@api.route("/topic/<tag>", methods = ['GET'])
def articles(tag):
    page = request.args.get('page', 1, type = int)
    logging.debug(f"Request to get articles by category: {tag}")
    country_id = request.args.get('country_id', None, type = int)
    category_id = request.args.get('category_id', None, type = int)
    article_filter_request = ArticleFilterRequest(page = page, country_id = country_id, category_id = category_id)
    response = (GetFiltredArticles(article_repository, CountryRepository())).execute(article_filter_request)
    return jsonify({'articles': response.get_data('articles')})


@api.route("/find/articles", methods = ['GET'])
def articles_search():
    q = request.args.get('q')
    logging.debug(f"Request to search articles q: {q}")
    page = request.args.get('page', 1, type = int)
    article_filter_request = ArticleFilterRequest(page = page, title = q.strip())
    response = (GetFiltredArticles(article_repository, CountryRepository())).execute(article_filter_request)
    return jsonify(response.get_data('articles'))


@api.route('/trend_articles', methods = ['GET'])
def trend_articles():
    logging.debug(f"Request to get trends articles")
    response = (GetTrendArticles(article_repository)).execute()
    return jsonify({'articles': response.get_data('articles')})


@api.route("/article/get_content/<uid>", methods = ['GET'])
def get_content(uid):
    logging.debug(f"Request to get article content: {uid}")
    response = (GetArticleContent(article_repository)).execute(uid)
    return jsonify({'article': response.get_data('article')})
