from flask import Blueprint, jsonify,request

from App.Domain.Article.UseCase.Category.GetCategories import GetCategories
from App.Domain.Localisation.UseCase.GetCountries import GetCountries
from ..repository.CategoryRepository import CategoryRepository
from ..repository.CountryRepository import CountryRepository

api = Blueprint('api', __name__)

from . import HomeController, CommentController, ArticlesController, MagazinesController, ProfilsController, FollowController, \
    UserArticleController, ChannelsController, UserVideoController, VideosController


@api.route("/countries", methods = ['GET'])
def countries():
    response = (GetCountries(CountryRepository())).execute()
    return jsonify(response.get_data('countries'))


@api.route("/categories", methods = ['GET'])
def categories():
    response = (GetCategories(CategoryRepository())).execute(image_path = request.url_root + 'static/upload/categories/')
    return jsonify(response.get_data('categories'))


