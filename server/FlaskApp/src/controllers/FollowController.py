import logging
from http import HTTPStatus

from flask import request, jsonify
from flask_login import current_user

from App.Domain.User.UseCase.FollowingCategory import FollowingCategory
from App.Domain.User.UseCase.FollowingChannel import FollowingChannel
from App.Domain.User.UseCase.FollowingMagazine import FollowingMagazine
from FlaskApp.src.helpers.auth import token_required
from . import api
from ..repository.CategoryRepository import CategoryRepository
from ..repository.ChannelRepository import ChannelRepository
from ..repository.FeedRepository import FeedRepository
from ..repository.MagazineRepository import MagazineRepository
from ..repository.UserRepository import UserRepository

feed_repository = FeedRepository()
user_repository = UserRepository()


@api.route("/follow/magazine/<uid>", methods = ['GET'])
@token_required
def user_following_magazine(uid):
    logging.debug(f"Request to (un)follow magazine: {uid}")
    follow = int(request.args.get("follow", 0))
    response = (FollowingMagazine(user_repository, MagazineRepository(), feed_repository)).execute(magazine_uid = uid, follow = bool(follow), user_uid = current_user.uid)
    if response.has_error():
        return jsonify({'error': response.errors}), HTTPStatus.BAD_REQUEST
    return jsonify(), HTTPStatus.OK


@api.route("/follow/channel/<uid>", methods = ['GET'])
@token_required
def user_following_channel(uid):
    logging.debug(f"Request to (un)follow channel: {uid}")
    follow = int(request.args.get("follow", 0))
    response = (FollowingChannel(user_repository, ChannelRepository(), feed_repository)).execute(uid, bool(follow), current_user.uid)
    if response.has_error():
        return jsonify({'error': response.errors}), HTTPStatus.BAD_REQUEST
    return jsonify(), HTTPStatus.OK


@api.route("/follow/category/<uid>", methods = ['GET'])
@token_required
def user_following_category(uid):
    logging.debug(f"Request to (un)follow category: {uid}")
    follow = int(request.args.get("follow", 0))
    response = (FollowingCategory(user_repository, CategoryRepository(), feed_repository)).execute(uid, bool(follow), current_user.uid)
    if response.has_error():
        return jsonify({'error': response.errors}), HTTPStatus.BAD_REQUEST
    return jsonify(), HTTPStatus.OK
