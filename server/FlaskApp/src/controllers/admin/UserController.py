from http import HTTPStatus

from flask import request, jsonify
from flask_login import login_required, current_user

from App.Domain.ImageRequest import ImageRequest
from App.Domain.User.Entity.User import User
from App.Domain.User.Request.UserPasswordRequest import UserPasswordRequest
from App.Domain.User.Request.UserRequest import UserRequest
from App.Domain.User.UseCase.UpdateUser import UpdateUser
from App.Domain.User.UseCase.UpdateUserPassword import UpdateUserPassword
from FlaskApp.src.helpers.auth import is_admin
from instance.config import UPLOAD_PATH
from . import admin
from ...repository.UserRepository import UserRepository

user_repository = UserRepository()


@admin.route("/password", methods = ['POST'])
@is_admin
@login_required
def password():
    user_request = UserPasswordRequest(uid = current_user.uid, password = request.form['password'], confirmation_password = request.form['confirmation_password'])
    response = (UpdateUserPassword(user_repository)).execute(user_request)
    if response.has_error():
        return jsonify({'error': str(response.errors)}), HTTPStatus.BAD_REQUEST
    return jsonify({'user': response.get_data('user')}), HTTPStatus.OK


@admin.route("/account", methods = ['POST'])
@is_admin
@login_required
def account():
    user_request = UserRequest(id = current_user.id, uid = current_user.uid, name = request.form['name'], email = request.form['email'], phone = request.form['phone'],
                               country_id = request.form['country'], role = User.ROLE_USER)
    image_request = ImageRequest(image = request.form['image'], path = UPLOAD_PATH + 'users/')
    response = (UpdateUser(user_repository)).execute(user_request, image_request)
    if response.has_error():
        return jsonify({'error': str(response.errors)}), HTTPStatus.BAD_REQUEST
    return jsonify({'user': response.get_data('user')}), HTTPStatus.OK
