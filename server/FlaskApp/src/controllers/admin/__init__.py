from flask import Blueprint

admin = Blueprint('admin', __name__)

from . import HomeController, CountriesController, CategoriesController, MagazinesController, ArticleController, MembersController, ErrorsController, ChannelController, VideoController
from FlaskApp.src.entities import Error


@admin.context_processor
def inject_categories():
    unresoved_errors = Error.query.filter(Error.resolved is False).order_by(Error.created_at.desc()).all()
    return dict(unresoved_errors = unresoved_errors)
