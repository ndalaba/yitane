import logging
from http import HTTPStatus

from flask import request, jsonify

from App.Domain.Video.Request.ChannelFilterRequest import ChannelFilterRequest
from App.Domain.Video.Request.ChannelRequest import ChannelRequest
from App.Domain.Video.UseCase.Channel.ApplyChannelAction import ApplyChannelAction
from App.Domain.Video.UseCase.Channel.CreateChannel import CreateChannel
from App.Domain.Video.UseCase.Channel.DeleteChannel import DeleteChannel
from App.Domain.Video.UseCase.Channel.GetChannel import GetChannel
from App.Domain.Video.UseCase.Channel.GetChannels import GetChannels
from App.Domain.Video.UseCase.Channel.GetFiltredChannels import GetFiltredChannels
from App.Domain.Video.UseCase.Channel.RefreshChannelsEntries import RefreshChannelsEntries
from App.Domain.Video.UseCase.Channel.UpdateChannel import UpdateChannel
from FlaskApp.src.helpers.auth import is_admin
from . import admin
from ...repository.CategoryRepository import CategoryRepository
from ...repository.ChannelRepository import ChannelRepository
from ...repository.CountryRepository import CountryRepository
from ...repository.VideoRepository import VideoRepository

channel_repository = ChannelRepository()
country_repository = CountryRepository()
category_repository = CategoryRepository()


@admin.route('/channels', methods = ['GET'])
@is_admin
def channels():
    action = request.args.get('doaction') if request.args is not None else ""
    if action == "Appliquer":
        logging.debug(f"Request to apply action to categories")
        todo = int(request.args.get('todo'))
        uids = request.args.getlist('uid')
        (ApplyChannelAction(channel_repository)).execute(todo, uids)

    elif action == "Filtrer":
        logging.debug(f"Request to filter channels")
        filter_request = ChannelFilterRequest(title = request.args.get('title'), country_id = request.args.get('country'),
                                              category_id = request.args.get('category'), published = bool(request.args.get('published')))
        response = (GetFiltredChannels(channel_repository)).execute(filter_request)
        return jsonify(response.get_data('channels')), HTTPStatus.OK

    response = (GetChannels(channel_repository)).execute()
    return jsonify(response.get_data('channels')), HTTPStatus.OK


@admin.route('/channels/add', methods = ['POST'])
@is_admin
def add_channel():
    logging.debug(f"Request to add channel: {request.form['title']}")
    channel_request = ChannelRequest(title = request.form['title'], country_id = request.form['country_id'], category_id = request.form['category_id'], link = request.form['linkd'],
                                     channel_id = request.form['channel_id'], description = request.form['description'], image = request.form['image'])
    response = (CreateChannel(channel_repository, country_repository, category_repository)).execute(channel_request)
    if response.has_error():
        return jsonify({'error': str(response.errors)}), HTTPStatus.BAD_REQUEST
    return jsonify(response.get_data('channel')), HTTPStatus.CREATED


@admin.route('/channels/edit/<uid>', methods = ['GET', 'POST'])
@is_admin
def edit_channel(uid):
    if request.method == 'POST':
        logging.debug(f"Request to update channel: {uid}")
        channel_request = ChannelRequest(title = request.form['title'], country_id = request.form['country_id'], category_id = request.form['category_id'], link = request.form['linkd'],
                                         channel_id = request.form['channel_id'], description = request.form['description'], image = request.form['image'], uid = uid, id = request.form['id'])
        response = (UpdateChannel(channel_repository, country_repository, category_repository)).execute(channel_request)
        if response.has_error():
            return jsonify({"error": str(response.errors)}), HTTPStatus.BAD_REQUEST
        return jsonify(response.success_message), HTTPStatus.OK

    response = (GetChannel(channel_repository)).execute(uid)
    return jsonify(response.get_data('channel')), HTTPStatus.OK


@admin.route('/channels/delete/<uid>')
@is_admin
def delete_channel(uid):
    logging.debug(f"Request to delete channel: {uid}")
    response = (DeleteChannel(channel_repository)).execute(uid)
    if response.has_error():
        return jsonify({"error": str(response.errors)}), HTTPStatus.BAD_REQUEST
    return jsonify(), HTTPStatus.NO_CONTENT


@admin.route("/channels/check_videos/<uid>")
@is_admin
def channel_check_videos(uid):
    logging.debug(f"Request to refresh channels vidéos: {uid}")
    response = (RefreshChannelsEntries(channel_repository, VideoRepository())).execute([uid], 100, False)
    if response.has_error():
        return jsonify({"error": str(response.errors)}), HTTPStatus.BAD_REQUEST
    return jsonify(response.success_message), HTTPStatus.OK
