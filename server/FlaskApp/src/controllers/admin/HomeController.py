from flask import jsonify
from flask_login import login_required

from App.Domain.Common.UseCase.GetDashboardStat import GetDashboardStat
from FlaskApp.src.helpers.auth import is_admin
from . import admin
from ...repository.ArticleRepository import ArticleRepository
from ...repository.CategoryRepository import CategoryRepository
from ...repository.ChannelRepository import ChannelRepository
from ...repository.CountryRepository import CountryRepository
from ...repository.MagazineRepository import MagazineRepository
from ...repository.UserRepository import UserRepository
from ...repository.VideoRepository import VideoRepository


@admin.route("/index")
@is_admin
@login_required
def home():
    response = (GetDashboardStat(ArticleRepository(), MagazineRepository(), VideoRepository(), ChannelRepository(), CountryRepository(), UserRepository(), CategoryRepository())).execute()
    return jsonify({'members': response.get_data('members'), 'categories': response.get_data('categories'), 'countries': response.get_data('countries'), 'articles': response.get_data('articles'),
                    'videos': response.get_data('videos'), 'magazines': response.get_data('magazines'), 'channels': response.get_data('channels')})
