import logging
from http import HTTPStatus

from flask import request, jsonify

from App.Domain.Video.Request.VideoFilterRequest import VideoFilterRequest
from App.Domain.Video.UseCase.Video.ApplyVideoAction import ApplyVideoAction
from App.Domain.Video.UseCase.Video.DeleteVideo import DeleteVideo
from App.Domain.Video.UseCase.Video.GetFiltredVideos import GetFiltredVideos
from App.Domain.Video.UseCase.Video.GetVideos import GetVideos
from FlaskApp.src.helpers.auth import is_admin
from . import admin
from ...repository.CountryRepository import CountryRepository
from ...repository.VideoRepository import VideoRepository

video_repository = VideoRepository()


@admin.route('/videos', methods = ['GET'])
@is_admin
def videos():
    action = request.args.get('doaction') if request.args is not None else ""
    page = request.args.get('page', 1, type = int)
    if action == "Appliquer":
        logging.debug(f"Request to apply action to categories")
        todo = int(request.args.get('todo'))
        uids = request.args.getlist('uid')
        (ApplyVideoAction(video_repository)).execute(todo, uids)

    elif action == "Filtrer":
        logging.debug(f"Request to filter videos")
        filter_request = VideoFilterRequest(title = request.args.get('title'), country_id = request.args.get('country'), channel_id = request.args.get('channel'), date = request.args.get('_date'),
                                            category_id = request.args.get('category'), published = bool(request.args.get('published')), order = request.args.get('order'), page = page)
        response = (GetFiltredVideos(video_repository, CountryRepository())).execute(filter_request)
        return jsonify(response.get_data('videos')), HTTPStatus.OK

    response = (GetVideos(video_repository)).execute()
    return jsonify(response.get_data('videos')), HTTPStatus.OK


@admin.route('/videos/delete/<uid>')
@is_admin
def delete_video(uid):
    logging.debug(f"Request to delete video {uid}")
    response = (DeleteVideo(video_repository)).execute(uid)
    if response.has_error():
        return jsonify({'error': str(response.errors)}), HTTPStatus.BAD_REQUEST
    return jsonify(), HTTPStatus.NO_CONTENT
