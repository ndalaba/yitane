import logging
from http import HTTPStatus

from flask import request, jsonify

from App.Domain.Article.Request.ArticleFilterRequest import ArticleFilterRequest
from App.Domain.Article.UseCase.Article.ApplyArticleAction import ApplyArticleAction
from App.Domain.Article.UseCase.Article.DeleteArticle import DeleteArticle
from App.Domain.Article.UseCase.Article.GetArticles import GetArticles
from App.Domain.Article.UseCase.Article.GetFiltredArticles import GetFiltredArticles
from FlaskApp.src.helpers.auth import is_admin
from . import admin
from ...repository.ArticleRepository import ArticleRepository
from ...repository.CountryRepository import CountryRepository

article_repository = ArticleRepository()


@admin.route('/articles', methods = ['GET'])
@is_admin
def articles():
    action = request.args.get('doaction') if request.args is not None else ""
    page = request.args.get('page', 1, type = int)
    if action == "Appliquer":
        logging.debug(f"Request to apply action to categories")
        todo = int(request.args.get('todo'))
        uids = request.args.getlist('uid')
        (ApplyArticleAction(article_repository)).execute(todo, uids)

    elif action == "Filtrer":
        logging.debug(f"Request to filter articles")
        filter_request = ArticleFilterRequest(title = request.args.get('title'), country_id = request.args.get('country'), magazine_id = request.args.get('magazine'), date = request.args.get('_date'),
                                              category_id = request.args.get('category'), published = bool(request.args.get('published')), order = request.args.get('order'), page = page)
        response = (GetFiltredArticles(article_repository, CountryRepository())).execute(filter_request)
        return jsonify(response.get_data('articles')), HTTPStatus.OK

    response = (GetArticles(article_repository)).execute()
    return jsonify(response.get_data('articles')), HTTPStatus.OK


@admin.route('/articles/delete/<uid>')
@is_admin
def delete_article(uid):
    logging.debug(f"Request to delete article {uid}")
    response = (DeleteArticle(article_repository)).execute(uid)
    if response.has_error():
        return jsonify({'error': str(response.errors)}), HTTPStatus.BAD_REQUEST
    return jsonify(), HTTPStatus.NO_CONTENT
