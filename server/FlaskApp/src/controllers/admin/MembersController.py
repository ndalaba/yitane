import logging
from http import HTTPStatus

from flask import request, jsonify

from App.Domain.User.Entity.User import User
from App.Domain.User.Request.UserFilterRequest import UserFilterRequest
from App.Domain.User.UseCase.ApplyUserAction import ApplyUserAction
from App.Domain.User.UseCase.DeleteUser import DeleteUser
from App.Domain.User.UseCase.GetUsers import GetUsers
from FlaskApp.src.helpers.auth import is_admin
from . import admin
from ...repository.UserRepository import UserRepository

user_repository = UserRepository()


@admin.route('/members', methods = ['GET'])
@is_admin
def members():
    action = request.args.get('doaction') if request.args is not None else ""
    page = request.args.get('page', 1, type = int)
    if action == "Appliquer":
        logging.debug(f"Request to apply action to categories")
        todo = int(request.args.get('todo'))
        uids = request.args.getlist('uid')
        (ApplyUserAction(user_repository)).execute(todo, uids)

    else:
        logging.debug(f"Request to filter users")
        filter_request = UserFilterRequest(email = request.args.get('email'), country_id = request.args.get('country'), role = User.ROLE_USER,
                                           name = request.args.get('name'), page = page)
        response = (GetUsers(user_repository)).execute(filter_request)
        return jsonify(response.get_data('users')), HTTPStatus.OK


@admin.route('/members/delete/<uid>')
@is_admin
def delete_member(uid):
    logging.debug(f"Request to delete user {uid}")
    response = (DeleteUser(user_repository)).execute(uid)
    if response.has_error():
        return jsonify({'error': str(response.errors)}), HTTPStatus.BAD_REQUEST
    return jsonify(), HTTPStatus.NO_CONTENT
