import logging
from http import HTTPStatus

from flask import request, jsonify

from App.Domain.ImageRequest import ImageRequest
from App.Domain.Localisation.Request.CountryRequest import CountryRequest
from App.Domain.Localisation.UseCase.CreateCountry import CreateCountry
from App.Domain.Localisation.UseCase.DeleteCountry import DeleteCountry
from App.Domain.Localisation.UseCase.GetCountries import GetCountries
from App.Domain.Localisation.UseCase.UpdateCountry import UpdateCountry
from FlaskApp.src.helpers.auth import is_admin
from . import admin
from ...repository.CountryRepository import CountryRepository

country_repository = CountryRepository()


@admin.route('/countries')
@is_admin
def countries():
    logging.debug("Request to get countries")
    response = (GetCountries(country_repository)).execute()
    return jsonify(response.get_data('countries')), HTTPStatus.OK


@admin.route('/countries/add', methods = ['POST'])
@is_admin
def add_country():
    logging.debug(f"Request to add a new country: {request.form['name']}")
    image_request = ImageRequest(image = request.form['image'], path = "static/upload/countries/") if request.form['image'] else None
    country_request = CountryRequest(name = request.form['name'])
    response = (CreateCountry(country_repository)).execute(country_request, image_request)
    if response.has_error():
        return jsonify({'error': str(response.errors)}), HTTPStatus.BAD_REQUEST
    return jsonify(response.get_data('country')), HTTPStatus.OK


@admin.route('/countries/edit/<uid>', methods = ['GET', 'POST'])
@is_admin
def edit_country(uid):
    logging.debug(f"Request to edit country: {uid}")
    image_request = ImageRequest(image = request.form['image'], path = "static/upload/countries/") if request.form['image'] else None
    country_request = CountryRequest(name = request.form['name'], uid = uid, id = request.form['id'])
    response = (UpdateCountry(country_repository)).execute(country_request, image_request)
    if response.has_error():
        return jsonify({'error': str(response.errors)}), HTTPStatus.BAD_REQUEST
    return jsonify(response.get_data('country')), HTTPStatus.OK


@admin.route('/countries/delete/<uid>')
@is_admin
def delete_country(uid):
    logging.debug(f"Request to delete country: {uid}")
    response = (DeleteCountry(country_repository)).execute(uid)
    if response.has_error():
        return jsonify({'error': str(response.errors)}), HTTPStatus.BAD_REQUEST
    return jsonify(), HTTPStatus.NO_CONTENT
