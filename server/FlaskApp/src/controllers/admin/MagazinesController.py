import logging
from http import HTTPStatus

from flask import request, jsonify

from App.Domain.Article.Request.MagazineFilterRequest import MagazineFilterRequest
from App.Domain.Article.Request.MagazineRequest import MagazineRequest
from App.Domain.Article.UseCase.Magazine.ApplyMagazineAction import ApplyMagazineAction
from App.Domain.Article.UseCase.Magazine.CheckMagazineArticles import CheckMagazineArticles
from App.Domain.Article.UseCase.Magazine.CreateMagazine import CreateMagazine
from App.Domain.Article.UseCase.Magazine.DeleteMagazine import DeleteMagazine
from App.Domain.Article.UseCase.Magazine.GetFiltredMagazines import GetFiltredMagazines
from App.Domain.Article.UseCase.Magazine.GetMagazines import GetMagazines
from App.Domain.Article.UseCase.Magazine.RefreshMagazinesEntries import RefreshMagazinesEntries
from App.Domain.Article.UseCase.Magazine.UpdateMagazine import UpdateMagazine
from FlaskApp.src.helpers.auth import is_admin
from . import admin
from ...repository.ArticleRepository import ArticleRepository
from ...repository.CategoryRepository import CategoryRepository
from ...repository.CountryRepository import CountryRepository
from ...repository.ErrorRepository import ErrorRepository
from ...repository.MagazineRepository import MagazineRepository

magazine_repository = MagazineRepository()


@admin.route('/magazines', methods = ['GET'])
@is_admin
def magazines():
    action = request.args.get('doaction') if request.args is not None else ""
    page = request.args.get('page', 1, type = int)
    if action == "Appliquer":
        logging.debug(f"Request to apply action to categories")
        todo = int(request.args.get('todo'))
        uids = request.args.getlist('uid')
        (ApplyMagazineAction(magazine_repository, ArticleRepository(), ErrorRepository())).execute(todo, uids)

    elif action == "Filtrer":
        logging.debug(f"Request to filter magazines")
        filter_request = MagazineFilterRequest(title = request.args.get('title'), country_id = request.args.get('country'), top = bool(request.args.get('top')),
                                               category_id = request.args.get('category'), published = bool(request.args.get('published')), page = page)
        response = (GetFiltredMagazines(magazine_repository)).execute(filter_request)
        return jsonify(response.get_data('magazines')), HTTPStatus.OK

    response = (GetMagazines(magazine_repository)).execute()
    return jsonify(response.get_data('magazines')), HTTPStatus.OK


@admin.route('/magazines/add', methods = ['POST', 'GET'])
@is_admin
def add_magazine():
    logging.debug(f'Request to create magazine')
    magazine_request = MagazineRequest(title = request.form['title'], feed = request.form['feed'], link = request.form['link'], image = request.form['image'],
                                       country_id = request.form.get('country', type = int), category_id = int(request.form['category']), published = bool(request.form['published']),
                                       description = request.form['description'], image_tag = request.form['image_tag'], body_tag = request.form['body_tag'],
                                       tag_to_remove = request.form['tag_to_remove'])
    response = (CreateMagazine(magazine_repository, CountryRepository(), CategoryRepository())).execute(magazine_request)
    if response.has_error():
        return jsonify({'error': str(response.errors)}), HTTPStatus.BAD_REQUEST
    return jsonify(response.get_data('magazine')), HTTPStatus.CREATED


@admin.route('/magazines/edit/<uid>', methods = ['GET', 'POST'])
@is_admin
def edit_magazine(uid):
    logging.debug(f'Request to update magazine: {uid}')
    magazine_request = MagazineRequest(title = request.form['title'], feed = request.form['feed'], uid = uid, id = int(request.form['id']), link = request.form['link'], image = request.form['image'],
                                       country_id = int(request.form['country']), category_id = int(request.form['category']), published = bool(request.form['published']),
                                       description = request.form['description'], image_tag = request.form['image_tag'],
                                       body_tag = request.form['body_tag'], tag_to_remove = request.form['tag_to_remove'])
    response = (UpdateMagazine(magazine_repository, CountryRepository(), CategoryRepository())).execute(magazine_request)
    if response.has_error():
        return jsonify({'error': str(response.errors)}), HTTPStatus.BAD_REQUEST
    return jsonify(response.get_data('magazine')), HTTPStatus.OK


@admin.route('/magazines/delete/<uid>')
@is_admin
def delete_magazine(uid):
    logging.debug(f"Request to delete magazine: {uid}")
    response = (DeleteMagazine(magazine_repository)).execute(uid)
    if response.has_error():
        return jsonify({"error": str(response.errors)}), HTTPStatus.BAD_REQUEST
    return jsonify(), HTTPStatus.NO_CONTENT


@admin.route("/magazines/check_articles/<uid>")
@is_admin
def check_magazine_article(uid):
    logging.debug(f"Request to check magazine article: {uid}")
    response = (CheckMagazineArticles(magazine_repository)).execute(uid)
    return jsonify(response.get_data('articles'))


@admin.route("/magazines/check_feed/<uid>")
@is_admin
def magazine_check_feed(uid):
    logging.debug(f"Request to refresh magazine articles: {uid}")
    response = (RefreshMagazinesEntries(magazine_repository, ArticleRepository(), ErrorRepository())).execute([uid], False)
    if response.has_error():
        return jsonify({"error": str(response.errors)}), HTTPStatus.BAD_REQUEST
    return jsonify(response.success_message), HTTPStatus.OK
