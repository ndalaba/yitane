import logging
from http import HTTPStatus

from flask import request, jsonify

from App.Domain.Article.Request.CategoryRequest import CategoryRequest
from App.Domain.Article.UseCase.Category.CreateCategory import CreateCategory
from App.Domain.Article.UseCase.Category.DeleteCategory import DeleteCategory
from App.Domain.Article.UseCase.Category.GetCategories import GetCategories
from App.Domain.Article.UseCase.Category.UpdateCategory import UpdateCategory
from App.Domain.ImageRequest import ImageRequest
from FlaskApp.src.helpers.auth import is_admin
from instance.config import UPLOAD_PATH
from . import admin
from ...repository.CategoryRepository import CategoryRepository

category_repository = CategoryRepository()


@admin.route('/categories')
@is_admin
def categories():
    logging.debug(f"Request to get all categories")
    response = (GetCategories(category_repository)).execute()
    return jsonify({'status': 1, 'categories': response.get_data('categories')})


@admin.route('/categories/add', methods = ['POST'])
@is_admin
def add_category():
    logging.debug(f"Request to add category: {request.form['name']}")
    category_request = CategoryRequest(name = request.form['name'])
    image_request = ImageRequest(image = request.form['image'], path = UPLOAD_PATH + 'categories/') if request.form['image'] else None
    response = (CreateCategory(category_repository)).execute(category_request, image_request)
    if response.has_error():
        return jsonify({'error': str(response.errors)}), HTTPStatus.BAD_REQUEST
    return jsonify(response.get_data('category')), HTTPStatus.OK


@admin.route('/categories/edit/<uid>', methods = ['GET', 'POST'])
@is_admin
def edit_category(uid):
    logging.debug(f"Request to edit category: {uid}")
    category_request = CategoryRequest(name = request.form['name'], uid = uid, id = request.form['id'])
    image_request = ImageRequest(image = request.form['image'], path = UPLOAD_PATH + 'categories/') if request.form['image'] else None
    response = (UpdateCategory(category_repository)).execute(category_request, image_request)
    if response.has_error():
        return jsonify({'error': str(response.errors)}), HTTPStatus.BAD_REQUEST
    return jsonify(response.get_data('category')), HTTPStatus.OK


@admin.route('/categories/delete/<uid>')
@is_admin
def delete_category(uid):
    logging.debug(f"Request to delete category: {uid}")
    response = (DeleteCategory(category_repository)).execute(uid)
    if response.has_error():
        return jsonify({'error': str(response.errors)}), HTTPStatus.BAD_REQUEST
    return jsonify(), HTTPStatus.NO_CONTENT
