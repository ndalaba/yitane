import logging
from http import HTTPStatus

from flask import request, jsonify

from App.Domain.Error.UseCase.ApplyErrorAction import ApplyErrorAction
from App.Domain.Error.UseCase.DeleteError import DeleteError
from App.Domain.Error.UseCase.GetErrors import GetErrors
from FlaskApp.src.helpers.auth import is_admin
from . import admin
from ...repository.ErrorRepository import ErrorRepository

error_repository = ErrorRepository()


@admin.route('/errors', methods = ['GET'])
@is_admin
def errors():
    logging.debug(f"Request to get error")
    page = request.args.get('page', 1, type = int)
    action = request.args.get('doaction') if request.args is not None else ""
    resolved = False
    if action == "Appliquer":
        todo = int(request.args.get('todo'))
        uids = request.args.getlist('uid')
        (ApplyErrorAction(error_repository)).execute(todo, uids)

    elif action == "Filtrer":
        response = (GetErrors(error_repository)).execute(resolved = request.args.get('resolved', resolved), page = page)
        return jsonify({'error': str(response.errors)}), HTTPStatus.BAD_REQUEST
    response = (GetErrors(error_repository)).execute(resolved = resolved, page = page)
    return jsonify(response.get_data('errors')), HTTPStatus.OK


@admin.route('/errors/delete/<uid>')
@is_admin
def delete_error(uid):
    logging.debug(f"Request to delete error: {uid}")
    response = (DeleteError(error_repository)).execute(uid)
    if response.has_error():
        return jsonify({'error': str(response.errors)}), HTTPStatus.BAD_REQUEST
    return jsonify(), HTTPStatus.NO_CONTENT
