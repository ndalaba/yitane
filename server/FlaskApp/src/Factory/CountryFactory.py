import factory
from faker import Faker

from FlaskApp import db
from FlaskApp.src.entities.Country import Country

fake = Faker()


class CountryFactory(factory.alchemy.SQLAlchemyModelFactory):
    name = fake.name()
    slug = fake.name()
    uid = factory.Sequence(lambda n: 'uid-%d' % n)
    created_at = fake.date_time()
    updated_at = fake.date_time()
    id = factory.Sequence(lambda n: '%d' % n)

    class Meta:
        model = Country
        sqlalchemy_session = db.session
