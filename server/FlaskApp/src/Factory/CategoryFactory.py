import factory
from faker import Faker

from FlaskApp import db
from FlaskApp.src.entities.Category import Category

fake = Faker()


class CategoryFactory(factory.alchemy.SQLAlchemyModelFactory):
    name = factory.Faker('name')
    slug = factory.Faker('name')
    uid = factory.Sequence(lambda n: 'uid-%d' % n)
    created_at = factory.Faker('date_time')
    updated_at = factory.Faker('date_time')
    id = factory.Sequence(lambda n: '%d' % n)

    class Meta:
        model = Category
        sqlalchemy_session = db.session
