from datetime import datetime

import factory
from faker import Faker

from FlaskApp import db
from FlaskApp.src.entities.Article import Article

fake = Faker()


class ArticleFactory(factory.alchemy.SQLAlchemyModelFactory):
    title = factory.Sequence(lambda n: fake.sentence() + '-%d' % n)
    slug = factory.Sequence(lambda n: fake.sentence() + '-%d' % n)
    summary = fake.text()
    tag_slug = fake.name()
    tag = fake.name()
    body = fake.text()
    link = factory.Sequence(lambda n: 'https://domaine%d.com' % n)
    guid = factory.Sequence(lambda n: 'https://domaine%d.com' % n)
    date_published = datetime.now()
    created_at = datetime.now()
    updated_at = datetime.now()
    id = factory.Sequence(lambda n: '%d' % n)
    uid = factory.Sequence(lambda n: 'uid-%d' % n)

    class Meta:
        model = Article
        sqlalchemy_session = db.session
