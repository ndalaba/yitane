from datetime import datetime

import factory
from faker import Faker

from FlaskApp import db
from FlaskApp.src.entities.Magazine import Magazine

fake = Faker()


class MagazineFactory(factory.alchemy.SQLAlchemyModelFactory):
    title = fake.company()
    slug = fake.company()
    feed = fake.domain_name()
    link = fake.domain_name()
    image = fake.image_url()
    uid = factory.Sequence(lambda n: 'uid-%d' % n)
    created_at = datetime.now()
    updated_at = datetime.now()
    id = factory.Sequence(lambda n: '%d' % n)

    class Meta:
        model = Magazine
        sqlalchemy_session = db.session
