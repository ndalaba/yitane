from flask_mail import Message

from App.Helper.Email import Mailer
from FlaskApp import mail


class Email(Mailer):

    def __init__(self, subject: str, sender: tuple, recipents: list, body: str):
        self.subject = subject
        self.sender = sender
        self.recipents = recipents
        self.body = body

    def send(self) -> bool:
        msg = Message(self.subject, sender = self.sender, recipients = self.recipents)
        msg.html = self.body
        try:
            mail.send(msg)
            return True
        except ConnectionError:
            return False
