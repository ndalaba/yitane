from functools import wraps

from flask import current_app, request, jsonify
from flask_login import current_user, login_user

import App.Helper.jwt_token as jwt
from FlaskApp.src.entities.User import User


def is_admin(func):
    @wraps(func)
    def decorated_view(*args, **kwargs):
        if not current_user and not current_user.is_admin:
            return current_app.login_manager.unauthorized()
        return func(*args, **kwargs)

    return decorated_view


def token_required(func):
    @wraps(func)
    def decorated_view(*args, **kwargs):
        token = request.args.get('token', '')
        if not token:
            token = request.headers.get("token")
        try:
            decoded = jwt.decode(token)
            user: User = User.get(decoded['uid'])
            if user is None:
                return jsonify({'message': "Token invalide"})
            login_user(user)
        except ValueError:
            pass
        return func(*args, **kwargs)

    return decorated_view


def authenticated(func):
    @wraps(func)
    def decorated_view(*args, **kwargs):
        token = request.args.get('token', '')
        if not token:
            token = request.headers.get("token")
        try:
            if token is not None:
                decoded = jwt.decode(token)
                user: User = User.get(decoded['uid'])
                if user is not None:
                    login_user(user)
        except ValueError:
            pass

        return func(*args, **kwargs)

    return decorated_view
