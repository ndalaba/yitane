from datetime import datetime

from sqlalchemy import Column, String, Text, Integer, ForeignKey, DateTime
from sqlalchemy.orm import relationship

from FlaskApp import db
from FlaskApp.src.entities.BaseEntity import BaseEntity


class Article(db.Model, BaseEntity):
    __tablename__ = "articles"
    id = Column(Integer, primary_key = True)
    title = Column(String(240), nullable = False, unique = True)
    tag = Column(String(190), nullable = False)
    youtube = Column(String(190), nullable = True)
    tag_slug = Column(String(200))
    body = Column(Text, nullable = False)
    summary = Column(Text, nullable = False)
    link = Column(Text, nullable = False)
    guid = Column(String(255), nullable = False, unique = True)
    author = Column(String(199), nullable = True)
    love = Column(Integer, default = 0)
    comments_count = Column(Integer, default = 0)
    vue = Column(Integer, default = 0)
    image = Column(Text, nullable = True)
    date_published = Column(DateTime)
    magazine_id = Column(Integer, ForeignKey('magazines.id'), nullable = False)

    magazine = relationship("Magazine", back_populates = "articles", lazy = "joined")
    lovers = relationship("User", secondary = "article_love", lazy = "dynamic")
    savers = relationship('User', secondary = "article_save", lazy = "dynamic")

    __table_args__ = (
        db.UniqueConstraint('magazine_id', 'guid', name = 'unique_constraint_magazine_guid'),
    )

    def __init__(self, title: str, slug: str, summary: str, tag: str, link: str, guid: str, date_published,
                 magazine_id: int, body: str, youtube: str = None, image: str = None, author: str = None, love: int = 0,
                 comments_count: int = 0, vue: int = 0, uid: str = None, tag_slug: str = None,
                 published: bool = False, created_at=None, updated_at=None, id: int = None):
        BaseEntity.__init__(self, uid, created_at, updated_at, published)

        self.title = title
        self.slug = slug
        self.youtube = youtube
        self.summary = summary
        self.tag_slug = tag_slug
        self.tag = tag
        self.body = body
        self.id = id
        self.link = link
        self.guid = guid
        self.author = author
        self.love = love
        self.comments_count = comments_count
        self.vue = vue
        self.image = image
        self.date_published = date_published
        self.magazine_id = magazine_id


class ArticleLove(db.Model):
    __tablename__ = "article_love"

    article_id = Column(Integer, ForeignKey('articles.id'), nullable = False, primary_key = True)
    user_id = Column(Integer, ForeignKey('users.id'), nullable = False, primary_key = True)
    created_at = Column(DateTime, default = datetime.utcnow)


class ArticleSave(db.Model):
    __tablename__ = "article_save"

    article_id = Column(Integer, ForeignKey('articles.id'), nullable = False, primary_key = True)
    user_id = Column(Integer, ForeignKey('users.id'), nullable = False, primary_key = True)
    created_at = Column(DateTime, default = datetime.utcnow)
