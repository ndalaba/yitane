from datetime import datetime

from sqlalchemy import Integer, String, Text, Column, ForeignKey, DateTime
from sqlalchemy.orm import relationship

from FlaskApp import db
from FlaskApp.src.entities import User


class Comment(db.Model):
    __tablename__ = "comments"

    id = Column(Integer, primary_key=True)
    commentable_id = Column(Integer, nullable=False)
    commentable_type = Column(String(100), nullable=False)
    content = Column(Text, nullable=False)
    created_at = Column(DateTime, default=datetime.utcnow)

    user_id = Column(Integer, ForeignKey('users.id'), nullable=False)
    user: User = relationship('User', back_populates="comments", lazy="joined")

    def __init__(self, **kwargs):
        if len(kwargs):
            for key, value in kwargs.items():
                setattr(self, key, value)

    def serialyze(self):
        return {
            'id': self.id,
            "content": self.content,
            "user": self.user.serialize(),
            "created_at": self.created_at
        }

    def save(self):
        db.session.add(self)
        db.session.commit()


class CommentableType():
    ARTICLE = "Article"
    VIDEO = "Video"
