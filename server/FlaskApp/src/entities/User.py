from datetime import datetime

from flask_login import UserMixin
from sqlalchemy import Column, Integer, String, Boolean, ForeignKey, DateTime, Text
from sqlalchemy.orm import relationship

from FlaskApp import db, login_manager
from FlaskApp.src.entities.Article import ArticleSave
from FlaskApp.src.entities.BaseEntity import BaseEntity
from FlaskApp.src.entities.Video import VideoSave


class User(db.Model, UserMixin, BaseEntity):
    __tablename__ = "users"

    id = Column(Integer, primary_key = True)
    name = Column(String(60))
    email = Column(String(60), unique = True, index = True)
    phone = Column(String(20), unique = True)
    role = Column(String(60))
    password_hash = Column(String(130))
    photo = Column(String(130), unique = True)
    confirmation_token = Column(String(190), unique = True)
    email_validated = Column(Boolean, default = False)
    country_id = Column(Integer, ForeignKey('countries.id'))
    last_login = Column(DateTime, nullable = True)

    oauth = relationship("OAuth", uselist = False, back_populates = "user", cascade = "all,delete")
    magazines = relationship('Magazine', secondary = "member_magazine", cascade = "all,delete")
    channels = relationship('Channel', secondary = "member_channel", cascade = "all,delete")
    categories = relationship('Category', secondary = "member_category", cascade = "all,delete")
    country = relationship('Country', back_populates = "users")

    loved = relationship('Article', secondary = 'article_love', cascade = "all,delete")
    saved = relationship('Article', secondary = "article_save", cascade = "all,delete", order_by = ArticleSave.created_at)
    loved_videos = relationship('Video', secondary = 'video_love', cascade = "all,delete")
    saved_videos = relationship('Video', secondary = "video_save", cascade = "all,delete", order_by = VideoSave.created_at)

    comments = relationship('Comment', back_populates = "user", lazy = "select", cascade = "all,delete")
    feeds = relationship('Feed', back_populates = "user", lazy = "select", cascade = "all,delete")

    def __init__(self, name: str, email: str, phone: str, role: str, country_id: int = None, photo: str = None,
                 uid: str = None, created_at=None, updated_at=None, published: bool = False):
        BaseEntity.__init__(self, uid, created_at, updated_at, published)
        self.name = name
        self.email = email
        self.phone = phone
        self.role = role
        self.country_id = country_id
        self.photo = photo


class OAuth(db.Model):
    __tablename__ = "oauths"
    id = Column(Integer, primary_key = True)
    provider = Column(String(50), nullable = False)
    created_at = Column(DateTime, default = datetime.utcnow)
    provider_user_id = Column(String(180), nullable = False)
    image = Column(Text, nullable = True)
    user_id = Column(Integer, ForeignKey(User.id))
    user = relationship(User)


@login_manager.user_loader
def load_user(user_id) -> User:
    return User.query.get(int(user_id))


def create_test_user():
    user = User(email = "dmn@dev-hoster.com", name = "Diallo N'Dalaba", role = "ROLE_ADMIN", photo = "e6ee8dbdfe635919.jpg")
    user.photo = "no-image.jpg"
    user.password = "azerty"
    user.save()
