from datetime import datetime

from sqlalchemy import Column, String, Text, Integer, ForeignKey, DateTime, SmallInteger
from sqlalchemy.orm import relationship

from FlaskApp import db
from FlaskApp.src.entities.BaseEntity import BaseEntity


class Channel(db.Model, BaseEntity):
    __tablename__ = "channels"

    id = Column(Integer, primary_key = True)
    link = Column(String(199), nullable = False, unique = True)
    channel_id = Column(String(199), nullable = False, unique = True)
    title = Column(String(199), nullable = False, unique = True)
    slug = Column(String(190), nullable = False, unique = True)
    description = Column(Text, nullable = True)
    image = Column(String(190), nullable = True)
    country_id = Column(Integer, ForeignKey('countries.id'), nullable = False)
    category_id = Column(Integer, ForeignKey('categories.id'), nullable = False)
    vue = Column(Integer, default = 0)
    last_build_date = Column(DateTime, default = datetime.utcnow)
    refresh_time = Column(SmallInteger, default = 24, nullable = True)

    videos = relationship('Video', back_populates = "channel", lazy = "select")
    country = relationship('Country', back_populates = "channels", lazy = "joined")
    category = relationship('Category', back_populates = "channels", lazy = "joined")
    members = relationship('User', secondary = "member_channel", lazy = "select")

    def __init__(self, link: str, channel_id: str, title: str, slug: str, country_id: int, category_id: int,
                 description: str = None, image: str = None, vue: int = 0, refresh_time: int = None,
                 uid: str = None, created_at=None, updated_at=None, published: bool = False, id: int = None):
        BaseEntity.__init__(self, uid, created_at, updated_at, published)
        self.id = id
        self.link = link
        self.title = title
        self.slug = slug
        self.country_id = country_id
        self.channel_id = channel_id
        self.category_id = category_id
        self.description = description
        self.image = image
        self.refresh_time = refresh_time
        self.vue = vue


class Memberchannel(db.Model):
    __tablename__ = "member_channel"
    member_id = Column(Integer, ForeignKey('users.id'), nullable = False, primary_key = True)
    channel_id = Column(Integer, ForeignKey("channels.id"), nullable = False, primary_key = True)
    created_at = Column(DateTime, default = datetime.utcnow)
