from datetime import datetime

from sqlalchemy import Column, String, Text, Integer, ForeignKey, DateTime
from sqlalchemy.orm import deferred, relationship

from FlaskApp import db
from FlaskApp.src.entities.BaseEntity import BaseEntity


class Video(db.Model, BaseEntity):
    __tablename__ = "videos"

    id = Column(Integer, primary_key = True)
    title = Column(String(240), nullable = False, unique = True)
    video_id = Column(String(190), nullable = False, unique = True)
    body = deferred(Column(Text, nullable = False))
    love = Column(Integer, default = 0)
    comments_count = Column(Integer, default = 0)
    vue = Column(Integer, default = 0)
    image = Column(Text, nullable = True)
    date_published = Column(DateTime)
    channel_id = Column(Integer, ForeignKey('channels.id'), nullable = False)

    channel = relationship("Channel", back_populates = "videos", lazy = "joined")
    lovers = relationship("User", secondary = "video_love", lazy = "dynamic")
    savers = relationship('User', secondary = "video_save", lazy = "dynamic")

    def __init__(self, title: str, video_id: str, body: str, channel_id: int, love: int = None, comments_count: int = None, vue: int = None, image: str = None,
                 date_published=None, uid: str = None, created_at=None, updated_at=None, published: bool = False, id: int = None):
        BaseEntity.__init__(self, uid, created_at, updated_at, published)
        self.id = id
        self.title = title
        self.channel_id = channel_id
        self.video_id = video_id
        self.body = body
        self.vue = vue
        self.love = love
        self.image = image
        self.date_published = date_published
        self.comments_count = comments_count


class VideoLove(db.Model):
    __tablename__ = "video_love"

    video_id = Column(Integer, ForeignKey('videos.id'), nullable = False, primary_key = True)
    user_id = Column(Integer, ForeignKey('users.id'), nullable = False, primary_key = True)
    created_at = Column(DateTime, default = datetime.utcnow)


class VideoSave(db.Model):
    __tablename__ = "video_save"

    video_id = Column(Integer, ForeignKey('videos.id'), nullable = False, primary_key = True)
    user_id = Column(Integer, ForeignKey('users.id'), nullable = False, primary_key = True)
    created_at = Column(DateTime, default = datetime.utcnow)
