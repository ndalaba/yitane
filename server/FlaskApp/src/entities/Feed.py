from datetime import datetime

from sqlalchemy import Integer, String, Column, ForeignKey, DateTime
from sqlalchemy.orm import relationship

from FlaskApp import db
from FlaskApp.src.entities import User


class Feed(db.Model):
    __tablename__ = "feeds"

    id = Column(Integer, primary_key = True)
    feedable_id = Column(Integer, nullable = False)
    feedable_type = Column(String(100), nullable = False)
    action = Column(String(100), nullable = False)
    created_at = Column(DateTime, default = datetime.utcnow)

    user_id = Column(Integer, ForeignKey('users.id'), nullable = False)
    user: User = relationship('User', back_populates = "feeds", lazy = "select")

    def __init__(self, **kwargs):
        if len(kwargs):
            for key, value in kwargs.items():
                setattr(self, key, value)
