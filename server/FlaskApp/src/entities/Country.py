from sqlalchemy import Column, String, Integer
from sqlalchemy.orm import relationship

from FlaskApp import db
from FlaskApp.src.entities.BaseEntity import BaseEntity


class Country(db.Model, BaseEntity):
    __tablename__ = "countries"
    id = Column(Integer, primary_key = True)
    name = Column(String(100), nullable = False)
    slug = Column(String(100), nullable = False, unique = True)
    image = Column(String(100), unique = True)

    magazines = relationship('Magazine', back_populates = "country")
    channels = relationship('Channel', back_populates = "country")
    users = relationship('User', back_populates = 'country')

    def __init__(self, name: str, slug: str, image: str = None,
                 uid: str = None, created_at=None, updated_at=None, published: bool = False, id: int = None):
        BaseEntity.__init__(self, uid, created_at, updated_at, published)
        self.id = id
        self.name = name
        self.slug = slug
        self.image = image
