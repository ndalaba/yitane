from datetime import datetime

from sqlalchemy import Column, String, DateTime, Boolean

from FlaskApp import db


class BaseEntity(object):
    query: db.session.query = None

    uid = Column(String(30), index = True, unique = True, nullable = False)
    created_at = Column(DateTime, default = datetime.utcnow)
    updated_at = Column(DateTime, default = datetime.utcnow)
    published = Column(Boolean, default = True)

    def __init__(self, uid: str = None, created_at=None, updated_at=None, published: bool = False):
        self.uid = uid
        self.created_at = created_at if created_at is not None else datetime.utcnow()
        self.published = published
        self.updated_at = updated_at if created_at is not None else datetime.utcnow()

    @classmethod
    def get_by_uid(cls, uid: str):
        return cls.query.filter_by(uid = uid).first_or_404()

    @classmethod
    def get(cls, uid: str):
        return cls.get_by_uid(uid)

    @staticmethod
    def bulk_save(objects):
        # stmt = cls.__table__.insert().prefix_with('IGNORE')
        db.session.prefix_with('IGNORE').bulk_save_objects(objects)
        db.session.commit()

    def save(self):
        db.session.add(self)
        db.session.commit()
        return self

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    @classmethod
    def remove_all_by_uid(cls, uids):
        db.session.query(cls).filter(cls.uid.in_(uids)).delete(synchronize_session = False)
        db.session.commit()

    @classmethod
    def publish_all_by_uid(cls, uids):
        db.session.query(cls).filter(cls.uid.in_(uids)).update({cls.published: True}, synchronize_session = False)
        db.session.commit()

    @classmethod
    def unpublish_all_by_uid(cls, uids):
        db.session.query(cls).filter(cls.uid.in_(uids)).update(dict(published = False), synchronize_session = False)
        db.session.commit()

    @classmethod
    def set_top(cls, uids):
        db.session.query(cls).filter(cls.uid.in_(uids)).update({cls.top: True}, synchronize_session = False)
        db.session.commit()

    @classmethod
    def set_normal(cls, uids):
        db.session.query(cls).filter(cls.uid.in_(uids)).update({cls.top: False}, synchronize_session = False)
        db.session.commit()

    @classmethod
    def remove_all_by_date_before(cls, _date):
        db.session.query(cls).filter(cls.date_published < _date).delete(synchronize_session = False)
        db.session.commit()
