from datetime import datetime

from sqlalchemy import Column, String, Integer, ForeignKey, DateTime
from sqlalchemy.orm import relationship

from FlaskApp import db
from FlaskApp.src.entities.BaseEntity import BaseEntity


class Category(db.Model, BaseEntity):
    __tablename__ = "categories"
    id = Column(Integer, primary_key = True)
    name = Column(String(100), nullable = False)
    slug = Column(String(100), nullable = True, unique = True)
    image = Column(String(190), nullable = True)

    magazines = relationship('Magazine', back_populates = "category")
    channels = relationship('Channel', back_populates = "category")
    members = relationship('User', secondary = "member_category")

    def __init__(self, name: str, slug: str, image: str = None,
                 uid: str = None, created_at=None, updated_at=None, published: bool = False, id: int = None):
        BaseEntity.__init__(self, uid, created_at, updated_at, published)
        self.id = id
        self.name = name
        self.slug = slug
        self.image = image


class MemberCategory(db.Model):
    __tablename__ = "member_category"
    member_id = Column(Integer, ForeignKey('users.id'), nullable = False, primary_key = True)
    category_id = Column(Integer, ForeignKey('categories.id'), nullable = False, primary_key = True)
    created_at = Column(DateTime, default = datetime.utcnow)
