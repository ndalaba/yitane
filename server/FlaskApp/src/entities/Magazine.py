from datetime import datetime

from sqlalchemy import Column, String, Text, Integer, ForeignKey, DateTime, Boolean
from sqlalchemy.orm import relationship

from FlaskApp import db
from FlaskApp.src.entities.BaseEntity import BaseEntity


class Magazine(db.Model, BaseEntity):
    __tablename__ = "magazines"

    id = Column(Integer, primary_key = True)
    link = Column(String(199), nullable = False, unique = True)
    feed = Column(String(199), nullable = False, unique = True)
    title = Column(String(199), nullable = False, unique = True)
    slug = Column(String(190), nullable = False, unique = True)
    description = Column(Text, nullable = True)
    image = Column(String(190), nullable = True)
    country_id = Column(Integer, ForeignKey('countries.id'), nullable = False)
    category_id = Column(Integer, ForeignKey('categories.id'), nullable = False)
    vue = Column(Integer, default = 0)
    top = Column(Boolean, default = False)
    image_tag = Column(String(199), nullable = True, default = None)
    body_tag = Column(Text, nullable = True, default = None)
    last_build_date = Column(DateTime, default = datetime.utcnow)
    tag_to_remove = Column(Text, nullable = True, default = None)

    articles = relationship('Article', back_populates = "magazine", lazy = "select")
    country = relationship('Country', back_populates = "magazines", lazy = "joined")
    category = relationship('Category', back_populates = "magazines", lazy = "joined")
    members = relationship('User', secondary = "member_magazine", lazy = "select")

    def __init__(self, link: str, feed: str, title: str, slug: str, image: str, country_id: int, category_id: int,
                 description: str = None, vue: int = 0, image_tag: str = None, id: int = None,
                 last_build_date=None, body_tag: str = None, tag_to_remove: str = None, top: bool = False,
                 uid: str = None, published: bool = False, created_at=None, updated_at=None):
        BaseEntity.__init__(self, uid, created_at, updated_at, published)
        self.slug = slug
        self.title = title
        self.link = link
        self.feed = feed
        self.top = top
        self.image = image
        self.country_id = country_id
        self.category_id = category_id
        self.description = description
        self.vue = vue
        self.image_tag = image_tag
        self.last_build_date = last_build_date
        self.body_tag = body_tag
        self.tag_to_remove = tag_to_remove
        self.id = id


class MemberMagazine(db.Model):
    __tablename__ = "member_magazine"
    member_id = Column(Integer, ForeignKey('users.id'), nullable = False, primary_key = True)
    magazine_id = Column(Integer, ForeignKey("magazines.id"), nullable = False, primary_key = True)
    created_at = Column(DateTime, default = datetime.utcnow)
