"""from FlaskApp import db
from FlaskApp.src.entities.BaseEntity import BaseEntity


class Tag(BaseEntity, db.Model):
    __tablename__ = "tags"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)

    articles = db.relationship('Article', secondary="article_tag")

    def __init__(self, **kwargs):
        BaseEntity.__init__(self)
        if len(kwargs):
            for key, value in kwargs.items():
                setattr(self, key, value)

    def __repr__(self):
        return self.name

    __table_args__ = (
        db.UniqueConstraint('name', name='unique_tag_name'),
    )


class ArticleTag(db.Model):
    __tablename__ = "article_tag"
    article_id = db.Column(db.Integer, db.ForeignKey('articles.id'), nullable=False, primary_key=True)
    tag_id = db.Column(db.Integer, db.ForeignKey('tags.id'), nullable=False, primary_key=True)
"""