from FlaskApp import db
from FlaskApp.src.entities.BaseEntity import BaseEntity


class Error(BaseEntity, db.Model):
    PER_PAGE = 12

    __tablename__ = "errors"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    file = db.Column(db.String(100))
    line = db.Column(db.String(100))
    description = db.Column(db.Text())
    resolved = db.Column(db.Boolean(), default=False)

    __table_args__ = (
        db.UniqueConstraint('name', 'file', 'line', name='unique_constraint_error'),
    )

    def __init__(self, name: str, file: str, line: str, description: str, resolved=False,
                 uid: str = None, created_at=None, updated_at=None, published: bool = False):
        BaseEntity.__init__(self, uid, created_at, updated_at, published)
        self.name = name
        self.file = file
        self.line = line
        self.description = description
        self.resolved = resolved
