from typing import List

from sqlalchemy import func, or_

from App.Domain.Video.Entity.Channel import Channel
from App.Domain.Video.Repository.ChannelRepositoryInterface import ChannelRepositoryInterface
from FlaskApp.src.entities.Channel import Channel as Model
from FlaskApp.src.entities.Video import Video as VideoModel
from FlaskApp.src.repository.CategoryRepository import CategoryRepository
from FlaskApp.src.repository.CountryRepository import CountryRepository


class ChannelRepository(ChannelRepositoryInterface):

    def find_channel(self, title: str, link: str, channel_id: str, image: str) -> Channel:
        model = Model.query.filter_by(or_(title = title, link = link, channel_id = channel_id, image = image)).first()
        return self.model_to_entity(model)

    def all(self) -> List[Channel]:
        models = Model.query.all()
        return [self.model_to_entity(model) for model in models]

    def videos_count(self, channel: Channel) -> int:
        return VideoModel.query(func.count(Model.id)).filter_by(channel_id = channel.id).scalar()

    @staticmethod
    def get_by_id(id: int) -> Channel:
        model = Model.query.filter_by(id = id).first()
        return ChannelRepository.model_to_entity(model)

    def get(self, uid) -> Channel:
        return self.model_to_entity(Model.get_by_uid(uid))

    def create(self, channel: Channel) -> Channel:
        model = Model(*vars(channel))
        return self.model_to_entity(model.save())

    def update(self, channel: Channel) -> Channel:
        model: Model = Model.get_by_uid(channel.uid)
        model.link = channel.link
        model.title = channel.title
        model.slug = channel.slug
        model.image = channel.image
        model.country_id = channel.country_id
        model.category_id = channel.category_id
        model.last_build_date = channel.last_build_date
        model.published = channel.published
        model.description = channel.description
        model.refresh_time = channel.refresh_time
        model.vue = channel.vue
        model.channel_id = channel.channel_id
        model.save()
        return channel

    def delete(self, uid: str) -> None:
        Model.get_by_uid(uid).delete()

    def filter(self, title: str = None, country_id: int = None, category_id: int = None, published: bool = None) -> \
            List[Channel]:
        query = Model.query
        query = query.filter(Model.title.like('%' + title + '%')) if title is not None and title != "" else query
        query = query.filter(Model.published == int(published)) if published is not None else query
        query = query.filter(Model.category_id == category_id) if category_id is not None else query
        query = query.filter(Model.country_id == country_id) if country_id is not None else query

        models = query.order_by(Model.created_at.desc()).all()
        return [self.model_to_entity(model) for model in models]

    @classmethod
    def model_to_entity(cls, model: Model) -> Channel:
        return Channel(link = model.link, channel_id = model.channel_id, title = model.title, country_id = model.country_id, category_id = model.category_id,
                       description = model.description, image = model.image, vue = model.vue, last_build_date = model.last_build_date, refresh_time = model.refresh_time,
                       published = model.published, created_at = model.created_at, updated_at = model.updated_at, id = model.id, uid = model.uid,
                       category = CategoryRepository.model_to_entity(model.category),
                       country = CountryRepository.model_to_entity(model.country))

    def remove_many_by_uids(self, uids: List[str]) -> None:
        Model.remove_all_by_uid(uids)

    def publish_all_by_uids(self, uids: List[str]) -> None:
        Model.publish_all_by_uid(uids)

    def unpublish_all_by_uids(self, uids: List[str]) -> None:
        Model.unpublish_all_by_uid(uids)

    def find_many_by_uids(self, uids: List[str]) -> List[Channel]:
        models = Model.query.filter(Model.uid.in_(uids)).all()
        return [self.model_to_entity(model) for model in models]

    def channels_count(self, published: bool = True) -> int:
        return Model.query(func.count(Model.id)).filter_by(published = published).scalar()
