from typing import List, Any

from sqlalchemy import func

from App.Domain.Article.Entity.Category import Category
from App.Domain.Article.Repository.CategoryRepositoryInterface import CategoryRepositoryInterface
from FlaskApp.src.entities.Category import Category as Model
from FlaskApp.src.entities.Channel import Channel as ChannelModel
from FlaskApp.src.entities.Magazine import Magazine as MagazineModel


class CategoryRepository(CategoryRepositoryInterface):

    def categories_count(self) -> int:
        return Model.query(func.count(Model.id)).scalar()

    def find_by(self, field: str, value: Any) -> Category:
        model = Model.query.filter_by(field = value).first()
        return self.model_to_entity(model)

    def get(self, uid: str) -> Category:
        return self.model_to_entity(Model.get_by_uid(uid))

    def create(self, category: Category) -> Category:
        model = Model(name = category.name, slug = category.slug, image = category.image, published = category.published, uid = category.uid)
        return CategoryRepository.model_to_entity(model.save())

    def update(self, category: Category) -> Category:
        model = Model.get_by_uid(category.uid)
        model.name = category.name
        model.slug = category.slug
        model.image = category.image
        model.published = category.published
        model.save()
        return category

    def delete(self, uid: str) -> None:
        Model.get_by_uid(uid).delete()

    def all(self) -> List[Category]:
        models = Model.query.filter_by(published=True).order_by(Model.name.asc()).all()
        return [CategoryRepository.model_to_entity(model) for model in models]

    def channels_count(self, category: Category) -> int:
        return ChannelModel.query(func.count(ChannelModel.id)).filter_by(category_id = category.id).scalar()

    def magazines_count(self, category: Category) -> int:
        return MagazineModel.query(func.count(MagazineModel.id)).filter_by(category_id = category.id).scalar()

    @staticmethod
    def model_to_entity(model: Model) -> Category:
        return Category(name = model.name, image = model.image, id = model.id, uid = model.uid, published = model.published,
                        created_at = model.created_at, updated_at = model.updated_at, members = model.members)
