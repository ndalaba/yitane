from typing import List, Any

from sqlalchemy import func, or_

from App.Domain.Article.Entity.Magazine import Magazine
from App.Domain.Article.Repository.MagazineRepositoryInterface import MagazineRepositoryInterface
from App.Domain.BaseEntity import BaseEntity
from FlaskApp.src.entities.Article import Article as ArticleModel
from FlaskApp.src.entities.Magazine import Magazine as Model
from FlaskApp.src.repository.CategoryRepository import CategoryRepository
from FlaskApp.src.repository.CountryRepository import CountryRepository


class MagazineRepository(MagazineRepositoryInterface):

    def find_magazine(self, title: str, link: str, feed: str, image: str):
        model = Model.query.filter_by(or_(title = title, link = link, feed = feed, image = image)).first()
        return self.model_to_entity(model)

    def all(self) -> List[Magazine]:
        models = Model.query.order_by(Model.title.asc()).all()
        return [self.model_to_entity(model) for model in models.items]

    def articles_count(self, magazine: Magazine) -> int:
        return ArticleModel.query(func.count(ArticleModel.id)).filter_by(magazine_id = magazine.id).scalar()

    @staticmethod
    def get_by_id(id: int) -> Magazine:
        model = Model.query.filter_by(id = id).first()
        return MagazineRepository.model_to_entity(model)

    def get(self, uid: str) -> Magazine:
        return self.model_to_entity(Model.get_by_uid(uid))

    def filter(self, title: str = None, top: bool = None, published: bool = None, country_id: int = None,
               category_id: int = None, page: int = 1, size=BaseEntity.PER_PAGE) -> List[Magazine]:
        query = Model.query
        query = query.filter(Model.title.like('%' + title + '%')) if title is not None and title != "" else query
        query = query.filter(Model.top == int(top)) if top is not None else query
        query = query.filter(Model.published == int(published)) if published is not None else query
        query = query.filter(Model.category_id == category_id) if category_id is not None else query
        query = query.filter(Model.country_id == country_id) if country_id is not None else query

        models = query.order_by(Model.created_at.desc()).paginate(page, size, False)

        return [self.model_to_entity(model) for model in models.items]

    def create(self, magazine: Magazine) -> Magazine:
        model = Model(title = magazine.title, country_id = magazine.country_id, category_id = magazine.category_id,
                      link = magazine.link, feed = magazine.feed, description = magazine.description, image = magazine.image)
        model.published = magazine.published
        model.image_tag = magazine.image_tag
        model.body_tag = magazine.body_tag
        model.tag_to_remove = magazine.tag_to_remove
        model.top = magazine.top
        return self.model_to_entity(model.save())

    def update(self, magazine: Magazine) -> Magazine:
        model: Model = Model.query.filter_by(uid = magazine.uid).first()
        model.title = magazine.title
        model.feed = magazine.feed
        model.image = magazine.image
        model.link = magazine.link
        model.top = magazine.top
        model.image_tag = magazine.image_tag
        model.body_tag = magazine.body_tag
        model.tag_to_remove = magazine.tag_to_remove
        model.slug = magazine.slug
        model.description = magazine.description
        model.category_id = magazine.category_id
        model.country_id = magazine.country_id
        model.published = magazine.published
        model.save()
        return magazine

    def find_many_by_uids(self, uids: List[str]) -> List[Magazine]:
        models = Model.query.filter(Model.uid.in_(uids)).all()
        return [self.model_to_entity(model) for model in models]

    def remove_many_by_uids(self, uids: List[str]) -> None:
        Model.remove_all_by_uid(uids)

    def publish_all_by_uids(self, uids: List[str]) -> None:
        Model.publish_all_by_uid(uids)

    def unpublish_all_by_uids(self, uids: List[str]) -> None:
        Model.unpublish_all_by_uid(uids)

    def set_top(self, uids: List[str]) -> None:
        Model.remove_all_by_uid(uids)

    def set_normal(self, uids: List[str]) -> None:
        Model.publish_all_by_uid(uids)

    def delete(self, uid: str) -> None:
        Model.get_by_uid(uid).delete()

    def magazines_count(self, published: bool = True) -> int:
        return Model.query(func.count(Model.id)).filter_by(published = published).scalar()

    @staticmethod
    def model_to_entity(model: Model) -> Magazine:
        return Magazine(link = model.link, feed = model.feed, title = model.title, image = model.image,
                        country_id = model.country_id, category_id = model.category_id, description = model.description,
                        vue = model.vue, image_tag = model.image_tag, last_build_date = model.last_build_date,
                        body_tag = model.body_tag, tag_to_remove = model.tag_to_remove, top = model.top,
                        category = CategoryRepository.model_to_entity(model.category),
                        country = CountryRepository.model_to_entity(model.country), updated_at = model.updated_at,
                        uid = model.uid, id = model.id, created_at = model.created_at, published = model.published)
