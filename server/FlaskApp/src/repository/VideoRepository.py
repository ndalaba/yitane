from datetime import datetime, timedelta
from typing import List

from sqlalchemy import func

from App.Domain.BaseEntity import BaseEntity
from App.Domain.User.Entity import User
from App.Domain.Video.Entity.Video import Video
from App.Domain.Video.Repository.VideoRepositoryInterface import VideoRepositoryInterface
from FlaskApp import db
from FlaskApp.src.entities.Category import Category as CategoryModel
from FlaskApp.src.entities.Channel import Channel as ChannelModel
from FlaskApp.src.entities.User import User as UserModel
from FlaskApp.src.entities.Video import Video as Model
from FlaskApp.src.repository.ChannelRepository import ChannelRepository


class VideoRepository(VideoRepositoryInterface):

    def related_videos(self, video: Video) -> List[Video]:
        query = Model.query.filter_by(published = True)
        query = query.join(Model.channel).filter(ChannelModel.category_id == video.channel.category_id)
        query = query.filter(Model.uid != video.uid)
        models = query.order_by(Model.date_published.desc()).paginate(1, BaseEntity.PER_PAGE, False)
        return [self.model_to_entity(model) for model in models.items]

    def get(self, uid) -> Video:
        return self.model_to_entity(Model.get_by_uid(uid))

    def insert(self, videos: List[dict]) -> None:
        stmt = Model.__table__.insert().prefix_with('IGNORE')
        db.engine.execute(stmt, videos)

    def create(self, video: Video) -> Video:
        model = Model(*vars(video))
        return self.model_to_entity(model.save())

    def update(self, video: Video) -> Video:
        model: Model = Model.get_by_uid(video.uid)
        model.title = video.title
        model.image = video.image
        model.body = video.body
        model.channel_id = video.channel_id
        model.vue = video.vue
        model.published = video.published
        model.video_id = video.video_id
        model.date_published = video.date_published
        model.save()
        return video

    def delete(self, uid: str) -> None:
        Model.get_by_uid(uid).delete()

    def filter(self, title: str = None, country_id: int = None, category_id: int = None, channel_id: int = None,
               published: bool = None, date=None, order: str = None, page=1, size=BaseEntity.PER_PAGE) -> List[Video]:
        query = Model.query
        query = query.filter(Model.title.like('%' + title + '%')) if title is not None and title != "" else query
        query = query.filter(Model.published == int(published)) if published is not None else query
        query = query.filter(Model.channel_id == channel_id) if channel_id is not None else query
        query = query.filter(Model.date_published <= date) if date is not None and date.strip() else query

        if category_id is not None:
            query = query.filter(Model.channel.has(category = CategoryModel(id = category_id)))

        order_array = order.split('-') if order is not None else []
        if len(order_array) == 2:
            field = getattr(Model, order_array[0])
            direction = order_array[1]
            if direction == "desc":
                query = query.order_by(field.desc())
            else:
                query = query.order_by(order)
        models = query.paginate(page, size, False)
        return [self.model_to_entity(model) for model in models.items]

    def trend_videos(self, hours=24, size: int = BaseEntity.PER_PAGE) -> List[Video]:
        since = datetime.now() - timedelta(hours = hours)
        query = Model.query.filter_by(published = True)
        models = query.filter(Model.date_published > since).order_by(Model.vue.desc()).paginate(1, size, False)
        return [self.model_to_entity(model) for model in models.items]

    def get_saved_videos(self, uid: str, page=1, size=BaseEntity.PER_PAGE) -> List[Video]:
        '''query = db.session.query(Model, UserModel) \
            .filter(UserModel.uid == uid) \
            .filter(VideoSave.video_id == Model.id, UserModel.id == VideoSave.user_id)
        models = query.order_by(VideoSave.created_at.desc()).paginate(page, size, False)'''
        user_model: UserModel = Model.query.filter_by(uid = uid).first()
        models = user_model.saved_videos
        return [self.model_to_entity(model) for model in models]

    def get_loved_videos(self, uid: str, page=1, size=BaseEntity.PER_PAGE) -> List[Video]:
        '''query = db.session.query(Model, UserModel) \
            .filter(UserModel.uid == uid) \
            .filter(VideoLove.video_id == Model.id, UserModel.id == VideoLove.user_id)
        models = query.order_by(VideoLove.created_at.desc()).paginate(page, size, False)'''
        user_model: UserModel = UserModel.query.filter_by(uid = uid).first()
        models = user_model.loved_videos
        return [self.model_to_entity(model) for model in models]

    def get_related_videos(self, video: Video) -> List[Video]:
        query = Model.query.filter_by(published = True)
        query = query.join(Model.channel).filter(ChannelModel.category_id == video.channel.category_id)
        # query = query.filter(Video.channel_id != video.channel_id, Video.uid != video.uid)
        query = query.filter(Model.uid != video.uid)
        models = query.order_by(Model.date_published.desc()).paginate(1, BaseEntity.PER_PAGE, False)
        return [self.model_to_entity(model) for model in models.items]

    def remove_all_by_date_before(self, _date) -> None:
        Model.remove_all_by_date_before(_date)

    def remove_all_by_uid(self, uids: List[str]) -> None:
        Model.remove_all_by_uid(uids)

    def publish_all_by_uid(self, uids: List[str]) -> None:
        Model.publish_all_by_uid(uids)

    def unpublish_all_by_uid(self, uids: List[str]) -> None:
        Model.unpublish_all_by_uid(uids)

    def member_prefered_videos(self, user: User, page=1, size=BaseEntity.PER_PAGE) -> List[Video]:
        magazines_ids = [m.id for m in user.channels]
        query = Model.query.filter_by(published = True)
        if len(magazines_ids):
            query = query.filter(Model.channel_id.in_(magazines_ids))
        models = query.order_by(Model.date_published.desc()).paginate(page, size, False)
        return [self.model_to_entity(model) for model in models.items]

    def videos_count(self, published: bool = True) -> int:
        return Model.query(func.count(Model.id)).filter_by(published = published).scalar()

    @classmethod
    def model_to_entity(cls, model: Model) -> Video:
        video = Video(title = model.title, video_id = model.video_id, body = model.body, channel_id = model.channel_id, date_published = model.date_published,
                      love = model.love, comments_count = model.comments_count, image = model.image, vue = model.vue, uid = model.uid, published = model.published, created_at = model.created_at,
                      updated_at = model.updated_at, channel = ChannelRepository.model_to_entity(model.channel))
        return video

    @staticmethod
    def get_by_id(id) -> Video:
        model = Model.query.filter_by(id = id).first()
        return VideoRepository.model_to_entity(model)
