from typing import List

from App.Domain.BaseEntity import BaseEntity
from App.Domain.Comment.Entity.Comment import Comment
from App.Domain.Comment.Repository.CommentRepositoryInterface import CommentRepositoryInterface
from FlaskApp.src.entities.Comment import Comment as Model


class CommentRepository(CommentRepositoryInterface):

    def get(self, id: int) -> Comment:
        model = Model.query.filter_by(id = id).first_or_404()
        return self.model_to_entity(model)

    @staticmethod
    def get_by_id(id: int) -> Comment:
        model = Model.query.filter_by(id = id).first_or_404()
        return CommentRepository.model_to_entity(model)

    def create(self, comment: Comment) -> Comment:
        model = Model(*vars(comment))
        return self.model_to_entity(model.save())

    def update(self, comment: Comment) -> Comment:
        model: Model = Model.query.filter_by(id = comment.id).first_or_404()
        model.content = comment.content
        model.commentable_id = comment.commentable_id
        model.commentable_type = comment.commentable_type
        model.save()
        return comment

    def delete(self, uid: str) -> None:
        model = Model.query.filter_by(id = id).first_or_404()
        model.delete()

    def get_comments(self, commentable_type: str, commentable_id: int, page: int = 1,
                     size: int = BaseEntity.PER_PAGE) -> List[Comment]:
        query = Model.query
        query = query.filter_by(commentable_type = commentable_type) if commentable_type is not None else query
        query = query.filter_by(commentable_id = commentable_id) if commentable_id is not None else query

        models = query.order_by(Model.created_at.desc()).paginate(page, size, False)

        return [self.model_to_entity(model) for model in models.items]

    @classmethod
    def model_to_entity(cls, model: Model) -> Comment:
        return Comment(*vars(model))
