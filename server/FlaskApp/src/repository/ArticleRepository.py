from datetime import timedelta, datetime
from typing import List

from sqlalchemy import or_, func

from App.Domain.Article.Entity.Article import Article
from App.Domain.Article.Repository.ArticleRepositoryInterface import ArticleRepositoryInterface
from App.Domain.BaseEntity import BaseEntity
from App.Domain.Localisation.Entity.Country import Country
from App.Domain.User.Entity.User import User
from FlaskApp import db
from FlaskApp.src.entities.Article import Article as Model, ArticleSave, ArticleLove
from FlaskApp.src.entities.Category import Category as CategoryModel
from FlaskApp.src.entities.Country import Country as CountryModel
from FlaskApp.src.entities.Magazine import Magazine as MagazineModel
from FlaskApp.src.entities.User import User as UserModel
from FlaskApp.src.repository.MagazineRepository import MagazineRepository


class ArticleRepository(ArticleRepositoryInterface):

    def increment(self, article: Article) -> None:
        db.session.query(Model).filter(Model.uid == article.uid).update({Model.vue: article.vue}, synchronize_session = False)
        db.session.commit()

    def get(self, uid: str) -> Article:
        model = Model.query.filter_by(uid = uid).first_or_404()
        return self.model_to_entity(model)

    @staticmethod
    def get_by_id(id: int) -> Article:
        model = Model.query.filter_by(id = id).first_or_404()
        return ArticleRepository.model_to_entity(model)

    def insert(self, articles: List[dict]) -> None:
        stmt = Model.__table__.insert().prefix_with('IGNORE')
        db.engine.execute(stmt, articles)

    def create(self, article: Article) -> Article:
        model: Model = Model(*vars(article))
        return self.model_to_entity(model.save())

    def update(self, article: Article) -> Article:
        model: Model = Model.get_by_uid(article.uid)
        model.magazine_id = article.magazine_id
        model.image = article.image
        model.title = article.title
        model.tag_slug = article.tag_slug
        model.link = article.link
        model.tag = article.tag
        model.uid = article.uid
        model.guid = article.guid
        model.published = article.published
        model.body = article.body
        model.author = article.author
        model.summary = article.summary
        model.youtube = article.youtube
        model.save()
        return article

    def delete(self, uid: str) -> None:
        Model.get_by_uid(uid).delete()

    def get_trend_articles(self, hours=24, size=BaseEntity.PER_PAGE) -> List[Article]:
        since = datetime.now() - timedelta(hours = hours)
        query = Model.query.filter_by(published = True)
        models = query.filter(Model.date_published > since).order_by(Model.vue.desc()).paginate(1, size, False)
        return [self.model_to_entity(model) for model in models.items]

    def filter(self, title: str = None, top: bool = None, published: bool = None, magazine_id: int = None,
               category_id: int = None, country: Country = None, date=None, order: str = 'date_published-desc',
               page=1, size=BaseEntity.PER_PAGE) -> List[Article]:
        query = Model.query
        query = query.filter(Model.title.like('%' + title + '%')) if title is not None and title != "" else query
        query = query.filter(Model.published == int(published)) if published is not None else query
        query = query.filter(Model.magazine_id == magazine_id) if magazine_id is not None else query
        query = query.filter(Model.date_published <= date) if date is not None and date.strip() else query

        if country is not None:
            query = query.filter(or_(Model.magazine.has(country = CountryModel(*vars(country))), Model.title.like('%' + country.name + '%')))
        if category_id is not None:
            query = query.filter(Model.magazine.has(category = CategoryModel(id = category_id)))

        order_array = order.split('-')
        if len(order_array) == 2:
            field = getattr(Model, order_array[0])
            direction = order_array[1]
            if direction == "desc":
                query = query.order_by(field.desc())
            else:
                query = query.order_by(order)
        models = query.paginate(page, size, False)
        return [self.model_to_entity(model) for model in models.items]

    def get_saved_articles(self, uid: str, page=1, size=BaseEntity.PER_PAGE) -> List[Article]:
        query = db.session.query(Model, UserModel) \
            .filter(UserModel.uid == uid) \
            .filter(ArticleSave.article_id == Model.id, UserModel.id == ArticleSave.user_id)
        models = query.order_by(ArticleSave.created_at.desc()).paginate(page, size, False)
        return [self.model_to_entity(model) for model in models.items]

    def get_loved_articles(self, uid: str, page=1, size=BaseEntity.PER_PAGE) -> List[Article]:
        query = db.session.query(Model, UserModel) \
            .filter(UserModel.uid == uid) \
            .filter(ArticleLove.article_id == Model.id, UserModel.id == ArticleLove.user_id)
        models = query.order_by(ArticleLove.created_at.desc()).paginate(page, size, False)
        return [self.model_to_entity(model) for model in models.items]

    def find_member_prefered_article(self, user: User, page=1, size=BaseEntity.PER_PAGE) -> List[Article]:
        magazines_ids = [m.id for m in user.magazines]
        query = Model.query.filter_by(published = True)
        # categories_id = [c.id for c in member.categories]
        if len(magazines_ids):
            query = query.filter(Model.magazine_id.in_(magazines_ids))
        # if len(categories_id):
        # query = query.filter(Article.magazine.has(Magazine.category_id.in_(categories_id)))
        models = query.order_by(Model.date_published.desc()).paginate(page, size, False)
        return [self.model_to_entity(model) for model in models.items]

    def set_top(self, uids: List[str]) -> None:
        Model.set_top(uids)

    def set_normal(self, uids: List[str]) -> None:
        Model.set_normal(uids)

    def remove_all_by_date_before(self, _date) -> None:
        Model.remove_all_by_date_before(_date)

    def remove_all_by_uid(self, uids: List[str]) -> None:
        Model.remove_all_by_uid(uids)

    def publish_all_by_uid(self, uids: List[str]) -> None:
        Model.publish_all_by_uid(uids)

    def un_publish_all_by_uid(self, uids: List[str]) -> None:
        Model.unpublish_all_by_uid(uids)

    def articles_count(self, published: bool = True) -> int:
        return Model.query(func.count(Model.id)).filter_by(published = published).scalar()

    def get_related_articles(self, article: Article, size=24) -> List[Article]:
        query = Model.query.filter_by(published = True)

        if len(article.tags):
            tag = str(article.tags[0])
            query = query.join(Model.magazine).filter(MagazineModel.category_id == article.magazine.category_id)
            # query = query.filter(Article.tag.like('%' + tag + '%'), Article.magazine_id != article.magazine_id, Article.uid != article.uid)
            query = query.filter(Model.tag.like('%' + tag + '%'), Model.uid != article.uid)
            models = query.order_by(Model.date_published.desc())[0:size]
        else:
            query = query.join(Model.magazine).filter(MagazineModel.category_id == article.magazine.category_id)
            query = query.filter(Model.magazine_id != article.magazine_id, Model.uid != article.uid)
            models = query.order_by(Model.date_published.desc())[0:size]
        return [self.model_to_entity(model) for model in models]

    @staticmethod
    def model_to_entity(model: Model) -> Article:
        article = Article(title = model.title, summary = model.summary, tag = model.tag, link = model.link, guid = model.guid, date_published = model.date_published,
                          magazine_id = model.magazine_id, body = model.body, youtube = model.youtube, image = model.image, author = model.author,
                          love = model.love, comments_count = model.comments_count, vue = model.vue, uid = model.uid, published = model.published, created_at = model.created_at,
                          updated_at = model.updated_at, id = model.id)
        article.magazine = MagazineRepository.model_to_entity(model.magazine)
        return article
