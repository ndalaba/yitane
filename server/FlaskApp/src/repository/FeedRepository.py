from typing import Any, List

from App.Domain.BaseEntity import BaseEntity
from App.Domain.Feed.Entity.Feed import Feed
from App.Domain.Feed.Entity.Feed import FeedType
from App.Domain.Feed.Repository.FeedRepositoryInterface import FeedRepositoryInterface
from FlaskApp.src.entities.Feed import Feed as Model
from FlaskApp.src.repository.ArticleRepository import ArticleRepository
from FlaskApp.src.repository.ChannelRepository import ChannelRepository
from FlaskApp.src.repository.CommentRepository import CommentRepository
from FlaskApp.src.repository.MagazineRepository import MagazineRepository
from FlaskApp.src.repository.UserRepository import UserRepository
from FlaskApp.src.repository.VideoRepository import VideoRepository


class FeedRepository(FeedRepositoryInterface):

    def check_feed(self, feedable_id: int, feedable_type: str, action: str, user_id: int) -> Feed:
        model = Model.query.filter_by(user_id = user_id, feedable_id = feedable_id, feedable_type = feedable_type, action = action).first()
        return self.model_to_entity(model)

    def get_feeds(self, id: int, page=1, size: int = BaseEntity.PER_PAGE) -> List[Feed]:
        models = Model.query.filter_by(user_id = id).paginate(page, size, False)
        return [self.model_to_entity(model) for model in models.items]

    def find_by(self, field: str, value: Any) -> Feed:
        model = Model.query.filter_by(field = value).first()
        return self.model_to_entity(model)

    def create(self, feed: Feed) -> Feed:
        model = Model(*vars(feed))
        return self.model_to_entity(model.save())

    @classmethod
    def model_to_entity(cls, model: Model) -> Feed:
        feed = Feed(feedable_id = model.feedable_id, feedable_type = model.feedable_type, action = model.action,
                    user_id = model.user_id, created_at = model.created_at, id = model.id)
        feed.user = UserRepository.model_to_entity(model.user)

        if model.feedable_type == FeedType.ARTICLE:
            feed.article = ArticleRepository.get_by_id(model.feedable_id)
        if model.feedable_type == FeedType.VIDEO:
            feed.video = VideoRepository.get_by_id(model.feedable_id)
        if model.feedable_type == FeedType.COMMENT:
            feed.comment = CommentRepository.get_by_id(model.feedable_id)
        if model.feedable_type == FeedType.CHANNEL:
            feed.channel = ChannelRepository.get_by_id(model.feedable_id)
        if model.feedable_type == FeedType.MAGAZINE:
            feed.magazine = MagazineRepository.get_by_id(model.feedable_id)

        return feed
