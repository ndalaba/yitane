from typing import List

from sqlalchemy import func

from App.Domain.Article.Entity.Category import Category
from App.Domain.Article.Entity.Magazine import Magazine
from App.Domain.BaseEntity import BaseEntity
from App.Domain.User.Entity.User import User
from App.Domain.User.Repository.UserRepositoryInterface import UserRepositoryInterface
from App.Domain.Video.Entity.Channel import Channel
from FlaskApp import db
from FlaskApp.src.entities.Article import Article as ArticleModel
from FlaskApp.src.entities.Category import Category as CategoryModel
from FlaskApp.src.entities.Channel import Channel as ChannelModel
from FlaskApp.src.entities.Magazine import Magazine as MagazineModel
from FlaskApp.src.entities.User import User as Model, OAuth
from FlaskApp.src.entities.Video import Video as VideoModel
from FlaskApp.src.repository.CategoryRepository import CategoryRepository
from FlaskApp.src.repository.ChannelRepository import ChannelRepository
from FlaskApp.src.repository.CountryRepository import CountryRepository
from FlaskApp.src.repository.MagazineRepository import MagazineRepository


class UserRepository(UserRepositoryInterface):

    def find_by_email(self, value: str) -> User:
        model = Model.query.filter_by(email = value).first()
        return self.model_to_entity(model)

    def get(self, uid) -> User:
        return self.model_to_entity(Model.get_by_uid(uid))

    def create(self, user: User) -> User:
        model = Model(*user.__dict__)
        return self.model_to_entity(model.save())

    def update(self, user: User) -> User:
        model: Model = Model.get_by_uid(user.uid)
        model.published = user.published
        model.name = user.name
        model.photo = user.photo
        model.country_id = user.country_id
        model.last_login = user.last_login
        model.email = user.email
        model.confirmation_token = user.confirmation_token
        model.email_validated = user.email_validated
        model.save()
        return user

    def update_email_validation(self, user: User) -> User:
        Model.query.filter_by(uid = user.uid).update({'email_validated': user.email_validated}, synchronize_session = False)
        return user

    def update_single_field(self, field: str, value, uid: str) -> None:
        Model.query.filter_by(uid = uid).update({field: value}, synchronize_session = False)

    def update_password(self, user: User) -> User:
        Model.query.filter_by(uid = user.uid).update({'password_hash': user.password_hash}, synchronize_session = False)
        return user

    def delete(self, uid: str) -> None:
        model: Model = Model.get_by_uid(uid)
        oauth = OAuth.query.filter_by(user_id = model.id).first()
        if oauth is not None:
            db.session.delete(oauth)
        model.delete()

    def remove_many_by_uids(self, uids: List[str]) -> None:
        Model.remove_all_by_uid(uids)

    def users_count(self, role: str = User.ROLE_USER) -> int:
        return Model.query(func.count(Model.id)).filter_by(role = role).scalar()

    def update_magazines(self, user: User) -> None:
        model: Model = Model.get(user.uid)
        model.magazines = [MagazineModel(*vars(magazine)) for magazine in user.magazines]
        model.save()

    def update_channels(self, user: User) -> None:
        model: Model = Model.get(user.uid)
        model.channels = [ChannelModel(*vars(channel)) for channel in user.channels]
        model.save()

    def update_categories(self, user: User) -> None:
        model: Model = Model.get(user.uid)
        model.categories = [CategoryModel(*vars(category)) for category in user.categories]
        model.save()

    def update_loved_articles(self, user: User) -> None:
        model: Model = Model.get(user.uid)
        model.loved = [ArticleModel(*vars(article)) for article in user.loved_articles]
        model.save()

    def update_loved_videos(self, user: User) -> None:
        model: Model = Model.get(user.uid)
        model.loved_videos = [VideoModel(*vars(video)) for video in user.loved_videos]
        model.save()

    def update_saved_articles(self, user: User) -> None:
        model: Model = Model.get(user.uid)
        model.saved = [ArticleModel(*vars(article)) for article in user.saved_articles]
        model.save()

    def update_saved_videos(self, user: User) -> None:
        model: Model = Model.get(user.uid)
        model.saved_videos = [VideoModel(*vars(video)) for video in user.saved_videos]
        model.save()

    def filter(self, name: str = None, email: str = None, role: str = None, country_id: int = None, page=1, size=BaseEntity.PER_PAGE) -> List[User]:
        query = Model.query
        query = query.filter(Model.name.like('%' + name + '%')) if name is not None and name != "" else query
        query = query.filter(Model.email.like('%' + email + '%')) if email is not None and email != "" else query
        query = query.filter_by(role = role) if role is not None else query
        query = query.filter_by(country_id = country_id) if country_id is not None else query
        models = query.order_by(Model.created_at.desc()).paginate(page, size, False)

        return [self.model_to_entity(model) for model in models.items]

    def get_magazines(self, uid: str) -> List[Magazine]:
        model: Model = Model.get(uid)
        return [MagazineRepository.model_to_entity(magazine) for magazine in model.magazines]

    def get_channels(self, uid: str) -> List[Channel]:
        model: Model = Model.get(uid)
        return [ChannelRepository.model_to_entity(channel) for channel in model.channels]

    def get_categories(self, uid: str) -> List[Category]:
        model: Model = Model.get(uid)
        return [CategoryRepository.model_to_entity(category) for category in model.categories]

    @classmethod
    def model_to_entity(cls, model: Model) -> User:
        user = User(name = model.name, email = model.email, uid = model.uid, created_at = model.created_at, updated_at = model.updated_at, published = model.published,
                    id = model.id, role = model.role, country_id = model.country_id, confirmation_token = model.confirmation_token, email_validated = model.email_validated,
                    last_login = model.last_login, phone = model.phone, country = CountryRepository.model_to_entity(model.country), photo = model.photo, password_hash = model.password_hash)
        user.magazines = [MagazineRepository.model_to_entity(m) for m in model.magazines]
        user.channels = [ChannelRepository.model_to_entity(m) for m in model.channels]
        user.categories = [CategoryRepository.model_to_entity(m) for m in model.categories]
        return user

    @classmethod
    def entity_to_model(cls, entity: User) -> Model:
        model = Model(name = entity.name, email = entity.email, uid = entity.uid, created_at = entity.created_at, updated_at = entity.updated_at,
                      role = entity.role, country_id = entity.country_id, phone = entity.phone, published = entity.published)
        model.id = entity.id
        model.password_hash = entity.password_hash
        return model
