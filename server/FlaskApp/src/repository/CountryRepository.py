from typing import List, Any

from sqlalchemy import func

from App.Domain.Localisation.Entity.Country import Country
from App.Domain.Localisation.Repository.CountryRepositoryInterface import CountryRepositoryInterface
from FlaskApp.src.entities.Channel import Channel as ChannelModel
from FlaskApp.src.entities.Country import Country as Model
from FlaskApp.src.entities.Magazine import Magazine as MagazineModel
from FlaskApp.src.entities.User import User as UserModel


class CountryRepository(CountryRepositoryInterface):

    def magazines_count(self, country: Country) -> int:
        return MagazineModel.query(MagazineModel.id).filter_by(country_id = country.id).count()

    def channels_count(self, country: Country) -> int:
        return ChannelModel.query(func.count(ChannelModel.id)).filter_by(country_id = country.id).scalar()

    def users_count(self, country: Country) -> int:
        return UserModel.query(func.count(UserModel.id)).filter_by(country_id = country.id).scalar()

    def countries_count(self) -> int:
        return Model.query(func.count(Model.id)).scalar()

    def find_by(self, field: str, value: Any) -> Country:
        model = Model.query.filter_by(field = value).first()
        return self.model_to_entity(model)

    def get(self, uid: str) -> Country:
        return self.model_to_entity(Model.get_by_uid(uid))

    def create(self, country: Country) -> Country:
        model = Model(name = country.name, slug = country.slug, image = country.image,
                      published = country.published, uid = country.uid)
        return CountryRepository.model_to_entity(model.save())

    def update(self, country: Country) -> Country:
        model = Model.get_by_uid(country.uid)
        model.name = country.name
        model.slug = country.slug
        model.image = country.image
        model.published = country.published
        model.save()
        return country

    def delete(self, uid: str) -> None:
        Model.get_by_uid(uid).delete()

    def all(self) -> List[Country]:
        models = Model.query.order_by(Model.name.asc()).all()
        return [self.model_to_entity(model) for model in models]

    @staticmethod
    def model_to_entity(model: Model) -> Country:
        return Country(uid = model.uid, id = model.id, image = model.image, published = model.published,
                       created_at = model.created_at, updated_at = model.updated_at, name = model.name)
