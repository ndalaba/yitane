from typing import List, Any

from App.Domain.BaseEntity import BaseEntity
from App.Domain.Error.Entity.Error import Error
from App.Domain.Error.Repository.ErrorRepositoryInterface import ErrorRepositoryInterface
from FlaskApp import db
from FlaskApp.src.entities.Error import Error as Model


class ErrorRepository(ErrorRepositoryInterface):

    def find_by(self, field: str, value: Any) -> Error:
        model = Model.query.filter_by(field = value).first()
        return self.model_to_entity(model)

    def insert_ignore(self, errors: list[dict]):
        stmt = Model.__table__.insert().prefix_with('IGNORE')
        db.engine.execute(stmt, errors)

    def create(self, error: Error) -> Error:
        model = Model(*vars(error))
        return self.model_to_entity(model.save())

    def delete(self, uid: str) -> None:
        Model.get_by_uid(uid).delete()

    def filter(self, resolved: bool, page=1, size=BaseEntity.PER_PAGE) -> List[Error]:
        query = Model.query
        query = query.filter(Model.resolved == int(resolved)) if resolved is not None else query
        errors = query.order_by(Model.created_at.desc()).paginate(page, size, False)
        return [self.model_to_entity(error) for error in errors.items]

    def set_resolved(self, uids: List[str]):
        db.session.query(Model).filter(Model.uid.in_(uids)).update({Model.resolved: True}, synchronize_session = False)
        db.session.commit()

    def set_unresolved(self, uids: List[str]):
        db.session.query(Model).filter(Model.uid.in_(uids)).update({Model.resolved: False}, synchronize_session = False)
        db.session.commit()

    def remove_many_by_uids(self, uids: List[str]):
        Model.remove_all_by_uid(uids)

    @classmethod
    def model_to_entity(cls, model: Model) -> Error:
        return Error(*vars(model))
