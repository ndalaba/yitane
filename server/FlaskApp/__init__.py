import logging
import operator

from flask import Flask
from flask_cors import CORS
# from flask_debug_api import DebugAPIExtension
# from flask_debugtoolbar import DebugToolbarExtension
from flask_login import LoginManager
from flask_mail import Mail
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__, instance_relative_config = True)
app.config.from_pyfile("config.py")

db = SQLAlchemy(app)
Migrate(app, db)

# DebugToolbarExtension(app)
# DebugAPIExtension(app)
# Append Browse API Panel to Flask-DebugToolbar defaults
# (or add explicitly)
# config = app.config
# panels = list(config['DEBUG_TB_PANELS'])
# panels.append('flask_debug_api.BrowseAPIPanel')
# config['DEBUG_TB_PANELS'] = panels
# Optional: Change API prefix to custom prefix
# to filter routes in Browse API Panel
# Can leave '' to browse all routes
# (Default is /api)
# config['DEBUG_API_PREFIX'] = '/'

# Create the logfile
logging.basicConfig(filename = 'record.log', level = logging.DEBUG, format = f'%(asctime)s %(levelname)s %(name)s %(threadName)s : %(message)s')

mail = Mail(app)

CORS(app)

login_manager = LoginManager(app)
login_manager.login_message = "Veillez vous connecter."
login_manager.login_view = "auth.login"

from FlaskApp.src.controllers.auth import auth
from FlaskApp.src.controllers.admin import admin
from FlaskApp.src.controllers import api

app.register_blueprint(admin, url_prefix = "/admin")
app.register_blueprint(api)
app.register_blueprint(auth, url_prefix = "/auth")


@app.cli.command()
@app.get("/routes")
def routes():
    rules = []
    routes = []
    for rule in app.url_map.iter_rules():
        methods = ','.join(sorted(rule.methods))
        rules.append((rule.endpoint, methods, str(rule)))

    sort_by_rule = operator.itemgetter(2)
    for endpoint, methods, rule in sorted(rules, key = sort_by_rule):
        route = '{:50s} {:25s} {}'.format(endpoint, methods, rule)
        routes.append(route)
        print(route)
    return routes
# with app.app_context():
# if not current_app.config["DEBUG"]:
# import App.Helper.FeedThread
# import App.Helper.FeedThread
