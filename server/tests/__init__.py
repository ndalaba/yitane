import os
import tempfile

import pytest

from FlaskApp import app, db
from FlaskApp.src.Factory.CategoryFactory import CategoryFactory
from FlaskApp.src.Factory.CountryFactory import CountryFactory
from FlaskApp.src.Factory.MagazineFactory import MagazineFactory


@pytest.fixture(scope = "module")
def client():
    db_fd, app.config['DATABASE'] = tempfile.mkstemp()
    app.config['TESTING'] = True
    app.config['DEBUG'] = False
    app.config.update(
            # SQLALCHEMY_DATABASE_URI = 'mysql://root:thesniper@127.0.0.1:3307/db_paper'
            SQLALCHEMY_DATABASE_URI = 'sqlite:///test_db.sqlite'
    )

    client = app.test_client()
    db.create_all()
    yield client

    os.close(db_fd)
    os.unlink(app.config['DATABASE'])
    db.session.remove()
    db.drop_all()


@pytest.fixture(scope = 'module')
def country():
    country = CountryFactory.create()
    return country


@pytest.fixture(scope = 'module')
def category():
    category = CategoryFactory.create()
    return category


@pytest.fixture(scope = 'module')
def magazine():
    country = CountryFactory.create()
    category = CategoryFactory.create()
    magazine = MagazineFactory.create(category_id = category.id, country_id = country.id)
    return magazine
