from http import HTTPStatus

from FlaskApp.src.Factory.ArticleFactory import ArticleFactory
from .. import client, magazine


class TestArticleResource:

    def test_get_last_articles(self, client, magazine):
        ArticleFactory.create_batch(magazine_id = magazine.id, size = 3)

        response = client.get("/")
        print(response.data)
        assert response.status_code == HTTPStatus.OK

