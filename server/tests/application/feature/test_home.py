from .. import client


class TestHome:

    def test_admin_view(self, client):
        target_url = "admin/index"
        response = client.get(target_url)
        assert response.status_code == 302

    def test_home_page(self, client):
        response = client.get('/')
        assert response.status_code == 200
