from FlaskApp.src.entities import User
from .. import client


class TestRegistration:
    user = None

    def setup_method(self):
        self.user = User(email="dmn@dev-hoster.com", name="Diallo N'Dalaba", role=User.ROLE_USER, photo="no-image.jpg")
        self.user.password = "thesniper"

    def test_registration_page_work(self, client):
        target = "/registration"
        response = client.get(target)
        assert response.status_code == 200

    def test_add_user(self, client):
        response = client.post('/registration', {'name': self.user.name, 'email': self.user.email, 'password': self.user.password, 'role': self.user.role}, follow_redirects=True)
        assert response.status_code == 200
        created_user = User.query.filter(email="dmn@dev-hoster.com").first()
        assert created_user.name == self.user.name

    def test_user_can_fully_confirm_their_email_addresses(self,client):
        pass

    def test_confirming_an_invalid_token(self,client):
        pass
