from .. import client


class TestMembers:

    def test_members_page(self, client):
        response = client.get("admin/members")
        assert response.status_code == 200
        assert b"Liste membres" in response.data