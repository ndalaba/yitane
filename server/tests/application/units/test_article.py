import pytest

from FlaskApp.src.entities import Article, Magazine


class TestArticle:
    article = None

    def setup_method(self):
        magazine = Magazine(title="RFI")
        self.article = Article(title="mon titre")
        self.article.magazine = magazine

    def test_url(self):
        assert self.article.url == "/rfi/mon-titre/" + self.article.uid

    def test_tag(self):
        self.article.tag = "politique, sport"
        assert 2 == len(self.article.tags)
        self.article.tag = ""
        assert 0 == len(self.article.tags)
