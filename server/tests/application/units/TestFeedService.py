from FlaskApp.src.entities import Magazine
from FlaskApp.src.services.FeedService import feedService


class TestFeedService:
    magazine = None

    def setup_method(self):
        self.magazine = Magazine(feed="https://www.socialnetlink.org/feed/", title="Social")

    def test_magazine_has_entries(self):
        has_entries = feedService.check_feed(self.magazine)
        assert True == has_entries

    def test_parsed_feed(self):
        parsed = feedService.get_feed()
        assert self.magazine.title in parsed.title

    def test_has_entries(self):
        entries = feedService.get_articles(self.magazine)
        print(entries)
        assert len(entries) > 0
