
import re
import secrets

from slugify import slugify
from werkzeug.security import generate_password_hash as password_hash, check_password_hash as check_password


def check_password_hash(hashed_password: str, password: str) -> bool:
    return check_password(hashed_password, password)


def generate_password_hash(password: str) -> str:
    return password_hash(password)


def slug(string: str) -> str:
    return slugify(string)


def generate_uuid(length: int = 5) -> str:
    return secrets.token_hex(length)


#  Remove xml style tags from an input string.
#
#  @param string The input string.
#  @param allowed_tags A string to specify tags which should not be removed.
TAG_RE = re.compile(r'<[^>]+>')


# def strip_tags(text):
# return TAG_RE.sub('', text)


def strip_tag(page: str, tag) -> str:
    element = re.compile(r'<' + tag + '[\s\S]+?/' + tag + '>')
    return re.sub(element, "", page)


def strip_tags(page_source: str) -> str:
    pattern = re.compile(r'\s?on\w+="[^"]+"\s?')
    result = re.sub(pattern, "", page_source)

    result = strip_tag(result, 'script')
    result = strip_tag(result, 'style')
    # result = strip_tag(result, 'iframe')
    result = strip_tag(result, 'form')
    result = strip_tag(result, 'ins')
    # result = strip_tag(result, 'a')
    result = strip_tag(result, 'button')
    result = strip_tag(result, 'svg')

    # img = re.compile(r'<img[\s\S]+?/>')
    # result = re.sub(img, "", result)

    input = re.compile(r'<input[\s\S]+?/>')
    result = re.sub(input, "", result)
    return result
