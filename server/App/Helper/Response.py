from typing import Dict, Any


class Response:

    def __init__(self, data: Dict = None, success_message: str = "", errors: Dict = None) -> None:
        self.data = data
        self.success_message = success_message
        self.errors = errors

    def add_error(self, field_name: str, error: str):
        self.errors[field_name] = error
        return self

    def set_success_message(self, message: str):
        self.success_message = message
        return self

    def has_error(self) -> bool:
        return self.errors is not None and len(self.errors) > 0

    def get_data(self, field_name: str):
        return self.data[field_name]

    def add_data(self, field: str, value: Any):
        self.data[field] = value
        return self

    def has_data(self) -> bool:
        return len(self.data) > 0

    @property
    def datas(self) -> dict:
        return self.data
