import time
from threading import Thread

from sqlalchemy.sql.expression import func

from App.Domain.Article.UseCase.Magazine.RefreshMagazinesEntries import RefreshMagazinesEntries
from App.Domain.Video.UseCase.Channel.RefreshChannelsEntries import RefreshChannelsEntries
from FlaskApp import app
from FlaskApp.src.entities.Channel import Channel
from FlaskApp.src.entities.Magazine import Magazine
from FlaskApp.src.repository.ArticleRepository import ArticleRepository
from FlaskApp.src.repository.ChannelRepository import ChannelRepository
from FlaskApp.src.repository.ErrorRepository import ErrorRepository
from FlaskApp.src.repository.MagazineRepository import MagazineRepository
from FlaskApp.src.repository.VideoRepository import VideoRepository

INTERVAL = 20  # minutes


def update_loop():
    while True:
        with app.app_context():
            magazines = Magazine.query.filter_by(published = True).order_by(func.rand()).all()
            channels = Channel.query.filter_by(published = True).order_by(func.rand()).all()
            mag_uids = [mag.uid for mag in magazines]
            chan_uids = [chan.uid for chan in channels]
            try:
                (RefreshMagazinesEntries(MagazineRepository(), ArticleRepository(), ErrorRepository())).execute(mag_uids)
                (RefreshChannelsEntries(ChannelRepository(), VideoRepository())).execute(chan_uids)
            except BlockingIOError:
                continue
        time.sleep(60 * INTERVAL)


thread = Thread(target = update_loop)
thread.start()
