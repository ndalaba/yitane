import requests
from bs4 import BeautifulSoup, Comment

from App.Domain.Article.Entity.Magazine import Magazine
from App.Domain.Error.Entity.Error import Error
from App.Helper import Str
from App.Helper import Youtube


class Scraper:
    soup = None
    errors = []

    def get_content(self, url: str) -> None:
        page = None
        try:
            page = requests.get(
                    url,
                    timeout = 5,
                    headers = {
                        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'
                    })
        except (requests.exceptions.ConnectTimeout, requests.exceptions.ReadTimeout, requests.exceptions.ConnectionError) as e:
            print(str(e))
            pass

        if page is not None:
            self.soup = BeautifulSoup(page.content, 'lxml')

    def get_article(self, magazine: Magazine):
        image = self.__get_image(magazine.image_tag)
        body = self.__get_body(magazine)
        youtube = self.__get_youtube(body)
        summary = Str.strip_tags(self.__get_summary())
        return {
            'image': image,
            'body': Str.strip_tags(body),
            'summary': summary,
            'youtube': youtube
        }

    def __get_image(self, image_tag=None) -> str:
        if image_tag is None or not image_tag:
            image = self.soup.find("meta", property = "og:image")
            return image['content'] if image else ""
        else:
            image = self.soup.select_one(image_tag)  # if soup.find(image_tag) else None
            return image["src"] if image else None

    def __get_summary(self) -> str:
        description = self.soup.find('meta', property = "og:description")
        return description['content'] if description else ""

    def __get_body(self, magazine: Magazine):
        body = self.__get_summary()
        if magazine.body_tag is not None and len(magazine.body_tag.strip()):
            try:
                if magazine.tag_to_remove is not None and magazine.tag_to_remove.split():
                    tags_to_remove = magazine.tag_to_remove.split(',')
                    if len(tags_to_remove):
                        for tag in tags_to_remove:
                            try:
                                for curr in self.soup.select(tag):
                                    curr.decompose()
                            except Exception as err:
                                error = (Error(name = "soupsieve.util.SelectorSyntaxError: " + magazine.title,
                                               file = "helpers/BeautifulSoup",
                                               line = "64",
                                               resolved = False,
                                               description = str(err) + " => " + magazine.tag_to_remove)).serialize()
                                self.errors.append(error)
                                # error.insert_ignore()

                for element in self.soup(text = lambda text: isinstance(text, Comment)):
                    element.extract()

                description = self.soup.select_one(magazine.body_tag)

                if description is not None:
                    for tag in description():
                        for attribute in ["class", "id", "name", "style"]:
                            del tag[attribute]
                    body = u' ' + str(description)
            except Exception as err:
                error = (Error(name = "soupsieve.util.SelectorSyntaxError: " +
                                      magazine.title,
                               file = "helpers/BeautifulSoup",
                               line = "59",
                               resolved = False,
                               description = str(err) + " => " + magazine.body_tag)).serialize()
                self.errors.append(error)
        return body

    @staticmethod
    def __get_youtube(body):
        youtube_ids = Youtube.get_videoids(body)
        return ','.join(youtube_ids)
