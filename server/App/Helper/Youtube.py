import re


def get_videoids(soup):
    # title = soup('title')[0].string

    videoids = []
    try:
        for element in soup('embed'):
            src = element.get('src')
            if re.search('v\/([-\w]+)', src):
                videoids.append(re.search('v\/([-\w]+)', src).group(1))
        for element in soup('iframe'):
            src = element.get('src')
            if re.search('youtube.com\/embed\/', src):
                videoids.append(re.search('embed\/([-\w]+)', src).group(1))
        for element in soup('iframe'):
            src = element.get('src')
            if re.search('youtube.com\/embed\/', src):
                videoids.append(re.search('embed\/([-\w]+)', src).group(1))
        for element in soup('a'):
            href = element.get('href')
            if href and re.search('youtube.com\/watch\?v=([-\w]+)', href):
                videoids.append(re.search('youtube.com\/watch\?v=([-\w]+)', href).group(1))
            if href and re.search('youtu\.be\/([-\w]+)', href):
                videoids.append(re.search('youtu\.be\/([-\w]+)', href).group(1))
    except:
        pass
    # res = {}
    # res['title'] = title
    # res['videoids'] = list(set(videoids))
    return list(set(videoids))
    # return res
