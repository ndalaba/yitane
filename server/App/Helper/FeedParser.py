from datetime import datetime
from time import mktime
from typing import Tuple

import feedparser
import requests

from App.Domain.Article.Entity.Magazine import Magazine
from App.Domain.Error.Entity.Error import Error
from App.Helper.Scraper import Scraper
from App.Helper.Str import generate_uuid, slug, strip_tags

scraper = Scraper()


def parse(url: str):
    parsed = None
    page = None
    try:
        page = requests.get(
                url,
                timeout = 5,
                headers = {
                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'})
    except (requests.exceptions.ConnectTimeout, requests.exceptions.ReadTimeout, requests.exceptions.ConnectionError) as e:
        print(str(e))
        pass

    if page is not None:
        try:
            parsed = feedparser.parse(url)
            # parsed = feedparser.parse(page)
        except TypeError:
            print("Object of type SAXParseException is not JSON serializable")

    if parsed is not None:
        feed = parsed.feed
        items = parsed['entries']
        return {"feed": feed, "items": items}
    return None


def get_source(parsed):
    if 'link' in parsed:
        return {
            'link': parsed['link'],
            'title': parsed['title'],
            'subtitle': parsed['subtitle']
        }
    else:
        return None


def scrape_article(link, magazine):
    article = None
    try:
        scraper.get_content(link)
        if scraper.soup is not None:
            soup_article = scraper.get_article(magazine)
            article = {
                'link': link,
                'body': soup_article.get('body'),
                'summary': soup_article.get('summary'),
                'image': soup_article.get('image'),
                "youtube": soup_article.get('youtube')
            }
            '''if article['title'] != article['summary'] and len(article['title']) >= 30:
                article['tag_slug'] = slug(article['tag']
            '''
    except ValueError:
        pass
    return article


def get_articles(items, magazine: Magazine, get_content=False) -> Tuple[list[dict], list[dict]]:
    articles = []
    errors = scraper.errors
    if items is not None:
        for item in items:
            published_parsed = None
            if "published_parsed" in item:
                published_parsed = datetime.fromtimestamp(mktime(item['published_parsed']))
            if "updated_parsed" in item:
                published_parsed = datetime.fromtimestamp(mktime(item['updated_parsed']))

            if get_content:
                if 'link' in item:
                    article = scrape_article(item.link, magazine)
                    articles.append(article) if article is not None else None
            else:
                article = {
                    "uid": generate_uuid(5),
                    'guid': item['guid'] if "guid" in item else item['link'],
                    'link': item['link'],
                    'title': u'' + item['title'],
                    'body': strip_tags(item['summary']) if "summary" in item else "",
                    'summary': strip_tags(item['summary']) if "summary" in item else "",
                    'tag': get_tags(item),
                    'date_published': published_parsed,
                    'image': get_image(item),
                    'author': item['author'] if 'author' in item else None,
                    'magazine_id': magazine.id,
                }
                if article['title'] != article['summary'] and len(article['title']) >= 30:
                    article['tag_slug'] = slug(article['tag'])
                    articles.append(article)

        try:
            if articles[0]['summary'] == articles[0]['body'] and get_content:
                error = (Error(name = magazine.title, file = __file__, line = "50", resolved = False, description = "Boby content and summary content are same")).serialize()
                errors.append(error)
        except IndexError:
            pass

    return articles, errors


def get_tags(item) -> str:
    try:
        if 'category' in item:
            tags = [t.term for t in item.get('tags', [])]
            return ", ".join(tags[0:3])
        else:
            return ""
    except ValueError:
        return ""


def get_image(item) -> [None, str]:
    try:
        if 'media_thumbnail' in item:
            return item.media_thumbnail[0]['url']
        if 'enclosures' in item:
            return item.enclosures[0].href if len(item.enclosures) else None
    except ValueError:
        return None
