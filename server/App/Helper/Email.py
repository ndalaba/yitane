from abc import ABC, abstractmethod


class Mailer(ABC):

    @abstractmethod
    def send(self) -> bool:
        pass
