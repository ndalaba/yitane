import jwt

SECRET_KEY = "zmlkj@678TVJN+468709890"


def encode(uid):
    encoded = jwt.encode({'uid': uid}, SECRET_KEY, algorithm='HS256')
    return encoded.decode('utf-8')


def decode(encoded_jwt):
    decoded = jwt.decode(encoded_jwt, SECRET_KEY, algorithms=['HS256'])
    return decoded
