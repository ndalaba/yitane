import os
import secrets

import PIL
import requests
from PIL import Image

size = 200, 200
random_ex = secrets.token_hex(3)


def upload_image_from_url(url: str, path: str) -> str:
    filename = random_ex + ".jpg"
    image_path = os.path.join(path, filename)
    with open(image_path, 'wb') as handle:
        response = requests.get(url, stream=True)
        if not response.ok:
            return ""
        for block in response.iter_content(1024):
            if not block:
                break
            handle.write(block)
    return filename


def upload_image(image, path: str, basewidth=200) -> str:
    _name, f_ext = os.path.splitext(image.filename)
    filename = _name + "_" + random_ex + f_ext
    image_path = os.path.join(path, filename)
    img = Image.open(image)
    wpercent = (basewidth / float(img.size[0]))
    hsize = int((float(img.size[1]) * float(wpercent)))
    img = img.resize((basewidth, hsize), PIL.Image.ANTIALIAS)
    img.save(image_path)

    return filename


def remove_image(current_image=None, path: str = None) -> None:
    if current_image is not None and path is not None:
        image_path = os.path.join(path, current_image)
        os.remove(image_path) if os.path.exists(image_path) else None
