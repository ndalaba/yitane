from App.Domain.Article.Entity import Category
from App.Domain.Article.Entity.Article import Article
from App.Domain.Article.Entity.Magazine import Magazine
from App.Domain.BaseEntity import BaseEntity
from App.Domain.Localisation.Entity import Country
from App.Domain.Video.Entity.Channel import Channel
from App.Domain.Video.Entity.Video import Video
from App.Helper.Str import generate_password_hash, check_password_hash, generate_uuid


class User(BaseEntity):
    ROLE_ADMIN = "ROLE_ADMIN"
    ROLE_USER = "ROLE_USER"
    PER_PAGE = 12
    DEFAULT_IMAGE = "account.svg"

    password_hash: str
    magazines: list[Magazine]
    channels: list[Channel]
    categories: list[Category]
    saved_articles: list[Article]
    loved_articles: list[Article]
    loved_videos: list[Video]
    saved_videos: list[Video]
    is_authenticated: bool = False

    def __init__(self, id: int, name: str, email: str, role: str = None, password_hash: str = None, country_id: int = None, confirmation_token: str = None, email_validated: bool = False,
                 last_login=None, phone: str = None, country: Country = None, photo: str = None, uid: str = None, published: bool = False, created_at=None, updated_at=None):
        BaseEntity.__init__(self, uid, published, created_at, updated_at)
        self.id = id
        self.name = name
        self.email = email
        self.phone = phone
        self.role = role if role is not None else self.ROLE_USER
        self.password_hash = password_hash
        self.photo = photo
        self.country = country
        self.country_id = country_id
        self.confirmation_token = confirmation_token
        self.email_validated = email_validated
        self.last_login = last_login
        self.image_path = ""

    @property
    def password(self):
        raise AttributeError('Peut pas modifier directement le mot de passe')

    @password.setter
    def password(self, password: str):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password) -> bool:
        return check_password_hash(self.password_hash, password)

    @property
    def is_admin(self):
        return self.role == self.ROLE_ADMIN

    def __repr__(self) -> str:
        return "User: {}".format(self.name)

    def validate_email(self, token) -> bool:
        if self.confirmation_token == token:
            self.email_validated = True
            return True
        else:
            return False

    def generate_token(self) -> str:
        self.confirmation_token = generate_uuid(12)
        return self.confirmation_token

    def serialize(self):
        return {
            'id': self.id,
            'name': self.name,
            'uid': self.uid,
            'image': self.image_path,
            "created_at": self.created_at,
            'country': self.country.serialize() if self.country is not None else None
        }

    def serialize_all(self):
        return {
            'id': self.id,
            'name': self.name,
            'email': self.email,
            'phone': self.phone,
            'uid': self.uid,
            "created_at": self.created_at,
            'image': self.image_path,
            'is_authenticated': self.is_authenticated,
            'country': self.country.serialize() if self.country is not None else None
        }

    def has_role(self, role: str) -> bool:
        return self.role == role

    def set_image_path(self, path: str):
        if self.photo is not None:
            self.image_path = path + self.photo
        else:
            self.image_path = path + self.DEFAULT_IMAGE
