from abc import abstractmethod, ABC
from typing import List, Any

from App.Domain.Article.Entity.Category import Category
from App.Domain.Article.Entity.Magazine import Magazine
from App.Domain.BaseEntity import BaseEntity
from App.Domain.User.Entity.User import User
from App.Domain.Video.Entity.Channel import Channel


class UserRepositoryInterface(ABC):

    @abstractmethod
    def find_by_email(self, value: str) -> User:
        pass

    @abstractmethod
    def get(self, uid) -> User:
        pass

    @abstractmethod
    def create(self, user: User) -> User:
        pass

    @abstractmethod
    def update(self, user: User) -> User:
        pass

    @abstractmethod
    def update_password(self, user: User) -> User:
        pass

    @abstractmethod
    def update_email_validation(self, user: User) -> User:
        pass

    @abstractmethod
    def update_single_field(self, field: str, value, uid) -> None:
        pass

    @abstractmethod
    def delete(self, uid: str) -> None:
        pass

    @abstractmethod
    def filter(self, name: str = None, email: str = None, role: str = None, country_id: int = None, page=1, size=BaseEntity.PER_PAGE) -> list[User]:
        pass

    @abstractmethod
    def remove_many_by_uids(self, uids: list[str]) -> None:
        pass

    @abstractmethod
    def users_count(self, role: str = User.ROLE_USER) -> int:
        pass

    @abstractmethod
    def update_magazines(self, user: User) -> None:
        pass

    @abstractmethod
    def update_channels(self, user: User) -> None:
        pass

    @abstractmethod
    def update_categories(self, user: User) -> None:
        pass

    @abstractmethod
    def update_saved_articles(self, user: User) -> None:
        pass

    @abstractmethod
    def update_saved_videos(self, user: User) -> None:
        pass

    @abstractmethod
    def update_loved_articles(self, user: User) -> None:
        pass

    @abstractmethod
    def update_loved_videos(self, user: User) -> None:
        pass

    @abstractmethod
    def get_magazines(self, uid: str) -> list[Magazine]:
        pass

    @abstractmethod
    def get_channels(self, uid: str) -> list[Channel]:
        pass

    @abstractmethod
    def get_categories(self, uid: str) -> list[Category]:
        pass
