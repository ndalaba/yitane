import validators

from App.Domain.ImageRequest import ImageRequest
from App.Domain.User.Entity.User import User
from App.Domain.User.Repository.UserRepositoryInterface import UserRepositoryInterface
from App.Domain.User.Request.UserRequest import UserRequest
from App.Helper.Str import generate_uuid
from App.Helper.Response import Response
from App.Helper.upload import upload_image


class CreateUser:

    def __init__(self, user_repository: UserRepositoryInterface):
        self.user_repository = user_repository

    def execute(self, user_request: UserRequest, image_request: ImageRequest = None) -> Response:
        response = Response()
        is_valid = self.is_request_valid(user_request, response) and self.is_user_valid(user_request, response)
        if is_valid:
            user = User(*vars(user_request))
            user.confirmation_token = generate_uuid(10)
            if image_request is not None and image_request.image is not None and image_request.path is not None:
                filename = upload_image(image_request.image, image_request.path)
                user.photo = filename
            created_user = self.user_repository.create(user)
            response.add_data("user", created_user.serialize_all())

        return response

    @staticmethod
    def is_request_valid(request: UserRequest, response: Response) -> bool:

        if len(request.name.strip()) < 3:
            response.add_error("name", "Le nom de l'utilisateur doit être renseigné (3 caractères au minimum)")

        if not validators.email(request.email):
            response.add_error("email", "L'email de l'utilisateur doit être renseigné.")

        return response.has_error()

    def is_user_valid(self, user_request: UserRequest, response: Response) -> bool:
        user_exist = self.user_repository.find_by('email', user_request.email)
        if user_exist is not None:
            response.add_error('user_exist', 'Ce utilisateur est déjà enregistré.')
            return False
        return True
