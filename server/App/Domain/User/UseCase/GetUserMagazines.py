from App.Domain.User.Repository.UserRepositoryInterface import UserRepositoryInterface
from App.Helper.Response import Response


class GetUserMagazines:

    def __init__(self, user_repository: UserRepositoryInterface):
        self.user_repository = user_repository

    def execute(self, uid: str) -> Response:
        magazines = [magazine.serialize() for magazine in self.user_repository.get_magazines(uid)]

        return Response(data = {'magazines': magazines})
