import validators

from App.Domain.ImageRequest import ImageRequest
from App.Domain.User.Entity.User import User
from App.Domain.User.Repository.UserRepositoryInterface import UserRepositoryInterface
from App.Domain.User.Request.UserRequest import UserRequest
from App.Helper.Response import Response
from App.Helper.upload import upload_image, remove_image


class UpdateUser:

    def __init__(self, user_repository: UserRepositoryInterface):
        self.user_repository = user_repository

    def execute(self, user_request: UserRequest, image_request: ImageRequest) -> Response:
        response = Response()
        is_valid = self.is_request_valid(user_request, response) and self.is_user_valid(user_request, response)
        if is_valid:
            user = User(*vars(user_request))
            if image_request.image is not None and image_request.path is not None:
                remove_image(image_request.image, image_request.path)
                filename = upload_image(image_request.image, image_request.path)
                user.photo = filename
            created_user = self.user_repository.update(user)
            response.add_data("user", created_user.serialize_all())

        return response

    @staticmethod
    def is_request_valid(request: UserRequest, response: Response) -> bool:

        if not request.uid:
            response.add_error('uid', "L'identifiant du l'utilisateur doit être renseigné.")
            return False

        if len(request.name.strip()) < 3:
            response.add_error("name", "Le nom de l'utilisateur doit être renseigné (3 caractères au minimum)")

        if not validators.email(request.email):
            response.add_error("email", "L'email de l'utilisateur doit être renseigné.")

        return response.has_error()

    def is_user_valid(self, user_request: UserRequest, response: Response) -> bool:
        user_exist = self.user_repository.get(user_request.uid)
        if user_exist is not None and user_exist.id != user_request.id:
            response.add_error('user_exist', 'Cet utilisateur est déjà enregistré.')
            return False
        return True
