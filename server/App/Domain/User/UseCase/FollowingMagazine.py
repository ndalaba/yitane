from App.Domain.Article.Repository.MagazineRepositoryInterface import MagazineRepositoryInterface
from App.Domain.Feed.Entity.Feed import FeedAction
from App.Domain.Feed.Repository.FeedRepositoryInterface import FeedRepositoryInterface
from App.Domain.Feed.Request.FeedRequest import FeedRequest
from App.Domain.Feed.UseCase.CreateFeed import CreateFeed
from App.Domain.User.Repository.UserRepositoryInterface import UserRepositoryInterface
from App.Helper.Response import Response


class FollowingMagazine:
    def __init__(self, user_repository: UserRepositoryInterface, magazine_repository: MagazineRepositoryInterface, feed_repository: FeedRepositoryInterface):
        self.magazine_repository = magazine_repository
        self.feed_repository = feed_repository
        self.user_repository = user_repository

    def execute(self, magazine_uid: str, follow: bool, user_uid: str) -> Response:
        response = Response()
        user = self.user_repository.get(user_uid)
        magazine = self.magazine_repository.get(magazine_uid)
        if follow:
            action = FeedAction.FOLLOW_MAG
            user.magazines.append(magazine)
        else:
            action = FeedAction.UNFOLLOW_MAG
            user.magazines.remove(magazine) if magazine in user.magazines else None
        self.user_repository.update_magazines(user)
        feed_request = FeedRequest(feedable_id = magazine.id, feedable_type = 'Magazine', action = action, user_id = user.id)
        create_feed_response = (CreateFeed(self.feed_repository)).execute(feed_request)
        if create_feed_response.has_error():
            for key, value in create_feed_response.errors:
                response.add_error(key, value)

        return response
