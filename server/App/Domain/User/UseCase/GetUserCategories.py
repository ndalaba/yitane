from App.Domain.User.Repository.UserRepositoryInterface import UserRepositoryInterface
from App.Helper.Response import Response


class GetUserCategories:

    def __init__(self, user_repository: UserRepositoryInterface):
        self.user_repository = user_repository

    def execute(self, uid: str) -> Response:
        categories = [category.serialize() for category in self.user_repository.get_categories(uid)]
        return Response(data = {'categories': categories})
