from App.Domain.User.Repository.UserRepositoryInterface import UserRepositoryInterface
from App.Helper.Response import Response


class GetUserChannels:

    def __init__(self, user_repository: UserRepositoryInterface):
        self.user_repository = user_repository

    def execute(self, uid: str) -> Response:
        channels = [channel.serialize() for channel in self.user_repository.get_channels(uid)]

        return Response(data = {'channels': channels})
