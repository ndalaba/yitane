from typing import List

from App.Domain.User.Repository.UserRepositoryInterface import UserRepositoryInterface

REMOVE_MANY_BY_UID = -1


class ApplyUserAction:
    def __init__(self, user_repository: UserRepositoryInterface):
        self.user_repository = user_repository

    def execute(self, action: int, uids: list[str]):
        if action == REMOVE_MANY_BY_UID:
            self.user_repository.remove_many_by_uids(uids)

