from App.Domain.Feed.Entity.Feed import FeedAction
from App.Domain.Feed.Repository.FeedRepositoryInterface import FeedRepositoryInterface
from App.Domain.Feed.Request.FeedRequest import FeedRequest
from App.Domain.Feed.UseCase.CreateFeed import CreateFeed
from App.Domain.User.Repository.UserRepositoryInterface import UserRepositoryInterface
from App.Domain.Video.Repository.ChannelRepositoryInterface import ChannelRepositoryInterface
from App.Helper.Response import Response


class FollowingChannel:
    def __init__(self, user_repository: UserRepositoryInterface, channel_repository: ChannelRepositoryInterface, feed_repository: FeedRepositoryInterface):
        self.channel_repository = channel_repository
        self.feed_repository = feed_repository
        self.user_repository = user_repository

    def execute(self, channel_uid: str, follow: bool, user_uid: str) -> Response:
        response = Response()
        user = self.user_repository.get(user_uid)
        channel = self.channel_repository.get(channel_uid)
        if follow:
            action = FeedAction.FOLLOW_MAG
            user.channels.append(channel)
        else:
            action = FeedAction.UNFOLLOW_MAG
            user.channels.remove(channel) if channel in user.channels else None
        self.user_repository.update_channels(user)
        feed_request = FeedRequest(feedable_id = channel.id, feedable_type = 'Channel', action = action, user_id = user.id)
        create_feed_response = (CreateFeed(self.feed_repository)).execute(feed_request)
        if create_feed_response.has_error():
            for key, value in create_feed_response.errors:
                response.add_error(key, value)

        return response
