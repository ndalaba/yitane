from http.client import HTTPException

from App.Domain.User.Entity import User
from App.Domain.User.Repository.UserRepositoryInterface import UserRepositoryInterface
from App.Helper.Response import Response


class DeleteUser:

    def __init__(self, user_repository: UserRepositoryInterface):
        self.user_repository = user_repository

    def execute(self, uid) -> Response:
        response = Response()
        user_exist, user = self.user_exist(uid)
        if not user_exist:
            self.user_repository.delete(uid)
            response.set_success_message("Utilisateur supprimé.")

        return response

    def user_exist(self, uid: str) -> (bool, User):
        try:
            user = self.user_repository.get(uid)
            return True, user
        except HTTPException:
            return False, None
