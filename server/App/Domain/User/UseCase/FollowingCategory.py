from App.Domain.Article.Repository.CategoryRepositoryInterface import CategoryRepositoryInterface
from App.Domain.Feed.Entity.Feed import FeedAction
from App.Domain.Feed.Repository.FeedRepositoryInterface import FeedRepositoryInterface
from App.Domain.Feed.Request.FeedRequest import FeedRequest
from App.Domain.Feed.UseCase.CreateFeed import CreateFeed
from App.Domain.User.Repository.UserRepositoryInterface import UserRepositoryInterface
from App.Helper.Response import Response


class FollowingCategory:
    def __init__(self, user_repository: UserRepositoryInterface, category_repository: CategoryRepositoryInterface, feed_repository: FeedRepositoryInterface):
        self.category_repository = category_repository
        self.feed_repository = feed_repository
        self.user_repository = user_repository

    def execute(self, category_uid: str, follow: bool, user_uid: str) -> Response:
        response = Response()
        user = self.user_repository.get(user_uid)
        category = self.category_repository.get(category_uid)
        if follow:
            action = FeedAction.FOLLOW_MAG
            user.categories.append(category)
        else:
            action = FeedAction.UNFOLLOW_MAG
            user.categories.remove(category) if category in user.categories else None
        self.user_repository.update_categories(user)
        feed_request = FeedRequest(feedable_id = category.id, feedable_type = 'Category', action = action, user_id = user.id)
        create_feed_response = (CreateFeed(self.feed_repository)).execute(feed_request)
        if create_feed_response.has_error():
            for key, value in create_feed_response.errors:
                response.add_error(key, value)

        return response
