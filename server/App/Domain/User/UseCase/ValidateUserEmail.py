from http.client import HTTPException

from App.Domain.User.Entity.User import User
from App.Domain.User.Repository.UserRepositoryInterface import UserRepositoryInterface
from App.Helper.Response import Response


class ValidateUserEmail:

    def __init__(self, user_repository: UserRepositoryInterface):
        self.user_repository = user_repository

    def execute(self, uid: str, token: str) -> Response:
        response = Response()
        user_exist, user = self.user_exist(uid)
        if not user_exist:
            response.add_error("not_found", "Cet utilisateur n'existe pas.")
            return response
        validated: bool = user.validate_email(token)
        if not validated:
            response.add_error("not_valid", "Cet token est invalide.")
            return response

        updated = self.user_repository.update_email_validation(user)
        response.add_data("user", updated.serialize_all())
        response.set_success_message("Votre email a été validé.")
        return response

    def user_exist(self, uid: str) -> (bool, User):
        try:
            user = self.user_repository.get(uid)
            return True, user
        except HTTPException:
            return False, None
