from App.Domain.User.Repository.UserRepositoryInterface import UserRepositoryInterface
from App.Helper.Response import Response


class GetUser:

    def __init__(self, user_repository: UserRepositoryInterface):
        self.user_repository = user_repository

    def execute(self, uid: str, image_path: str = None) -> Response:
        user = self.user_repository.get(uid)
        if image_path is not None:
            user.set_image_path(image_path)
        magazines = [mag.serialize() for mag in user.magazines]
        channels = [channel.serialize() for channel in user.channels]
        categories = [cat.serialize() for cat in user.categories]

        return Response(data = {'user': user.serialize_all(), 'magazines': magazines, 'channels': channels, 'categories': categories})
