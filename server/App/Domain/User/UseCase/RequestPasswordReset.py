from http.client import HTTPException

from App.Domain.User.Entity.User import User
from App.Domain.User.Repository.UserRepositoryInterface import UserRepositoryInterface
from App.Helper.Email import Mailer
from App.Helper.Response import Response


class RequestPasswordReset:

    def __init__(self, user_repository: UserRepositoryInterface, mail: Mailer):
        self.user_repository = user_repository
        self.mail = mail

    def execute(self, email: str, user: User = None) -> Response:
        response = Response()
        user_exist = False if user is None else True
        if user is None:
            user_exist, user = self.user_exist(email)
        if not user_exist:
            response.add_error("not_found", "Cet utilisateur n'existe pas.")
            return response

        user.generate_token()

        self.user_repository.update_single_field('confirmation_token', user.confirmation_token, user.uid)
        status = self.mail.send()
        if status:
            response.set_success_message('Consulter votre mail pour la réinitialisation de votre mot de passe.')
        else:
            response.add_error('send_mail', 'Erreur envoi message')

        return response

    def user_exist(self, email: str) -> (bool, User):
        try:
            user = self.user_repository.find_by('email', email)
            return True, user
        except HTTPException:
            return False, None
