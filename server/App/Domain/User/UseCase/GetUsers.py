from App.Domain.User.Repository.UserRepositoryInterface import UserRepositoryInterface
from App.Domain.User.Request.UserFilterRequest import UserFilterRequest
from App.Helper.Response import Response


class GetUsers:

    def __init__(self, user_repository: UserRepositoryInterface):
        self.user_repository = user_repository

    def execute(self, request: UserFilterRequest) -> Response:
        users = self.user_repository.filter(name = request.name, email = request.email, role = request.role, country_id = request.country_id, page = request.page)
        return Response(data = {'users': [user.serialize_all() for user in users]})
