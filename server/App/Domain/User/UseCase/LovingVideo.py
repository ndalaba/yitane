from App.Domain.Feed.Entity.Feed import FeedAction
from App.Domain.Feed.Repository.FeedRepositoryInterface import FeedRepositoryInterface
from App.Domain.Feed.Request.FeedRequest import FeedRequest
from App.Domain.Feed.UseCase.CreateFeed import CreateFeed
from App.Domain.User.Repository.UserRepositoryInterface import UserRepositoryInterface
from App.Domain.Video.Repository.VideoRepositoryInterface import VideoRepositoryInterface
from App.Helper.Response import Response


class LovingVideo:
    def __init__(self, user_repository: UserRepositoryInterface, video_repository: VideoRepositoryInterface, feed_repository: FeedRepositoryInterface):
        self.video_repository = video_repository
        self.feed_repository = feed_repository
        self.user_repository = user_repository

    def execute(self, video_uid: str, love: bool, user_uid: str) -> Response:
        response = Response()
        user = self.user_repository.get(user_uid)
        video = self.video_repository.get(video_uid)
        if love:
            action = FeedAction.LIKE_POST
            user.loved_videos.append(video)
        else:
            action = FeedAction.UNLIKE_POST
            user.loved_videos.remove(video) if video in user.loved_videos else None
        self.user_repository.update_loved_videos(user)
        feed_request = FeedRequest(feedable_id = video.id, feedable_type = 'Video', action = action, user_id = user.id)
        create_feed_response = (CreateFeed(self.feed_repository)).execute(feed_request)
        if create_feed_response.has_error():
            for key, value in create_feed_response.errors:
                response.add_error(key, value)

        return response
