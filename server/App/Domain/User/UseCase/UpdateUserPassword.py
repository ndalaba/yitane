from http.client import HTTPException

from App.Domain.User.Entity.User import User
from App.Domain.User.Repository.UserRepositoryInterface import UserRepositoryInterface
from App.Domain.User.Request.UserPasswordRequest import UserPasswordRequest
from App.Helper.Response import Response


class UpdateUserPassword:

    def __init__(self, user_repository: UserRepositoryInterface):
        self.user_repository = user_repository

    def execute(self, user_request: UserPasswordRequest) -> Response:
        response = Response()
        is_valid = self.is_request_valid(user_request, response)
        user_exist, user = self.user_exist(user_request.uid)
        if is_valid and user_exist:
            user.password = user_request.password
            updated = self.user_repository.update_password(user)
            response.add_data("user", updated.serialize_all())

        return response

    def user_exist(self, uid: str) -> (bool, User):
        try:
            user = self.user_repository.get(uid)
            return True, user
        except HTTPException:
            return False, None

    @staticmethod
    def is_request_valid(request: UserPasswordRequest, response: Response) -> bool:

        if not request.uid:
            response.add_error('uid', "L'identifiant du l'utilisateur doit être renseigné.")
            return False

        if len(request.password.strip()) < 3 or len(request.confirmation_password.strip()) < 3:
            response.add_error("password", "Les mots de passe doivent être renseignés.")

        if request.password != request.confirmation_password:
            response.add_error("confirmation_password", "Le mot de passe et la confirmation du mot de passe doivent être identiques.")

        return response.has_error()
