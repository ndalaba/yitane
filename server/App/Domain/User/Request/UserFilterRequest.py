class UserFilterRequest:

    def __init__(self, name: str = None, email: str = None, role: str = None, country_id: int = None, page=1):
        self.name = name
        self.email = email
        self.role = role
        self.country_id = country_id
        self.page = page
