class UserPasswordRequest:

    def __init__(self, password: str, confirmation_password: str, uid: str):
        self.password = password
        self.confirmation_password = confirmation_password
        self.uid = uid
