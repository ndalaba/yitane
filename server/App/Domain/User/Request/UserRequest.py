class UserRequest:
    def __init__(self, name: str, email: str, role: str = None, photo: str = None, country_id: int = None, phone: str = None, uid: str = None, id: int = None ):
        self.name = name
        self.email = email
        self.phone = phone
        self.role = role
        self.country_id = country_id
        self.uid = uid
        self.id = id
        self.photo = photo
