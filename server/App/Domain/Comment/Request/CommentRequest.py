class CommentRequest:

    def __init__(self, commentable_id: int, commentable_type: str, content: str, user_id: int, id: int = None):
        self.commentable_id = commentable_id
        self.commentable_type = commentable_type
        self.user_id = user_id
        self.id = id
        self.content = content
