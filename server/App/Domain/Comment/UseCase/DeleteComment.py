from http.client import HTTPException

from App.Domain.Comment.Entity.Comment import Comment
from App.Domain.Comment.Repository.CommentRepositoryInterface import CommentRepositoryInterface
from App.Domain.User.Entity.User import User
from App.Helper.Response import Response


class DeleteComment:

    def __init__(self, comment_repository: CommentRepositoryInterface):
        self.comment_repository = comment_repository

    def execute(self, id: int, user: User) -> Response:
        response = Response()
        comment_exist, comment = self.comment_exist(id)
        if comment_exist and self.can_user_delete(comment_exist, user, response):
            self.comment_repository.delete(id)
            response.set_success_message("Commentaire supprimé.")

        return response

    def comment_exist(self, id: int) -> (bool, Comment):
        try:
            comment = self.comment_repository.find_by('id', id)
            return True, comment
        except HTTPException:
            return False, None

    @staticmethod
    def can_user_delete(comment: Comment, user: User, response: Response) -> bool:
        if comment.user_id != user.id or not user.is_admin:
            # raise Exception("Opération non permise")
            response.add_error('not_allowed', "Opération non permise.")
            return False
        return True
