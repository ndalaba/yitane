from App.Domain.Comment.Repository.CommentRepositoryInterface import CommentRepositoryInterface
from App.Helper.Response import Response


class GetComment:

    def __init__(self, comment_repository: CommentRepositoryInterface):
        self.comment_repository = comment_repository

    def execute(self, id: int) -> Response:
        comment = self.comment_repository.find_by('id', id)
        return Response(data = {'comment': comment})
