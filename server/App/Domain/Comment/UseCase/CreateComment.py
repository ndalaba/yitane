from typing import Any

from App.Domain.Article.Repository.ArticleRepositoryInterface import ArticleRepositoryInterface
from App.Domain.Comment.Entity.Comment import Comment, CommentableType
from App.Domain.Comment.Repository.CommentRepositoryInterface import CommentRepositoryInterface
from App.Domain.Comment.Request.CommentRequest import CommentRequest
from App.Domain.Feed.Entity.Feed import FeedAction
from App.Domain.Feed.Repository.FeedRepositoryInterface import FeedRepositoryInterface
from App.Domain.Feed.Request.FeedRequest import FeedRequest
from App.Domain.Feed.UseCase.CreateFeed import CreateFeed
from App.Domain.Video.Repository.VideoRepositoryInterface import VideoRepositoryInterface
from App.Helper.Response import Response


class CreateComment:

    def __init__(self, comment_repository: CommentRepositoryInterface, article_repository: ArticleRepositoryInterface, video_repository: VideoRepositoryInterface,
                 feed_repository: FeedRepositoryInterface):
        self.comment_repository = comment_repository
        self.video_repository = video_repository
        self.article_repository = article_repository
        self.feed_repository = feed_repository

    def execute(self, request: CommentRequest) -> Response:
        response = Response()
        is_valid, entity = self.is_request_valid(request, response)
        if is_valid:
            comment = Comment(commentable_id = request.commentable_id, commentable_type = request.commentable_type, user_id = request.user_id, content = request.content)
            created_comment: Comment = self.comment_repository.create(comment)
            self.increment_comment_count(request.commentable_type, entity)
            feed_request = FeedRequest(feedable_id = comment.id, feedable_type = 'Comment', action = FeedAction.POST_COMMENT, user_id = request.user_id)
            create_feed_response = (CreateFeed(self.feed_repository)).execute(feed_request)
            if create_feed_response.has_error():
                for key, value in create_feed_response.errors:
                    response.add_error(key, value)
            response.add_data("comment", created_comment.serialize())

        return response

    def is_request_valid(self, comment_request: CommentRequest, response: Response) -> (bool, Any):
        if comment_request.commentable_type not in (CommentableType.VIDEO, CommentableType.ARTICLE):
            response.add_error("comment_type", "L'élément commenté doit être renseigné.")
            return False, None

        entity = None

        if comment_request.commentable_type == CommentableType.ARTICLE:
            entity = self.article_repository.find_by('id', comment_request.commentable_id)
            if entity is None:
                response.add_error('not_found', "Article non trouvé.")

        if comment_request.commentable_type == CommentableType.VIDEO:
            entity = self.video_repository.find_by('id', comment_request.commentable_id)
            if entity is None:
                response.add_error('not_found', "Vidéo non trouvé.")

        return response.has_error(), entity

    def increment_comment_count(self, commentable_type: str, entity) -> None:
        if commentable_type == CommentableType.ARTICLE:
            entity.add_comment_count()
            self.article_repository.update(entity)
        if commentable_type == CommentableType.VIDEO:
            entity.add_comment_count()
            self.video_repository.update(entity)
