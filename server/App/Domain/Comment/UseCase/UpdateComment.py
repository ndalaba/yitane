from App.Domain.Article.Repository.ArticleRepositoryInterface import ArticleRepositoryInterface
from App.Domain.Comment.Entity.Comment import Comment
from App.Domain.Comment.Repository.CommentRepositoryInterface import CommentRepositoryInterface
from App.Domain.Comment.Request.CommentRequest import CommentRequest
from App.Domain.User.Entity.User import User
from App.Domain.Video.Repository.VideoRepositoryInterface import VideoRepositoryInterface
from App.Helper.Response import Response


class CreateComment:

    def __init__(self, comment_repository: CommentRepositoryInterface, article_repository: ArticleRepositoryInterface, video_repository: VideoRepositoryInterface):
        self.comment_repository = comment_repository
        self.video_repository = video_repository
        self.article_repository = article_repository

    def execute(self, request: CommentRequest, user: User) -> Response:
        response = Response()
        is_valid = self.is_request_valid(request, response) and self.can_user_delete(self.comment_repository.find_by('id', request.id), user, response)
        if is_valid:
            comment = Comment(id = request.id, commentable_id = request.commentable_id, commentable_type = request.commentable_type, user_id = request.user_id, content = request.content)
            created_comment = self.comment_repository.update(comment)
            response.add_data("comment", created_comment)

        return response

    @staticmethod
    def can_user_delete(comment: Comment, user: User, response: Response) -> bool:
        if comment.user_id != user.id or not user.is_admin:
            # raise Exception("Opération non permise")
            response.add_error('not_allowed', "Opération non permise.")
            return False
        return True

    def is_request_valid(self, comment_request: CommentRequest, response: Response) -> bool:

        if not comment_request.id:
            response.add_error("id", "L'identifiant doit être renseigné.")
            return False

        if comment_request.commentable_type not in Comment.COMMENTABLE_TYPE.values():
            response.add_error("comment_type", "L'élément commenté doit être renseigné.")
            return False

        if comment_request.commentable_type == Comment.COMMENTABLE_TYPE['ARTICLE']:
            article = self.article_repository.find_by('id', comment_request.commentable_id)
            if article is None:
                response.add_error('not_found', "Article non trouvé.")

        if comment_request.commentable_type == Comment.COMMENTABLE_TYPE['VIDEO']:
            article = self.video_repository.find_by('id', comment_request.commentable_id)
            if article is None:
                response.add_error('not_found', "Vidéo non trouvé.")

        return response.has_error()
