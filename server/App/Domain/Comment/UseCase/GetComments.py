from App.Domain.Comment.Repository.CommentRepositoryInterface import CommentRepositoryInterface
from App.Helper.Response import Response


class GetComments:

    def __init__(self, comment_repository: CommentRepositoryInterface):
        self.comment_repository = comment_repository

    def execute(self, commentable_type: str, commentable_id: int, page: int = 1) -> Response:
        comments = self.comment_repository.get_comments(commentable_type, commentable_id, page)
        comments = [comment.serialize() for comment in comments]
        return Response(data = {'comments': comments})
