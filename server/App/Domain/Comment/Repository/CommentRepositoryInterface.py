from abc import ABC, abstractmethod
from typing import List, Any

from App.Domain.BaseEntity import BaseEntity
from App.Domain.Comment.Entity.Comment import Comment


class CommentRepositoryInterface(ABC):

    @abstractmethod
    def get(self, id: int) -> Comment:
        pass

    @abstractmethod
    def create(self, comment: Comment) -> Comment:
        pass

    @abstractmethod
    def update(self, comment: Comment) -> Comment:
        pass

    @abstractmethod
    def delete(self, id: int) -> None:
        pass

    @abstractmethod
    def get_comments(self, commentable_type: str, commentable_id: int, page: int = 1,
                     size: int = BaseEntity.PER_PAGE) -> list[Comment]:
        pass
