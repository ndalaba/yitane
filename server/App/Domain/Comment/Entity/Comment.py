from datetime import datetime

from App.Domain.User.Entity.User import User


class Comment:

    def __init__(self, commentable_id: int, commentable_type: str, content: str, user_id: int, created_at=None,
                 user: User = None, id: int = None):
        self.id = id
        self.commentable_id = commentable_id
        self.commentable_type = commentable_type
        self.created_at = created_at if created_at is not None else datetime.utcnow()
        self.user_id = user_id
        self.user = user
        self.content = content

    def serialize(self):
        return {
            'id': self.id,
            "content": self.content,
            "user": self.user.serialize(),
            "created_at": self.created_at
        }


class CommentableType:
    ARTICLE = 'Article'
    VIDEO = 'Video'
