from App.Domain.Article.Repository.ArticleRepositoryInterface import ArticleRepositoryInterface
from App.Domain.Article.Repository.CategoryRepositoryInterface import CategoryRepositoryInterface
from App.Domain.Article.Repository.MagazineRepositoryInterface import MagazineRepositoryInterface
from App.Domain.Localisation.Repository.CountryRepositoryInterface import CountryRepositoryInterface
from App.Domain.User.Repository.UserRepositoryInterface import UserRepositoryInterface
from App.Domain.Video.Repository.ChannelRepositoryInterface import ChannelRepositoryInterface
from App.Domain.Video.Repository.VideoRepositoryInterface import VideoRepositoryInterface
from App.Helper.Response import Response


class GetDashboardStat:

    def __init__(self, article_rep: ArticleRepositoryInterface, mag_rep: MagazineRepositoryInterface, video_rep: VideoRepositoryInterface,
                 channel_rep: ChannelRepositoryInterface, country_repo: CountryRepositoryInterface, user_repo: UserRepositoryInterface, cat_rep: CategoryRepositoryInterface):
        self.article_rep = article_rep
        self.mag_rep = mag_rep
        self.video_rep = video_rep
        self.channel_rep = channel_rep
        self.country_rep = country_repo
        self.user_rep = user_repo
        self.cat_rep = cat_rep

    def execute(self) -> Response:
        response = Response()
        response.add_data('articles', self.article_rep.articles_count())
        response.add_data('magazines', self.mag_rep.magazines_count())
        response.add_data('videos', self.video_rep.videos_count())
        response.add_data('channels', self.channel_rep.channels_count())
        response.add_data('countries', self.country_rep.countries_count())
        response.add_data('categories', self.cat_rep.categories_count())
        response.add_data('members', self.user_rep.users_count())
        return response
