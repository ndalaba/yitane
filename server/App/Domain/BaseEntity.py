from datetime import datetime

from App.Helper.Str import generate_uuid


class BaseEntity:
    PER_PAGE = 20

    def __init__(self, uid: str = None, published: bool = False, created_at=None, updated_at=None) -> None:
        self.uid = uid if uid is not None else generate_uuid()
        self.created_at = created_at if created_at is not None else datetime.utcnow()
        self.published = published
        self.updated_at = updated_at if created_at is not None else datetime.utcnow()

