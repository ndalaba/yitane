class FeedRequest:

    def __init__(self, feedable_id: int, feedable_type: str, action: str, user_id: int, created_at=None, id: int = None):
        self.feedable_id = feedable_id
        self.feedable_type = feedable_type
        self.action = action
        self.id = id
        self.user_id = user_id
