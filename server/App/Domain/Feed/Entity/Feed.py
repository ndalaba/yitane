from datetime import datetime

from App.Domain.Article.Entity import Magazine
from App.Domain.Article.Entity.Article import Article
from App.Domain.Comment.Entity import Comment
from App.Domain.User.Entity import User
from App.Domain.Video.Entity.Channel import Channel
from App.Domain.Video.Entity.Video import Video


class Feed:

    def __init__(self, feedable_id: int, feedable_type: str, action: str, user_id: int, created_at=None,
                 user: User = None, id: int = None, article: Article = None, comment: Comment = None,
                 video: Video = None, channel: Channel = None, magazine: Magazine = None):

        self.created_at = created_at if created_at is not None else datetime.utcnow()
        self.feedable_id = feedable_id
        self.feedable_type = feedable_type
        self.action = action
        self.id = id
        self.user = user
        self.user_id = user_id
        self.article = article
        self.comment = comment
        self.video = video
        self.channel = channel
        self.magazine = magazine

    def serialize(self) -> dict:
        return {
            'id': self.id,
            "action": self.action,
            "created_at": self.created_at,
            "feedable_type": self.feedable_type,
            "feedable_id": self.feedable_id,
            "article": self.article.serialize() if self.article is not None else None,
            "comment": self.comment.serialyze() if self.comment is not None else None,
            "text": self.text(),
            "video": self.video.serialize() if self.video is not None else None,
            "channel": self.channel.serialize() if self.channel is not None else None,
            "magazine": self.magazine.serialize() if self.magazine is not None else None
        }

    def text(self) -> str:
        if self.action == FeedAction.LIKE_POST:
            return " a aimé un article "
        if self.action == FeedAction.POST_COMMENT or self.action == FeedAction.VIDEO_COMMENT:
            return " a posté un commentaire "
        if self.action == FeedAction.FOLLOW_MAG or self.action == FeedAction.FOLLOW_CHANNEL:
            return " a commencé à suivre "
        if self.action == FeedAction.UNFOLLOW_MAG or self.action == FeedAction.UNFOLLOW_CHANNEL:
            return " a arrêté de suivre "
        if self.action == FeedAction.UNFOLLOW_CAT:
            return " a arrêté de suivre "
        if self.action == FeedAction.FOLLOW_CAT:
            return " a commencé à suivre "
        if self.action == FeedAction.LIKE_VIDEO:
            return " a aimé une vidéo "


class FeedType:
    ARTICLE = 'Article'
    VIDEO = 'Video'
    COMMENT = 'Comment'
    USER = 'User'
    CHANNEL = 'Channel'
    MAGAZINE = 'Magazine'

    @classmethod
    def get_types(cls) -> list[str]:
        return [cls.ARTICLE, cls.VIDEO, cls.COMMENT, cls.USER]


class FeedAction:
    LIKE_POST = "like_post"
    UNLIKE_POST = "unlike_post"
    POST_COMMENT = "post_comment"
    FOLLOW_MAG = "follow_mag"
    UNFOLLOW_MAG = "unfollow_mag"
    FOLLOW_CAT = "follow_cat"
    UNFOLLOW_CAT = "unfollow_cat"
    UPDATE_PROFIL = "update_profil"
    UPDATE_PASSWORD = "update_password"
    SAVE_POST = "save_post"
    UNSAVE_POST = "unsave_post"
    SAVE_VIDEO = "save_video"
    UNSAVE_VIDEO = "unsave_video"
    LIKE_VIDEO = "like_video"
    UNLIKE_VIDEO = "unlike_video"
    VIDEO_COMMENT = "video_comment"
    FOLLOW_CHANNEL = "follow_channel"
    UNFOLLOW_CHANNEL = "unfollow_channel"
