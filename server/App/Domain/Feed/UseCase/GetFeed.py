from App.Domain.Feed.Repository.FeedRepositoryInterface import FeedRepositoryInterface
from App.Helper.Response import Response


class GetFeed:

    def __init__(self, feed_repository: FeedRepositoryInterface):
        self.feed_repository = feed_repository

    def execute(self, uid: str) -> Response:
        feed = self.feed_repository.find_by('uid', uid)
        return Response(data = {'feed': feed})
