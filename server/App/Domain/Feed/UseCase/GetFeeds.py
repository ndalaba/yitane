from App.Domain.Feed.Repository.FeedRepositoryInterface import FeedRepositoryInterface
from App.Helper.Response import Response


class GetFeeds:

    def __init__(self, feed_repository: FeedRepositoryInterface):
        self.feed_repository = feed_repository

    def execute(self, id: int, page=1) -> Response:
        feeds = self.feed_repository.get_feeds(id, page)
        feeds = [feed.serialize() for feed in feeds]
        return Response(data = {'feeds': feeds})
