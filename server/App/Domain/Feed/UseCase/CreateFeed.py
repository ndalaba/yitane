from App.Domain.Feed.Entity.Feed import Feed, FeedType
from App.Domain.Feed.Repository.FeedRepositoryInterface import FeedRepositoryInterface
from App.Domain.Feed.Request.FeedRequest import FeedRequest
from App.Helper.Response import Response


class CreateFeed:

    def __init__(self, feed_repository: FeedRepositoryInterface):
        self.feed_repository = feed_repository

    def execute(self, request: FeedRequest) -> Response:
        response = Response()
        is_valid = self.is_request_valid(request, response) and self.is_feed_valid(request, response)
        if is_valid:
            feed = Feed(*vars(request))
            created_feed = self.feed_repository.create(feed)
            response.add_data("feed", created_feed)

        return response

    @staticmethod
    def is_request_valid(request: FeedRequest, response: Response) -> bool:

        if not request.feedable_id:
            response.add_error('feedable_type', "Le type du feed doit être renseigné.")

        if not request.action:
            response.add_error('action', "L'action du feed doit être renseigné.")

        if not request.user_id:
            response.add_error('user_id', "L'id de l'utilisateur doit être renseigné.")

        if request.feedable_type not in FeedType.get_types():
            response.add_error('feedable_type', "Le type du feed doit être renseigné.")

        return response.has_error()

    def is_feed_valid(self, request: FeedRequest, response: Response) -> bool:
        feed_exist = self.feed_repository.check_feed(request.feedable_id, feedable_type = request.feedable_type, action = request.action, user_id = request.user_id)
        if feed_exist is not None:
            response.add_error('feed_exist', 'Ce feed est déjà enregistré est déjà enregistré.')
            return False
        return True
