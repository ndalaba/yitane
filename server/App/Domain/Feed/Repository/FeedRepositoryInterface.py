from abc import ABC, abstractmethod
from typing import Any, List

from App.Domain.BaseEntity import BaseEntity
from App.Domain.Feed.Entity import Feed
from App.Domain.User.Entity.User import User


class FeedRepositoryInterface(ABC):

    @abstractmethod
    def find_by(self, field: str, value: Any) -> Feed:
        pass

    @abstractmethod
    def create(self, feed: Feed) -> Feed:
        pass

    @abstractmethod
    def get_feeds(self, id: int, page=1, size: int = BaseEntity.PER_PAGE) -> list[Feed]:
        pass

    @abstractmethod
    def check_feed(self, feedable_id: int, feedable_type: str, action: str, user_id: int) -> Feed:
        pass
