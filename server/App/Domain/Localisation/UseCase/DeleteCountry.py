from http.client import HTTPException

from App.Domain.Localisation.Entity import Country
from App.Domain.Localisation.Repository.CountryRepositoryInterface import CountryRepositoryInterface
from App.Helper.Response import Response


class DeleteCountry:

    def __init__(self, country_repository: CountryRepositoryInterface):
        self.country_repository = country_repository

    def execute(self, uid) -> Response:
        response = Response()
        country_exist, country = self.country_exist(uid)
        if country_exist and not self.country_has_users(country, response) \
                and not self.country_has_magazines(country, response) \
                and not self.country_has_channels(country, response):
            self.country_repository.delete(uid)
            response.set_success_message("Pays supprimé.")

        return response

    def country_exist(self, uid: str) -> (bool, Country):
        try:
            country = self.country_repository.get(uid)
            return True, country
        except HTTPException:
            return False, None

    def country_has_users(self, country: Country, response: Response) -> bool:
        if self.country_repository.users_count(country) > 0:
            response.add_error('has_users', "Ce pays a des membres.")
            return True
        return False

    def country_has_magazines(self, country: Country, response: Response) -> bool:
        if self.country_repository.magazines_count(country) > 0:
            response.add_error('has_magazines', "Ce pays a des magasines.")
            return True
        return False

    def country_has_channels(self, country: Country, response: Response) -> bool:
        if self.country_repository.channels_count(country) > 0:
            response.add_error('has_channels', "Ce pays a des chaines.")
            return True
        return False
