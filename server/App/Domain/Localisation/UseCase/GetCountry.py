from App.Domain.Localisation.Repository.CountryRepositoryInterface import CountryRepositoryInterface
from App.Helper.Response import Response


class GetCountry:

    def __init__(self, country_repository: CountryRepositoryInterface):
        self.country_repository = country_repository

    def execute(self, uid: str) -> Response:
        country = self.country_repository.get(uid)
        return Response(data={'country': country})
