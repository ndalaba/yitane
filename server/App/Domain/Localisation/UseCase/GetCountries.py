from App.Domain.Localisation.Repository.CountryRepositoryInterface import CountryRepositoryInterface
from App.Helper.Response import Response


class GetCountries:

    def __init__(self, country_repository: CountryRepositoryInterface):
        self.country_repository = country_repository

    def execute(self) -> Response:
        countries = self.country_repository.all()
        countries = [country.serialize() for country in countries]
        return Response(data = {'countries': countries})
