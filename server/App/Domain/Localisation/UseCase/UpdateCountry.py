from App.Domain.ImageRequest import ImageRequest
from App.Domain.Localisation.Entity.Country import Country
from App.Domain.Localisation.Repository.CountryRepositoryInterface import CountryRepositoryInterface
from App.Domain.Localisation.Request.CountryRequest import CountryRequest
from App.Helper.Response import Response
from App.Helper.upload import upload_image


class UpdateCountry:

    def __init__(self, country_repository: CountryRepositoryInterface):
        self.country_repository = country_repository

    def execute(self, country_request: CountryRequest, image_request: ImageRequest) -> Response:
        response = Response()
        is_valid = self.is_request_valid(country_request, response) and self.is_country_valid(country_request, response)
        if is_valid:
            country = Country(name=country_request.name, published=country_request.published, id=country_request.id)
            if image_request.image is not None and image_request.path is not None:
                filename = upload_image(image_request.image, image_request.path)
                country.image = filename
            created_country = self.country_repository.update(country)
            response.add_data("country", created_country)

        return response

    @staticmethod
    def is_request_valid(country_request: CountryRequest, response: Response) -> bool:

        if len(country_request.name.strip()) < 3:
            response.add_error("name", "Le nom du pays doit être renseigné (3 caractères au minimum)")

        return response.has_error()

    def is_country_valid(self, country_request: CountryRequest, response: Response) -> bool:
        country_exist = self.country_repository.find_by('name', country_request.name)
        if country_exist is not None and country_exist.id != country_request.id:
            response.add_error('country_exist', 'Ce pays est déjà enregistré.')
            return False
        return True
