from App.Domain.BaseEntity import BaseEntity
from App.Helper.Str import slug


class Country(BaseEntity):

    def __init__(self, name: str, image: str = None, uid: str = None, published: bool = False, created_at=None,
                 updated_at=None, id: int = None):
        BaseEntity.__init__(self, uid, created_at, updated_at, published)
        self.id = id
        self.name = name
        self.slug = slug(name)
        self.image = image

    def __repr__(self):
        return self.name

    def serialize(self):
        return {
            'id': self.id,
            'uid': self.uid,
            'name': self.name,
            'slug': self.slug,
            'image': self.image,
        }
