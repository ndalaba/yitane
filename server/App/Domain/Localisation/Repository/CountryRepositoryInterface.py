from abc import ABC, abstractmethod
from typing import List, Any

from App.Domain.Localisation.Entity.Country import Country


class CountryRepositoryInterface(ABC):

    @abstractmethod
    def find_by(self, field: str, value: Any) -> Country:
        pass

    @abstractmethod
    def get(self, uid: str) -> Country:
        pass

    @abstractmethod
    def create(self, country: Country) -> Country:
        pass

    @abstractmethod
    def update(self, country: Country) -> Country:
        pass

    @abstractmethod
    def delete(self, uid: str) -> None:
        pass

    @abstractmethod
    def all(self) -> list[Country]:
        pass

    @abstractmethod
    def magazines_count(self, country: Country) -> int:
        pass

    @abstractmethod
    def channels_count(self, country: Country) -> int:
        pass

    @abstractmethod
    def users_count(self, country: Country) -> int:
        pass

    @abstractmethod
    def countries_count(self) -> int:
        pass
