class CountryRequest:

    def __init__(self, id: int = None, name: str = None, image: str = None, uid: str = None, published: bool = False):
        self.id = id
        self.name = name
        self.published = published
        self.image = image
        self.uid = uid
