class ArticleRequest:

    def __init__(self, title: str, summary: str, tag: str, link: str, guid: str, date_published, magazine_id: int,
                 body: str, youtube: str = None, image: str = None, author: str = None,
                 uid: str = None, published: bool = False, id: int = None):
        self.title = title
        self.link = link
        self.summary = summary
        self.tag = tag
        self.guid = guid
        self.date_published = date_published
        self.magazine_id = magazine_id
        self.body = body
        self.youtube = youtube
        self.id = id
        self.uid = uid
        self.image = image
        self.author = author
        self.published = published
