class CategoryRequest:
    def __init__(self, name: str, id: int = None, uid: str = None, published: bool = False):
        self.name = name
        self.id = id
        self.uid = uid
        self.published = published
