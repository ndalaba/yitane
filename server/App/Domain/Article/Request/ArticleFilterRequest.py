class ArticleFilterRequest:

    def __init__(self, title: str = None, top: bool = None, published: bool = None, magazine_id: int = None,
                 category_id: int = None, country_id: int = None, date=None, order: str = 'date_published-desc',
                 page=1):
        self.title = title
        self.top = top
        self.published = published
        self.magazine_id = magazine_id
        self.country_id = country_id
        self.category_id = category_id
        self.page = page
        self.date = date
        self.order = order
