class MagazineRequest:

    def __init__(self, link: str, feed: str, title: str, image: str, country_id: int, category_id: int, uid: str = None,
                 published: bool = False, description: str = None, image_tag: str = None, id: int = None, body_tag: str = None,
                 tag_to_remove: str = None):
        self.title = title
        self.link = link
        self.feed = feed
        self.image = image
        self.country_id = country_id
        self.category_id = category_id
        self.description = description
        self.body_tag = body_tag
        self.tag_to_remove = tag_to_remove
        self.id = id
        self.uid = uid
        self.published = published
        self.image_tag = image_tag
