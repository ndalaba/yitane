class MagazineFilterRequest:

    def __init__(self, title: str = None, top: bool = None, published: bool = None, country_id: int = None,
                 category_id: int = None, page: int = 1):
        self.title = title
        self.top = top
        self.published = published
        self.country_id = country_id
        self.category_id = category_id
        self.page = page
