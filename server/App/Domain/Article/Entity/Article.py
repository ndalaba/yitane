from datetime import datetime

from App.Domain.Article.Entity.Magazine import Magazine
from App.Domain.BaseEntity import BaseEntity
from App.Helper.Str import slug


class Article(BaseEntity):

    def __init__(self, title: str, summary: str, tag: str, link: str, guid: str, date_published, magazine_id: int,
                 body: str, youtube: str = None, image: str = None, author: str = None, love: int = 0,
                 comments_count: int = 0, vue: int = 0, magazine: Magazine = None, uid: str = None,
                 published: bool = False, created_at=None, updated_at=None, id: int = None):
        BaseEntity.__init__(self,uid, published, created_at, updated_at)

        self.title = title
        self.slug = slug(title)
        self.youtube = youtube
        self.summary = summary
        self.tag_slug = slug(tag)
        self.tag = tag
        self.body = body if len(body.strip()) else summary
        self.id = id
        self.link = link
        self.guid = guid
        self.author = author
        self.love = love
        self.comments_count = comments_count
        self.vue = vue
        self.image = image
        self.date_published = date_published
        self.magazine_id = magazine_id
        self.magazine = magazine
        self.ccatemagazine = magazine

    def increment(self) -> None:
        self.vue += 1

    def add_comment_count(self) -> None:
        self.comments_count += 1

    def add_love(self) -> None:
        self.love += 1

    def remove_love(self) -> None:
        self.love -= 1

    def __repr__(self) -> str:
        return self.title

    def serialize(self) -> dict:
        return {
            'id': self.id,
            'has_image': self.image is not None,
            'uid': self.uid,
            'title': self.title,
            'vue': self.vue,
            'summary': self.summary,
            'image': self.image,
            'date_published': self.created_at,
            'url': self.url,
            'magazine': self.magazine.serialize(),
            'slug': self.slug,
            "love": self.love,
            "comments_count": self.comments_count,
            'body': self.body,
        }

    def serialize_all(self) -> dict:
        return {
            'has_image': self.image is not None,
            'link': self.link,
            'id': self.id,
            'uid': self.uid,
            'slug': self.slug,
            'title': self.title,
            'guid': self.guid,
            'tag': self.tag,
            'youtube': self.youtube,
            'tag_slug': self.tag_slug,
            'body': self.body,
            'summary': self.summary,
            'author': self.author if self.author is not None else "",
            'vue': self.vue,
            'image': self.image,
            'date_published': self.created_at,
            'url': self.url,
            'tags': self.tags,
            'magazine': self.magazine.serialize(),
            "love": self.love,
            "comments_count": self.comments_count,
            "created_at": self.created_at
        }

    @property
    def tags(self) -> list[str]:
        tags = self.tag.split(", ")
        return list(filter(None, tags))

    @property
    def url(self) -> str:
        return "/" + self.magazine.slug + "/" + slug(self.title) + "/" + self.uid

    @property
    def published_datetime(self):
        return datetime.timestamp(self.date_published)
