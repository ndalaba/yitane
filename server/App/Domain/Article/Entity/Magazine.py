from App.Domain.Article.Entity import Category
from App.Domain.BaseEntity import BaseEntity
from App.Domain.Localisation.Entity import Country
from App.Domain.User.Entity import User
from App.Helper.Str import slug


class Magazine(BaseEntity):

    def __init__(self, link: str, feed: str, title: str, image: str, country_id: int, category_id: int,
                 description: str = None, vue: int = 0, image_tag: str = None, id: int = None, top: bool = False,
                 last_build_date=None, body_tag: str = None, tag_to_remove: str = None, category: Category = None,
                 country: Country = None, uid: str = None, published: bool = False, created_at=None, updated_at=None,
                 members: list[User] = None):
        BaseEntity.__init__(self, uid, published, created_at, updated_at)
        self.slug = slug(title)
        self.title = title
        self.link = link
        self.feed = feed
        self.image = image
        self.country_id = country_id
        self.category_id = category_id
        self.description = description
        self.vue = vue
        self.top = top
        self.image_tag = image_tag
        self.last_build_date = last_build_date
        self.body_tag = body_tag
        self.tag_to_remove = tag_to_remove
        self.category = category
        self.country = country
        self.id = id
        self.members = members if members is not None else []

    def __repr__(self):
        return self.title

    def increment(self):
        self.vue += 1

    def update_last_build_date(self, updated):
        self.last_build_date = updated

    def is_followed(self, user) -> bool:
        return user in self.members

    def serialize(self):
        return {
            'id': self.id,
            'title': self.title,
            'uid': self.uid,
            'slug': self.slug,
            'image': self.image,
            'description': self.description,
            'category': self.category.serialize(),
            'country': self.country.serialize(),
        }

    def serialize_all(self):
        return {
            'id': self.id,
            'title': self.title,
            'uid': self.uid,
            'slug': self.slug,
            'link': self.link,
            'image': self.image,
            'description': self.description,
            'country': self.country.serialize(),
            'category': self.category.serialize()
        }
