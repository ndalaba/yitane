from App.Domain.BaseEntity import BaseEntity
from App.Domain.User.Entity.User import User
from App.Helper.Str import slug


class Category(BaseEntity):
    image_path: str

    def __init__(self, name: str, image: str = None, id: int = None, uid: str = None, published: bool = False,
                 created_at=None, updated_at=None, members: list[User] = None) -> None:
        BaseEntity.__init__(self, uid, created_at, updated_at, published)
        self.id = id
        self.slug = slug(name)
        self.name = name
        self.image = image
        self.members = members if members is not None else []

    def serialize(self) -> dict:
        return {
            'name': self.name,
            'slug': self.slug,
            "uid": self.uid,
        }

    def serialize_all(self, user=None, image_path: str = None) -> dict:
        if image_path is not None:
            self.set_image_path(image_path)
        return {
            'id': self.id,
            'uid': self.uid,
            'name': self.name,
            'slug': self.slug,
            'image': self.image_path,
            'is_followed': self.is_followed(user),
            'can_followed': True,
        }

    def __repr__(self) -> str:
        return self.name

    def is_followed(self, user: User) -> bool:
        return user in self.members

    def set_image_path(self, path: str):
        if self.image is not None:
            self.image_path = path + self.image
