from abc import ABC, abstractmethod

from App.Domain.Article.Entity.Article import Article
from App.Domain.BaseEntity import BaseEntity
from App.Domain.Localisation.Entity.Country import Country
from App.Domain.User.Entity import User


class ArticleRepositoryInterface(ABC):

    @abstractmethod
    def get(self, uid: str) -> Article:
        pass

    @abstractmethod
    def insert(self, articles: list[dict]) -> None:
        pass

    @abstractmethod
    def create(self, article: Article) -> Article:
        pass

    @abstractmethod
    def update(self, article: Article) -> Article:
        pass

    @abstractmethod
    def delete(self, uid: str) -> None:
        pass

    @abstractmethod
    def get_trend_articles(self, hours=24, size=BaseEntity.PER_PAGE) -> list[Article]:
        pass

    @abstractmethod
    def filter(self, title: str = None, top: bool = None, published: bool = None, magazine_id: int = None,
               category_id: int = None, country: Country = None, date=None, order: str = 'date_published-desc',
               page=1, size=BaseEntity.PER_PAGE) -> list[Article]:
        pass

    @abstractmethod
    def get_saved_articles(self, uid: str, page=1, size=BaseEntity.PER_PAGE) -> list[Article]:
        pass

    @abstractmethod
    def get_loved_articles(self, uid: str, page=1, size=BaseEntity.PER_PAGE) -> list[Article]:
        pass

    @abstractmethod
    def remove_all_by_date_before(self, _date) -> None:
        pass

    @abstractmethod
    def find_member_prefered_article(self, user: User, page=1, size=BaseEntity.PER_PAGE) -> list[Article]:
        pass

    @abstractmethod
    def remove_all_by_uid(self, uids: list[str]) -> None:
        pass

    @abstractmethod
    def publish_all_by_uid(self, uids: list[str]) -> None:
        pass

    @abstractmethod
    def un_publish_all_by_uid(self, uids: list[str]) -> None:
        pass

    @abstractmethod
    def set_top(self, uids: list[str]) -> None:
        pass

    @abstractmethod
    def set_normal(self, uids: list[str]) -> None:
        pass

    @abstractmethod
    def articles_count(self, published: bool = True) -> int:
        pass

    @abstractmethod
    def get_related_articles(self, article: Article, size: int = 24) -> list[Article]:
        pass

    @abstractmethod
    def increment(self, article: Article) -> None:
        pass
