from abc import ABC, abstractmethod

from App.Domain.Article.Entity.Magazine import Magazine
from App.Domain.BaseEntity import BaseEntity


class MagazineRepositoryInterface(ABC):

    @abstractmethod
    def find_magazine(self, title: str, link: str, feed: str, image: str):
        pass

    @abstractmethod
    def get(self, uid: str) -> Magazine:
        pass

    @abstractmethod
    def create(self, magazine: Magazine) -> Magazine:
        pass

    @abstractmethod
    def update(self, magazine: Magazine) -> Magazine:
        pass

    @abstractmethod
    def delete(self, uid: str) -> None:
        pass

    @abstractmethod
    def filter(self, title: str = None, top: bool = None, published: bool = None, country_id: int = None,
               category_id: int = None, page: int = 1, size=BaseEntity.PER_PAGE) -> list[Magazine]:
        pass

    @abstractmethod
    def articles_count(self, magazine: Magazine) -> int:
        pass

    @abstractmethod
    def all(self) -> list[Magazine]:
        pass

    @abstractmethod
    def find_many_by_uids(self, uids: list[str]) -> list[Magazine]:
        pass

    @abstractmethod
    def remove_many_by_uids(self, uids: list[str]) -> None:
        pass

    @abstractmethod
    def publish_all_by_uids(self, uids: list[str]) -> None:
        pass

    @abstractmethod
    def unpublish_all_by_uids(self, uids: list[str]) -> None:
        pass

    @abstractmethod
    def set_top(self, uids: list[str]) -> None:
        pass

    @abstractmethod
    def set_normal(self, uids: list[str]) -> None:
        pass

    @abstractmethod
    def magazines_count(self, published: bool = True) -> int:
        pass
