from abc import ABC, abstractmethod
from typing import List, Any

from App.Domain.Article.Entity.Category import Category


class CategoryRepositoryInterface(ABC):

    @abstractmethod
    def find_by(self, field: str, value: Any) -> Category:
        pass

    @abstractmethod
    def get(self, uid: str) -> Category:
        pass

    @abstractmethod
    def create(self, category: Category) -> Category:
        pass

    @abstractmethod
    def update(self, category: Category) -> Category:
        pass

    @abstractmethod
    def delete(self, uid: str) -> None:
        pass

    @abstractmethod
    def all(self) -> list[Category]:
        pass

    @abstractmethod
    def magazines_count(self, category: Category) -> int:
        pass

    @abstractmethod
    def channels_count(self, category: Category) -> int:
        pass

    @abstractmethod
    def categories_count(self) -> int:
        pass
