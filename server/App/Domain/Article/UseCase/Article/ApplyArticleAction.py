from typing import List

from App.Domain.Article.Repository.ArticleRepositoryInterface import ArticleRepositoryInterface

REMOVE_MANY_BY_UID = -1
PUBLISH_MANY_BY_UID = 1
UNPUBLISH_MANY_BY_UID = 0
REMOVE_MANY_BY_DATE_BEFORE = -11


class ApplyArticleAction:
    def __init__(self, article_repository: ArticleRepositoryInterface):
        self.article_repository = article_repository

    def execute(self, action: int, uids: list[str], _date=None):
        if action == REMOVE_MANY_BY_UID:
            self.article_repository.remove_all_by_uid(uids)
        elif action == PUBLISH_MANY_BY_UID:
            self.article_repository.publish_all_by_uid(uids)
        elif action == UNPUBLISH_MANY_BY_UID:
            self.article_repository.un_publish_all_by_uid(uids)
        elif action == REMOVE_MANY_BY_DATE_BEFORE:
            self.article_repository.remove_all_by_date_before(_date)
