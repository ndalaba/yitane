from App.Domain.Article.Repository.ArticleRepositoryInterface import ArticleRepositoryInterface
from App.Helper.Response import Response


class GetUserLovedArticles:

    def __init__(self, article_repository: ArticleRepositoryInterface):
        self.article_repository = article_repository

    def execute(self, uid: str, page=1) -> Response:
        articles = self.article_repository.get_loved_articles(uid = uid, page = page)
        return Response(data = {'articles': articles})
