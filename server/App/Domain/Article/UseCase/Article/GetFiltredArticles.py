from App.Domain.Article.Repository.ArticleRepositoryInterface import ArticleRepositoryInterface
from App.Domain.Article.Request.ArticleFilterRequest import ArticleFilterRequest
from App.Domain.Localisation.Repository.CountryRepositoryInterface import CountryRepositoryInterface
from App.Helper.Response import Response


class GetFiltredArticles:

    def __init__(self, article_repository: ArticleRepositoryInterface, country_repository: CountryRepositoryInterface):
        self.article_repository = article_repository
        self.country_repository = country_repository

    def execute(self, request: ArticleFilterRequest) -> Response:
        request.country = self.country_repository.find_by('id', request.country_id) if request.country_id is not None else None
        articles = self.article_repository.filter(title = request.title, top = request.top, published = request.published,
                                                  magazine_id = request.magazine_id, category_id = request.category_id,
                                                  country = request.country, date = request.date, order = request.order,
                                                  page = request.page)
        articles = [article.serialize() for article in articles]
        return Response(data = {'articles': articles})
