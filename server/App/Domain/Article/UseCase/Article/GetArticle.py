from App.Domain.Article.Repository.ArticleRepositoryInterface import ArticleRepositoryInterface
from App.Helper.Response import Response


class GetArticle:

    def __init__(self, article_repository: ArticleRepositoryInterface):
        self.article_repository = article_repository

    def execute(self, uid: str) -> Response:
        article = self.article_repository.get(uid)
        article.increment()
        self.article_repository.increment(article)
        articles = self.article_repository.get_related_articles(article)
        related_articles = [article.serialize() for article in articles]
        return Response(data = {'article': article.serialize_all(), 'related_articles': related_articles})
