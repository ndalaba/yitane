from App.Domain.Article.Repository.ArticleRepositoryInterface import ArticleRepositoryInterface
from App.Domain.BaseEntity import BaseEntity
from App.Helper.Response import Response


class GetTrendArticles:

    def __init__(self, article_repository: ArticleRepositoryInterface):
        self.article_repository = article_repository

    def execute(self, hours=24, size=BaseEntity.PER_PAGE) -> Response:
        articles = self.article_repository.get_trend_articles(hours, size)
        return Response(data = {'articles': articles})
