from App.Domain.Article.Repository.ArticleRepositoryInterface import ArticleRepositoryInterface
from App.Helper import FeedParser
from App.Helper.Response import Response


class GetArticleContent:

    def __init__(self, article_repository: ArticleRepositoryInterface):
        self.article_repository = article_repository

    def execute(self, uid: str) -> Response:
        article = self.article_repository.get(uid)
        if article is not None and article.summary == article.body:
            content = FeedParser.scrape_article(article.link, article.magazine)
            if content is not None:
                article.body = content['body']
                article.summary = content['summary']
                article.youtube = content['youtube']
                article.image = content['image']
                self.article_repository.update(article)
        return Response(data = {'article': article.serialize_all()})
