from App.Domain.Article.Repository.ArticleRepositoryInterface import ArticleRepositoryInterface
from App.Helper.Response import Response


class DeleteArticle:

    def __init__(self, article_repository: ArticleRepositoryInterface):
        self.article_repository = article_repository

    def execute(self, uid) -> Response:
        response = Response()
        try:
            self.article_repository.delete(uid)
            response.set_success_message("Article supprimé.")
        except ValueError:
            response.add_error('not_found', "Article non trouvé.")

        return response
