import validators

from App.Domain.Article.Entity.Article import Article
from App.Domain.Article.Repository.ArticleRepositoryInterface import ArticleRepositoryInterface
from App.Domain.Article.Repository.MagazineRepositoryInterface import MagazineRepositoryInterface
from App.Domain.Article.Request.ArticleRequest import ArticleRequest
from App.Helper.Response import Response


class CreateArticle:

    def __init__(self, article_repository: ArticleRepositoryInterface, magazine_repository: MagazineRepositoryInterface):
        self.article_repository = article_repository
        self.magazine_repository = magazine_repository

    def execute(self, request: ArticleRequest) -> Response:
        response = Response()
        is_valid = self.is_request_valid(request, response)
        if is_valid:
            article = Article(*vars(request))
            created_article = self.article_repository.create(article)
            response.add_data("article", created_article)

        return response

    def is_request_valid(self, request: ArticleRequest, response: Response) -> bool:
        try:
            self.magazine_repository.find_by('id', request.magazine_id)
        except ValueError:
            response.add_error('magazine_id', "Le magasine de l'article n'existe pas.")
            return False

        if len(request.title.strip()) < 1:
            response.add_error("title", "Le titre de l'article doit être renseigné.")

        if len(request.guid.strip()) < 1:
            response.add_error("guid", "Le guid de l'article doit être renseigné.")

        if len(request.tag.strip()) < 1:
            response.add_error("tag", "Le tag de l'article doit être renseigné.")

        if len(request.date_published.strip()) < 1:
            response.add_error("date_published", "La date de publication de l'article doit être renseigné.")

        if len(request.summary.strip()) < 10:
            response.add_error("summary", "L'extrait de l'article doit être renseigné(Minimum 10 caractères).")

        if not validators.url(request.link):
            response.add_error("link", "Le lien du l'article doit être renseigné.")

        return response.has_error()
