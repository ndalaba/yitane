from App.Domain.Article.Repository.ArticleRepositoryInterface import ArticleRepositoryInterface
from App.Helper.Response import Response


class GetArticles:

    def __init__(self, article_repository: ArticleRepositoryInterface):
        self.article_repository = article_repository

    def execute(self, page=1) -> Response:
        articles = self.article_repository.filter(page=page)
        return Response(data={'articles': articles})
