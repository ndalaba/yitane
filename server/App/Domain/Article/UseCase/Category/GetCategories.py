from App.Domain.Article.Repository.CategoryRepositoryInterface import CategoryRepositoryInterface
from App.Helper.Response import Response


class GetCategories:

    def __init__(self, category_repository: CategoryRepositoryInterface):
        self.category_repository = category_repository

    def execute(self, image_path: str = None) -> Response:
        categories = self.category_repository.all()
        categories = [category.serialize_all(image_path = image_path) for category in categories]
        return Response(data = {'categories': categories})
