from App.Domain.Article.Entity.Category import Category
from App.Domain.Article.Repository.CategoryRepositoryInterface import CategoryRepositoryInterface
from App.Domain.Article.Request.CategoryRequest import CategoryRequest
from App.Domain.ImageRequest import ImageRequest
from App.Helper.Response import Response
from App.Helper.upload import upload_image, remove_image


class UpdateCategory:

    def __init__(self, category_repository: CategoryRepositoryInterface):
        self.category_repository = category_repository

    def execute(self, category_request: CategoryRequest, image_request: ImageRequest = None) -> Response:
        response = Response()
        is_valid = self.is_request_valid(category_request, response) and self.is_category_valid(category_request, response)
        if is_valid:
            category = Category(name = category_request.name, published = category_request.published, id = category_request.id)
            if image_request.image is not None and image_request.path is not None:
                remove_image(category.image, image_request.path)
                filename = upload_image(image_request.image, image_request.path)
                category.image = filename
            created_category = self.category_repository.update(category)
            response.add_data("category", created_category)

        return response

    @staticmethod
    def is_request_valid(category_request: CategoryRequest, response: Response) -> bool:

        if len(category_request.name.strip()) < 3:
            response.add_error("name", "Le nom de la catégorie doit être renseigné (3 caractères au minimum)")

        return response.has_error()

    def is_category_valid(self, category_request: CategoryRequest, response: Response) -> bool:
        category_exist = self.category_repository.find_by('name', category_request.name)
        if category_exist is not None and category_exist.id != category_request.id:
            response.add_error('category_exist', 'Cette catégorie est déjà enregistrée.')
            return False
        return True
