from App.Domain.Article.Repository.CategoryRepositoryInterface import CategoryRepositoryInterface
from App.Helper.Response import Response


class GetCategory:

    def __init__(self, category_repository: CategoryRepositoryInterface):
        self.category_repository = category_repository

    def execute(self, uid: str) -> Response:
        category = self.category_repository.get(uid)
        return Response(data={'category': category})
