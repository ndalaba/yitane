from http.client import HTTPException

from App.Domain.Article.Entity import Category
from App.Domain.Article.Repository.CategoryRepositoryInterface import CategoryRepositoryInterface
from App.Helper.Response import Response
from App.Helper.upload import remove_image


class DeleteCategory:

    def __init__(self, category_repository: CategoryRepositoryInterface):
        self.category_repository = category_repository

    def execute(self, uid, image_path: str = None) -> Response:
        response = Response()
        category_exist, category = self.category_exist(uid)
        if category_exist and not self.category_has_channels(category, response) and not self.category_has_magazines(category, response):
            remove_image(category.image, image_path)
            self.category_repository.delete(uid)
            response.set_success_message("Categorie supprimée.")

        return response

    def category_exist(self, uid: str) -> (bool, Category):
        try:
            category = self.category_repository.get(uid)
            return True, category
        except HTTPException:
            return False, None

    def category_has_magazines(self, category: Category, response: Response) -> bool:
        if self.category_repository.magazines_count(category) > 0:
            response.add_error('has_magazines', "Cette catégorie a des magasines.")
            return True
        return False

    def category_has_channels(self, category: Category, response: Response) -> bool:
        if self.category_repository.channels_count(category) > 0:
            response.add_error('has_channels', "Cette catégorie a des chaines.")
            return True
        return False
