from datetime import datetime, timedelta

from App.Domain.Article.Repository.ArticleRepositoryInterface import ArticleRepositoryInterface
from App.Domain.Article.Repository.MagazineRepositoryInterface import MagazineRepositoryInterface
from App.Domain.Error.Repository.ErrorRepositoryInterface import ErrorRepositoryInterface
from App.Helper import FeedParser
from App.Helper.Response import Response


class RefreshMagazinesEntries:
    def __init__(self, magazine_repository: MagazineRepositoryInterface, article_repository: ArticleRepositoryInterface, error_repository: ErrorRepositoryInterface):
        self.article_repository = article_repository
        self.magazine_repository = magazine_repository
        self.error_repository = error_repository

    def execute(self, uids: list[str], verify_date: bool = True, get_content: bool = False) -> Response:
        magazines = self.magazine_repository.find_many_by_uids(uids)
        response = Response()
        hour = timedelta(minutes = 40)
        now = datetime.now()
        articles = []
        for magazine in magazines:
            if magazine.last_build_date is not None:
                if now - magazine.last_build_date > hour or verify_date is False:
                    parsed = FeedParser.parse(magazine.feed)
                    if parsed is not None:
                        # feed = parsed.get('feed')
                        items = parsed.get("items")
                        posts, errors = FeedParser.get_articles(items, magazine, get_content)
                        for post in posts:
                            articles.append(post)
                        magazine.update_last_build_date(datetime.now())
                        self.magazine_repository.update(magazine)
                        self.error_repository.insert_ignore(errors)
            else:
                magazine.update_last_build_date(datetime.now())
                self.magazine_repository.update(magazine)

        self.article_repository.insert(articles)
        return response
