from App.Domain.Article.Repository.ArticleRepositoryInterface import ArticleRepositoryInterface
from App.Domain.Article.Repository.MagazineRepositoryInterface import MagazineRepositoryInterface
from App.Domain.Article.UseCase.Magazine.RefreshMagazinesEntries import RefreshMagazinesEntries
from App.Domain.Error.Repository.ErrorRepositoryInterface import ErrorRepositoryInterface

REMOVE_MANY_BY_UID = -1
PUBLISH_MANY_BY_UID = 1
UNPUBLISH_MANY_BY_UID = 0
REFRESH_MAGAZINES_ENTRIES = 10
SET_TOP = 2
SET_NORMAL = -2


class ApplyMagazineAction:
    def __init__(self, magazine_repository: MagazineRepositoryInterface, article_repository: ArticleRepositoryInterface, error_repository: ErrorRepositoryInterface):
        self.magazine_repository = magazine_repository
        self.article_repository = article_repository
        self.error_repository = error_repository
        self.refresh_magazines_entries = RefreshMagazinesEntries(magazine_repository, article_repository, error_repository)

    def execute(self, action: int, uids: list[str]):
        if action == REMOVE_MANY_BY_UID:
            self.magazine_repository.remove_many_by_uids(uids)
        elif action == PUBLISH_MANY_BY_UID:
            self.magazine_repository.publish_all_by_uids(uids)
        elif action == UNPUBLISH_MANY_BY_UID:
            self.magazine_repository.unpublish_all_by_uids(uids)
        elif action == REFRESH_MAGAZINES_ENTRIES:
            self.refresh_magazines_entries.execute(uids)
        elif action == SET_TOP:
            self.magazine_repository.set_top(uids)
        elif action == SET_NORMAL:
            self.magazine_repository.set_normal(uids)
