import validators

from App.Domain.Article.Entity.Magazine import Magazine
from App.Domain.Article.Repository.CategoryRepositoryInterface import CategoryRepositoryInterface
from App.Domain.Article.Repository.MagazineRepositoryInterface import MagazineRepositoryInterface
from App.Domain.Article.Request.MagazineRequest import MagazineRequest
from App.Domain.Localisation.Repository.CountryRepositoryInterface import CountryRepositoryInterface
from App.Helper.Response import Response


class UpdateMagazine:

    def __init__(self, magazine_repository: MagazineRepositoryInterface, country_repository: CountryRepositoryInterface,
                 category_repository: CategoryRepositoryInterface):
        self.magazine_repository = magazine_repository
        self.country_repository = country_repository
        self.category_repository = category_repository

    def execute(self, request: MagazineRequest) -> Response:
        response = Response()
        is_valid = self.is_request_valid(request, response) and self.is_magazine_valid(request, response)
        if is_valid:
            magazine = Magazine(*vars(request))
            created_magazine = self.magazine_repository.update(magazine)
            response.add_data("magazine", created_magazine)

        return response

    def is_request_valid(self, request: MagazineRequest, response: Response) -> bool:

        try:
            self.category_repository.find_by('id', request.category_id)
        except ValueError:
            response.add_error('category_id', "La catégorie du magasine n'existe pas.")
            return False

        try:
            self.country_repository.find_by('id', request.country_id)
        except ValueError:
            response.add_error('country_id', "Le pays du magasine n'existe pas.")
            return False

        if len(request.title.strip()) < 1:
            response.add_error("title", "Le titre du magasine doit être renseigné.")

        if not validators.uuid(request.uid):
            response.add_error("uid", "L'identifiant du magasine doit être renseigné.")

        if not validators.url(request.feed):
            response.add_error("feed", "Le lien du flux du magasine doit être renseigné.")

        if not validators.url(request.link):
            response.add_error("link", "Le lien du magasine doit être renseigné.")

        if not validators.url(request.image):
            response.add_error("image", "Le lien du logo du magasine doit être renseigné.")

        return response.has_error()

    def is_magazine_valid(self, request: MagazineRequest, response: Response) -> bool:
        magazine_exist = self.magazine_repository.find_magazine(title = request.title, link = request.link,
                                                                feed = request.link, image = request.image)

        if magazine_exist is not None:
            response.add_error('magazine_exist', 'Ce magasine est déjà enregistré.')
            return False
        return True
