from App.Domain.Article.Repository.MagazineRepositoryInterface import MagazineRepositoryInterface
from App.Helper import FeedParser
from App.Helper.Response import Response


class CheckMagazineArticles:
    def __init__(self, repository: MagazineRepositoryInterface):
        self.repository = repository

    def execute(self, uid: str) -> Response:
        magazine = self.repository.get(uid)
        parsed = FeedParser.parse(magazine.feed)
        articles = []
        if parsed is not None:
            articles, _ = FeedParser.get_articles(parsed.get('items'), magazine)
        return Response({'articles': articles})
