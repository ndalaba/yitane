from App.Domain.Article.Repository.MagazineRepositoryInterface import MagazineRepositoryInterface
from App.Domain.Article.Request.MagazineFilterRequest import MagazineFilterRequest
from App.Helper.Response import Response


class GetFiltredMagazines:

    def __init__(self, magazine_repository: MagazineRepositoryInterface):
        self.magazine_repository = magazine_repository

    def execute(self, request: MagazineFilterRequest) -> Response:
        magazines = self.magazine_repository.filter(*vars(request))
        magazines = [magazine.serialize() for magazine in magazines]
        return Response(data = {'magazines': magazines})
