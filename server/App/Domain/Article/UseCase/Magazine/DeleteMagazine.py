from http.client import HTTPException

from App.Domain.Article.Entity import Magazine
from App.Domain.Article.Repository.MagazineRepositoryInterface import MagazineRepositoryInterface
from App.Helper.Response import Response


class DeleteMagazine:

    def __init__(self, magazine_repository: MagazineRepositoryInterface):
        self.magazine_repository = magazine_repository

    def execute(self, uid) -> Response:
        response = Response()
        magazine_exist, magazine = self.magazine_exist(uid)
        if magazine_exist and not self.magazine_has_articles(magazine, response):
            self.magazine_repository.delete(uid)
            response.set_success_message("Magasine supprimé.")

        return response

    def magazine_exist(self, uid: str) -> (bool, Magazine):
        try:
            magazine = self.magazine_repository.get(uid)
            return True, magazine
        except HTTPException:
            return False, None

    def magazine_has_articles(self, magazine: Magazine, response: Response) -> bool:
        if self.magazine_repository.articles_count(magazine) > 0:
            response.add_error('has_articles', "Ce magasine a des articles.")
            return True
        return False
