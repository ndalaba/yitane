from App.Domain.Article.Repository.ArticleRepositoryInterface import ArticleRepositoryInterface
from App.Domain.Article.Repository.MagazineRepositoryInterface import MagazineRepositoryInterface
from App.Helper.Response import Response


class GetMagazine:

    def __init__(self, magazine_repository: MagazineRepositoryInterface, article_repository: ArticleRepositoryInterface):
        self.magazine_repository = magazine_repository
        self.article_repository = article_repository

    def execute(self, uid: str, page: int = 1) -> Response:
        try:
            magazine = self.magazine_repository.get(uid)
        except ValueError:
            magazine = self.magazine_repository.find_by('slug', uid)
        if magazine is None:
            return Response(errors = {'not_found': 'Magazine non trouvée.'})
        articles = self.article_repository.filter(magazine_id = magazine.id, page = page)
        articles = [article.serialize() for article in articles]
        magazine.increment()
        self.magazine_repository.update(magazine)

        return Response(data = {'magazine': magazine, 'articles': articles})
