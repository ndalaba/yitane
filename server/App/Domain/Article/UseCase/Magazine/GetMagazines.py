from App.Domain.Article.Repository.MagazineRepositoryInterface import MagazineRepositoryInterface
from App.Helper.Response import Response


class GetMagazines:

    def __init__(self, magazine_repository: MagazineRepositoryInterface):
        self.magazine_repository = magazine_repository

    def execute(self) -> Response:
        magazines = self.magazine_repository.all()
        return Response(data={'magazines': magazines})
