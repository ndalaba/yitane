from App.Domain.Video.Repository.ChannelRepositoryInterface import ChannelRepositoryInterface
from App.Helper.Response import Response


class GetChannels:

    def __init__(self, channel_repository: ChannelRepositoryInterface):
        self.channel_repository = channel_repository

    def execute(self) -> Response:
        channels = self.channel_repository.all()
        return Response(data={'channels': channels})
