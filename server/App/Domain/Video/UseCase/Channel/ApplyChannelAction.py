from App.Domain.Video.Repository.ChannelRepositoryInterface import ChannelRepositoryInterface
from App.Domain.Video.Repository.VideoRepositoryInterface import VideoRepositoryInterface
from App.Domain.Video.UseCase.Channel.RefreshChannelsEntries import RefreshChannelsEntries

REMOVE_MANY_BY_UID = -1
PUBLISH_MANY_BY_UID = 1
UNPUBLISH_MANY_BY_UID = 0
REFRESH_CHANNELS_ENTRIES = 10


class ApplyChannelAction:
    def __init__(self, channel_repository: ChannelRepositoryInterface, video_repository: VideoRepositoryInterface):
        self.channel_repository = channel_repository
        self.video_repository = video_repository
        self.refresh_channels_entries = RefreshChannelsEntries(channel_repository, video_repository)

    def execute(self, action: int, uids: list[str]):
        if action == REMOVE_MANY_BY_UID:
            self.channel_repository.remove_many_by_uids(uids)
        elif action == PUBLISH_MANY_BY_UID:
            self.channel_repository.publish_all_by_uids(uids)
        elif action == UNPUBLISH_MANY_BY_UID:
            self.channel_repository.unpublish_all_by_uids(uids)
        elif action == REFRESH_CHANNELS_ENTRIES:
            self.refresh_channels_entries.execute(uids)
