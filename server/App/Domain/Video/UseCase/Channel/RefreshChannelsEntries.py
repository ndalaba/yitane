import json
from datetime import datetime, timedelta
from typing import List

import requests
from dateutil import parser
from requests import Response as RequestsResponse

from App.Domain.Video.Repository.ChannelRepositoryInterface import ChannelRepositoryInterface
from App.Domain.Video.Repository.VideoRepositoryInterface import VideoRepositoryInterface
from App.Helper.Str import generate_uuid
from App.Helper.Response import Response
from instance.config import YOUTUBE_API_KEY


class RefreshChannelsEntries:
    def __init__(self, channel_repository: ChannelRepositoryInterface, video_repository: VideoRepositoryInterface):
        self.video_repository = video_repository
        self.channel_repository = channel_repository

    def execute(self, uids: list[str], max_result=10, verify_date=True) -> Response:
        channels = self.channel_repository.find_many_by_uids(uids)
        response = Response()
        videos = []
        now = datetime.now()
        for channel in channels:
            hour = timedelta(hours = channel.refresh_time)
            if channel.last_build_date is not None:
                if now - channel.last_build_date > hour or verify_date is False:
                    try:
                        url = "https://www.googleapis.com/youtube/v3/search?part=snippet&channelId=" + channel.channel_id + "&maxResults=" \
                              + str(max_result) + "&order=date&type=video&key=" + YOUTUBE_API_KEY
                        request_response: RequestsResponse = requests.get(url)
                        content = json.loads(request_response.text)

                        for item in content['items']:
                            parsed_time = parser.isoparse(item['snippet']['publishedAt'])
                            videos.append({
                                "uid": generate_uuid(5),
                                "video_id": item['id']['videoId'],
                                "title": item['snippet']['title'],
                                "body": item['snippet']['description'],
                                "channel_id": channel.id,
                                "image": item['snippet']['thumbnails']['high']['url'],
                                "date_published": parsed_time
                            })
                        channel.update_last_build_date(datetime.now())
                        self.channel_repository.update(channel)
                    except ConnectionError:
                        response.add_error('channel' + channel.channel_id, 'Erreur récupération vidéo')
            else:
                channel.update_last_build_date(datetime.now())
                self.channel_repository.update(channel)

        self.video_repository.insert(videos)
        return response
