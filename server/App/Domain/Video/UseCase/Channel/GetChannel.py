from App.Domain.Video.Repository.ChannelRepositoryInterface import ChannelRepositoryInterface
from App.Domain.Video.Repository.VideoRepositoryInterface import VideoRepositoryInterface
from App.Helper.Response import Response


class GetChannel:

    def __init__(self, channel_repository: ChannelRepositoryInterface, video_repository: VideoRepositoryInterface):
        self.channel_repository = channel_repository
        self.video_repository = video_repository

    def execute(self, uid: str, page=1) -> Response:
        try:
            channel = self.channel_repository.get(uid)
        except ValueError:
            channel = self.channel_repository.find_by('slug', uid)
        if channel is None:
            return Response(errors = {'not_found': 'Chaine non trouvée.'})
        videos = self.video_repository.filter(channel_id = channel.id, page = page)
        videos = [video.serialize() for video in videos]
        channel.increment()
        self.channel_repository.update(channel)

        return Response(data = {'channel': channel, 'videos': videos})
