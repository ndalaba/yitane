from App.Domain.Video.Repository.ChannelRepositoryInterface import ChannelRepositoryInterface
from App.Domain.Video.Request.ChannelFilterRequest import ChannelFilterRequest
from App.Helper.Response import Response


class GetFiltredChannels:

    def __init__(self, channel_repository: ChannelRepositoryInterface):
        self.channel_repository = channel_repository

    def execute(self, request: ChannelFilterRequest) -> Response:
        channels = self.channel_repository.filter(*vars(request))
        channels = [channel.serialize() for channel in channels]
        return Response(data = {'channels': channels})
