import validators

from App.Domain.Article.Repository.CategoryRepositoryInterface import CategoryRepositoryInterface
from App.Domain.Localisation.Repository.CountryRepositoryInterface import CountryRepositoryInterface
from App.Domain.Video.Entity.Channel import Channel
from App.Domain.Video.Repository.ChannelRepositoryInterface import ChannelRepositoryInterface
from App.Domain.Video.Request.ChannelRequest import ChannelRequest
from App.Helper.Response import Response


class UpdateChannel:

    def __init__(self, channel_repository: ChannelRepositoryInterface, country_repository: CountryRepositoryInterface,
                 category_repository: CategoryRepositoryInterface):
        self.channel_repository = channel_repository
        self.country_repository = country_repository
        self.category_repository = category_repository

    def execute(self, request: ChannelRequest) -> Response:
        response = Response()
        is_valid = self.is_request_valid(request, response) and self.is_channel_valid(request, response)
        if is_valid:
            channel = Channel(*vars(request))
            created_channel = self.channel_repository.update(channel)
            response.add_data("channel", created_channel)

        return response

    def is_request_valid(self, request: ChannelRequest, response: Response) -> bool:
        try:
            self.category_repository.find_by('id', request.category_id)
        except ValueError:
            response.add_error('category_id', "La catégorie de la chaine n'existe pas.")
            return False

        try:
            self.country_repository.find_by('id', request.country_id)
        except ValueError:
            response.add_error('country_id', "Le pays de la chaine n'existe pas.")
            return False

        if not validators.uuid(request.uid):
            response.add_error("uid", "L'identifiant du magasine doit être renseigné.")

        if len(request.title.strip()) < 1:
            response.add_error("title", "Le titre de la chaine doit être renseigné.")

        if len(request.channel_id.strip()) < 1:
            response.add_error("channel_id", "L'ID de la chaine doit être renseigné.")

        if not validators.url(request.link):
            response.add_error("link", "Le lien de la chaine doit être renseigné.")

        if not validators.url(request.image):
            response.add_error("image", "Le lien du logo de la chaine doit être renseigné.")

        return response.has_error()

    def is_channel_valid(self, request: ChannelRequest, response: Response) -> bool:
        channel_exist = self.channel_repository.find_channel(title = request.title, link = request.link, channel_id = request.channel_id, image = request.image)

        if channel_exist is not None:
            response.add_error('channel_exist', 'Ce magasine est déjà enregistré.')
            return False
        return True
