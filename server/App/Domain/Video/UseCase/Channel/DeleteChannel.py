from http.client import HTTPException

from App.Domain.Video.Entity import Channel
from App.Domain.Video.Repository.ChannelRepositoryInterface import ChannelRepositoryInterface
from App.Helper.Response import Response


class DeleteChannel:

    def __init__(self, channel_repository: ChannelRepositoryInterface):
        self.channel_repository = channel_repository

    def execute(self, uid) -> Response:
        response = Response()
        channel_exist, channel = self.channel_exist(uid)
        if channel_exist and not self.channel_has_articles(Channel, response):
            self.channel_repository.delete(uid)
            response.set_success_message("Chaine supprimée.")

        return response

    def channel_exist(self, uid: str) -> (bool, Channel):
        try:
            channel = self.channel_repository.get(uid)
            return True, channel
        except HTTPException:
            return False, None

    def channel_has_articles(self, channel: Channel, response: Response) -> bool:
        if self.channel_repository.videos_count(channel) > 0:
            response.add_error('has_videos', "Cette chaine a des vidéos.")
            return True
        return False
