from App.Domain.Video.Repository.VideoRepositoryInterface import VideoRepositoryInterface
from App.Helper.Response import Response


class GetUserSavedVideos:

    def __init__(self, video_repository: VideoRepositoryInterface):
        self.video_repository = video_repository

    def execute(self, uid: str, page=1) -> Response:
        videos = self.video_repository.get_loved_videos(uid = uid, page = page)
        videos = [video.serialize() for video in videos]
        return Response(data = {'videos': videos})
