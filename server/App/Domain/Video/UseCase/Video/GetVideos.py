from App.Domain.Video.Repository.VideoRepositoryInterface import VideoRepositoryInterface
from App.Helper.Response import Response


class GetVideos:

    def __init__(self, video_repository: VideoRepositoryInterface):
        self.video_repository = video_repository

    def execute(self, page=1) -> Response:
        videos = self.video_repository.filter(page=page)
        return Response(data={'videos': videos})
