from App.Domain.Video.Repository.VideoRepositoryInterface import VideoRepositoryInterface
from App.Helper.Response import Response


class GetVideo:

    def __init__(self, video_repository: VideoRepositoryInterface):
        self.video_repository = video_repository

    def execute(self, uid: str) -> Response:
        video = self.video_repository.find_by('uid', uid)
        video.increment()
        self.video_repository.update(video)
        related_videos = [video.serialize() for video in self.video_repository.get_related_videos(video)]
        return Response(data = {'video': video.serialize(), 'related_videos': related_videos})
