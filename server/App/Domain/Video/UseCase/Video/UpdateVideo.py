import validators

from App.Domain.Video.Entity.Video import Video
from App.Domain.Video.Repository.ChannelRepositoryInterface import ChannelRepositoryInterface
from App.Domain.Video.Repository.VideoRepositoryInterface import VideoRepositoryInterface
from App.Domain.Video.Request.VideoRequest import VideoRequest
from App.Helper.Response import Response


class UpdateVideo:

    def __init__(self, video_repository: VideoRepositoryInterface, channel_repository: ChannelRepositoryInterface):
        self.video_repository = video_repository
        self.channel_repository = channel_repository

    def execute(self, request: VideoRequest) -> Response:
        response = Response()
        is_valid = self.is_request_valid(request, response)
        if is_valid:
            video = Video(*vars(request))
            created_video = self.video_repository.update(video)
            response.add_data("video", created_video)

        return response

    def is_request_valid(self, request: VideoRequest, response: Response) -> bool:
        try:
            self.channel_repository.find_by('id', request.channel_id)
        except ValueError:
            response.add_error('channel_id', "Le magasine de la video n'existe pas.")
            return False

        if not validators.uuid(request.uid):
            response.add_error("uid", "L'identifiant de la vidéo doit être renseigné.")

        if len(request.title.strip()) < 1:
            response.add_error("title", "Le titre de la video doit être renseigné.")

        if len(request.video_id.strip()) < 1:
            response.add_error("guid", "Le guid de la video doit être renseigné.")

        if len(request.body.strip()) < 1:
            response.add_error("body", "Le contenu de la video doit être renseigné.")

        if len(request.image.strip()) < 1:
            response.add_error("image", "L'image de la video doit être renseignée.")

        if len(request.date_published.strip()) < 1:
            response.add_error("date_published", "La date de publication de la video doit être renseignée.")

        return response.has_error()
