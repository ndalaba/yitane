from App.Domain.Localisation.Repository.CountryRepositoryInterface import CountryRepositoryInterface
from App.Domain.Video.Repository.VideoRepositoryInterface import VideoRepositoryInterface
from App.Domain.Video.Request.VideoFilterRequest import VideoFilterRequest
from App.Helper.Response import Response


class GetFiltredVideos:

    def __init__(self, video_repository: VideoRepositoryInterface, country_repository: CountryRepositoryInterface):
        self.video_repository = video_repository
        self.country_repository = country_repository

    def execute(self, request: VideoFilterRequest) -> Response:
        request.country = self.country_repository.find_by('id', request.country_id) if request.country_id is not None else None
        videos = self.video_repository.filter(title = request.title, country_id = request.country_id, category_id = request.category_id, channel_id = request.channel_id,
                                              published = request.published, date = request.date, order = request.order, page = request.page)
        videos = [video.serialize() for video in videos]
        return Response(data = {'videos': videos})
