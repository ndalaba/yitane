from App.Domain.Video.Repository.VideoRepositoryInterface import VideoRepositoryInterface
from App.Helper.Response import Response


class DeleteVideo:

    def __init__(self, video_repository: VideoRepositoryInterface):
        self.video_repository = video_repository

    def execute(self, uid) -> Response:
        response = Response()
        try:
            self.video_repository.delete(uid)
            response.set_success_message("Vidéo supprimé.")
        except ValueError:
            response.add_error('not_found', "Vidéo non trouvé.")

        return response
