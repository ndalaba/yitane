from typing import List

from App.Domain.Video.Repository.VideoRepositoryInterface import VideoRepositoryInterface

REMOVE_MANY_BY_UID = -1
PUBLISH_MANY_BY_UID = 1
UNPUBLISH_MANY_BY_UID = 0
REMOVE_MANY_BY_DATE_BEFORE = -11


class ApplyVideoAction:
    def __init__(self, video_repository: VideoRepositoryInterface):
        self.video_repository = video_repository

    def execute(self, action: int, uids: list[str], _date=None):
        if action == REMOVE_MANY_BY_UID:
            self.video_repository.remove_all_by_uid(uids)
        elif action == PUBLISH_MANY_BY_UID:
            self.video_repository.publish_all_by_uid(uids)
        elif action == UNPUBLISH_MANY_BY_UID:
            self.video_repository.unpublish_all_by_uid(uids)
        elif action == REMOVE_MANY_BY_DATE_BEFORE:
            self.video_repository.remove_all_by_date_before(_date)
