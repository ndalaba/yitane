class ChannelFilterRequest:

    def __init__(self, title: str = None, country_id: int = None, category_id: int = None, published: bool = None):
        self.title = title
        self.published = published
        self.country_id = country_id
        self.category_id = category_id
