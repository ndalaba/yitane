class ChannelRequest:

    def __init__(self, link: str, channel_id: str, title: str, country_id: int, category_id: int,
                 description: str = None, image: str = None, last_build_date=None, refresh_time: int = None,
                 uid: str = None, published: bool = False, id: int = None):
        self.title = title
        self.link = link
        self.channel_id = channel_id
        self.image = image
        self.country_id = country_id
        self.category_id = category_id
        self.description = description
        self.last_build_date = last_build_date
        self.refresh_time = refresh_time
        self.id = id
        self.uid = uid
        self.published = published
