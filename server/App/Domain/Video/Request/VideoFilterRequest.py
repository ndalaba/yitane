class VideoFilterRequest:

    def __init__(self, title: str = None, country_id: int = None, category_id: int = None, channel_id: int = None,
                 published: bool = None, date=None, order: str = None, page=1):
        self.title = title.strip()
        self.published = published
        self.channel_id = channel_id
        self.country_id = country_id
        self.category_id = category_id
        self.page = page
        self.date = date
        self.order = order
