class VideoRequest:

    def __init__(self, title: str, video_id: str, body: str, channel_id: int, date_published, image: str = None,
                 published: bool = False, uid: str = None, id: int = None):
        self.title = title
        self.video_id = video_id
        self.body = body
        self.channel_id = channel_id
        self.id = id
        self.uid = uid
        self.date_published = date_published
        self.image = image
        self.published = published
