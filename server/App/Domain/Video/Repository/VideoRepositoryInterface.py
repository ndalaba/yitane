from abc import ABC, abstractmethod
from typing import List, Any

from App.Domain.BaseEntity import BaseEntity
from App.Domain.User.Entity import User
from App.Domain.Video.Entity.Video import Video


class VideoRepositoryInterface(ABC):

    @abstractmethod
    def get(self, uid) -> Video:
        pass

    @abstractmethod
    def insert(self, videos: list[dict]) -> None:
        pass

    @abstractmethod
    def create(self, video: Video) -> Video:
        pass

    @abstractmethod
    def update(self, video: Video) -> Video:
        pass

    @abstractmethod
    def delete(self, uid: str) -> None:
        pass

    @abstractmethod
    def filter(self, title: str = None, country_id: int = None, category_id: int = None, channel_id: int = None,
               published: bool = None, date=None, order: str = None,
               page=1, size=BaseEntity.PER_PAGE) -> list[Video]:
        pass

    @abstractmethod
    def related_videos(self, video: Video) -> list[Video]:
        pass

    @abstractmethod
    def trend_videos(self, hours=24, size: int = BaseEntity.PER_PAGE) -> list[Video]:
        pass

    @abstractmethod
    def remove_all_by_date_before(self, _date) -> None:
        pass

    @abstractmethod
    def remove_all_by_uid(self, uids: list[str]) -> None:
        pass

    @abstractmethod
    def publish_all_by_uid(self, uids: list[str]) -> None:
        pass

    @abstractmethod
    def unpublish_all_by_uid(self, uids: list[str]) -> None:
        pass

    @abstractmethod
    def member_prefered_videos(self, user: User, page=1, size=BaseEntity.PER_PAGE) -> list[Video]:
        pass

    @abstractmethod
    def videos_count(self, published: bool = True) -> int:
        pass

    @abstractmethod
    def get_saved_videos(self, uid: str, page=1, size=BaseEntity.PER_PAGE) -> list[Video]:
        pass

    @abstractmethod
    def get_loved_videos(self, uid: str, page=1, size=BaseEntity.PER_PAGE) -> list[Video]:
        pass

    @abstractmethod
    def get_related_videos(self, video: Video) -> list[Video]:
        pass
