from abc import ABC, abstractmethod

from App.Domain.Video.Entity.Channel import Channel


class ChannelRepositoryInterface(ABC):

    @abstractmethod
    def create(self, channel: Channel) -> Channel:
        pass

    @abstractmethod
    def update(self, channel: Channel) -> Channel:
        pass

    @abstractmethod
    def delete(self, uid: str) -> None:
        pass

    @abstractmethod
    def filter(self, title: str = None, country_id: int = None, category_id: int = None,
               published: bool = None) -> list[Channel]:
        pass

    @abstractmethod
    def get(self, uid) -> Channel:
        pass

    @abstractmethod
    def videos_count(self, channel: Channel) -> int:
        pass

    @abstractmethod
    def all(self) -> list[Channel]:
        pass

    @abstractmethod
    def find_channel(self, title: str, link: str, channel_id: str, image: str) -> Channel:
        pass

    @abstractmethod
    def remove_many_by_uids(self, uids: list[str]) -> None:
        pass

    @abstractmethod
    def publish_all_by_uids(self, uids: list[str]) -> None:
        pass

    @abstractmethod
    def unpublish_all_by_uids(self, uids: list[str]) -> None:
        pass

    @abstractmethod
    def find_many_by_uids(self, uids: list[str]) -> list[Channel]:
        pass

    @abstractmethod
    def channels_count(self, published: bool = True) -> int:
        pass
