from App.Domain.BaseEntity import BaseEntity
from App.Domain.Video.Entity.Channel import Channel
from App.Helper.Str import slug


class Video(BaseEntity):

    def __init__(self, title: str, video_id: str, body: str, channel_id: int, date_published, love: int = 0,
                 comments_count: int = 0, image: str = None, vue: int = 0, channel: Channel = None,
                 uid: str = None, published: bool = False, created_at=None, updated_at=None, id: int = None):
        BaseEntity.__init__(self, uid, created_at, updated_at, published)

        self.id = id
        self.title = title
        self.slug = slug(title)
        self.video_id = video_id
        self.body = body
        self.love = love
        self.comments_count = comments_count
        self.vue = vue
        self.image = image
        self.date_published = date_published
        self.channel_id = channel_id
        self.channel = channel

    def increment(self) -> None:
        self.vue += 1

    def add_comment_count(self) -> None:
        self.comments_count += 1

    def add_love(self) -> None:
        self.love += 1

    def remove_love(self) -> None:
        self.love -= 1

    def __repr__(self) -> str:
        return self.title

    def serialize(self) -> dict:
        return {
            'id': self.id,
            'video_id': self.video_id,
            'title': self.title,
            "uid": self.uid,
            'vue': self.vue,
            'body': self.body,
            'image': self.image,
            'date_published': self.created_at,
            'channel': self.channel.serialize(),
            'slug': self.slug,
            "love": self.love,
            "url": self.url,
            "comments_count": self.comments_count,
            "created_at": self.created_at
        }

    @property
    def url(self):
        return "/videos/" + self.channel.slug + "/" + self.slug + "/" + self.uid
