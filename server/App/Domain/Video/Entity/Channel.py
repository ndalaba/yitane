from datetime import datetime

from App.Domain.Article.Entity import Category
from App.Domain.BaseEntity import BaseEntity
from App.Domain.Localisation.Entity import Country
from App.Domain.User.Entity import User
from App.Helper.Str import slug


class Channel(BaseEntity):

    def __init__(self, link: str, channel_id: str, title: str, country_id: int, category_id: int,
                 description: str = None, image: str = None, vue: int = 0, last_build_date=None,
                 refresh_time: int = None, category: Category = None, country: Country = None,
                 uid: str = None, published: bool = False, created_at=None, updated_at=None, id: int = None,
                 members: list[User] = None):
        BaseEntity.__init__(self, uid, created_at, updated_at, published)

        self.id = id
        self.link = link
        self.title = title
        self.slug = slug(title)
        self.country_id = country_id
        self.channel_id = channel_id
        self.category_id = category_id
        self.description = description
        self.image = image
        self.refresh_time = refresh_time
        self.country = country
        self.category = category
        self.vue = vue
        self.last_build_date = last_build_date if last_build_date is not None else datetime.utcnow()
        self.members = members

    def __repr__(self) -> str:
        return self.title

    def increment(self):
        self.vue += 1

    def update_last_build_date(self, updated):
        self.last_build_date = updated

    def is_followed(self, user) -> bool:
        return user in self.members

    def serialize(self):
        return {
            'id': self.id,
            'title': self.title,
            'channel_id': self.channel_id,
            'uid': self.uid,
            'slug': self.slug,
            'image': self.image,
            'description': self.description,
            'category': self.category.serialize(),
            'country': self.country.serialize(),
        }

    def serialize_all(self):
        return {
            'id': self.id,
            'title': self.title,
            'channel_id': self.channel_id,
            'uid': self.uid,
            'slug': self.slug,
            'link': self.link,
            'image': self.image,
            'description': self.description,
            'country': self.country.serialize(),
            'category': self.category.serialize()
        }
