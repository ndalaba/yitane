from typing import Any


class ImageRequest:
    def __init__(self, image: Any, path: str = ''):
        self.image = image
        self.path = path
