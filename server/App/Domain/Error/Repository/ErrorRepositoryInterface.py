from abc import ABC, abstractmethod
from typing import List, Any

from App.Domain.BaseEntity import BaseEntity
from App.Domain.Error.Entity.Error import Error


class ErrorRepositoryInterface(ABC):

    @abstractmethod
    def find_by(self, field: str, value: Any) -> Error:
        pass

    @abstractmethod
    def insert_ignore(self, errors: list[dict]):
        pass

    @abstractmethod
    def create(self, error: Error) -> Error:
        pass

    @abstractmethod
    def delete(self, uid: str) -> None:
        pass

    @abstractmethod
    def filter(self, resolved: bool, page=1, size=BaseEntity.PER_PAGE) -> list[Error]:
        pass

    @abstractmethod
    def set_resolved(self, uids: list[str]):
        pass

    @abstractmethod
    def set_unresolved(self, uids: list[str]):
        pass

    @abstractmethod
    def remove_many_by_uids(self, uids: list[str]):
        pass
