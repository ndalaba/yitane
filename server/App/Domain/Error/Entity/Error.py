from App.Domain.BaseEntity import BaseEntity


class Error(BaseEntity):
    PER_PAGE = 12

    def __init__(self, name: str, file: str, line: str, description: str, resolved: bool = False, id: int = None,
                 uid: str = None, published: bool = False, created_at=None, updated_at=None):
        BaseEntity.__init__(self, uid, created_at, updated_at, published)

        self.id = id
        self.name = name
        self.file = file
        self.line = line
        self.description = description
        self.resolved = resolved

    def __repr__(self):
        return self.name

    def serialize(self):
        return {
            'name': self.name,
            'file': self.file,
            'line': self.line,
            'description': self.description,
            "resolved": self.resolved,
            "uid": self.uid
        }
