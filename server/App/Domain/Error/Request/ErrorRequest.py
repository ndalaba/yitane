class ErrorRequest:
    def __init__(self, name: str, file: str, line: str, description: str, resolved: bool = False, id: int = None, uid: str = None):
        self.id = id
        self.name = name
        self.uid = uid
        self.file = file
        self.line = line
        self.description = description
        self.resolved = resolved
