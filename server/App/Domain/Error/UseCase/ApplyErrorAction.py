from typing import List

from App.Domain.Error.Repository.ErrorRepositoryInterface import ErrorRepositoryInterface

REMOVE_MANY_BY_UID = -1
SET_RESOLVED = 1
SET_UNRESOLVED = 0


class ApplyErrorAction:
    def __init__(self, error_repository: ErrorRepositoryInterface):
        self.error_repository = error_repository

    def execute(self, action: int, uids: list[str]):
        if action == REMOVE_MANY_BY_UID:
            self.error_repository.remove_many_by_uids(uids)
        elif action == SET_RESOLVED:
            self.error_repository.set_resolved(uids)
        elif action == SET_UNRESOLVED:
            self.error_repository.set_unresolved(uids)
