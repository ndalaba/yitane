from App.Domain.Error.Entity.Error import Error
from App.Domain.Error.Repository.ErrorRepositoryInterface import ErrorRepositoryInterface
from App.Domain.Error.Request.ErrorRequest import ErrorRequest
from App.Helper.Response import Response


class CreateError:

    def __init__(self, error_repository: ErrorRepositoryInterface):
        self.error_repository = error_repository

    def execute(self, error_request: ErrorRequest) -> Response:
        response = Response()
        is_valid = self.is_request_valid(error_request, response)
        if is_valid:
            error = Error(*vars(error_request))
            created_error = self.error_repository.create(error)
            response.add_data("error", created_error)

        return response

    @staticmethod
    def is_request_valid(error_request: ErrorRequest, response: Response) -> bool:

        if len(error_request.name.strip()) < 3:
            response.add_error("name", "Le titre de l'erreur doit etre renseigné.")
        if len(error_request.file.strip()) < 3:
            response.add_error("file", "Le fichier de l'erreur doit etre renseigné.")
        if len(error_request.line.strip()) < 1:
            response.add_error("line", "La ligne de l'erreur doit etre renseigné.")
        if len(error_request.description.strip()) < 3:
            response.add_error("description", "La description de l'erreur doit etre renseigné.")

        return response.has_error()
