from App.Domain.Error.Repository.ErrorRepositoryInterface import ErrorRepositoryInterface
from App.Helper.Response import Response


class GetErrors:

    def __init__(self, error_repository: ErrorRepositoryInterface):
        self.error_repository = error_repository

    def execute(self, resolved: bool = False, page=1) -> Response:
        errors = self.error_repository.filter(resolved = resolved, page = page)
        return Response(data = {'errors': errors})
