from typing import List

from App.Domain.Error.Repository.ErrorRepositoryInterface import ErrorRepositoryInterface
from App.Helper.Response import Response


class UpdateErrors:

    def __init__(self, error_repository: ErrorRepositoryInterface):
        self.error_repository = error_repository

    def execute(self, uids: list[str], resolved: bool = True) -> Response:
        response = Response()
        is_valid = self.is_request_valid(uids, response)
        if is_valid:
            created_error = self.error_repository.set_resolved(uids) if resolved else self.error_repository.set_unresolved(uids)
            response.add_data("error", created_error)

        return response

    @staticmethod
    def is_request_valid(uids: list[str], response: Response) -> bool:
        if not len(uids):
            return response.add_error('uids', "Les identifiants des erreurs doivent être renseignés.")

        return response.has_error()
