from App.Domain.Error.Repository.ErrorRepositoryInterface import ErrorRepositoryInterface
from App.Helper.Response import Response


class GetError:

    def __init__(self, error_repository: ErrorRepositoryInterface):
        self.error_repository = error_repository

    def execute(self, uid: str) -> Response:
        error = self.error_repository.find_by('uid', uid)
        return Response(data = {'error': error})
