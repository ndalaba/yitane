from http.client import HTTPException

from App.Domain.Error.Entity import Error
from App.Domain.Error.Repository.ErrorRepositoryInterface import ErrorRepositoryInterface
from App.Helper.Response import Response


class DeleteError:

    def __init__(self, error_repository: ErrorRepositoryInterface):
        self.error_repository = error_repository

    def execute(self, uid) -> Response:
        response = Response()
        error_exist, error = self.error_exist(uid)
        if error_exist:
            self.error_repository.delete(uid)
            response.set_success_message("Erreur supprimée.")

        return response

    def error_exist(self, uid: str) -> (bool, Error):
        try:
            error = self.error_repository.find_by('uid', uid)
            return True, error
        except HTTPException:
            return False, None
